import csv
import logging
from django.utils.text import slugify
from django.db.models import (
    Func,
    F,
    Q,
    When,
    Case,
    Value,
    OuterRef,
    Count,
    Subquery,
    Exists,
)
from django.contrib.postgres.aggregates import StringAgg
from django.db.models.expressions import RawSQL
from django.shortcuts import redirect
from django.http import JsonResponse, StreamingHttpResponse
from django.views.generic import TemplateView, FormView, View
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from datetime import timedelta
from django.utils.timezone import now
from django.utils.html import escape, strip_tags
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django_datatables_view.base_datatable_view import BaseDatatableView, DatatableMixin
from rules.contrib.views import PermissionRequiredMixin
from rest_framework import viewsets, permissions
from django.urls import reverse, reverse_lazy
from django.views.decorators.clickjacking import xframe_options_exempt
from django.core.exceptions import FieldDoesNotExist
from itertools import chain


from relatorios.utils import (
    get_totais,
    get_totais_gerais,
    get_cadastros,
    get_territorio_search_columns,
    get_territorio_search_columns_icontains,
    get_territorio_export_columns,
    filter_queryset_territorios,
    build_annotate_relevancia,
    build_annotate_cadastrante,
)
from relatorios.serializers import TerritorioSerializer
from relatorios.models import TerritorioAgregado

from main.utils import (
    get_areas_de_uso_list,
    get_conflitos_list,
    get_perfil,
    get_territorios_visiveis_revisora,
    get_territorios_visiveis,
    get_choice,
    get_choice_name,
    get_model,
)
from main.models import (
    Territorio,
    Anexo,
    Usuaria,
    Municipio,
    Estado,
    Bioma,
    AreaDeUso,
    TipoAreaDeUso,
    Conflito,
    TipoConflito,
    TipoComunidade,
    TerritorioRevisaoStatus,
    TerritorioStatus,
    TerritorioUsuaria,
    Organizacao,
    OrganizacaoUsuaria,
    Mensagem,
)
from main.forms import (
    MapaFiltroForm,
    MapaPublicoFiltroForm,
    UsuariaTerritorioAssociarForm,
    TerritorioAutoAssociarRevisoraForm,
    OrganizacaoTerritorioAssociarForm,
    UsuariaAssociarForm,
)

logging.basicConfig(
    filename="debug_local.log", filemode="w", format="CooperativaEITA - %(message)s"
)


class IndexView(TemplateView, PermissionRequiredMixin):
    template_name = "relatorios/index.html"
    permission_required = "permissao_padrao"
    organizacao_id = None
    organizacao = None
    territorios_qs = None
    tipos_comunidade_qs = None
    areas_de_uso_qs = None
    conflitos_qs = None

    def dispatch(self, request, *args, **kwargs):
        self.organizacao_id = kwargs.get("organizacao_id", None)
        if self.request.user.is_anonymous:
            return redirect("MainIndex")
        if self.request.user.is_reviewer:
            return redirect("ReviewerIndex")
        perfil = get_perfil(self.request.user)
        if perfil == "Cadastrante":
            return redirect("CadastranteIndex")

        self.territorios_qs = self.get_territorios_queryset()
        self.tipos_comunidade_qs = self.get_tipos_comunidade_queryset()
        self.areas_de_uso_qs = self.get_areas_de_uso_queryset()
        self.conflitos_qs = self.get_conflitos_queryset()

        if (
            not self.request.user.has_perm("territorio.ver_todos")
            and not self.organizacao_id
        ):
            organizacoes_que_administra = self.request.user.organizacoes_que_administra
            target_organizacao_id = organizacoes_que_administra.first().id
            return redirect("RelatoriosIndex", organizacao_id=target_organizacao_id)

        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_territorios_queryset(self):
        if not self.territorios_qs:
            if self.organizacao_id:
                if not self.organizacao:
                    self.organizacao = get_model("Organizacao").objects.get(
                        pk=self.organizacao_id
                    )
                self.territorios_qs = self.organizacao.territorios_visiveis().filter(
                    teste=False
                )
            else:
                self.territorios_qs = Territorio.objects.filter(teste=False)

        return self.territorios_qs

    def get_tipos_comunidade_queryset(self):
        return TipoComunidade.objects.filter(
            territorios__in=self.territorios_qs.values_list("pk"),
            territorios__status_id__in=TerritorioStatus.STATUS_APROVADOS,
        )

    def get_areas_de_uso_queryset(self):
        return AreaDeUso.objects.filter(
            territorio__in=self.territorios_qs.values_list("pk"),
            territorio__status_id__in=TerritorioStatus.STATUS_APROVADOS,
        )

    def get_conflitos_queryset(self):
        return Conflito.objects.filter(
            territorio__in=self.territorios_qs.values_list("pk"),
            territorio__status_id__in=TerritorioStatus.STATUS_APROVADOS,
        )

    def get_mensagens_sac(self):
        territorios_sac = Territorio.objects.filter(
            Q(
                sac_status__in=[
                    Territorio.SAC_OPTIONS["ABERTO"],
                    Territorio.SAC_OPTIONS["ESPERANDO_RESPOSTA"],
                ]
            )
            & Q(Q(sac_assigned_to__isnull=True) | Q(sac_assigned_to=self.request.user))
        )
        mensagens_sac = []
        for territorio in territorios_sac:
            mensagem = territorio.mensagens.filter(origin_is_device=True).last()
            if mensagem:
                mensagens_sac.append(mensagem)
        return mensagens_sac

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["organizacao_id"] = self.organizacao_id
        context["totais"] = get_totais(
            self.territorios_qs,
            self.tipos_comunidade_qs,
            self.areas_de_uso_qs,
            self.conflitos_qs,
            self.organizacao_id,
        )
        context["mensagens_sac"] = self.get_mensagens_sac()
        context["modulo"] = "panorama"
        context["title"] = "Panorama"
        context["temFooter"] = True
        return context


class IndexOrganizacaoView(IndexView):
    pass


class PanoramaReviewerView(TemplateView):
    template_name = "relatorios/panorama_reviewer.html"
    organizacao_id = None
    organizacao = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not self.organizacao_id:
            self.organizacao_id = self.kwargs.get("organizacao_id", None)

        territorios_qs = get_territorios_visiveis_revisora(
            self.request.user, self.organizacao_id
        )
        territorios_associados = self.request.user.territorios_revisora

        context["modulo"] = "panorama"
        context["organizacao_id"] = self.organizacao_id
        context["title"] = "Panorama"
        context["temFooter"] = True
        context["totais_gerais"] = get_totais_gerais(
            territorios_qs, self.organizacao_id
        )
        context["evolucao_cadastros"] = get_cadastros(
            territorios_associados, None, self.request.user.is_reviewer
        )

        # Número total de comunidades cadastradas no sistema
        # Número de comunidades cadastradas por ele próprio

        # Lista das mensagens recebidas dos cadastros em análise
        # Lista dos novos cadastros que chegaram
        # Lista dos cadastros em análise
        # Lista dos cadastros já processados (validados ou não)

        # TOTAL DO SISTEMA (isso que falei acima)
        # Comunidades que necessitam sua revisão
        # Comunidades validadas por vc
        # Comunidades rejeitadas por vc

        return context


class PanoramaCadastranteView(TemplateView):
    template_name = "relatorios/panorama_cadastrante.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "panorama"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["temFooter"] = True

        return context


class MensagensTerritoriosAnalise(TemplateView):
    template_name = "relatorios/mensagens_territorios_em_analise.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Mensagens"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)


class TerritorioAgregadoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = TerritorioAgregado.objects.all()
    serializer_class = TerritorioSerializer
    permission_classes = [permissions.IsAuthenticated]


#


class TerritorioView(PermissionRequiredMixin, FormView):
    model = Territorio
    form_class = UsuariaTerritorioAssociarForm
    template_name = "relatorios/territorios.html"
    permission_required = "permissao_padrao"
    organizacao_id = None
    organizacao = None
    view_facet = None
    datatable_url = reverse_lazy("TerritorioAgregadoDatatable")

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get("is_testes_view", False):
            self.view_facet = "testes"
            self.datatable_url = reverse_lazy("TerritorioAgregadoTestesDatatable")
        elif kwargs.get("is_validados_view", False):
            self.view_facet = "validados"
            self.datatable_url = reverse_lazy("TerritorioAgregadoValidadosDatatable")
        elif kwargs.get("is_em_analise_view", False):
            self.view_facet = "em_analise"
            self.datatable_url = reverse_lazy("TerritorioAgregadoEmAnaliseDatatable")

        self.organizacao_id = kwargs.get("organizacao_id", None)
        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)

        # anonymous will be denied access by PermissionRequiredMixin
        # admin_geral will have all access
        if self.request.user.is_anonymous or self.request.user.has_perm(
            "territorio.ver_todos"
        ):
            return super(TerritorioView, self).dispatch(request, *args, **kwargs)

        if self.request.user.is_reviewer:
            # reviewer will go to reviewer panorama (homepage) if the request has organizacao_id. Not allowed here with organizacao_id
            if self.organizacao_id:
                return redirect("TerritorioAgregadoIndex")

            self.form_class = TerritorioAutoAssociarRevisoraForm
            return super(TerritorioView, self).dispatch(request, *args, **kwargs)

        if not self.organizacao_id and self.request.user.has_perm(
            "usuaria.administrar_organizacao"
        ):
            organizacoes_que_administra = self.request.user.organizacoes_que_administra
            target_organizacao_id = organizacoes_que_administra.first().id
            return redirect(
                "TerritorioAgregadoIndex", organizacao_id=target_organizacao_id
            )

        return super(TerritorioView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(TerritorioView, self).get_form_kwargs()
        if not self.request.user.is_reviewer:
            kwargs.update(
                {"usuaria": self.request.user, "organizacao_id": self.organizacao_id}
            )
        return kwargs

    def post(self, request, *args, **kwargs):
        """metodo POST utilizado para (des)associação de revisor(a)"""
        status_error = {"status": 500, "statusText": "FAIL"}
        status_success = {"status": 200, "statusText": "OK"}

        territorio_id = request.POST.get("territorio", None)
        revisora_id = request.POST.get("revisora", None)
        organizacao_id = request.POST.get("organizacao", None)

        if not territorio_id:
            status_error["content"] = "Comunidade inválida"
            return JsonResponse(status_error)

        try:
            territorio = Territorio.objects.get(pk=territorio_id)
        except:
            status_error["content"] = "Comunidade inválida"
            return JsonResponse(status_error)

        if revisora_id:
            # admin is assigning revisora
            if not request.user.has_perm("usuaria.administrar"):
                return {
                    "status": 403,
                    "statusText": "PERMISSION DENIED",
                    "content": "Permissão negada para esta ação!",
                }

            try:
                usuaria = Usuaria.objects.get(pk=revisora_id)
            except:
                status_error["content"] = "Revisor/a não encontrado/a"
                return JsonResponse(status_error)

            territorio.revisora = usuaria
            territorio.save()
            status_success["revisora"] = f"{usuaria}"
            status_success["revisora_id"] = f"{usuaria.id}"
            status_success["territorio"] = f"{territorio}"
            status_success["territorio_id"] = f"{territorio.id}"
            status_success["revisora_update_url"] = reverse(
                "usuaria_update", args=(usuaria.id,)
            )
            status_success["revisora_desassociar_url"] = reverse(
                "revisora_desassociar", kwargs={"pk": territorio.id}
            )
            return JsonResponse(status_success)

        if organizacao_id:
            # admin is assigning organizacao to territorio:
            try:
                organizacao = Organizacao.objects.get(pk=organizacao_id)
            except:
                status_error["content"] = "Organização não encontrada"
                return JsonResponse(status_error)

            if not request.user.has_perm("organizacao.alterar", organizacao):
                return {
                    "status": 403,
                    "statusText": "PERMISSION DENIED",
                    "content": "Permissão negada para esta ação!",
                }

            organizacao.territorios.add(territorio)
            status_success["organizacao"] = f"{organizacao}"
            status_success["organizacao_id"] = f"{organizacao.id}"
            status_success["territorio"] = f"{territorio}"
            status_success["territorio_id"] = f"{territorio.id}"
            status_success["organizacao_update_url"] = reverse(
                "organizacao_update", args=(organizacao.id,)
            )
            status_success["organizacao_desassociar_url"] = reverse(
                "organizacao_desassociar", kwargs={"pk": territorio.id}
            )
            return JsonResponse(status_success)

        # reviewer is auto assigning review
        if not request.user.is_reviewer:
            return {
                "status": 403,
                "statusText": "PERMISSION DENIED",
                "content": "Permissão negada para esta ação!",
            }

        territorio.revisora = request.user
        territorio.save()
        status_success["revisora"] = f"{request.user}"
        status_success["revisora_id"] = f"{request.user.id}"
        status_success["territorio"] = f"{territorio}"
        status_success["territorio_id"] = f"{territorio.id}"
        status_success["revisora_update_url"] = reverse(
            "usuaria_update", args=(request.user.id,)
        )
        status_success["revisora_desassociar_url"] = reverse(
            "revisora_desassociar", kwargs={"pk": territorio.id}
        )
        return JsonResponse(status_success)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "territorio"
        context["organizacao_id"] = self.organizacao_id
        context["organizacao"] = self.organizacao
        context["title"] = "Comunidades"
        context["temFooter"] = False
        context["view_facet"] = self.view_facet
        context["show_organizacoes"] = (
            self.request.user.has_perm("territorio.ver_todos")
            or self.request.user.organizacoes.count() > 1
        )
        context["formOrganizacao"] = OrganizacaoTerritorioAssociarForm(
            usuaria=self.request.user
        )
        context["view_facet"] = self.view_facet
        context["datatable_url"] = self.datatable_url
        context["datatable_columns"] = [
            {
                "title": d["title"],
                "class": "" if not "class" in d else d["class"],
                "orderable": true if not "orderable" in d else d["orderable"],
            }
            for d in Territorio.DATATABLE_COLUMNS
        ]
        if self.organizacao:
            for i, column in enumerate(Territorio.DATATABLE_COLUMNS):
                if column["field"] == "revisor":
                    context["datatable_columns"][i]["class"] = "colHasSearchPane"
                    continue
                if column["field"] == "tipos_comunidade":
                    context["datatable_columns"][i]["class"] = "colStartHidden"
                    continue

        remove_relevancia = self.request.user.is_parceira or self.view_facet in [
            "testes",
            "validados",
        ]
        if remove_relevancia:
            for i, column in enumerate(Territorio.DATATABLE_COLUMNS):
                if column["field"] == "relevancia":
                    context["datatable_columns"][i][
                        "class"
                    ] = "colStartHidden not-colvis"
                    break

        data_filter = self.request.GET
        status_revisao = data_filter.get("status_revisao", None)
        if status_revisao:
            if status_revisao == "revisar":
                context["filter_status_revisao"] = "a revisar"
            if status_revisao == "em-analise":
                context["filter_status_revisao"] = "em revisão"
            if status_revisao == "processados":
                context["filter_status_revisao"] = "processadas"
        usuaria_id = data_filter.get("usuaria", None)
        if usuaria_id:
            try:
                usuaria = Usuaria.objects.get(pk=usuaria_id)
            except:
                return context

            perfil = (
                "revisor/a"
                if usuaria.is_reviewer and not usuaria.has_perm("territorio.ver_todos")
                else "usuário/a"
            )
            context["filter_usuaria"] = f"associadas ao/à {perfil} {usuaria}"

        municipio_id = data_filter.get("municipio", None)
        if municipio_id:
            try:
                municipio = Municipio.objects.get(pk=municipio_id)
            except:
                return context

            context["filter_municipio"] = f'relacionadas ao município "{municipio}"'

        return context


class TerritorioDatatableView(BaseDatatableView):
    model = Territorio
    columns = [d["field"] for d in Territorio.DATATABLE_COLUMNS]
    order_columns = columns
    search_columns = get_territorio_search_columns()
    search_columns_icontains = get_territorio_search_columns_icontains()
    searchpane_columns = [
        d["field"]
        for d in Territorio.DATATABLE_COLUMNS
        if "search_pane" in d and d["search_pane"] == True
    ]
    territorios = None
    organizacao = None
    view_facet = None

    def get_columns(self):
        if self.request.user.is_parceira or self.view_facet in ["testes", "validados"]:
            self.columns = [
                d["field"]
                for d in Territorio.DATATABLE_COLUMNS
                if d["field"] != "relevancia"
            ]
            return self.columns

        return super().get_columns()

    def get_initial_queryset(self):
        kwargs = self.kwargs

        if kwargs.get("is_testes_view", False):
            self.view_facet = "testes"
        elif kwargs.get("is_validados_view", False):
            self.view_facet = "validados"
        elif kwargs.get("is_em_analise_view", False):
            self.view_facet = "em_analise"

        self.organizacao_id = kwargs.get("organizacao_id", None)
        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)

        qs = self.get_territorios()
        return qs

    def get_territorios_queryset(self):
        data_filter = self.request.GET

        if self.request.user.is_reviewer:
            status_revisao = data_filter.get("status_revisao", None)
            if status_revisao:
                if status_revisao == "revisar":
                    return self.request.user.territorios_para_revisar
                if status_revisao == "em-analise":
                    return self.request.user.territorios_em_analise
                if status_revisao == "processados":
                    return self.request.user.territorios_processados
                return Territorio.objects.none()

            territorios = get_territorios_visiveis_revisora(self.request.user)
        elif self.organizacao:
            territorios = self.organizacao.territorios_visiveis()
        else:
            territorios = Territorio.objects

        filter_usuaria = data_filter.get("usuaria", None)
        if filter_usuaria:
            territorios = territorios.filter(terr_usuarias__usuaria=filter_usuaria)

        filter_municipio = data_filter.get("municipio", None)
        if filter_municipio:
            territorios = territorios.filter(municipio_referencia=filter_municipio)

        if self.request.user.is_parceira:
            territorios = territorios.filter(
                status__in=TerritorioStatus.STATUS_APROVADOS, teste=False, publico=True
            )
        elif self.view_facet == "testes":
            territorios = territorios.filter(teste=True)
        elif self.view_facet == "em_analise":
            territorios = territorios.filter(
                status=TerritorioStatus.ENVIADO_PENDENTE, teste=False
            )
        elif self.view_facet == "validados":
            territorios = territorios.filter(
                status__in=TerritorioStatus.STATUS_APROVADOS, teste=False
            )

        territorios = territorios.select_related(
            "revisora",
            "organizacao",
            "bioma",
            "municipio_referencia",
            "municipio_referencia__estado",
        ).prefetch_related(
            "usuarias",
            "tipos_comunidade",
            "areas_de_uso",
            "conflitos",
        )
        return territorios

    def get_territorios(self):
        if self.territorios:
            return self.territorios

        territorios_qs = self.get_territorios_queryset()

        if self.request.user.is_parceira:
            self.territorios = territorios_qs
            return self.territorios

        if self.view_facet == "testes":
            self.territorios = territorios_qs
            return self.territorios

        if self.view_facet == "validados":
            self.territorios = territorios_qs
            return self.territorios

        annotate = {
            "relevancia": build_annotate_relevancia(),
            "cadastrante": build_annotate_cadastrante(),
        }

        self.territorios = territorios_qs.annotate(**annotate)

        return self.territorios

    def render_column(self, row, column):
        if column == "nome":
            value = super().render_column(row, column)
            tag_a_attr = 'a class="cell_comunidade" '
            tag_a_attr += f"""data-title="Comunidade '{ row.nome }'" """
            tag_a_attr += f"data-territorio='{ row.nome }' "
            tag_a_attr += f"data-territorio_id='{ row.id }' "
            tag_a_attr += f'data-revisora="{ row.revisora }" '
            revisora_id = row.revisora.id if row.revisora else ""
            tag_a_attr += f'data-revisora_id="{ revisora_id }" '
            tag_a_attr += f'data-organizacao="{ row.organizacao }" '
            tag_a_attr += "href"
            value = value.replace("a href", tag_a_attr)
            return value

        if column == "revisora":
            if row.revisora:
                target_url = reverse_lazy("usuaria_update", args=(row.revisora.id,))
                desassociar_url = reverse_lazy("revisora_desassociar", args=(row.id,))
                return f"""
                        <div class="cell_revisora inner_cell_with_button">
                            <div class="inner_cell_content">
                                    <a href="{ target_url }">{ row.revisora }</a>
                            </div>
                            <div class="inner_cell_button">
                                    <a href="{ desassociar_url }" class="revisora_desassociar btn btn-sm btn-remove">
                                        <i class="fa fa-minus"></i>
                                    </a>
                            </div>
                        </div>
                """

            return """
                    <div class="inner_cell_with_button">
                        <div class="inner_cell_content"></div>
                        <div class="inner_cell_button">
                            <button class="revisora_associar btn btn-sm btn-default">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
            """

        if column == "cadastrante":
            cell = '<div class="inner_cell_content">'
            for cadastrante in row.usuarias.all():
                if (
                    cadastrante.is_admin
                    or cadastrante.is_reviewer
                    or cadastrante.is_parceira
                ):
                    continue
                target_url = reverse_lazy("usuaria_update", args=(cadastrante.id,))
                cell += f'<a href="{ target_url }">{ cadastrante }</a>'
            cell += "</div>"
            return cell

        if column == "organizacao":
            if row.organizacao:
                target_url = reverse_lazy(
                    "organizacao_update", args=(row.organizacao.id,)
                )
                desassociar_url = reverse_lazy(
                    "organizacao_desassociar", args=(row.id,)
                )
                return f"""
                        <div class="cell_organizacao inner_cell_with_button">
                            <div class="inner_cell_content">
                                <a href="{ target_url }">{ row.organizacao }</a>
                            </div>
                            <div class="inner_cell_button">
                                <a href="{desassociar_url}" class="organizacao_desassociar btn btn-sm btn-remove">
                                    <i class="fa fa-minus"></i>
                                </a>
                            </div>
                        </div>
                """

            return """
                    <div class="inner_cell_with_button">
                        <div class="inner_cell_content"></div>
                        <div class="inner_cell_button">
                            <button class="organizacao_associar btn btn-sm btn-default">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
            """

        if column == "data_validacao":
            if row.data_validacao:
                return row.data_validacao.strftime("%d/%m/%Y")

            return ""

        if column == "ultima_alteracao":
            if row.ultima_alteracao:
                return row.ultima_alteracao.strftime("%d/%m/%Y")

            return ""

        if column == "municipio_referencia__estado__nome":
            if row.municipio_referencia:
                return row.municipio_referencia.estado.nome

            return ""

        if column == "tipos_comunidade":
            data = ""
            for tipo_comunidade in row.tipos_comunidade.all():
                data += f'<span class="badge badge-tipo">{ tipo_comunidade }</span>'
            return data

        if column == "areas_de_uso":
            return ", ".join([str(i) for i in row.areas_de_uso.all()])

        if column == "conflitos":
            return ", ".join([str(i) for i in row.conflitos.all()])

        # return getattr(row, column, "")

        return super(TerritorioDatatableView, self).render_column(row, column)

    def filter_queryset(self, qs):
        return filter_queryset_territorios(
            self.request.POST, self.search_columns, self.search_columns_icontains, qs
        )

    # def prepare_searchpanes_columns(self):
    #     return self.searchpane_columns

    # def prepare_searchpanes_opt_cadastrantes(self):
    #     qs = self.get_initial_queryset()
    #     spcolumn = "usuarias__id"
    #     columns_usuarias = [spcolumn, "usuarias__first_name", "usuarias__last_name"]
    #     qs_column = (
    #         qs.filter(
    #             (Q(usuarias__is_admin__isnull=True) | Q(usuarias__is_admin=False)),
    #             (
    #                 Q(usuarias__is_parceira__isnull=True)
    #                 | Q(usuarias__is_parceira=False)
    #             ),
    #         )
    #         .values(*columns_usuarias)
    #         .annotate(dcount=Count(columns_usuarias))
    #         .order_by(*columns_usuarias)
    #     )

    #     option_label = (
    #         "' '.join([item['usuarias__first_name'], item['usuarias__last_name']])"
    #     )
    #     option_label = f"{option_label}.strip()"

    #     options = []
    #     for item in qs_column:
    #         if not item[spcolumn]:
    #             option_label = '"<i>(nenhuma)</i>"'
    #         options.append(
    #             {
    #                 "label": f"{eval(option_label)}",
    #                 "total": f"{item['dcount']}",
    #                 "value": f"{item[spcolumn]}",
    #                 "count": f"{item['dcount']}",
    #             }
    #         )

    #     return options

    # def prepare_searchpane_options(self):
    #     spanes_options = {}
    #     qs = self.get_initial_queryset()
    #     for spcolumn in self.searchpane_columns:

    #         if spcolumn == "cadastrante":
    #             spanes_options[spcolumn] = self.prepare_searchpanes_opt_cadastrantes()
    #             continue

    #         option_values = [
    #             spcolumn,
    #         ]
    #         option_label = "item[spcolumn]"
    #         option_label_empty = '"<i>(sem dados)</i>"'

    #         if spcolumn == "revisora":
    #             option_values = [
    #                 spcolumn,
    #                 "revisora__first_name",
    #                 "revisora__last_name",
    #                 "revisora__username",
    #             ]
    #             # option_label = "' '.join([item['revisora__first_name'], item['revisora__last_name'], item['revisora__username']])"
    #             option_label = "' '.join([item['revisora__first_name'], item['revisora__last_name']])"
    #             option_label = f"{option_label}.strip()"

    #         if spcolumn in ["organizacao", "tipos_comunidade"]:
    #             option_values = [spcolumn, f"{spcolumn}__nome"]
    #             option_label = f"item['{spcolumn}__nome']"

    #         if spcolumn == "tipos_comunidade":
    #             option_label_empty = '"<i>(nenhuma)</i>"'

    #         qs_column = (
    #             qs.values(*option_values)
    #             .annotate(dcount=Count(option_values))
    #             .order_by(*option_values)
    #         )
    #         spanes_options[spcolumn] = []
    #         for item in qs_column:
    #             if not item[spcolumn]:
    #                 option_label = option_label_empty

    #             spanes_options[spcolumn].append(
    #                 {
    #                     "label": f"{eval(option_label)}",
    #                     "total": f"{item['dcount']}",
    #                     "value": f"{item[spcolumn]}",
    #                     "count": f"{item['dcount']}",
    #                 }
    #             )
    #     return spanes_options

    # def prepare_searchpanes(self):
    #     searchpane_options = self.prepare_searchpane_options()
    #     spanes = {"options": {}}
    #     for x, column in enumerate(self.columns):
    #         if not column in self.searchpane_columns:
    #             continue
    #         spanes["options"][x] = searchpane_options[column]
    #     return spanes


class CSVBuffer:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Return the string to write."""
        return value


class CSVStream:
    """Class to stream (download) an iterator to a
    CSV file."""

    def iter_items(self, header_columns, items, pseudo_buffer):
        writer = csv.DictWriter(pseudo_buffer, fieldnames=header_columns)
        # yield pseudo_buffer.write(header_columns)
        columns_object = {}
        for field in header_columns:
            columns_object[field] = field
        yield writer.writerow(columns_object)

        for item in items:
            yield writer.writerow(item)

    def export(self, filename, queryset, fields):
        # 1. Create our writer object with the pseudo buffer
        writer = csv.writer(CSVBuffer())

        # 2. Create the StreamingHttpResponse using our iterator as streaming content
        response = StreamingHttpResponse(
            streaming_content=self.iter_items(
                fields, queryset.values(*fields), CSVBuffer()
            ),
            content_type="text/csv",
        )

        # 3. Add additional headers to the response
        response["Content-Disposition"] = f"attachment; filename={filename}.csv"
        # 4. Return the response
        return response


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(xframe_options_exempt, name="dispatch")
class TerritorioDatatableExportAll(View):
    filename = "dados_tonomapa"
    fields = get_territorio_export_columns()
    search_columns = get_territorio_search_columns()
    search_columns_icontains = get_territorio_search_columns_icontains()
    territorios = None
    organizacao = None

    def get_territorios(self):
        if self.territorios:
            return self.territorios

        status_relevantes = TerritorioRevisaoStatus.STATUS_RELEVANTES
        status_validados = TerritorioRevisaoStatus.STATUS_VALIDADOS
        status_em_preenchimento = TerritorioRevisaoStatus.STATUS_EM_PREENCHIMENTO

        territorios_qs = self.get_territorios_queryset()

        annotate = {}

        # if (
        #     not self.view_facet
        #     or self.request.user.is_parceira
        #     or self.view_facet == "validados"
        # ):
        #     annotate["area"] = RawSQL("ST_AREA(poligono::geography)", [])
        annotate["area"] = RawSQL("ST_AREA(poligono::geography)", [])
        annotate["cadastrante"] = build_annotate_cadastrante()

        annotate["tipos_comunidade_aggr"] = StringAgg(
            "tipos_comunidade__nome", delimiter=", ", distinct=True
        )
        annotate["areas_de_uso_aggr"] = StringAgg(
            "areas_de_uso__nome", delimiter=", ", distinct=True
        )
        annotate["conflitos_aggr"] = StringAgg(
            "conflitos__nome", delimiter=", ", distinct=True
        )

        for tipo_comunidade in TipoComunidade.objects.all():
            annotate[f"tipoComunidade_{slugify(tipo_comunidade.nome)}"] = Case(
                When(
                    Exists(
                        Territorio.tipos_comunidade.through.objects.filter(
                            territorio_id=OuterRef("pk"),
                            tipocomunidade_id=tipo_comunidade.id,
                        )
                    ),
                    then=Value(1),
                ),
                default=0,
            )

        for tipo_area_de_uso in TipoAreaDeUso.objects.all():
            counter = (
                AreaDeUso.objects.filter(
                    territorio_id=OuterRef("pk"),
                    tipo_area_de_uso_id=tipo_area_de_uso.id,
                )
                .order_by()
                .annotate(count=Func(F("id"), function="Count"))
                .values("count")
            )
            annotate[f"tipoAreaDeUso_{slugify(tipo_area_de_uso.nome)}"] = Subquery(
                counter
            )

        for tipo_conflito in TipoConflito.objects.all():
            counter = (
                Conflito.objects.filter(
                    territorio_id=OuterRef("pk"),
                    tipo_conflito_id=tipo_conflito.id,
                )
                .order_by()
                .annotate(count=Func(F("id"), function="Count"))
                .values("count")
            )
            annotate[f"tipoConflito_{slugify(tipo_conflito.nome)}"] = Subquery(counter)

        territorios_qs = territorios_qs.annotate(**annotate)
        self.territorios = self.filter_queryset(territorios_qs)

        return self.territorios

    def get_queryset(self):
        kwargs = self.kwargs

        self.view_facet = self.request.GET.get("view_facet", None)

        self.organizacao_id = self.request.GET.get("organizacao_id", None)
        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)

        qs = self.get_territorios()

        return qs

    def get_territorios_queryset(self):
        data_filter = self.request.GET

        if self.request.user.is_reviewer:
            status_revisao = data_filter.get("status_revisao", None)
            if status_revisao:
                if status_revisao == "revisar":
                    return self.request.user.territorios_para_revisar
                if status_revisao == "em-analise":
                    return self.request.user.territorios_em_analise
                if status_revisao == "processados":
                    return self.request.user.territorios_processados
                return Territorio.objects.none()

            territorios = get_territorios_visiveis_revisora(self.request.user)
        elif self.organizacao:
            territorios = self.organizacao.territorios_visiveis()
        else:
            territorios = Territorio.objects

        filter_usuaria = data_filter.get("usuaria", None)
        if filter_usuaria:
            territorios = territorios.filter(terr_usuarias__usuaria=filter_usuaria)

        filter_municipio = data_filter.get("municipio", None)
        if filter_municipio:
            territorios = territorios.filter(municipio_referencia=filter_municipio)

        if self.request.user.is_parceira:
            territorios = territorios.filter(
                status__in=TerritorioStatus.STATUS_APROVADOS, teste=False, publico=True
            )
        elif self.view_facet == "testes":
            territorios = territorios.filter(teste=True)
        elif self.view_facet == "em_analise":
            territorios = territorios.filter(
                status=TerritorioStatus.ENVIADO_PENDENTE, teste=False
            )
        elif self.view_facet == "validados":
            territorios = territorios.filter(
                status__in=TerritorioStatus.STATUS_APROVADOS, teste=False
            )

        territorios = territorios.select_related(
            "revisora",
            "organizacao",
            "bioma",
            "municipio_referencia",
            "municipio_referencia__estado",
        ).prefetch_related(
            "usuarias",
            "tipos_comunidade",
            "areas_de_uso",
            "conflitos",
        )
        return territorios

    def filter_queryset(self, qs):
        return filter_queryset_territorios(
            self.request.POST, self.search_columns, self.search_columns_icontains, qs
        )

    def iter_items(self, pseudo_buffer):
        fields = self.fields
        queryset = self.get_queryset()
        writer = csv.DictWriter(pseudo_buffer, fieldnames=fields)
        # yield pseudo_buffer.write(header_columns)
        columns_object = {}
        for field in fields:
            found = False
            for column in Territorio.DATATABLE_COLUMNS:
                if (
                    column["field"] == field
                    or (
                        "searchable" in column
                        and type(column["searchable"]) == list
                        and column["searchable"][0] == field
                    )
                    or ("many" in column and f"{column['field']}_aggr" == field)
                ):
                    columns_object[field] = column["title"]
                    found = True
                    break
            if not found:
                columns_object[field] = field
        yield writer.writerow(columns_object)

        # for obj in queryset:
        #     item = {}
        #     for field in fields:
        #         item[field] = self.get_field_value(obj, field)
        #     yield writer.writerow(item)
        items = queryset.values(*fields)
        for item in items:
            yield writer.writerow(item)

    def post(self, request):
        writer = csv.writer(CSVBuffer())
        response = StreamingHttpResponse(
            streaming_content=self.iter_items(CSVBuffer()),
            content_type="text/csv",
        )
        response["Content-Disposition"] = f"attachment; filename={self.filename}.csv"
        return response


class MunicipioDatatableView(BaseDatatableView):
    model = Municipio
    columns = ["municipio_usuarias__usuaria", "nome", "estado", "territorios"]
    order_columns = ["municipio_usuarias__usuaria", "nome", "estado", "territorios"]
    max_display_length = 6000

    def filter_queryset(self, qs):
        qs = Municipio.objects.all()

        organizacao_id = self.kwargs.get("organizacao_id", None)
        if organizacao_id:
            qs = qs.filter(territorios__organizacao_id=organizacao_id)

        search = self.request.GET.get("search[value]", None)
        if search:
            qs = qs.filter(nome__icontains=search)

        usuaria = self.request.GET.get("usuaria", None)
        if usuaria:
            qs = qs.filter(municipio_usuarias__usuaria=usuaria)

        estado = self.request.GET.get("estado", None)
        if estado:
            qs = qs.filter(estado__id=estado)

        municipio = self.request.GET.get("municipio", None)
        if municipio:
            qs = qs.filter(id=municipio)

        return qs

    def render_column(self, row, column):
        organizacao_id = self.kwargs.get("organizacao_id", None)
        if column == "municipio_usuarias__usuaria":
            revisora_list = [
                revisora.usuaria
                for revisora in row.municipio_usuarias.all()
                if revisora.usuaria.is_reviewer
            ]
            if not self.request.user.has_perm("usuaria.administrar"):
                if revisora_list:
                    return ", ".join([f"{revisora}" for revisora in revisora_list])
                else:
                    return "Nenhuma revisor/a associada"

            html = ""
            for i, revisora in enumerate(revisora_list):
                kwargs_usuaria = {"pk": revisora.id}
                kwargs_municipio = {"pk": row.id, "usuaria_id": revisora.id}
                if organizacao_id:
                    kwargs_usuaria["organizacao_id"] = organizacao_id
                    kwargs_municipio["organizacao_id"] = organizacao_id
                usuaria_update_url = reverse("usuaria_update", kwargs=kwargs_usuaria)
                object_desassociar_url = reverse(
                    "municipio_desassociar", kwargs=kwargs_municipio
                )
                html += f'<a href="{usuaria_update_url}">{revisora}</a> '
                html += f'(<a href="{object_desassociar_url}" class="object_desassociar">desassociar</a>)'
                if i > 0:
                    html += ", "
            if not revisora_list:
                html += f'<a href="#" data-title="Município \'{row}\'" data-object_id="{row.id}" class="object_associar">'
                html += "associar revisor(a) </a>"
            return html

        if column == "estado":
            if self.request.user.has_perm("usuaria.listar"):
                url = (
                    reverse("MunicipioList")
                    if not organizacao_id
                    else reverse(
                        "MunicipioList", kwargs={"organizacao_id": organizacao_id}
                    )
                )
                filter_estado_url = f"{url}?estado={row.estado.id}"
                html = f"<a href='{filter_estado_url}' title='Filtrar por Estado'>{row.estado}</a>"
            else:
                html = f"{row.estado}"
            return html

        if column == "territorios":
            territorios = row.territorios
            if organizacao_id:
                territorios = territorios.filter(organizacao_id=organizacao_id)
            n_territorios = territorios.count()
            url = (
                reverse("TerritorioAgregadoIndex")
                if not organizacao_id
                else reverse(
                    "TerritorioAgregadoIndex", kwargs={"organizacao_id": organizacao_id}
                )
            )
            if n_territorios:
                if self.request.user.has_perm("usuaria.administrar"):
                    html = f"<a href='{url}?municipio={row.id}' title='ver comunidades'>{n_territorios} associadas</a>"
                else:
                    html = f"{n_territorios} associadas"
            else:
                html = "Nenhuma comunidade associada"
            return html

        return super().render_column(row, column)


class MunicipioView(PermissionRequiredMixin, FormView):
    model = Municipio
    form_class = UsuariaAssociarForm
    template_name = "relatorios/municipios.html"
    permission_required = "usuaria.listar"

    def dispatch(self, request, *args, **kwargs):
        if (
            not self.request.user.has_perm("visualizar.territorios")
            and "organizacao_id" not in kwargs
        ):
            organizacoes_que_administra_ids = (
                self.request.user.organizacoes_que_administra_ids
            )
            organizacao_id = organizacoes_que_administra_ids.first()[0]
            return redirect("MunicipioList", organizacao_id=organizacao_id)

        return super(MunicipioView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "municipio"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        if context["organizacao_id"]:
            context["organizacao"] = Organizacao.objects.get(
                pk=context["organizacao_id"]
            )
        context["title"] = "Municípios"
        context["temFooter"] = False

        data_filter = self.request.GET
        usuaria_id = data_filter.get("usuaria", None)
        if usuaria_id:
            try:
                usuaria = Usuaria.objects.get(pk=usuaria_id)
            except:
                return context

            context["title"] = f"Municípios relacionados a { usuaria }"
            perfil = (
                "revisora"
                if usuaria.is_reviewer and not usuaria.has_perm("territorio.ver_todos")
                else "usuária"
            )
            context["filter_usuaria"] = f"associados à {perfil} {usuaria}"

        estado_id = data_filter.get("estado", None)
        if estado_id:
            try:
                estado = Estado.objects.get(pk=estado_id)
            except:
                return context

            context["title"] = f"Municípios relacionados ao Estado {estado}"
            context["filter_estado"] = f'relacionados ao Estado "{estado}"'

        municipio_id = data_filter.get("municipio", None)
        if municipio_id:
            try:
                municipio = Municipio.objects.get(pk=municipio_id)
            except:
                return context

            context["title"] = f"Municípios relacionados a {municipio}"
            context["filter_municipio"] = f'"{municipio}"'

        return context

    def post(self, request, *args, **kwargs):
        """metodo POST utilizado para (des)associação de revisor(a)"""

        # check permission
        if not request.user.has_perm("usuaria.administrar"):
            return {
                "status": 403,
                "statusText": "PERMISSION DENIED",
                "content": "Permissão negada para esta ação!",
            }

        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            municipio = Municipio.objects.get(pk=request.POST.get("object_id", None))
        except:
            status_error["content"] = "Município inválido"
            return JsonResponse(status_error)

        try:
            usuaria = Usuaria.objects.get(pk=request.POST.get("revisora", None))
        except:
            status_error["content"] = "Revisor/a não encontrado/a"
            return JsonResponse(status_error)

        usuaria.municipios.add(municipio)
        status_success = {
            "status": 200,
            "statusText": "OK",
            "revisora": f"{usuaria}",
            "revisora_id": f"{usuaria.id}",
            "object_id": f"{municipio.id}",
            "revisora_update_url": reverse("usuaria_update", args=(usuaria.id,)),
            "object_desassociar_url": reverse(
                "municipio_desassociar", args=(municipio.id, usuaria.id)
            ),
        }

        return JsonResponse(status_success)


class MapaView(PermissionRequiredMixin, FormView):
    permission_required = "permissao_padrao"
    template_name = "relatorios/mapa.html"
    form_class = MapaFiltroForm
    organizacao_id = None
    organizacao = None
    map_queryset = None

    def dispatch(self, request, *args, **kwargs):
        self.organizacao_id = kwargs.get("organizacao_id", None)
        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)
        if self.request.user.is_anonymous or self.request.user.has_perm(
            "territorio.ver_todos"
        ):
            return super(MapaView, self).dispatch(request, *args, **kwargs)

        if self.request.user.is_reviewer:
            return (
                redirect("dashboard-mapa")
                if self.organizacao_id
                else super(MapaView, self).dispatch(request, *args, **kwargs)
            )

        if not self.organizacao_id:
            organizacoes_que_administra_ids = (
                self.request.user.organizacoes_que_administra_ids
            )
            organizacao_que_administra_id = organizacoes_que_administra_ids.first()[0]
            return redirect(
                "dashboard-mapa", organizacao_id=organizacao_que_administra_id
            )

        return super(MapaView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        return self.render_to_response(self.get_context_data(form=form))

    def get_form_kwargs(self):
        kwargs = super(MapaView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "mapa"
        context["organizacao_id"] = self.organizacao_id
        context["organizacao"] = self.organizacao
        context["title"] = "Mapa"
        context["filtros"] = self.get_filtros(context["form"])
        context["temFooter"] = False
        context["position_initial"] = [-17.6311697, -54.1642034]
        context["territorios"] = self.get_territorios_list(context["form"])
        context["qtde_territorios"] = len(context["territorios"])
        context["MAPBOX_ACCESS_TOKEN"] = settings.MAPBOX_ACCESS_TOKEN
        context["SITE_URL"] = settings.SITE_URL

        return context

    def get_map_queryset(self):
        if self.map_queryset:
            return self.map_queryset

        if self.request.user.is_parceira:
            queryset = (
                Territorio.objects.prefetch_related(
                    "conflitos",
                    "conflitos__tipo_conflito",
                    "areas_de_uso",
                    "areas_de_uso__tipo_area_de_uso",
                    "status_revisao",
                )
                .select_related(
                    "municipio_referencia",
                    "municipio_referencia__estado",
                )
                .filter(
                    teste=False,
                    status__in=TerritorioStatus.STATUS_APROVADOS,
                    publico=True,
                )
            )
            self.map_queryset = queryset
            return self.map_queryset

        if not self.request.user.is_reviewer or self.request.user.has_perm(
            "territorio.ver_todos"
        ):
            queryset = (
                Territorio.objects.prefetch_related(
                    "conflitos",
                    "conflitos__tipo_conflito",
                    "areas_de_uso",
                    "areas_de_uso__tipo_area_de_uso",
                    "status_revisao",
                )
                .select_related(
                    "municipio_referencia",
                    "municipio_referencia__estado",
                )
                .filter(teste=False)
            )

            if self.organizacao_id:
                queryset = queryset.filter(organizacao_id=self.organizacao_id)

            if self.request.user.has_perm("territorio.ver_todos"):
                self.map_queryset = queryset
                return self.map_queryset

            organizacoes_ids = self.request.user.organizacoes_que_administra_ids
            queryset = queryset.filter(organizacao_id__in=organizacoes_ids)
            self.map_queryset = queryset
            return self.map_queryset

        if self.request.user.territorios_revisora:
            queryset = (
                self.request.user.territorios_revisora.prefetch_related(
                    "conflitos",
                    "conflitos__tipo_conflito",
                    "areas_de_uso",
                    "areas_de_uso__tipo_area_de_uso",
                    "status_revisao",
                )
                .select_related("municipio_referencia", "municipio_referencia__estado")
                .filter(teste=False)
            )

            if self.organizacao_id:
                queryset = queryset.filter(organizacao_id=self.organizacao_id)

            self.map_queryset = queryset
            return self.map_queryset

        self.map_queryset = Territorio.objects.none()
        return self.map_queryset

    def get_filtros(self, form):
        if self.request.method != "POST":
            return ""

        data_filter = {
            k: form.data.getlist(k)
            for k, v in form.data.items()
            if k not in ["csrfmiddlewaretoken", "submit"]
        }
        if not data_filter:
            return ""

        filtros = {}

        estado_id = data_filter.get("estado", None)
        if estado_id:
            filtros["Estado"] = Estado.objects.filter(id__in=estado_id)

        bioma_id = data_filter.get("bioma", None)
        if bioma_id:
            filtros["Bioma"] = Bioma.objects.filter(id__in=bioma_id)

        comunidade_id = data_filter.get("comunidade", None)
        if comunidade_id:
            filtros["Tipo de comunidade"] = TipoComunidade.objects.filter(
                id__in=comunidade_id
            )

        # status_revisao_id = form.data.get("status_revisao", None)
        status_revisao_id = data_filter.get("status_revisao", None)
        if status_revisao_id and status_revisao_id != "0":
            filtros["Status"] = []
            for item_id in status_revisao_id:
                filtros["Status"].append(get_choice(int(item_id)))

        publico_id = form.data.get("publico", "")
        if publico_id and publico_id != "":
            choice = "Sim" if publico_id == "1" else "Não"
            filtros["Público"] = [choice]

        revisora_id = data_filter.get("revisora", None)
        if revisora_id:
            filtros["Revisor(a)"] = Usuaria.objects.filter(id__in=revisora_id)

        return filtros

    def get_territorios_queryset(self, form):
        queryset = self.get_map_queryset()

        if self.request.method != "POST":
            return queryset

        data_filter = {
            k: form.data.getlist(k)
            for k, v in form.data.items()
            if k not in ["csrfmiddlewaretoken", "submit"]
        }
        if not data_filter:
            return queryset

        estado_id = data_filter.get("estado", None)
        if estado_id:
            queryset = queryset.filter(municipio_referencia__estado__in=estado_id)

        bioma_id = data_filter.get("bioma", None)
        if bioma_id:
            queryset = queryset.filter(bioma__in=bioma_id)

        comunidade_id = data_filter.get("comunidade", None)
        if comunidade_id:
            queryset = queryset.filter(tipos_comunidade__in=comunidade_id)

        status_revisao_id = data_filter.get("status_revisao", None)
        if status_revisao_id and status_revisao_id[0] != "0":
            queryset = queryset.filter(status_revisao__status__in=status_revisao_id)

        publico_id = data_filter.get("publico", None)
        if publico_id and publico_id[0] != "":
            publico = publico_id[0] == 1
            queryset = queryset.filter(publico=publico)

        revisora_id = data_filter.get("revisora", None)
        if revisora_id:
            queryset = queryset.filter(
                Q(terr_usuarias__usuaria__in=revisora_id)
                | Q(municipio_referencia__municipio_usuarias__usuaria__in=revisora_id)
            )

        return queryset

    def get_territorios_list(self, form):
        territorios_qs = self.get_territorios_queryset(form)
        territorios = []
        for territorio in territorios_qs:
            status_revisao = territorio.status_revisao.all()
            territorios.append(
                {
                    "id": territorio.id,
                    "nome": territorio.nome,
                    "conflitos": get_conflitos_list(territorio.conflitos),
                    "areas_de_uso": get_areas_de_uso_list(territorio.areas_de_uso),
                    "poligonos": territorio.geo_poligonos,
                    "poligono_marker": territorio.geo_posicao_inicial,
                    "status_revisao": (
                        get_choice(status_revisao[0].status)
                        if len(status_revisao)
                        else ""
                    ),
                }
            )
        return territorios


class MapaPublicoView(FormView):
    template_name = "relatorios/mapa_publico.html"
    form_class = MapaPublicoFiltroForm

    def dispatch(self, request, *args, **kwargs):
        if len(request.GET):
            initial = {k: request.GET.getlist(k) for k, v in request.GET.items()}
            self.initial = initial
        return super(MapaPublicoView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        territorios = self.get_territorios_list(context["form"])
        qtde_territorios = 0
        for territorio in territorios:
            if territorio["publico"]:
                qtde_territorios += 1
                continue

            qtde_territorios += territorio["qtde"]

        context["modulo"] = "mapa"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["title"] = "Mapa"
        context["filtros"] = self.get_filtros(context["form"])
        context["temFooter"] = False
        context["no_right_col"] = True
        context["position_initial"] = [-17.6311697, -54.1642034]
        context["territorios"] = territorios
        context["qtde_territorios"] = qtde_territorios
        context["MAPBOX_ACCESS_TOKEN"] = settings.MAPBOX_ACCESS_TOKEN
        context["SITE_URL"] = settings.SITE_URL

        return context

    def map_queryset(self):
        return (
            Territorio.objects.prefetch_related(
                "conflitos",
                "conflitos__tipo_conflito",
                "areas_de_uso",
                "areas_de_uso__tipo_area_de_uso",
                "status_revisao",
                "tipos_comunidade",
            )
            .select_related("municipio_referencia", "municipio_referencia__estado")
            .filter(
                status__in=[
                    TerritorioStatus.OK_TONOMAPA,
                    TerritorioStatus.OK_PTT,
                    TerritorioStatus.EM_PREENCHIMENTO_PTT,
                    TerritorioStatus.ENVIO_PTT_NAFILA,
                    TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA,
                ],
                teste=False,
            )
        )

    def get_filtros(self, form):
        data_filter = {
            k: form.data.getlist(k)
            for k, v in form.data.items()
            if k not in ["csrfmiddlewaretoken", "submit"]
        }

        if not data_filter and len(self.request.GET) > 0:
            data_filter = {
                k: self.request.GET.getlist(k) for k, v in self.request.GET.items()
            }

        if not data_filter:
            return ""

        filtros = {}

        if "estado" in data_filter:
            filtros["Estado"] = Estado.objects.filter(id__in=data_filter["estado"])

        if "bioma" in data_filter:
            filtros["Bioma"] = Bioma.objects.filter(id__in=data_filter["bioma"])

        if "comunidade" in data_filter:
            filtros["Tipo de comunidade"] = TipoComunidade.objects.filter(
                id__in=data_filter["comunidade"]
            )

        if "status_revisao" in form.data and form.data["status_revisao"] != "0":
            filtros["Status"] = [get_choice(int(form.data["status_revisao"]))]

        if "publico" in form.data and form.data["publico"] != "":
            choice = "Sim" if form.data["publico"] == "1" else "Não"
            filtros["Público"] = [choice]

        if "revisora" in data_filter:
            filtros["Revisor(a)"] = Usuaria.objects.filter(
                id__in=data_filter["revisora"]
            )

        return filtros

    def get_territorios_list(self, form):
        queryset = self.get_territorios_queryset(form)
        territorios = []

        for territorio in queryset:
            if territorio.publico:
                status_revisao = territorio.status_revisao.all()
                segmentos = []
                for segmento in territorio.tipos_comunidade.all():
                    segmentos.append(segmento.nome)
                territorio.segmentos = segmentos
                territorios.append(
                    {
                        "id": territorio.id,
                        "publico": True,
                        "nome": territorio.nome,
                        "municipio": territorio.municipio_referencia.nome,
                        "estado": territorio.municipio_referencia.estado.nome,
                        "qtde_familias": territorio.qtde_familias,
                        "conflitos": get_conflitos_list(territorio.conflitos),
                        "areas_de_uso": get_areas_de_uso_list(territorio.areas_de_uso),
                        "poligonos": territorio.geo_poligonos,
                        "segmentos": territorio.segmentos,
                        "poligono_marker": territorio.geo_posicao_inicial,
                        "status_revisao": (
                            get_choice(status_revisao[0].status)
                            if len(status_revisao)
                            else ""
                        ),
                    }
                )
                continue

            added = False
            for i, territorio_loop in enumerate(territorios):
                if not isinstance(territorio_loop, dict) and territorio_loop.publico:
                    continue
                if territorio_loop["id"] == territorio.municipio_referencia.id:
                    territorios[i]["qtde"] += 1
                    for segmento in territorio.tipos_comunidade.all():
                        if segmento.nome not in territorios[i]["segmentos"]:
                            territorios[i]["segmentos"].append(segmento.nome)
                    added = True
                    break

            if not added:
                segmentos = []
                for segmento in territorio.tipos_comunidade.all():
                    segmentos.append(segmento.nome)
                territorios.append(
                    {
                        "id": territorio.municipio_referencia.id,
                        "publico": False,
                        "municipio": str(territorio.municipio_referencia),
                        "lat": territorio.municipio_referencia.latitude,
                        "lng": territorio.municipio_referencia.longitude,
                        "qtde": 1,
                        "segmentos": segmentos,
                    }
                )

        return territorios

    def get_territorios_queryset(self, form):
        queryset = self.map_queryset()

        data_filter = {
            k: form.data.getlist(k)
            for k, v in form.data.items()
            if k not in ["csrfmiddlewaretoken", "submit"]
        }
        if not data_filter and len(self.request.GET) > 0:
            data_filter = {
                k: self.request.GET.getlist(k) for k, v in self.request.GET.items()
            }

        if not data_filter:
            return queryset

        if "estado" in data_filter:
            queryset = queryset.filter(
                municipio_referencia__estado__in=data_filter["estado"]
            )

        if "bioma" in data_filter:
            queryset = queryset.filter(bioma__in=data_filter["bioma"])

        if "comunidade" in data_filter:
            queryset = queryset.filter(tipos_comunidade__in=data_filter["comunidade"])

        if "publico" in data_filter:
            if "publico" in data_filter and data_filter["publico"][0] != "":
                publico = data_filter["publico"][0] == "1"
                queryset = queryset.filter(publico=publico)

        return queryset


#########################


# class TerritorioView(PermissionRequiredMixin, FormView):
#     model = Territorio
#     form_class = UsuariaTerritorioAssociarForm
#     template_name = "relatorios/territorios.html"
#     permission_required = "permissao_padrao"
#     tipos_areas_de_uso = TipoAreaDeUso.objects.all()
#     tipos_conflitos = TipoConflito.objects.all()
#     tipos_comunidade = TipoComunidade.objects.all()
#     territorios = None
#     organizacao_id = None
#     organizacao = None
#     view_facet = None

#     STATUS_RELEVANTES = [
#         TerritorioRevisaoStatus.A_REVISAR,
#         TerritorioRevisaoStatus.REVISANDO,
#         TerritorioRevisaoStatus.COM_DUVIDAS,
#     ]
#     STATUS_VALIDADOS = [
#         TerritorioRevisaoStatus.ENVIADO_PTT,
#         TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA,
#         TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA_ARQUIVOS,
#         TerritorioRevisaoStatus.APROVADO,
#     ]
#     STATUS_EM_PREENCHIMENTO = [
#         TerritorioRevisaoStatus.EM_PREENCHIMENTO_TONOMAPA,
#     ]

#     def dispatch(self, request, *args, **kwargs):
#         if kwargs.get("is_testes_view", False):
#             self.view_facet = "testes"
#         elif kwargs.get("is_validados_view", False):
#             self.view_facet = "validados"
#         elif kwargs.get("is_em_analise_view", False):
#             self.view_facet = "em_analise"

#         self.organizacao_id = kwargs.get("organizacao_id", None)
#         if self.organizacao_id:
#             self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)

#         # anonymous will be denied access by PermissionRequiredMixin
#         # admin_geral will have all access
#         if self.request.user.is_anonymous or self.request.user.has_perm(
#             "territorio.ver_todos"
#         ):
#             return super(TerritorioView, self).dispatch(request, *args, **kwargs)

#         if self.request.user.is_reviewer:
#             # reviewer will go to reviewer panorama (homepage) if the request has organizacao_id. Not allowed here with organizacao_id
#             if self.organizacao_id:
#                 return redirect("TerritorioAgregadoIndex")

#             self.form_class = TerritorioAutoAssociarRevisoraForm
#             return super(TerritorioView, self).dispatch(request, *args, **kwargs)

#         if not self.organizacao_id and self.request.user.has_perm(
#             "usuaria.administrar_organizacao"
#         ):
#             organizacoes_que_administra = self.request.user.organizacoes_que_administra
#             target_organizacao_id = organizacoes_que_administra.first().id
#             return redirect(
#                 "TerritorioAgregadoIndex", organizacao_id=target_organizacao_id
#             )

#         return super(TerritorioView, self).dispatch(request, *args, **kwargs)

#     def get_form_kwargs(self):
#         kwargs = super(TerritorioView, self).get_form_kwargs()
#         if not self.request.user.is_reviewer:
#             kwargs.update(
#                 {"usuaria": self.request.user, "organizacao_id": self.organizacao_id}
#             )
#         return kwargs

#     def post(self, request, *args, **kwargs):
#         """metodo POST utilizado para (des)associação de revisor(a)"""
#         status_error = {"status": 500, "statusText": "FAIL"}
#         status_success = {"status": 200, "statusText": "OK"}

#         territorio_id = request.POST.get("territorio", None)
#         revisora_id = request.POST.get("revisora", None)
#         organizacao_id = request.POST.get("organizacao", None)

#         if not territorio_id:
#             status_error["content"] = "Comunidade inválida"
#             return JsonResponse(status_error)

#         try:
#             territorio = Territorio.objects.get(pk=territorio_id)
#         except:
#             status_error["content"] = "Comunidade inválida"
#             return JsonResponse(status_error)

#         if revisora_id:
#             # admin is assigning revisora
#             if not request.user.has_perm("usuaria.administrar"):
#                 return {
#                     "status": 403,
#                     "statusText": "PERMISSION DENIED",
#                     "content": "Permissão negada para esta ação!",
#                 }

#             try:
#                 usuaria = Usuaria.objects.get(pk=revisora_id)
#             except:
#                 status_error["content"] = "Revisor/a não encontrado/a"
#                 return JsonResponse(status_error)

#             territorio.revisora = usuaria
#             territorio.save()
#             status_success["revisora"] = f"{usuaria}"
#             status_success["revisora_id"] = f"{usuaria.id}"
#             status_success["territorio"] = f"{territorio}"
#             status_success["territorio_id"] = f"{territorio.id}"
#             status_success["revisora_update_url"] = reverse(
#                 "usuaria_update", args=(usuaria.id,)
#             )
#             status_success["revisora_desassociar_url"] = reverse(
#                 "revisora_desassociar", kwargs={"pk": territorio.id}
#             )
#             return JsonResponse(status_success)

#         if organizacao_id:
#             # admin is assigning organizacao to territorio:
#             try:
#                 organizacao = Organizacao.objects.get(pk=organizacao_id)
#             except:
#                 status_error["content"] = "Organização não encontrada"
#                 return JsonResponse(status_error)

#             if not request.user.has_perm("organizacao.alterar", organizacao):
#                 return {
#                     "status": 403,
#                     "statusText": "PERMISSION DENIED",
#                     "content": "Permissão negada para esta ação!",
#                 }

#             organizacao.territorios.add(territorio)
#             status_success["organizacao"] = f"{organizacao}"
#             status_success["organizacao_id"] = f"{organizacao.id}"
#             status_success["territorio"] = f"{territorio}"
#             status_success["territorio_id"] = f"{territorio.id}"
#             status_success["organizacao_update_url"] = reverse(
#                 "organizacao_update", args=(organizacao.id,)
#             )
#             status_success["organizacao_desassociar_url"] = reverse(
#                 "organizacao_desassociar", kwargs={"pk": territorio.id}
#             )
#             return JsonResponse(status_success)

#         # reviewer is auto assigning review
#         if not request.user.is_reviewer:
#             return {
#                 "status": 403,
#                 "statusText": "PERMISSION DENIED",
#                 "content": "Permissão negada para esta ação!",
#             }

#         territorio.revisora = request.user
#         territorio.save()
#         status_success["revisora"] = f"{request.user}"
#         status_success["revisora_id"] = f"{request.user.id}"
#         status_success["territorio"] = f"{territorio}"
#         status_success["territorio_id"] = f"{territorio.id}"
#         status_success["revisora_update_url"] = reverse(
#             "usuaria_update", args=(request.user.id,)
#         )
#         status_success["revisora_desassociar_url"] = reverse(
#             "revisora_desassociar", kwargs={"pk": territorio.id}
#         )
#         return JsonResponse(status_success)

#     def get_territorios_queryset(self):
#         data_filter = self.request.GET

#         if self.request.user.is_reviewer:
#             status_revisao = data_filter.get("status_revisao", None)
#             if status_revisao:
#                 if status_revisao == "revisar":
#                     return self.request.user.territorios_para_revisar
#                 if status_revisao == "em-analise":
#                     return self.request.user.territorios_em_analise
#                 if status_revisao == "processados":
#                     return self.request.user.territorios_processados
#                 return Territorio.objects.none()

#             territorios = get_territorios_visiveis_revisora(self.request.user)
#         elif self.organizacao:
#             territorios = self.organizacao.territorios_visiveis()
#         else:
#             territorios = Territorio.objects

#         filter_usuaria = data_filter.get("usuaria", None)
#         if filter_usuaria:
#             territorios = territorios.filter(terr_usuarias__usuaria=filter_usuaria)

#         filter_municipio = data_filter.get("municipio", None)
#         if filter_municipio:
#             territorios = territorios.filter(municipio_referencia=filter_municipio)

#         # TODO: transition to server-side pagination, or, even better, migrate to drf-react-by-schema
#         if self.request.user.is_parceira:
#             territorios = territorios.filter(
#                 status__in=TerritorioStatus.STATUS_APROVADOS, teste=False, publico=True
#             )
#         elif self.view_facet == "testes":
#             territorios = territorios.filter(teste=True)
#         elif self.view_facet == "em_analise":
#             territorios = territorios.filter(
#                 status=TerritorioStatus.ENVIADO_PENDENTE, teste=False
#             )
#         elif self.view_facet == "validados":
#             territorios = territorios.filter(
#                 status__in=TerritorioStatus.STATUS_APROVADOS, teste=False
#             )

#         # temporary: hide territorios not changed for 18 months.
#         # TODO: transition to server-side pagination, or, even better, migrate to drf-react-by-schema
#         # if not self.view_facet:
#         #     territorios = territorios.filter(
#         #         Q(status__in=TerritorioStatus.STATUS_APROVADOS)
#         #         | Q(ultima_alteracao__gt=now() + timedelta(weeks=-(12 * 4)))
#         #     )

#         # self.territorios = territorios.order_by("-ultima_alteracao")
#         territorios = territorios.select_related(
#             "organizacao",
#             "municipio_referencia",
#             "municipio_referencia__estado",
#             "revisora",
#             "bioma",
#         ).prefetch_related(
#             "tipos_comunidade",
#             "areas_de_uso",
#             "areas_de_uso__tipo_area_de_uso",
#             "conflitos",
#             "conflitos__tipo_conflito",
#         )
#         return territorios

#     def get_territorios(self):
#         if self.territorios:
#             return self.territorios
#         status_relevantes = self.STATUS_RELEVANTES
#         status_validados = self.STATUS_VALIDADOS
#         status_em_preenchimento = self.STATUS_EM_PREENCHIMENTO

#         territorios_qs = self.get_territorios_queryset()

#         if self.request.user.is_parceira:
#             self.territorios = territorios_qs.annotate(
#                 area=RawSQL("ST_AREA(poligono::geography)", [])
#             )
#             return self.territorios

#         if self.view_facet == "testes":
#             self.territorios = territorios_qs
#             return self.territorios

#         if self.view_facet == "validados":
#             self.territorios = territorios_qs.annotate(
#                 area=RawSQL("ST_AREA(poligono::geography)", [])
#             )
#             return self.territorios

#         self.territorios = territorios_qs.annotate(
#             area=RawSQL("ST_AREA(poligono::geography)", []),
#             relevancia=build_annotate_relevancia(),
#         )
#         return self.territorios

#     def get_territorios_usuarias(self):
#         territorios = self.get_territorios()
#         territorios_usuarias_qs = (
#             TerritorioUsuaria.objects.prefetch_related(
#                 "territorio__municipio_referencia__municipio_usuarias"
#             )
#             .select_related(
#                 "usuaria",
#                 "territorio",
#                 "territorio__organizacao",
#             )
#             .filter(territorio__in=territorios)
#             .order_by("territorio_id")
#         )

#         organizacoes_que_administra_ids = (
#             self.request.user.organizacoes_que_administra_ids
#         )
#         usuarias_que_administra_ids = OrganizacaoUsuaria.objects.filter(
#             organizacao_id__in=organizacoes_que_administra_ids
#         ).values_list("usuaria_id", flat=True)
#         cadastrantes = {}
#         for territorio_usuaria in territorios_usuarias_qs:
#             is_editable = (
#                 self.request.user.has_perm("admin_geral")
#                 or territorio_usuaria.usuaria.id in usuarias_que_administra_ids
#             )
#             territorio_id = territorio_usuaria.territorio.id
#             territorio_usuaria_dict = {
#                 "territorio_id": territorio_id,
#                 "usuaria_id": territorio_usuaria.usuaria.id,
#                 "usuaria__first_name": territorio_usuaria.usuaria.first_name,
#                 "usuaria__last_name": territorio_usuaria.usuaria.last_name,
#                 "usuaria__is_editable": is_editable,
#             }

#             if not territorio_usuaria.usuaria.is_parceira:
#                 if not territorio_usuaria.territorio.id in cadastrantes:
#                     cadastrantes[territorio_id] = []
#                 cadastrantes[territorio_id].append(territorio_usuaria_dict)

#         return {
#             "cadastrantes": cadastrantes,
#         }

#     def get_territorios_revisao_status(self):
#         territorios_revisao_status_qs = (
#             TerritorioRevisaoStatus.objects.filter(status__isnull=False)
#             .values(
#                 "territorio_id",
#                 "status",
#             )
#             .annotate(
#                 **{
#                     "status_nome": get_choice_name(
#                         TerritorioRevisaoStatus.CHOICES, "status"
#                     )
#                 }
#             )
#             .order_by("territorio_id")
#         )
#         territorios_revisao_status = {}
#         for territorio_revisao_status in territorios_revisao_status_qs:
#             if (
#                 not territorio_revisao_status["territorio_id"]
#                 in territorios_revisao_status
#             ):
#                 territorios_revisao_status[
#                     territorio_revisao_status["territorio_id"]
#                 ] = []

#             if territorio_revisao_status["status"] in [
#                 TerritorioRevisaoStatus.ENVIADO_PTT,
#                 TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA,
#                 TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA_ARQUIVOS,
#             ]:
#                 territorio_revisao_status["status"] = TerritorioRevisaoStatus.APROVADO
#                 territorio_revisao_status["status_nome"] = "Validado"

#             territorios_revisao_status[
#                 territorio_revisao_status["territorio_id"]
#             ].append(territorio_revisao_status)
#         return territorios_revisao_status

#     def get_territorios_tipos_comunidade(self):
#         tipos_comunidade = {}
#         for tipo_comunidade in self.tipos_comunidade:
#             tipos_comunidade[tipo_comunidade.id] = 0

#         territorios_qs = self.get_territorios()

#         territorios_tipos_comunidade = {}
#         territorios_tipos_comunidade_counter = {}

#         for territorio in territorios_qs:
#             if not territorio.id in territorios_tipos_comunidade_counter:
#                 territorios_tipos_comunidade_counter[territorio.id] = (
#                     tipos_comunidade.copy()
#                 )

#             for tipo_comunidade in territorio.tipos_comunidade.all():
#                 territorios_tipos_comunidade_counter[territorio.id][
#                     tipo_comunidade.id
#                 ] += 1
#                 if not territorio.id in territorios_tipos_comunidade:
#                     territorios_tipos_comunidade[territorio.id] = []
#                 if (
#                     not tipo_comunidade.nome
#                     in territorios_tipos_comunidade[territorio.id]
#                 ):
#                     territorios_tipos_comunidade[territorio.id].append(
#                         tipo_comunidade.nome
#                     )

#         return {
#             "territorios_tipos_comunidade": territorios_tipos_comunidade,
#             "territorios_tipos_comunidade_counter": territorios_tipos_comunidade_counter,
#         }

#     def get_territorios_usos_e_conflitos(self):
#         tipos_usos = {}
#         for tipo_uso in self.tipos_areas_de_uso:
#             tipos_usos[tipo_uso.id] = 0

#         tipos_conflitos = {}
#         for tipo_conflito in self.tipos_conflitos:
#             tipos_conflitos[tipo_conflito.id] = 0

#         territorios_qs = self.get_territorios()

#         territorios_usos = {}
#         territorios_usos_counter = {}
#         territorios_conflitos = {}
#         territorios_conflitos_counter = {}

#         for territorio in territorios_qs:
#             if not territorio.id in territorios_usos_counter:
#                 territorios_usos_counter[territorio.id] = tipos_usos.copy()

#             for area_de_uso in territorio.areas_de_uso.all():
#                 territorios_usos_counter[territorio.id][
#                     area_de_uso.tipo_area_de_uso.id
#                 ] += 1
#                 if not territorio.id in territorios_usos:
#                     territorios_usos[territorio.id] = []
#                 if (
#                     not area_de_uso.tipo_area_de_uso.nome
#                     in territorios_usos[territorio.id]
#                 ):
#                     territorios_usos[territorio.id].append(
#                         area_de_uso.tipo_area_de_uso.nome
#                     )

#             if not territorio.id in territorios_conflitos_counter:
#                 territorios_conflitos_counter[territorio.id] = tipos_conflitos.copy()

#             for conflito in territorio.conflitos.all():
#                 territorios_conflitos_counter[territorio.id][
#                     conflito.tipo_conflito.id
#                 ] += 1
#                 if not territorio.id in territorios_conflitos:
#                     territorios_conflitos[territorio.id] = []
#                 if (
#                     not conflito.tipo_conflito.nome
#                     in territorios_conflitos[territorio.id]
#                 ):
#                     territorios_conflitos[territorio.id].append(
#                         conflito.tipo_conflito.nome
#                     )

#         return {
#             "territorios_usos": territorios_usos,
#             "territorios_usos_counter": territorios_usos_counter,
#             "territorios_conflitos": territorios_conflitos,
#             "territorios_conflitos_counter": territorios_conflitos_counter,
#         }

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["modulo"] = "territorio"
#         context["organizacao_id"] = self.organizacao_id
#         context["organizacao"] = self.organizacao
#         context["title"] = "Comunidades"
#         context["temFooter"] = False
#         context["territorios"] = self.get_territorios()
#         territorios_usuarias = self.get_territorios_usuarias()
#         context["cadastrantes"] = territorios_usuarias["cadastrantes"]
#         context["territorios_revisao_status"] = self.get_territorios_revisao_status()
#         context["territorios_tipos_comunidade"] = (
#             self.get_territorios_tipos_comunidade()
#         )
#         context["territorios_usos_e_conflitos"] = (
#             self.get_territorios_usos_e_conflitos()
#         )
#         context["tipos_comunidade"] = self.tipos_comunidade
#         context["tipos_areas_de_uso"] = self.tipos_areas_de_uso
#         context["tipos_conflitos"] = self.tipos_conflitos
#         context["show_organizacoes"] = (
#             self.request.user.has_perm("territorio.ver_todos")
#             or self.request.user.organizacoes.count() > 1
#         )
#         context["formOrganizacao"] = OrganizacaoTerritorioAssociarForm(
#             usuaria=self.request.user
#         )
#         context["view_facet"] = self.view_facet

#         data_filter = self.request.GET

#         status_revisao = data_filter.get("status_revisao", None)
#         if status_revisao:
#             if status_revisao == "revisar":
#                 context["filter_status_revisao"] = "a revisar"
#             if status_revisao == "em-analise":
#                 context["filter_status_revisao"] = "em revisão"
#             if status_revisao == "processados":
#                 context["filter_status_revisao"] = "processadas"
#         usuaria_id = data_filter.get("usuaria", None)
#         if usuaria_id:
#             try:
#                 usuaria = Usuaria.objects.get(pk=usuaria_id)
#             except:
#                 return context

#             perfil = (
#                 "revisor/a"
#                 if usuaria.is_reviewer and not usuaria.has_perm("territorio.ver_todos")
#                 else "usuário/a"
#             )
#             context["filter_usuaria"] = f"associadas ao/à {perfil} {usuaria}"

#         municipio_id = data_filter.get("municipio", None)
#         if municipio_id:
#             try:
#                 municipio = Municipio.objects.get(pk=municipio_id)
#             except:
#                 return context

#             context["filter_municipio"] = f'relacionadas ao município "{municipio}"'

#         return context
