from rest_framework import serializers

# Serializers define the API representation.
from relatorios.models import TerritorioAgregado


class TerritorioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TerritorioAgregado
        fields = [
            "id",
            "nome",
            "qtde_familias",
            "ano_fundacao",
            "criacao",
            "ultima_alteracao",
            "publico",
            "municipio_nome",
            "estado_uf",
            "estado_nome",
            "area",
            "status_nome",
            "tipos_comunidade",
            "tipos_areas_de_uso",
            "tipos_conflitos",
        ]
