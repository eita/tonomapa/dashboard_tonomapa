# Generated by Django 3.2 on 2022-05-16 19:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0037_auto_20220516_1450'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CadastrosTerritorios',
        ),
        migrations.DeleteModel(
            name='CadastrosTerritoriosOrganizacao',
        ),
        migrations.DeleteModel(
            name='CadastrosUsuarias',
        ),
        migrations.DeleteModel(
            name='CadastrosUsuariasOrganizacao',
        ),
        migrations.DeleteModel(
            name='EstadosTerritorios',
        ),
        migrations.DeleteModel(
            name='EstadosTerritoriosOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisAreasDeUso',
        ),
        migrations.DeleteModel(
            name='TotaisAreasDeUsoOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisAreasDeUsoPorEstado',
        ),
        migrations.DeleteModel(
            name='TotaisAreasDeUsoPorEstadoOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisConflitos',
        ),
        migrations.DeleteModel(
            name='TotaisConflitosOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisConflitosPorEstado',
        ),
        migrations.DeleteModel(
            name='TotaisConflitosPorEstadoOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisGerais',
        ),
        migrations.DeleteModel(
            name='TotaisGeraisOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisStatus',
        ),
        migrations.DeleteModel(
            name='TotaisStatusOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisTipoComunidade',
        ),
        migrations.DeleteModel(
            name='TotaisTipoComunidadeOrganizacao',
        ),
        migrations.DeleteModel(
            name='TotaisTipoComunidadePorEstado',
        ),
        migrations.DeleteModel(
            name='TotaisTipoComunidadePorEstadoOrganizacao',
        ),
    ]
