# Generated by Django 3.2.13 on 2022-04-24 14:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0019_totaistipocomunidade_organizacao'),
    ]

    operations = [
        migrations.CreateModel(
            name='TotaisTipoComunidadeOrganizacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=255)),
                ('qtd_territorios', models.IntegerField()),
                ('qtd_familias', models.IntegerField()),
                ('organizacao_id', models.IntegerField()),
            ],
            options={
                'managed': False,
            },
        ),
    ]
