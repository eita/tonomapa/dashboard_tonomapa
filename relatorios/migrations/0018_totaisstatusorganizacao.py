# Generated by Django 3.2.13 on 2022-04-24 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0017_totaisstatus_organizacao'),
    ]

    operations = [
        migrations.CreateModel(
            name='TotaisStatusOrganizacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('organizacao_id', models.IntegerField()),
                ('nome', models.CharField(max_length=255)),
                ('qtd_territorios', models.IntegerField()),
            ],
            options={
                'managed': False,
            },
        ),
    ]
