# Generated by Django 3.0.3 on 2020-12-01 19:06

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0151_territorio_outro_tipo_comunidade"),
    ]

    operations = [
        # 1. TotaisGerais
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_totaisgerais;
            CREATE OR REPLACE VIEW relatorios_totaisgerais AS
            SELECT row_number() OVER ()           AS id,
                   sum(a.qtd_territorios)  AS qtd_territorios,
                   sum(a.area_territorios) AS area_territorios,
                   sum(a.qtd_familias)     AS qtd_familias,
                   sum(a.qtd_usuarios)     AS qtd_usuarios,
                   sum(a.qtd_areasdeuso)   AS qtd_areasdeuso,
                   sum(a.qtd_conflitos)    AS qtd_conflitos
            FROM (
                     SELECT count(t.id)                          AS qtd_territorios,
                            sum(st_area(t.poligono)) AS area_territorios,
                            sum(qtde_familias)                   AS qtd_familias,
                            0                                    AS qtd_usuarios,
                            0                                    AS qtd_areasdeuso,
                            0                                    AS qtd_conflitos
                     FROM main_territorio t
                     WHERE t.deleted IS NULL
                     UNION
                     SELECT 0        AS qtd_territorios,
                            0        AS area_territorios,
                            0        AS qtd_familias,
                            count(u) AS qtd_usuarios,
                            0        AS qtd_areasdeuso,
                            0        AS qtd_conflitos
                     FROM usuaria u
                     UNION
                     SELECT 0            AS qtd_territorios,
                            0            AS area_territorios,
                            0            AS qtd_familias,
                            0            AS qtd_usuarios,
                            count(au.id) AS qtd_areasdeuso,
                            0            AS qtd_conflitos
                     FROM main_areadeuso au
                     WHERE au.deleted IS NULL
                     UNION
                     SELECT 0           AS qtd_territorios,
                            0           AS area_territorios,
                            0           AS qtd_familias,
                            0           AS qtd_usuarios,
                            0           AS qtd_areasdeuso,
                            count(c.id) AS qtd_conflitos
                     FROM main_conflito c
                     WHERE c.deleted IS NULL
                 ) a
            """
        ),
        # 2. TotaisTipoComunidade
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_totaistipocomunidade;
            CREATE OR REPLACE VIEW relatorios_totaistipocomunidade AS
            SELECT mt.id       AS id,
                   mt.nome,
                   count(t.id) AS qtd_territorios,
                   sum(t.qtde_familias) as qtd_familias
            FROM main_territorio t
                     JOIN main_territorio_tipos_comunidade mttc ON t.id = mttc.territorio_id
                     JOIN main_tipocomunidade mt ON mttc.tipocomunidade_id = mt.id
            WHERE t.deleted IS NULL
            GROUP BY mt.id, mt.nome
            ORDER BY qtd_territorios DESC, mt.id;
            """
        ),
        # 3. TotaisAreasDeUso
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_totaisareasdeuso;
            CREATE OR REPLACE VIEW relatorios_totaisareasdeuso AS
            SELECT mt.id,
                   mt.nome,
                   count(au.id) AS qtd_areasdeuso
            FROM main_areadeuso au
                     JOIN main_tipoareadeuso mt ON au.tipo_area_de_uso_id = mt.id
            WHERE au.deleted IS NULL
            GROUP BY mt.id, mt.nome
            ORDER BY qtd_areasdeuso DESC, mt.id;
            """
        ),
        # 4. TotaisConflitos
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_totaisconflitos;
            CREATE OR REPLACE VIEW relatorios_totaisconflitos AS
            SELECT tc.id,
                   tc.nome,
                   count(cf.id) AS qtd_conflitos
            FROM main_conflito cf
                     join main_tipoconflito tc ON cf.tipo_conflito_id = tc.id
            WHERE cf.deleted IS NULL
            GROUP BY tc.id, tc.nome
            ORDER BY qtd_conflitos DESC, tc.id;
            """
        ),
        # 5. Totais - TerritorioStatus
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_totaisstatus;
            CREATE OR REPLACE VIEW relatorios_totaisstatus AS
            SELECT st.id,
                   st.nome,
                   count(t.id) AS qtd_territorios
            FROM main_territorio t
                     JOIN main_territoriostatus st ON t.status_id = st.id
            WHERE t.deleted IS NULL
            GROUP BY st.id, st.nome
            ORDER BY qtd_territorios desc, st.id;
            """
        ),
        # 6. Totais - CadastrosUsuarias
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_cadastrosusuarias;
            CREATE OR REPLACE VIEW relatorios_cadastrosusuarias AS
            WITH data AS (
                SELECT EXTRACT(EPOCH FROM date_trunc('day', date_joined)) AS dia,
                       count(1)                                           AS ct
                FROM usuaria
                GROUP BY 1
            )
            SELECT row_number() OVER ()                                                             AS id,
                   dia,
                   sum(ct) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS qtd_usuarias
            FROM data;
            """
        ),
        # 6. Totais - CadastrosTerritorios
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_cadastrosterritorios;
            CREATE OR REPLACE VIEW relatorios_cadastrosterritorios AS
            WITH data AS (
                SELECT EXTRACT(EPOCH FROM date_trunc('day', criacao)) AS dia,
                       count(1)                                       AS ct
                FROM main_territorio
                GROUP BY 1
            )
            SELECT row_number() OVER ()                                                             AS id,
                   dia,
                   sum(ct) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS qtd_territorios
            FROM data;
            """
        ),
        # 6. Totais - EstadosTerritorios
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_estadosterritorios;
            CREATE OR REPLACE VIEW relatorios_estadosterritorios AS
            SELECT me.id          AS id,
                   lower(me.uf)   AS uf,
                   me.nome        AS nome,
                   count(terr.id) AS qtd_territorios
            FROM main_territorio terr
                     JOIN main_municipio mm ON terr.municipio_referencia_id = mm.id
                     JOIN main_estado me ON mm.estado_id = me.id
            WHERE terr.deleted IS NULL
            GROUP BY me.id, me.uf, me.nome
            ORDER BY qtd_territorios DESC, me.nome
            """
        ),
        # 7. TerritorioAgregado
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS relatorios_territorioagregado;
            CREATE OR REPLACE VIEW relatorios_territorioagregado AS
            SELECT t.id,
                   t.nome,
                   t.qtde_familias,
                   t.ano_fundacao,
                   t.status_id,
                   t.criacao,
                   t.ultima_alteracao,
                   t.municipio_referencia_id,
                   t.publico,
                   mun.nome AS municipio_nome,
                   est.uf   AS estado_uf,
                   est.nome AS estado_nome,
                   st.nome  AS status_nome,
                   tc.tipos_comunidade,
                   auso.tipos_areas_de_uso,
                   confs.tipos_conflitos
            FROM main_territorio t
                     JOIN main_municipio mun ON t.municipio_referencia_id = mun.id
                     JOIN main_estado est ON mun.estado_id = est.id
                     LEFT JOIN main_territoriostatus st ON t.status_id = st.id
                     LEFT JOIN (SELECT terr.id,
                                       string_agg(tpcom.nome, ', ' ORDER BY tpcom.nome) AS tipos_comunidade
                                FROM main_territorio_tipos_comunidade ttc
                                         JOIN main_tipocomunidade tpcom ON ttc.tipocomunidade_id = tpcom.id
                                         JOIN main_territorio terr ON ttc.territorio_id = terr.id
                                WHERE tpcom.deleted IS NULL
                                GROUP BY terr.id
            ) tc ON tc.id = t.id
                     LEFT JOIN (
                SELECT a1.id,
                       string_agg(a1.nome, ', ' ORDER BY a1.nome) AS tipos_areas_de_uso
                FROM (
                         SELECT DISTINCT terr.id,
                                         tau.nome
                         FROM main_areadeuso au
                                  JOIN main_territorio terr ON au.territorio_id = terr.id
                                  JOIN main_tipoareadeuso tau ON au.tipo_area_de_uso_id = tau.id
                         WHERE tau.deleted IS NULL
                     ) a1
                GROUP BY a1.id
            ) auso ON auso.id = t.id
                     LEFT JOIN (
                SELECT a2.id,
                       string_agg(a2.nome, ', ' ORDER BY a2.nome) AS tipos_conflitos
                FROM (
                         SELECT DISTINCT terr.id,
                                         tcf.nome
                         FROM main_conflito cf
                                  JOIN main_territorio terr ON cf.territorio_id = terr.id
                                  JOIN main_tipoconflito tcf ON cf.tipo_conflito_id = tcf.id
                         WHERE tcf.deleted IS NULL
                     ) a2
                GROUP BY a2.id
            ) confs ON confs.id = t.id
            WHERE t.deleted IS NULL
            ORDER BY t.id
            """
        ),
    ]
