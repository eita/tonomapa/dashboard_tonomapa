# Generated by Django 3.2 on 2022-05-16 17:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relatorios', '0036_cadastrosusuariasorganizacao'),
    ]

    operations = [
        migrations.RunSQL(
            """
                DROP VIEW IF EXISTS relatorios_totaisgerais;
                DROP VIEW IF EXISTS relatorios_totaisstatus;
                DROP VIEW IF EXISTS relatorios_totaistipocomunidade;
                DROP VIEW IF EXISTS relatorios_totaistipocomunidadeporestado;
                DROP VIEW IF EXISTS relatorios_totaisareasdeuso;
                DROP VIEW IF EXISTS relatorios_totaisareasdeusoporestado;
                DROP VIEW IF EXISTS relatorios_totaisconflitos;
                DROP VIEW IF EXISTS relatorios_totaisconflitosporestado;
                DROP VIEW IF EXISTS relatorios_estadosterritorios;
                DROP VIEW IF EXISTS relatorios_cadastrosterritorios;
                DROP VIEW IF EXISTS relatorios_cadastrosusuarias;

                DROP VIEW IF EXISTS relatorios_totaisgeraisorganizacao;
                DROP VIEW IF EXISTS relatorios_totaisstatusorganizacao;
                DROP VIEW IF EXISTS relatorios_totaistipocomunidadeorganizacao;
                DROP VIEW IF EXISTS relatorios_totaistipocomunidadeporestadoorganizacao;
                DROP VIEW IF EXISTS relatorios_totaisareasdeusoorganizacao;
                DROP VIEW IF EXISTS relatorios_totaisareasdeusoporestadoorganizacao;
                DROP VIEW IF EXISTS relatorios_totaisconflitosorganizacao;
                DROP VIEW IF EXISTS relatorios_totaisconflitosporestadoorganizacao;
                DROP VIEW IF EXISTS relatorios_estadosterritoriosorganizacao;
                DROP VIEW IF EXISTS relatorios_cadastrosterritoriosorganizacao;
                DROP VIEW IF EXISTS relatorios_cadastrosusuariasorganizacao;
            """
        )
    ]
