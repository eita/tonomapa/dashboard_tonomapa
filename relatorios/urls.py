from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()

# ATENÇÂO: veja que tem mais uma url 'territorios' abaixo
router.register(r"territorios", views.TerritorioAgregadoViewSet)

urlpatterns = [
    # path('',views.index,name='MainIndex'),
    path("", views.IndexView.as_view(), name="RelatoriosIndex"),
    path(
        "panorama/reviewer", views.PanoramaReviewerView.as_view(), name="ReviewerIndex"
    ),
    path(
        "panorama/cadastrante",
        views.PanoramaCadastranteView.as_view(),
        name="CadastranteIndex",
    ),
    path(
        "mensagens/territorios/analise",
        views.MensagensTerritoriosAnalise.as_view(),
        name="mensagens-territorios-analise",
    ),
    path("territorios", views.TerritorioView.as_view(), name="TerritorioAgregadoIndex"),
    # path("frontend/territorios", views.TerritorioFormView.as_view(), name="TerritorioAgregadoFrontend"),
    path(
        "datatable/territorios/",
        views.TerritorioDatatableView.as_view(),
        name="TerritorioAgregadoDatatable",
    ),
    path(
        "datatable/territorios/export-all/",
        views.TerritorioDatatableExportAll.as_view(),
        name="TerritorioAgregadoDatatableExportAll",
    ),
    path(
        "territorios_testes",
        views.TerritorioView.as_view(),
        {"is_testes_view": True},
        name="TerritorioAgregadoTestesIndex",
    ),
    path(
        "datatable/territorios_testes/",
        views.TerritorioDatatableView.as_view(),
        {"is_testes_view": True},
        name="TerritorioAgregadoTestesDatatable",
    ),
    path(
        "territorios_validados",
        views.TerritorioView.as_view(),
        {"is_validados_view": True},
        name="TerritorioAgregadoValidadosIndex",
    ),
    path(
        "datatable/territorios_validados/",
        views.TerritorioDatatableView.as_view(),
        {"is_validados_view": True},
        name="TerritorioAgregadoValidadosDatatable",
    ),
    path(
        "territorios_em_analise",
        views.TerritorioView.as_view(),
        {"is_em_analise_view": True},
        name="TerritorioAgregadoEmAnaliseIndex",
    ),
    path(
        "datatable/territorios_em_analise/",
        views.TerritorioDatatableView.as_view(),
        {"is_em_analise_view": True},
        name="TerritorioAgregadoEmAnaliseDatatable",
    ),
    path("municipios", views.MunicipioView.as_view(), name="MunicipioList"),
    path(
        "municipios/datatable",
        views.MunicipioDatatableView.as_view(),
        name="MunicipioDatatableList",
    ),
    path("mapa", views.MapaView.as_view(), name="dashboard-mapa"),
    path(
        "mapa_publico", views.MapaPublicoView.as_view(), name="dashboard-mapa-publico"
    ),
    path("api/", include(router.urls)),
]
