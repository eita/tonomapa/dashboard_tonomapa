from django import template

register = template.Library()


@register.inclusion_tag("relatorios/inclusiontags/grafico_donut.html")
def grafico_donut(*args, **kwargs):  # pylint: disable=unused-argument
    grafico_args = {}
    grafico_args["jsonify_queryset_opts"] = "id," + kwargs["label_field"] + "," + kwargs["value_field"]

    return {**kwargs, **grafico_args}


@register.inclusion_tag("relatorios/inclusiontags/grafico_barras.html")
def grafico_barras(*args, **kwargs):  # pylint: disable=unused-argument
    id_field = kwargs.get("id_field", "id")
    label_field = kwargs.get("label_field", "nome")
    value_field = kwargs.get("value_field", "qtde")
    jsonify_queryset_opts = f"{id_field},{label_field},{value_field}"
    estado_field = kwargs.get("estado_field", None)
    value_estado_field = kwargs.get("value_estado_field", "qtde")
    jsonify_queryset_opts_estado = (
        f"{id_field},{label_field},{value_estado_field},{estado_field}" if estado_field else ""
    )
    return {
        "tipo_grafico": kwargs.get("tipo_grafico", "bar"),
        "titulo": kwargs.get("titulo", "Gráfico"),
        "dados": kwargs.get("dados", []),
        "id_field": id_field,
        "label_field": label_field,
        "value_field": value_field,
        "jsonify_queryset_opts": jsonify_queryset_opts,
        "dados_por_estado": kwargs.get("dados_por_estado", []),
        "estado_field": estado_field,
        "value_estado_field": value_estado_field,
        "jsonify_queryset_opts_estado": jsonify_queryset_opts_estado,
    }


@register.inclusion_tag("relatorios/inclusiontags/grafico_plot.html")
def grafico_plot(*args, **kwargs):  # pylint: disable=unused-argument
    return {**kwargs}


@register.inclusion_tag("relatorios/inclusiontags/grafico_plot_merged.html")
def grafico_plot_merged(*args, **kwargs):  # pylint: disable=unused-argument
    return {**kwargs}


@register.inclusion_tag("relatorios/inclusiontags/grafico_mapa.html")
def grafico_mapa(*args, **kwargs):  # pylint: disable=unused-argument
    grafico_args = {"jsonify_queryset_opts": f"id,{kwargs['territory_label']},{kwargs['territory_value']}"}
    return {**kwargs, **grafico_args}
