from django.utils.text import slugify
from django.db.models import (
    Q,
    Exists,
    Sum,
    Count,
    F,
    When,
    Case,
    Value,
    OuterRef,
    Subquery,
)
from django.db.models.functions import Concat
from django.contrib.postgres.aggregates import StringAgg
from django.contrib.postgres.search import SearchVector, SearchQuery
from django.db.models.expressions import RawSQL
from main.models import (
    Usuaria,
    Estado,
    TerritorioStatus,
    TerritorioRevisaoStatus,
    Territorio,
    TipoComunidade,
    TipoAreaDeUso,
    TipoConflito,
    Anexo,
)
from main.utils import datespan, get_choice_name


def get_totais_gerais(territorios_qs, organizacao_id):
    territorios = territorios_qs.annotate(
        area=RawSQL("ST_AREA(poligono::geography)", []),
        qtde_areas_de_uso=Count(
            "areas_de_uso", distinct=True, filter=Q(areas_de_uso__deleted__isnull=True)
        ),
        qtde_conflitos=Count(
            "conflitos", distinct=True, filter=Q(conflitos__deleted__isnull=True)
        ),
    )
    filter_aprovados = Q(status_id__in=TerritorioStatus.STATUS_APROVADOS)
    totais_gerais = territorios.aggregate(
        total_territorios=Count("id"),
        total_territorios_aprovados=Count("id", filter=filter_aprovados),
        area_territorios=Sum("area"),
        area_territorios_aprovados=Sum("area", filter=filter_aprovados),
        total_familias=Sum("qtde_familias"),
        total_familias_aprovadas=Sum("qtde_familias", filter=filter_aprovados),
        total_areas_de_uso=Sum("qtde_areas_de_uso"),
        total_areas_de_uso_aprovadas=Sum("qtde_areas_de_uso", filter=filter_aprovados),
        total_conflitos=Sum("qtde_conflitos"),
        total_conflitos_aprovados=Sum("qtde_conflitos", filter=filter_aprovados),
    )
    totais_gerais["qtde_usuarias"] = Usuaria.objects.count()

    if organizacao_id:
        totais_gerais["qtde_revisoras"] = Usuaria.objects.filter(
            org_usuarias__organizacao_id=organizacao_id, org_usuarias__is_admin=False
        ).count()

    return totais_gerais


def get_totais_status(territorios_qs):
    totais = (
        TerritorioRevisaoStatus.objects.filter(
            territorio__in=territorios_qs.values_list("pk")
        )
        .values(
            "status",
        )
        .annotate(
            nome=get_choice_name(TerritorioRevisaoStatus.CHOICES, "status"),
            qtde=Count("status"),
        )
        .order_by("-qtde")
    )
    return totais


def get_totais_tipo_comunidade(tipos_comunidade_qs, por_estado=False):
    values = (
        "id",
        "nome",
    )
    if por_estado:
        values += ("territorios__municipio_referencia__estado__nome",)
    totais = (
        tipos_comunidade_qs.values(*values).annotate(qtde=Count("id")).order_by("-qtde")
    )
    return totais


def get_totais_areas(queryset, tipo_de_area, por_estado=False):
    values = (
        "label_id",
        "label_nome",
    )
    labels = {
        "label_id": F(f"tipo_{tipo_de_area}_id"),
        "label_nome": F(f"tipo_{tipo_de_area}__nome"),
    }
    if por_estado:
        labels["label_estado"] = F("territorio__municipio_referencia__estado__nome")
        values += ("label_estado",)
    totais = (
        queryset.annotate(**labels)
        .values(*values)
        .annotate(qtde=Count("id"))
        .order_by("-qtde")
    )
    return totais


def get_estados_territorios(territorios_qs, por_estado=False):
    totais = Estado.objects.filter(
        municipios__territorios__in=territorios_qs.values_list("pk"),
        municipios__territorios__status_id__in=TerritorioStatus.STATUS_APROVADOS,
    )

    values = (
        "id",
        "uf",
        "nome",
    )
    if por_estado:
        values += ("label_estado",)
        totais = totais.annotate(
            label_estado=F("territorios__municipio_referencia__estado__nome")
        )

    totais = (
        totais.values(*values)
        .annotate(qtde=Count("municipios__territorios"))
        .order_by("-qtde")
    )
    return totais


def get_cadastros_territorios(territorios_qs, por_estado=False):
    totais = territorios_qs.annotate(
        dia=RawSQL("EXTRACT(EPOCH FROM date_trunc('day', criacao))", []),
    )

    values = ("dia",)
    if por_estado:
        values += (
            "label_estado",
            "label_uf",
        )
        totais = totais.annotate(
            label_estado=F("municipio_referencia__estado__nome"),
            label_uf=F("municipio_referencia__estado__uf"),
        )

    totais = (
        totais.values(*values)
        .annotate(qtde=Count("id"))
        .filter(qtde__gt=0)
        .order_by("dia")
    )

    if not totais.first():
        return None

    normalized_cadastros_territorios = []
    estados = Estado.objects.all()

    primeiro_dia = totais.first().get("dia", 0)
    ultimo_dia = totais.last().get("dia", 0)

    qtde = 0
    qtde_por_estado = {}
    for estado in estados:
        qtde_por_estado[estado.nome] = 0

    range = datespan(primeiro_dia, ultimo_dia)
    for i, span in enumerate(range):
        # totais_do_dia = []
        for total in totais:
            dia = total.get("dia")
            if (i == 0 and dia == span) or (
                i > 0 and dia > range[i - 1] and dia <= span
            ):
                if not por_estado:
                    qtde += total.get("qtde")
                    break

                estado = total.get("label_estado")
                if estado:
                    qtde_por_estado[estado] += total.get("qtde")

        if por_estado:
            for estado in estados:
                normalized_cadastros_territorios.append(
                    {
                        "estado": estado.nome,
                        "dia": span,
                        "qtde": qtde_por_estado[estado.nome],
                    }
                )
        else:
            normalized_cadastros_territorios.append({"dia": span, "qtde": qtde})
    return normalized_cadastros_territorios


def get_cadastros_usuarias():
    totais = Usuaria.objects.annotate(
        dia=RawSQL("EXTRACT(EPOCH FROM date_trunc('day', date_joined))", []),
    )
    values = ("dia",)
    totais = (
        totais.values(*values)
        .annotate(qtde=Count("id"))
        .filter(qtde__gt=0)
        .order_by("dia")
    )

    if not totais.first():
        return None

    normalized_cadastros_usuarias = []

    primeiro_dia = totais.first().get("dia", 0)
    ultimo_dia = totais.last().get("dia", 0)

    qtde = 0
    range = datespan(primeiro_dia, ultimo_dia)
    for i, span in enumerate(range):
        # totais_do_dia = []
        for total in totais:
            dia = total.get("dia")
            if (i == 0 and dia == span) or (
                i > 0 and dia > range[i - 1] and dia <= span
            ):
                qtde += total.get("qtde")
                break

        normalized_cadastros_usuarias.append({"dia": span, "qtde": qtde})
    return normalized_cadastros_usuarias


def get_cadastros(territorios_qs, organizacao_id, is_reviewer=False):
    cadastros = {}
    cadastros["territorios"] = get_cadastros_territorios(territorios_qs)
    cadastros["territorios_por_estado"] = get_cadastros_territorios(
        territorios_qs, True
    )
    if not organizacao_id and not is_reviewer:
        cadastros["usuarias"] = get_cadastros_usuarias()
        if cadastros["usuarias"] and cadastros["territorios"]:
            ultimo_dia_usuarias = cadastros["usuarias"][-1]
            ultimo_dia_territorios = cadastros["territorios"][-1]
            if ultimo_dia_usuarias["dia"] > ultimo_dia_territorios["dia"]:
                for usuaria in cadastros["usuarias"]:
                    if usuaria["dia"] > ultimo_dia_territorios["dia"]:
                        cadastros["territorios"].append(
                            {
                                "dia": usuaria["dia"],
                                "qtde": ultimo_dia_territorios["qtde"],
                            }
                        )
            else:
                for territorio in cadastros["territorios"]:
                    if territorio["dia"] > ultimo_dia_usuarias["dia"]:
                        cadastros["usuarias"].append(
                            {
                                "dia": territorio["dia"],
                                "qtde": ultimo_dia_usuarias["qtde"],
                            }
                        )

    return cadastros


def get_totais(
    territorios_qs, tipos_comunidade_qs, areas_de_uso_qs, conflitos_qs, organizacao_id
):
    obj = {}
    obj["totais_gerais"] = get_totais_gerais(territorios_qs, organizacao_id)
    obj["totais_status"] = get_totais_status(territorios_qs)
    obj["totais_tipo_comunidade"] = get_totais_tipo_comunidade(tipos_comunidade_qs)
    obj["totais_tipo_comunidade_por_estado"] = get_totais_tipo_comunidade(
        tipos_comunidade_qs, True
    )
    obj["totais_areas_de_uso"] = get_totais_areas(areas_de_uso_qs, "area_de_uso")
    obj["totais_areas_de_uso_por_estado"] = get_totais_areas(
        areas_de_uso_qs, "area_de_uso", True
    )
    obj["totais_conflitos"] = get_totais_areas(conflitos_qs, "conflito")
    obj["totais_conflitos_por_estado"] = get_totais_areas(
        conflitos_qs, "conflito", True
    )
    obj["estados_territorios"] = get_estados_territorios(territorios_qs)
    obj["evolucao_cadastros"] = get_cadastros(territorios_qs, organizacao_id)
    return obj


def build_annotate_relevancia():
    status_relevantes = TerritorioRevisaoStatus.STATUS_RELEVANTES
    status_validados = TerritorioRevisaoStatus.STATUS_VALIDADOS
    status_em_preenchimento = TerritorioRevisaoStatus.STATUS_EM_PREENCHIMENTO
    return Case(
        When(
            Q(qtde_familias__gt=0)
            & Q(poligono__isnull=False)
            & Exists(
                Territorio.tipos_comunidade.through.objects.filter(
                    territorio_id=OuterRef("pk")
                )
            )
            & Exists(
                Anexo.objects.filter(territorio_id=OuterRef("pk"), tipo_anexo="ata")
            )
            & Exists(
                TerritorioRevisaoStatus.objects.filter(
                    territorio_id=OuterRef("pk"),
                    status__in=status_relevantes,
                )
            )
            & Q(teste=False)
            & Q(revisora__isnull=True),
            then=Value("6 - Relevante e sem revisor/a"),
        ),
        When(
            Q(qtde_familias__gt=0)
            & Q(poligono__isnull=False)
            & Exists(
                Territorio.tipos_comunidade.through.objects.filter(
                    territorio_id=OuterRef("pk")
                )
            )
            & Exists(
                Anexo.objects.filter(territorio_id=OuterRef("pk"), tipo_anexo="ata")
            )
            & Exists(
                TerritorioRevisaoStatus.objects.filter(
                    territorio_id=OuterRef("pk"),
                    status__in=status_relevantes,
                )
            )
            & Q(teste=False)
            & Q(revisora__isnull=False),
            then=Value("5 - Relevante e com revisor/a"),
        ),
        When(
            Q(qtde_familias__gt=0)
            & Q(poligono__isnull=False)
            & Exists(
                Territorio.tipos_comunidade.through.objects.filter(
                    territorio_id=OuterRef("pk")
                )
            )
            & ~Exists(
                Anexo.objects.filter(territorio_id=OuterRef("pk"), tipo_anexo="ata")
            )
            & Exists(
                TerritorioRevisaoStatus.objects.filter(
                    territorio_id=OuterRef("pk"),
                    status__in=status_relevantes,
                )
            )
            & Q(teste=False),
            then=Value("4 - Parcialmente completo"),
        ),
        When(
            (
                Q(qtde_familias=0)
                | Q(qtde_familias__isnull=True)
                | Q(poligono__isnull=True)
                | ~Exists(
                    Territorio.tipos_comunidade.through.objects.filter(
                        territorio_id=OuterRef("pk")
                    )
                )
            )
            & ~Exists(
                Anexo.objects.filter(territorio_id=OuterRef("pk"), tipo_anexo="ata")
            )
            & Exists(
                TerritorioRevisaoStatus.objects.filter(
                    territorio_id=OuterRef("pk"),
                    status__in=status_relevantes,
                )
            )
            & Q(teste=False),
            then=Value("3 - Frágil"),
        ),
        When(
            Q(status__in=TerritorioStatus.STATUS_APROVADOS) & Q(teste=False),
            then=Value("2 - Validado"),
        ),
        When(
            ~Exists(
                TerritorioRevisaoStatus.objects.filter(
                    territorio_id=OuterRef("pk"),
                    status__in=status_em_preenchimento,
                )
            )
            & Q(teste=False)
            & Q(status__isnull=False),
            then=Value("1 - Em preenchimento"),
        ),
        When(
            Q(teste=True),
            then=Value("0 - Teste"),
        ),
        default=Value("0 - Rejeitadas ou em situação desconhecida"),
    )


def build_annotate_cadastrante():
    return StringAgg(
        Concat(
            F("usuarias__first_name"),
            Value(" "),
            F("usuarias__last_name"),
        ),
        delimiter=" ",
        distinct=True,
    )


def get_territorio_search_columns():
    columns = []
    for column in Territorio.DATATABLE_COLUMNS:
        if "searchable" in column:
            searchable = column["searchable"]
            if type(searchable) == type(True):
                if searchable:
                    columns.append(column["field"])
            else:
                for searchable_field in searchable:
                    columns.append(searchable_field)
    return columns


def get_territorio_search_columns_icontains():
    columns = []
    for column in Territorio.DATATABLE_COLUMNS:
        if "searchable_icontains" in column:
            searchable_icontains = column["searchable_icontains"]
            for searchable_icontains_field in searchable_icontains:
                columns.append(searchable_icontains_field)
    return columns


def get_territorio_export_columns():
    fields = []
    for column in Territorio.DATATABLE_COLUMNS:
        if "searchable" in column and type(column["searchable"]) is list:
            fields.append(column["searchable"][0])
            continue
        if "many" in column and column["many"]:
            fields.append(f"{column['field']}_aggr")
            continue
        if column["field"] not in [
            "relevancia",
            "tipos_comunidade",
            "areas_de_uso",
            "conflitos",
        ]:
            fields.append(column["field"])
    # columns = [
    #     d["field"]
    #     for d in Territorio.DATATABLE_COLUMNS
    #     if d["field"]
    #     not in ["relevancia", "tipos_comunidade", "areas_de_uso", "conflitos"]
    # ]
    for tipo_comunidade in TipoComunidade.objects.all():
        fields.append(f"tipoComunidade_{slugify(tipo_comunidade.nome)}")
    for tipo_area_de_uso in TipoAreaDeUso.objects.all():
        fields.append(f"tipoAreaDeUso_{slugify(tipo_area_de_uso.nome)}")
    for tipo_conflito in TipoConflito.objects.all():
        fields.append(f"tipoConflito_{slugify(tipo_conflito.nome)}")
    fields.append("area")
    return fields


def filter_queryset_territorios(data, search_columns, search_columns_icontains, qs):
    search = data.get("search[value]", None)
    if search:
        q = Q()
        if len(search_columns):
            search_query = SearchQuery(
                search, config="portuguese_unaccent", search_type="websearch"
            )
            search_vector = SearchVector(*search_columns, config="portuguese_unaccent")
            qs = qs.annotate(search=search_vector)
            q |= Q(search=search_query)
        if len(search_columns_icontains):
            for column in search_columns_icontains:
                q |= Q(**{"{0}__icontains".format(column): search})
        qs = qs.filter(q).distinct()

    return qs
    # return qs.distinct("id").order_by("id")
