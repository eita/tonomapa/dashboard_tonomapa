from django.contrib.gis.db import models
from main.models import TerritorioStatus, Municipio


class TerritorioAgregado(models.Model):
    nome = models.CharField(max_length=255, blank=True, null=True)
    qtde_familias = models.IntegerField(blank=True, null=True)
    ano_fundacao = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey(TerritorioStatus, models.DO_NOTHING, related_name="territorios_a", blank=True, null=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    municipio_referencia = models.ForeignKey(Municipio, models.DO_NOTHING, related_name="territorios_a")
    publico = models.BooleanField(default=False, blank=True, null=True)
    teste = models.BooleanField(default=False, blank=True, null=True)
    area = models.DecimalField(max_digits=12, decimal_places=9)
    poligono_kml = models.TextField(blank=True, null=True)
    municipio_nome = models.CharField(max_length=150, blank=True, null=True)
    estado_uf = models.CharField(max_length=2, blank=True, null=True)
    estado_nome = models.CharField(max_length=50, blank=True, null=True)
    status_nome = models.CharField(max_length=255, blank=True, null=True)
    tipos_comunidade = models.CharField(max_length=4096, blank=True, null=True)
    tipos_areas_de_uso = models.CharField(max_length=4096, blank=True, null=True)
    tipos_conflitos = models.CharField(max_length=4096, blank=True, null=True)

    class Meta:
        managed = False
