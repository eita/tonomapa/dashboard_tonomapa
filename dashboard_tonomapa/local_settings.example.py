# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "*changeme*changeme*changeme*changeme*changeme*chan"

DEBUG = True

ALLOWED_HOSTS = ["localhost"]

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "dbname",
        "USER": "dbuser",
        # 'PASSWORD': '',
        # 'HOST': '127.0.0.1',
        # 'PORT': '5432',
    }
}

MAPBOX_ACCESS_TOKEN = "myToken"

SITE_URL = "http://myurl.com"

PTT_HOST = "https://host"
PTT_API_KEY = "api-key"

CHROME_PATH = "/usr/bin/chromium-browser"

STATIC_REPORT_URL = "http://localhost:8000/"
