from django.contrib.auth.backends import BaseBackend

from main.models import Usuaria


class TelefoneBackend(BaseBackend):
    # pylint: disable=arguments-differ
    def authenticate(self, request, telefone=None, codigo=None):
        if telefone and codigo:
            try:
                return Usuaria.objects.get(telefone=telefone, codigo_uso_celular=codigo)
            except Usuaria.DoesNotExist:
                return None
        return None
