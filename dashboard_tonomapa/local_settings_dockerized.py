import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY", "")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = bool(eval(os.environ.get("DEBUG", 'False')))
LOCAL = bool(eval(os.environ.get("LOCAL", 'False')))

ALLOWED_HOSTS = list(eval(os.environ.get("ALLOWED_HOSTS", "[]")))

DATABASES = {
    "default": {
        "ENGINE": 'django.contrib.gis.db.backends.postgis',
        "NAME": os.environ.get("SQL_DATABASE", ""),
        "USER": os.environ.get("SQL_USER", ""),
        "PASSWORD": os.environ.get("SQL_PASSWORD", ""),
        "HOST": os.environ.get("SQL_HOST", ""),
        "PORT": os.environ.get("SQL_PORT", ""),
    }
}

EMAIL_HOST          = os.environ.get("EMAIL_HOST", "")
EMAIL_HOST_USER     = os.environ.get("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD", "")
EMAIL_PORT          = os.environ.get("EMAIL_PORT", "")
EMAIL_USE_TLS       = os.environ.get("EMAIL_USE_TLS", "")
SERVER_EMAIL        = os.environ.get("EMAIL_HOST_USER", "")

EMAIL_SUBJECT_PREFIX= os.environ.get("EMAIL_SUBJECT_PREFIX", "")
ACCOUNT_EMAIL_SUBJECT_PREFIX = os.environ.get("EMAIL_SUBJECT_PREFIX", "")
DEFAULT_FROM_EMAIL  = os.environ.get("DEFAULT_FROM_EMAIL", "")
ADMINS              = tuple(eval(os.environ.get("ADMINS", "")))
MANAGERS            = tuple(eval(os.environ.get("MANAGERS", "")))

# Show message in terminal instead send mail. Set only in dev env
if os.environ.get("EMAIL_BACKEND", ""):
    EMAIL_BACKEND = os.environ.get("EMAIL_BACKEND", "")

# MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiZHR5Z2VsIiwiYSI6IjY4YjRhNjU1NWJhYTU2OGU1YWQ1OTcxYjRjNDcyNzIxIn0.Go-whnL9yIt3rHicQ5gqbA";
MAPBOX_ACCESS_TOKEN = os.environ.get("MAPBOX_ACCESS_TOKEN", "")

SITE_URL = os.environ.get("SITE_URL", "")
