import graphene
from graphene_django.debug import DjangoDebug

import main.schema


class Queries(main.schema.Query, graphene.ObjectType):
    dummy = graphene.String()
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutations(main.schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Queries, mutation=Mutations)
