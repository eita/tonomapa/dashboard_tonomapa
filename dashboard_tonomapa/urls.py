from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from rest_framework import routers
from notifications import urls as notifications_urls  # type: ignore
from graphene_file_upload.django import FileUploadGraphQLView

router = routers.DefaultRouter()

from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token,
)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


urlpatterns = [
    path("relatorios/", include("relatorios.urls")),
    path("", include("main.urls")),
    path("", include("drf_react_by_schema.urls")),
    path("organizacao/<int:organizacao_id>/relatorios/", include("relatorios.urls")),
    path("organizacao/<int:organizacao_id>/", include("main.urls")),
    path("", include("main.organizacao_urls")),
    path("rest/", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("admin/", admin.site.urls),
    path("graphql", csrf_exempt(FileUploadGraphQLView.as_view(graphiql=True))),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    url(r"^accounts/", include("allauth.urls")),
    url(r"^gql", csrf_exempt(GraphQLView.as_view(batch=True))),
    url(r"^api-token-auth/", obtain_jwt_token),
    url(r"^api-token-refresh/", refresh_jwt_token),
    url(r"^api-token-verify/", verify_jwt_token),

    path("api-authentication/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("api-authentication/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("api-authentication/token/verify/", TokenVerifyView.as_view(), name="token_verify"),

    path("logs/", include("log_viewer.urls")),
]

urlpatterns += [
    # Notifications
    path(
        "inbox/notifications/", include(notifications_urls, namespace="notifications")
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
