#!/bin/bash

AWS_PROFILE_NAME=default

source docker_restore_config.sh

if [ -z $AWS_S3_BUCKET_NAME ]; then
    echo "Não foi possível executar o programa por falta de parâmetros. Considere configurar .env.prod.backup AWS_S3_BUCKET_NAME; AWS_PROFILE_NAME"
    echo ""
    echo "find -name .env.prod.backup:"
    find -name .env.prod.backup
    exit 0
fi

if [ -z $POSTGRES_USER ]; then
    echo "Não foi possível executar o programa por falta de parâmetros. Considere configurar .env.prod.db POSTGRES_USER; POSTGRES_DB"
    echo ""
    echo "find -name .env.prod.db:"
    find -name .env.prod.db
    exit 0
fi

echo ""
echo "Download latest backup database from AWS - Origin Contabo ..."
cd /tmp
FILENAME_ZIP="$(docker run --rm -it -v ~/.aws:/root/.aws amazon/aws-cli s3 --profile $AWS_PROFILE_NAME ls $AWS_S3_BUCKET_NAME | grep db | grep ctb | sort | tail -n 1 | awk '{print $4}')";
# echo "docker run --rm -it -v ~/.aws:/root/.aws amazon/aws-cli s3 --profile $AWS_PROFILE_NAME ls $AWS_S3_BUCKET_NAME | grep db | grep ctb | sort | tail -n 1 | awk '{print $4}'"
# TODO melhorar a atribuição de FILENAME_ZIP pois precisou das linhas abaixo por tentativa e erro
FILENAME_ZIP="${FILENAME_ZIP@Q}"
FILENAME_ZIP="${FILENAME_ZIP/\$\'/""}"
FILENAME_ZIP="${FILENAME_ZIP/\\r\'/""}"
docker run --rm -it -v ~/.aws:/root/.aws -v $(pwd):/aws amazon/aws-cli s3 cp --profile $AWS_PROFILE_NAME s3://$AWS_S3_BUCKET_NAME/$FILENAME_ZIP .

echo ""
echo "Unzip backup ..."
gzip -d $FILENAME_ZIP
FILENAME="${FILENAME_ZIP/\.gz/""}"
echo $FILENAME

echo ""
echo "Reset DB ..."

docker stop $(docker ps -q -f name=tonomapa_web)
docker exec -i $(docker ps -q -f name=tonomapa_db) psql -U $POSTGRES_USER -d postgres -c "drop database $POSTGRES_DB"
docker exec -i $(docker ps -q -f name=tonomapa_db) psql -U $POSTGRES_USER -d postgres -c "create database $POSTGRES_DB"

echo ""
echo "Import data ..."
cat $FILENAME | docker exec -i $(docker ps -q -f name=tonomapa_db) psql $POSTGRES_DB -U $POSTGRES_USER

echo ""
echo "Checking service web ..."
docker_service_web="$(docker service ls -q -f name=dashboard_tonomapa_web)"
echo $docker_service_web
if [[ ! -z $docker_service_web ]]; then
    echo ""
    echo "Restarting service web ..."
    cd /var/www/dashboard_tonomapa
    ./docker_service_start.sh
    docker service update $docker_service_web
    cd /tmp
else
    docker start $(docker ps -qa -f name=tonomapa_web)
fi

echo ""
echo "Migrate ..."
docker exec $(docker ps -q -f name=tonomapa_web) python manage.py migrate

echo ""
echo "Collect static ..."
docker exec $(docker ps -q -f name=tonomapa_web) python manage.py collectstatic --noinput

echo ""
echo "clear tmp ..."
rm -rf $FILENAME*

echo ""
echo "Create superuser 'useradmin'? 'CTRL C' to exit."
docker exec -it $(docker ps -q -f name=tonomapa_web) python manage.py createsuperuser --user useradmin --email useradmin@domain
