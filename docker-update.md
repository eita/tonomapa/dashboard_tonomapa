# cd dashboard_tonomapa

cd ~/dashboard_tonomapa

# Update repos

git pull origin

# Build source

docker build -f docker_django/app/Dockerfile.prod -t eitacoop/dashboard_tonomapa:1.0-python-gunicorn .

# Push image

docker push eitacoop/dashboard_tonomapa:1.0-python-gunicorn

# Stop service

./docker-django.sh service stop

# Prune images

docker image prune

# Get image updated

docker pull eitacoop/dashboard_tonomapa:1.0-python-gunicorn

# Up sercice

./docker-django.sh service start

# Check services

docker service ls

# Check service logs

docker service logs [SERVICE_ID]

# Migrate

docker exec $(docker ps -q -f name=tonomapa_web) python manage.py migrate

# Collect Static

docker exec $(docker ps -q -f name=tonomapa_web) python manage.py collectstatic --noinput
