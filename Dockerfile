FROM python:3.7-alpine3.13

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=dashboard_tonomapa.settings

# To production
RUN mkdir static
RUN mkdir media

# install psycopg2 dependencies
RUN apk update \
        && apk --no-cache add gcc python3-dev musl-dev \
        git jpeg-dev zlib-dev
RUN apk --no-cache add bash openrc \
        postgresql postgresql-dev postgresql-contrib postgis \
        gdal-dev geos-dev libmagic graphviz-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt --no-cache

COPY ./entrypoint.sh .

# copy project
COPY . .

# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
