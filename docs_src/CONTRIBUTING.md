# Contributing

This project is open-source and, as such, grows (or shrinks) & improves in part
due to the community. Below are some guidelines on how to help with the project.

## Guidelines For Contributing Code

If you're ready to take the plunge & contribute back some code/docs, the
process should look like:

-   Fork the project on GitLab into your own account.
-   Clone your copy of dashboard_tonomapa.
-   Make a new branch in git & commit your changes there.
-   Push your new branch up to GitLab.
-   Again, ensure there isn't already an issue or pull request out there on it.
    If there is & you feel you have a better fix, please take note of the issue
    number & mention it in your pull request.
-   Create a new pull request (based on your branch), including what the
    problem/feature is, versions of your software & referencing any related
    issues/pull requests.
