.. Dashboard Tô no Mapa documentation master file, created by
   sphinx-quickstart on Sun Jun  6 21:41:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação do Dashboard Tô no Mapa
================================================

.. toctree::
   :maxdepth: 1
   :caption: Sumário:

   Sobre Dashboard Tô no Mapa <README.md>
   Informações sobre instalação <installation.md>
   Informações sobre instalação com Docker <docker_django-README.md>
   Como contribuir <CONTRIBUTING.md>
   Módulo principal <main>
   Modelo de dados do módulo principal <model_main>
   Módulo de Relatórios <relatorios>
   Modelo de dados do módulo de relatórios <model_relatorios>


Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`
