# Instructions

## Instalation - development environment

* requirements:
  * python >= 3.7
  * postgresql with postgis
  * to install postgis: follow [these instructions](https://docs.djangoproject.com/en/3.1/ref/contrib/gis/install/postgis/). There are pre-compiled packages in most distributions.


```console
# assure you have python >= 3.7 installed
$ python3 -V
Python 3.7.X

# download code
$ git clone https://gitlab.com/eita/tonomapa/dashboard_tonomapa.git
$ cd dashboard_tonomapa

# create virtual environment to project
$ python3 -m venv env

# activate environment
$ source env/bin/activate

# download project requirements / libraries
(env) $ pip3 install -r requirements.txt

# create a local_settings file with the database configurations (and others)
(env) $ cp dashboard_tonomapa/local_settings.example.py dashboard_tonomapa/local_settings.py

# create a postgres database with postgis enabled (follow the instructions as pointed above)

# edit dashboard_tonomapa/local_settings.py with your editor

# start server in development environment
(env) $ python3 manage.py runserver
```

## Instalation - production environment

There are several ways to install in a production environment. 

* In local_settings.py change the DEBUG configuration to False.
* We used the *supervisor* package to install it in the system. The django process runs as a service. Besides that, we installed a proxy in apache to send to the port the django process is running. There are other (several) ways to run django projects in production. Other possible way is to use passenger.

## Others

* Para gerar o arquivo de esquema no diretório ../tonomapa/db/schema.json :
    `
    python manage.py graphql_schema --out ../tonomapa/db/schema.json
    `
    (é importante rodar isso depois de mudar as operações no django)
