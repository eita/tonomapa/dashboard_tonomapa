from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

# from django.views.generic import RedirectView

from . import views
from .autocomplete import (
    TerritorioAutocomplete,
    ReviewerAutocomplete,
    MunicipioAutocomplete,
    TipoComunidadeAutocomplete,
    EstadoAutocomplete,
    BiomaAutocomplete,
    UserNotReviewerAutocomplete,
)

urlpatterns = [
    path("", views.IndexView.as_view(), name="MainIndex"),
    path("anexos/", views.AnexoView.as_view(), name="anexos_view"),
    path(
        "codigo-seguranca/",
        views.PedidoCodigoSegurancaView.as_view(),
        name="pedido_codigo_seguranca_view",
    ),
    path("termosdeuso", views.TermosDeUsoView.as_view(), name="termosdeuso_view"),
    path("ticcas", views.TICCAsView.as_view(), name="ticcas_view"),
]

urlpatterns += (
    path("usuarias/", views.UsuariaListView.as_view(), name="usuaria_list"),
    path("revisoras/", views.RevisoraListView.as_view(), name="revisora_list"),
    path("usuarias/create/", views.UsuariaCreateView.as_view(), name="usuaria_create"),
    path(
        "usuarias/detail/<int:pk>/",
        views.UsuariaDetailView.as_view(),
        name="usuaria_detail",
    ),
    path(
        "usuarias/update/<int:pk>/",
        views.UsuariaUpdateView.as_view(),
        name="usuaria_update",
    ),
)

urlpatterns += (
    path(
        "territorio/detail/<int:pk>/",
        views.TerritorioDetailView.as_view(),
        name="territorio_detail",
    ),
    path(
        "territorio/relatorio/<int:pk>/",
        views.TerritorioDetailReportView.as_view(),
        name="territorio_report",
    ),
    path(
        "territorio/update/<int:pk>/",
        views.TerritorioUpdateView.as_view(),
        name="territorio_update",
    ),
    path(
        "territorio/delete/<int:pk>/",
        views.TerritorioDeleteView.as_view(),
        name="territorio_delete",
    ),
    path(
        "territorio/mensagens/<int:territorio_id>/",
        views.MensagemCreateView.as_view(),
        name="territorio_mensagens",
    ),
    path(
        "territorio/revisora/desassociar/<int:pk>",
        views.RevisoraTerritorioDesassociarView.as_view(),
        name="revisora_desassociar",
    ),
    path(
        "territorio/organizacao/desassociar/<int:pk>",
        views.OrganizacaoTerritorioDesassociarView.as_view(),
        name="organizacao_desassociar",
    ),
)

urlpatterns += (
    # urls for TerritorioUsuaria
    path(
        "usuarias/detail/<int:pk>/territorios/",
        views.UsuariaTerritorioUpdateView.as_view(),
        name="usuaria_territorios",
    ),
)

urlpatterns += (
    # urls for MunicipioUsuaria
    path(
        "usuarias/detail/<int:pk>/municipios/",
        views.UsuariaMunicipioUpdateView.as_view(),
        name="usuaria_municipios",
    ),
    path(
        "municipio/desassociar/<int:pk>/<int:usuaria_id>/",
        views.UsuariaMunicipioDesassociarView.as_view(),
        name="municipio_desassociar",
    ),
)

urlpatterns += [
    # Autocomplete
    path(
        "territorio-autocomplete/",
        TerritorioAutocomplete.as_view(),
        name="territorio-autocomplete",
    ),
    path(
        "reviewer-autocomplete/",
        ReviewerAutocomplete.as_view(),
        name="reviewer-autocomplete",
    ),
    path(
        "user-not-reviewer-autocomplete/",
        UserNotReviewerAutocomplete.as_view(create_field="username"),
        name="user-not-reviewer-autocomplete",
    ),
    path(
        "municipio-autocomplete/",
        MunicipioAutocomplete.as_view(),
        name="municipio-autocomplete",
    ),
    path(
        "comunidade-autocomplete/",
        TipoComunidadeAutocomplete.as_view(),
        name="comunidade-autocomplete",
    ),
    path(
        "estado-autocomplete/",
        EstadoAutocomplete.as_view(),
        name="estado-autocomplete",
    ),
    path(
        "bioma-autocomplete/",
        BiomaAutocomplete.as_view(),
        name="bioma-autocomplete",
    ),
    path(
        "ajax/municipios/<int:organizacao_id>/<int:estado_id>",
        views.AjaxMunicipiosOuEstadosList.as_view(),
        name="ajax_municipios_list",
    ),
    path(
        "ajax/municipios/",
        views.AjaxMunicipiosOuEstadosList.as_view(),
        name="ajax_municipios_list",
        kwargs={"empty": True},
    ),
    path(
        "ajax/estados/<int:organizacao_id>",
        views.AjaxMunicipiosOuEstadosList.as_view(),
        name="ajax_estados_list",
    ),
    path(
        "ajax/estados/",
        views.AjaxMunicipiosOuEstadosList.as_view(),
        name="ajax_estados_list",
        kwargs={"empty": True},
    ),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
