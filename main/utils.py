import re
import logging
import os
import uuid
from http.client import HTTPConnection
from datetime import timedelta, timezone
import curlify
import requests
from django.apps import apps
from django.core.exceptions import ValidationError
from django.db.models import (
    Q,
    When,
    Case,
    Value,
    Exists,
    Count,
    CharField,
)
from django.db import transaction
from django.conf import settings
from rest_framework.renderers import JSONRenderer


def get_model(model_name, app="main"):
    return apps.get_model(app_label=app, model_name=model_name)


def get_conflitos_list(conflitos):
    try:
        return [
            {
                "lat": conflito.posicao.tuple[1],
                "lng": conflito.posicao.tuple[0],
                "nome": conflito.nome,
                "id": conflito.id,
                "descricao": conflito.descricao if conflito.descricao else "",
                "tipo_conflito_id": conflito.tipo_conflito.id,
                "tipo_conflito_nome": conflito.tipo_conflito.nome,
            }
            for conflito in conflitos.all()
        ]
    except:
        return []


def get_areas_de_uso_list(areas_de_uso):
    try:
        return [
            {
                "lat": area.posicao.tuple[1],
                "lng": area.posicao.tuple[0],
                "nome": area.nome,
                "id": area.id,
                "descricao": area.descricao if area.descricao else "",
                "tipo_area_de_uso_id": area.tipo_area_de_uso.id,
                "tipo_area_de_uso_nome": area.tipo_area_de_uso.nome,
            }
            for area in areas_de_uso.all()
        ]
    except:
        return []


def get_prefix_url_organizacao(organizacao_id, path):
    if not organizacao_id:
        return ""
    url = re.sub(r".*(\/organizacao\/[0-9]+)(.*)?", r"\1", path)
    if url == path:
        return ""
    return url


class PTTUtils:
    @staticmethod
    def send_territorio_to_ptt(territorio):
        territorio.validate_ptt()

        # logging
        log = logging.getLogger("urllib3")
        log.setLevel(logging.DEBUG)

        # logging from urllib3 to console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        log.addHandler(stream_handler)

        # print statements from `http.client.HTTPConnection` to console/stdout
        HTTPConnection.debuglevel = 1

        payload = PTTUtils.serialize_territorio(territorio)

        print("\npayload:")
        print(payload.decode("utf-8"))
        headers = {"api-key": settings.PTT_API_KEY, "Content-Type": "application/json"}
        if territorio.ptt_id_territorio is None:
            url = settings.PTT_HOST + "/integracao/territorio/cadastrar"
            response = requests.post(url, headers=headers, data=payload, timeout=60)
            http_method = "post"
        else:
            url = (
                settings.PTT_HOST
                + "/integracao/territorio/"
                + str(territorio.ptt_id_territorio)
            )
            response = requests.put(url, data=payload, headers=headers, timeout=60)
            http_method = "put"

        print("\nResponse:")
        print(response.text)

        ptt_log = get_model("ComunicacaoPttLog")()
        ptt_log.envio = payload
        ptt_log.resposta = response.text
        ptt_log.http_status = response.status_code
        ptt_log.http_method = http_method
        ptt_log.territorio = territorio
        ptt_log.url = url
        ptt_log.save()

        if response.status_code == 200:
            response_dict = response.json()
            print(response_dict)
            territorio.ptt_id_territorio = response_dict["idTerritorio"]
            territorio.ptt_status_territorio = response_dict["statusTerritorio"]
            territorio.status_id = get_model(
                "TerritorioStatus"
            ).ENVIO_ARQUIVOS_PTT_NAFILA
            territorio.save()

        print("\nCURL equivalent command:")
        print(curlify.to_curl(response.request))
        return response.status_code

    @staticmethod
    def send_territorio_files_to_ptt(territorio):
        territorio.validate_ptt()

        HTTPConnection.debuglevel = 0

        if territorio.ptt_id_territorio is None:
            raise ValidationError({"base": "Esta comunidade não tem ID no PTT"})

        ptt_files_dir = os.path.join(settings.MEDIA_ROOT, "files_ptt")
        try:
            os.mkdir(ptt_files_dir)
        except FileExistsError:
            pass

        compressed_file_path = os.path.join(ptt_files_dir, f"{territorio.id}.zip")

        anexos = territorio.get_anexos_ptt()

        anexos_files = ['"' + anexo.arquivo.path + '"' for anexo in anexos]

        # compress files
        return_code = os.system(
            f"zip -j \"{compressed_file_path}\" {' '.join(anexos_files)}"
        )
        if return_code == 0:

            headers = {"api-key": settings.PTT_API_KEY}

            url = (
                settings.PTT_HOST
                + f"/integracao/territorio/{territorio.ptt_id_territorio}/arquivos"
            )
            files = {"file": open(compressed_file_path, "rb")}
            response = requests.post(url, headers=headers, files=files, timeout=60)
            http_method = "post"
            print("request")
            print(response.request.headers)
            print(response.request.method)
            print(response.request.url)

            print("response")
            print(response.headers)
            print(response.text)
            print(response.status_code)
            # else:
            #     url = settings.PTT_HOST + "/integracao/territorio/" + str(territorio.ptt_id_territorio)
            #     r = requests.put(url,
            #                       data=payload,
            #                       headers=headers)
            #     http_method = 'put'

            ptt_log = get_model("ComunicacaoPttLog")()
            ptt_log.envio = compressed_file_path
            ptt_log.resposta = response.text
            ptt_log.http_status = response.status_code
            ptt_log.http_method = http_method
            ptt_log.territorio = territorio
            ptt_log.url = url
            ptt_log.save()

            if response.status_code == 200:
                response_dict = response.json()
                print(response_dict)
                territorio.ptt_id_territorio = response_dict["idTerritorio"]
                territorio.ptt_status_territorio = response_dict["statusTerritorio"]
                territorio.status_id = get_model("TerritorioStatus").OK_PTT
                territorio.save()
            return response.status_code

    @staticmethod
    def serialize_territorio(territorio):
        """
        Serializes a Territorio to a json string
        :param territorio:main.models.Territorio
        :return: string
        """
        from main.serializers import PTTSerializer

        res = JSONRenderer().render(PTTSerializer(territorio).data)
        return res


def get_perfil(usuaria, org_usuarias=None):
    if usuaria.is_superuser:
        return "Superadmin"
    if usuaria.is_admin:
        return "Administrador/a Geral do Sistema"
    if usuaria.is_reviewer:
        return "Revisor/a"
    if usuaria.is_parceira:
        return "Parceira/o"

    if not org_usuarias:
        org_usuarias = usuaria.org_usuarias
    eh_admin_de_alguma_organizacao = False
    for org_usuaria in org_usuarias.all():
        if org_usuaria.is_admin:
            eh_admin_de_alguma_organizacao = True
            break
    if eh_admin_de_alguma_organizacao:
        return "Administrador/a de Organização"

    return "Cadastrante"


def get_choice_name(choices, field):
    options = [When(**{field: k, "then": Value(v)}) for k, v in choices]
    return Case(*options, output_field=CharField())


def datespan(start_epoch, end_epoch, delta=timedelta(days=1)):
    current_epoch = start_epoch
    span = [start_epoch]
    while current_epoch < end_epoch:
        current_epoch += int(delta.total_seconds())
        span.append(current_epoch)
    return span


def get_territorios_visiveis_organizacao(organizacao, municipios=None, estados=None):
    if not municipios:
        municipios = organizacao.municipios
    if not estados:
        estados = organizacao.estados

    return get_model("Territorio").objects.filter(
        Q(organizacao=organizacao)
        | Q(municipio_referencia__in=municipios.all())
        | Q(municipio_referencia__estado__in=estados.all())
    )


def conta_territorios_organizacao(organizacao, municipios=None, estados=None):
    return get_territorios_visiveis_organizacao(
        organizacao, municipios, estados
    ).count()


def get_territorios_visiveis_revisora(usuaria, organizacao_id=None):
    if not usuaria.is_reviewer:
        return get_model("Territorio").objects.none()

    organizacoes = (
        usuaria.organizacoes.all()
        if not organizacao_id
        else get_model("Organizacao").objects.filter(pk=organizacao_id)
    )

    municipios_visiveis = get_model("Municipio").objects.filter(
        organizacoes__in=organizacoes
    )
    estados_visiveis = get_model("Estado").objects.filter(organizacoes__in=organizacoes)

    return get_model("Territorio").objects.filter(
        Q(id__in=usuaria.territorios_revisora.all())
        | Q(organizacao__in=organizacoes)
        | Q(municipio_referencia__in=municipios_visiveis.all())
        | Q(municipio_referencia__estado__in=estados_visiveis.all())
    )


def get_territorios_visiveis(usuaria):
    if usuaria.has_perm("admin_geral"):
        return get_model("Territorio").objects.all()

    if usuaria.is_reviewer:
        return get_territorios_visiveis_revisora(usuaria)

    organizacoes = usuaria.organizacoes_que_administra

    municipios_visiveis = get_model("Municipio").objects.filter(
        organizacoes__in=organizacoes
    )
    estados_visiveis = get_model("Estado").objects.filter(organizacoes__in=organizacoes)

    return get_model("Territorio").objects.filter(
        Q(organizacao__in=organizacoes)
        | Q(municipio_referencia__in=municipios_visiveis.all())
        | Q(municipio_referencia__estado__in=estados_visiveis.all())
    )


def get_choice(status_id):
    choice_name = ""
    for choice in get_model("TerritorioRevisaoStatus").CHOICES:
        if choice[0] == status_id:
            choice_name = choice[1]
            break
    return choice_name


def display_value(choices, field):
    options = [When(**{field: k, "then": Value(v)}) for k, v in choices]
    return Case(*options, output_field=CharField())


def get_mapa_status():
    TerritorioRevisaoStatus = get_model("TerritorioRevisaoStatus")
    TerritorioStatus = get_model("TerritorioStatus")
    return {
        TerritorioStatus.EM_PREENCHIMENTO: TerritorioRevisaoStatus.EM_PREENCHIMENTO_TONOMAPA,
        TerritorioStatus.ENVIADO_PENDENTE: TerritorioRevisaoStatus.A_REVISAR,
        TerritorioStatus.OK_TONOMAPA: TerritorioRevisaoStatus.APROVADO,
        TerritorioStatus.REVISADO_COM_PENDENCIAS: TerritorioRevisaoStatus.REVISADO_COM_PENDENCIAS,
        TerritorioStatus.REJEITADO_TONOMAPA: TerritorioRevisaoStatus.REJEITADO,
        TerritorioStatus.OK_PTT: TerritorioRevisaoStatus.ENVIADO_PTT,
        TerritorioStatus.EM_PREENCHIMENTO_PTT: TerritorioRevisaoStatus.APROVADO,
        TerritorioStatus.ENVIO_PTT_NAFILA: TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA,
        TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA: TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA_ARQUIVOS,
    }


def get_mapa_status_rev():
    territorio_revisao_status = get_model("TerritorioRevisaoStatus")
    territorio_status = get_model("TerritorioStatus")
    return {
        territorio_revisao_status.EM_PREENCHIMENTO_TONOMAPA: territorio_status.EM_PREENCHIMENTO,
        territorio_revisao_status.A_REVISAR: territorio_status.ENVIADO_PENDENTE,
        territorio_revisao_status.REVISANDO: territorio_status.ENVIADO_PENDENTE,
        territorio_revisao_status.COM_DUVIDAS: territorio_status.ENVIADO_PENDENTE,
        territorio_revisao_status.APROVADO: territorio_status.OK_TONOMAPA,
        territorio_revisao_status.REVISADO_COM_PENDENCIAS: territorio_status.REVISADO_COM_PENDENCIAS,
        territorio_revisao_status.REJEITADO: territorio_status.REJEITADO_TONOMAPA,
        territorio_revisao_status.ENVIADO_PTT: territorio_status.OK_PTT,
        territorio_revisao_status.ENVIADO_PTT_NAFILA: territorio_status.ENVIO_PTT_NAFILA,
        territorio_revisao_status.ENVIADO_PTT_NAFILA_ARQUIVOS: territorio_status.ENVIO_ARQUIVOS_PTT_NAFILA,
    }


def inserir_territorio(territorio_input, context):
    with transaction.atomic():
        territorio_data = territorio_input.get_local_data()

        territorio, territorio_created = get_model(
            "Territorio"
        ).objects.update_or_create(pk=territorio_input.id, defaults=territorio_data)

        # Assign Bioma to territorio:
        if territorio.poligono:
            polygon_error = False
            biomas_filtered = []
            poligono = territorio.poligono
            if not poligono.valid:
                poligono = territorio.poligono.convex_hull
            try:
                qs = list(Bioma.objects.filter(geom__intersects=poligono).all())
            except:
                polygon_error = True
            if not polygon_error:
                for bioma in qs:
                    try:
                        polygon_in_bioma = poligono.intersection(bioma.geom)
                    except:
                        polygon_error = True
                        break

                    biomas_filtered.append(
                        {
                            "bioma": bioma,
                            "area": polygon_in_bioma.area,
                        }
                    )
                if not polygon_error:
                    if len(biomas_filtered) == 1:
                        territorio.bioma = biomas_filtered[0]["bioma"]
                        territorio.save()
                    else:
                        bioma_chosen = {
                            "bioma": None,
                            "area": 0,
                        }
                        for item in biomas_filtered:
                            if bioma_chosen["area"] < item["area"]:
                                bioma_chosen = {
                                    "bioma": item["bioma"],
                                    "area": item["area"],
                                }
                        if bioma_chosen["area"] > 0:
                            territorio.bioma = bioma_chosen["bioma"]
                            territorio.save()

        if territorio_input.conflitos is not None:
            novos_conflitos_id = []
            for conflito_input in territorio_input.conflitos:
                conflito_data = dict(conflito_input.__dict__)
                conflito_data["territorio_id"] = territorio.id
                conflito, _ = get_model("Conflito").objects.update_or_create(
                    pk=conflito_input.id, defaults=conflito_data
                )
                novos_conflitos_id.append(conflito.id)
            get_model("Conflito").objects.filter(territorio_id=territorio.id).exclude(
                id__in=novos_conflitos_id
            ).delete()

        if territorio_input.areas_de_uso is not None:
            novas_areas_de_uso_id = []
            for area_de_uso_input in territorio_input.areas_de_uso:
                area_de_uso_data = dict(area_de_uso_input.__dict__)
                area_de_uso_data["territorio_id"] = territorio.id
                area_de_uso, _ = get_model("AreaDeUso").objects.update_or_create(
                    pk=area_de_uso_input.id, defaults=area_de_uso_data
                )
                novas_areas_de_uso_id.append(area_de_uso.id)
            get_model("AreaDeUso").objects.filter(territorio_id=territorio.id).exclude(
                id__in=novas_areas_de_uso_id
            ).delete()

        if territorio_input.anexos is not None:
            novos_anexos_id = []
            for anexo_input in territorio_input.anexos:
                anexo_data = dict(anexo_input.__dict__)
                if anexo_data["id"] is not None:
                    try:
                        anexo = get_model("Anexo").objects.get(pk=anexo_input.id)
                    except get_model("Anexo").DoesNotExist:
                        continue
                    if int(anexo.territorio_id) != int(territorio.id):
                        continue
                    anexo.nome = anexo_input.nome
                    anexo.descricao = anexo_input.descricao
                    anexo.save()
                    novos_anexos_id.append(anexo.id)
            get_model("Anexo").objects.filter(territorio_id=territorio.id).exclude(
                id__in=novos_anexos_id
            ).delete()

        if territorio_input.mensagens is not None:
            for mensagem_input in territorio_input.mensagens:
                if mensagem_input.id is None:
                    mensagem = get_model("Mensagem")(
                        remetente=context.user,
                        territorio=territorio,
                        texto=mensagem_input.texto,
                        extra=None,
                        data_envio=timezone.now(),
                        origin_is_device=True,
                    )
                    mensagem.save()
                    break

        if territorio_input.tipos_comunidade is not None:
            tipos_comunidade = []
            for tipo_comunidade_input in territorio_input.tipos_comunidade:
                tipo_comunidade_data = dict(tipo_comunidade_input.__dict__)
                try:
                    tipo_comunidade = get_model("TipoComunidade").objects.get(
                        pk=tipo_comunidade_data["id"]
                    )
                except get_model("TipoComunidade").DoesNotExist:
                    continue
                tipos_comunidade.append(tipo_comunidade)
            territorio.tipos_comunidade.clear()
            territorio.tipos_comunidade.add(*tipos_comunidade)
            territorio.save()

        if territorio_input.tipo_comunidade_id is not None:
            try:
                tipo_comunidade = get_model("TipoComunidade").objects.get(
                    pk=territorio_input.tipo_comunidade_id
                )
                territorio.tipos_comunidade.clear()
                territorio.tipos_comunidade.add(tipo_comunidade)
                territorio.save()
            except get_model("TipoComunidade").DoesNotExist:
                pass

        if territorio_input.segmento_id is not None:
            try:
                segmento = get_model("TipoComunidadePTT").objects.get(
                    pk=territorio_input.segmento_id
                )
                territorio.segmento = segmento
                territorio.save()
            except get_model("TipoComunidadePTT").DoesNotExist:
                pass

        return [territorio, territorio_created]


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))
        return True
    except ValueError:
        return False
