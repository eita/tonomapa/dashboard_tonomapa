from django.core.management.base import BaseCommand
from django.contrib.gis.geos import MultiPolygon

# pylint: disable=import-error
from main import models


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("### REMOÇÃO DE POLÍGONOS DUPLICADOS DAS COMUNIDADES ###")
        territorios = models.Territorio.objects.all()

        number_total = 0
        for territorio in territorios:
            poligonos = territorio.poligono
            qtde = 0
            poligonos_limpos = []
            if poligonos is None or len(poligonos) <= 1:
                continue
            for poligono in poligonos:
                is_duplicated = False
                for poligono_j in poligonos_limpos:
                    if poligono.equals(poligono_j):
                        is_duplicated = True
                        qtde += 1
                if not is_duplicated:
                    poligonos_limpos.append(poligono)

            if qtde > 0:
                territorio.poligono = MultiPolygon(poligonos_limpos)
                territorio.save()
                self.stdout.write(
                    self.style.ERROR(f"{territorio.nome} ({territorio.id}): {qtde} duplicado(s)...")
                    + self.style.SUCCESS(" Removida(s)!")
                )
                number_total += 1

        if number_total > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"Sucesso: {number_total} comunidades tinham polígonos duplicados que foram removidos!"
                )
            )
        else:
            self.stdout.write(self.style.SUCCESS("Parabéns: não havia nenhuma comunidade com polígonos repetidos!"))
