import os
from django.core.management.base import BaseCommand
from django.contrib.gis.utils import LayerMapping
from main.models import Territorio, Bioma


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("### IMPORTAR BIOMAS ###")

        # Each key in the biomas_mapping dictionary corresponds to
        # a field in the Bioma model.
        biomas_mapping = {
            "bioma": "Bioma",
            "cd_bioma": "CD_Bioma",
            "geom": "MULTIPOLYGON",
        }

        biomas_shp = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                "../../static/main/biomas",
                "lm_bioma_250.shp",
            ),
        )

        lm = LayerMapping(
            Bioma,
            biomas_shp,
            biomas_mapping,
            transform=False,
            encoding="utf-8",
            unique=("cd_bioma"),
        )
        lm.save(strict=True, verbose=True)

        self.stdout.write(
            self.style.SUCCESS(
                f"Sucesso: Biomas foram importados ou atualizados ao modelo de dados!"
            )
        )
