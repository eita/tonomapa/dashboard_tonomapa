from django.core.management.base import BaseCommand

from main.models import Territorio, Bioma


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("### ASSOCIAÇÃO DE BIOMA A CADA TERRITÓRIO ###")
        territorios = Territorio.objects.all().order_by("-ultima_alteracao")

        qtde_ok = 0
        qtde_error = 0
        qtde_conflict = 0
        qtde_no_polygon = 0
        for territorio in territorios:
            self.stdout.write("")
            self.stdout.write("")
            self.stdout.write(f'Comunidade: "{territorio.nome}" (id={territorio.id})')
            if not territorio.poligono:
                self.stdout.write(f"Não tem polígono. Ignorando...")
                qtde_no_polygon += 1
                continue
            biomas_filtered = []
            poligono = territorio.poligono
            if not poligono.valid:
                poligono = territorio.poligono.convex_hull
            try:
                qs = list(Bioma.objects.filter(geom__intersects=poligono).all())
            except e:
                qtde_error += 1
                self.stdout.write(
                    self.style.ERROR(
                        f"Erro: O polígono da comunidade é inválido. Ignorando..."
                    )
                )
                print(e)
                continue

            polygon_error = False
            for bioma in qs:
                try:
                    polygon_in_bioma = poligono.intersection(bioma.geom)
                except Exception as e:
                    polygon_error = True
                    print(e)
                    break

                biomas_filtered.append(
                    {
                        "bioma": bioma,
                        "area": polygon_in_bioma.area,
                    }
                )

            if polygon_error:
                qtde_error += 1
                self.stdout.write(
                    self.style.ERROR(
                        f"Erro: O polígono da comunidade é inválido. Ignorando..."
                    )
                )
                continue

            if len(biomas_filtered) == 1:
                self.stdout.write(
                    f"...território inscrito num único bioma: {biomas_filtered[0]['bioma'].bioma}..."
                )
                territorio.bioma = biomas_filtered[0]["bioma"]
                territorio.save()
                qtde_ok += 1
                self.stdout.write(f"Salvo com sucesso!")
                continue

            bioma_chosen = {
                "bioma": None,
                "area": 0,
            }
            self.stdout.write(
                f"...território inscrito em {len(biomas_filtered)} biomas..."
            )
            for item in biomas_filtered:
                self.stdout.write(
                    f"- {item['bioma'].bioma}: área de {item['area']} graus;"
                )
                if bioma_chosen["area"] < item["area"]:
                    bioma_chosen = {
                        "bioma": item["bioma"],
                        "area": item["area"],
                    }

            if bioma_chosen["area"] == 0:
                qtde_error += 1
                self.stdout.write(
                    self.style.ERROR(
                        f"Erro: O polígono da comunidade tem área nula. Ignorando..."
                    )
                )
                continue

            self.stdout.write(
                f"Bioma escolhido para a comunidade: {bioma_chosen['bioma'].bioma}..."
            )
            territorio.bioma = bioma_chosen["bioma"]
            territorio.save()
            qtde_conflict += 1
            qtde_ok += 1
            self.stdout.write(f"Salvo com sucesso!")

        self.stdout.write(f"")
        self.stdout.write(f"")
        self.stdout.write(f"##################")
        self.stdout.write(f"Operação finalizada!")
        self.stdout.write(
            self.style.SUCCESS(
                f"{qtde_ok} comunidades foram associadas ao bioma, das quais {qtde_conflict} estavam contidos em mais de um bioma e o que tinha maior área da comunidade foi escolhido."
            )
        )
        self.stdout.write(
            f"{qtde_no_polygon} comunidades não tinham polígono, e portanto foram ignoradas"
        )
        self.stdout.write(
            self.style.ERROR(
                f"{qtde_error} comunidades não puderam ser associadas ao bioma por erro no respectivo polígono, portanto foram ignoradas."
            )
        )
