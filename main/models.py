import json
import os
import re
import uuid
from datetime import datetime
from random import randint

import magic
from dirtyfields import DirtyFieldsMixin
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

# from typing import Optional
from django.core.exceptions import ValidationError
from django.db import IntegrityError, transaction
from django.db.models import JSONField, Q
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.functional import cached_property
from django_archive_mixin.mixins import ArchiveMixin  # type: ignore
from exponent_server_sdk import (
    DeviceNotRegisteredError,
    PushClient,
    PushMessage,
    PushServerError,
    PushTicketError,
)
from collections import defaultdict
from imagekit import ImageSpec  # type: ignore
from notifications.signals import notify
from pilkit.processors import ResizeToFill, Transpose  # type: ignore
from requests.exceptions import ConnectionError, HTTPError

from main.utils import (
    conta_territorios_organizacao,
    get_choice,
    get_mapa_status,
    get_mapa_status_rev,
    get_territorios_visiveis_organizacao,
)


class BaseModel(ArchiveMixin, models.Model):
    class Meta:
        abstract = True


class Device(models.Model):
    usuaria = models.ForeignKey("Usuaria", models.CASCADE, related_name="devices")
    push_token = models.CharField(max_length=120)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ("-ultima_alteracao",)


class Usuaria(AbstractUser):
    telefone = models.CharField(max_length=15, blank=True, null=True)
    codigo_uso_celular = models.CharField(max_length=8, blank=True, null=True)
    is_admin = models.BooleanField(blank=True, null=True)
    is_reviewer = models.BooleanField(blank=True, null=True)
    is_parceira = models.BooleanField(blank=True, null=True)
    aprovado = models.BooleanField(blank=False, null=False, default=False)
    cpf = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    solicitou_codigo = models.BooleanField(blank=True, null=True)
    foto_documento = models.ImageField(
        null=True, blank=True, upload_to="anexos/%Y/%m/%d/"
    )
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    current_territorio = models.ForeignKey(
        "Territorio", models.SET_NULL, blank=True, null=True
    )

    __original_codigo_uso_celular = None
    organizacoes_que_administra_cached = None
    organizacoes_que_administra_ids_cached = None
    usuarias_de_organizacoes_que_administra_cached = None

    def __init__(self, *args, **kwargs):
        super(Usuaria, self).__init__(*args, **kwargs)
        self.__original_codigo_uso_celular = self.codigo_uso_celular

    territorios = models.ManyToManyField(
        "Territorio",
        through="TerritorioUsuaria",
        related_name="usuarias",
        blank=True,
    )

    municipios = models.ManyToManyField(
        "Municipio",
        through="MunicipioUsuaria",
        related_name="usuarias",
        blank=True,
    )

    @property
    def territorios_revisora_stats(self):
        stats = {}
        for territorio in self.territorios_revisora.all():
            status = territorio.status_revisao.first().status
            if status not in stats:
                stats[status] = {"label": get_choice(status), "count": 0}
            stats[status]["count"] += 1
        return stats

    @property
    def territorios_para_revisar(self):
        return self.territorios_revisora.filter(
            status_revisao__status=TerritorioRevisaoStatus.A_REVISAR
        )

    @property
    def territorios_para_revisar_novos(self):
        return self.territorios_revisora.filter(
            status_revisao__status=TerritorioRevisaoStatus.A_REVISAR, eh_reenvio=False
        )

    @property
    def territorios_para_revisar_reenviados(self):
        return self.territorios_revisora.filter(
            status_revisao__status=TerritorioRevisaoStatus.A_REVISAR, eh_reenvio=True
        )

    @property
    def territorios_que_aprovou(self):
        return self.territorios_revisora.filter(
            status_revisao__status=TerritorioRevisaoStatus.APROVADO
        )

    @property
    def territorios_que_rejeitou(self):
        return self.territorios_revisora.filter(
            status_revisao__status=TerritorioRevisaoStatus.REJEITADO
        )

    @property
    def territorios_em_analise(self):
        return self.territorios_revisora.filter(
            status_revisao__status__in=[
                TerritorioRevisaoStatus.REVISANDO,
                TerritorioRevisaoStatus.COM_DUVIDAS,
                TerritorioRevisaoStatus.REVISADO_COM_PENDENCIAS,
            ]
        )

    @property
    def territorios_processados(self):
        return self.territorios_revisora.filter(
            status_revisao__status__in=[
                TerritorioRevisaoStatus.APROVADO,
                TerritorioRevisaoStatus.REJEITADO,
                TerritorioRevisaoStatus.ENVIADO_PTT,
                TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA,
                TerritorioRevisaoStatus.ENVIADO_PTT_NAFILA_ARQUIVOS,
            ]
        )

    @property
    def mensagens_territorios_em_analise(self):
        territorios = self.territorios_em_analise
        return Mensagem.objects.filter(territorio__in=territorios)

    @property
    def is_cadastrante(self):
        return not self.has_perm("permissao_padrao")

    def eh_cadastrante_do_territorio(self, territorio):
        if not self.is_cadastrante:
            return False
        try:
            self.territorios.get(pk=territorio.id)
            return True
        except:
            return False

    @property
    def organizacoes_que_administra(self):
        if self.organizacoes_que_administra_cached:
            return self.organizacoes_que_administra_cached

        if self.is_reviewer:
            self.organizacoes_que_administra_cached = Organizacao.objects.none()
            return self.organizacoes_que_administra_cached

        self.organizacoes_que_administra_cached = Organizacao.objects.all()

        if not self.has_perm("admin_geral"):
            organizacoes_que_administra_ids = self.org_usuarias.filter(
                is_admin=True
            ).values_list("organizacao_id")
            self.organizacoes_que_administra_cached = (
                self.organizacoes_que_administra_cached.filter(
                    id__in=organizacoes_que_administra_ids
                )
            )

        return self.organizacoes_que_administra_cached

    @property
    def organizacoes_que_administra_ids(self):
        if self.organizacoes_que_administra_ids_cached:
            return self.organizacoes_que_administra_ids_cached

        self.organizacoes_que_administra_ids_cached = (
            self.organizacoes_que_administra.values_list("id")
        )
        return self.organizacoes_que_administra_ids_cached

    @property
    def usuarias_de_organizacoes_que_administra(self):
        if self.usuarias_de_organizacoes_que_administra_cached:
            return self.usuarias_de_organizacoes_que_administra_cached

        usuarias = Usuaria.objects

        if self.is_reviewer:
            return usuarias.none()

        if self.has_perm("admin_geral"):
            self.usuarias_de_organizacoes_que_administra_cached = usuarias.all()
            return self.usuarias_de_organizacoes_que_administra_cached

        # Warning: the field for distinct must be the same os the field of "order_by"! In this case: 'first_name'
        self.usuarias_de_organizacoes_que_administra_cached = usuarias.filter(
            organizacoes__in=self.organizacoes_que_administra
        ).distinct("first_name")
        return self.usuarias_de_organizacoes_que_administra_cached

    @property
    def revisoras_de_organizacoes_que_administra(self):
        return self.usuarias_de_organizacoes_que_administra.filter(is_reviewer=True)

    @property
    def territorios_de_organizacoes_que_administra(self):
        return Territorio.objects.filter(
            organizacao_id__in=self.organizacoes_que_administra_ids
        )

    def get_current_territorio(self, territorio_id=None):
        if not territorio_id and self.current_territorio:
            return self.current_territorio

        if territorio_id:
            if (
                not self.current_territorio
                or self.current_territorio.id != territorio_id
            ):
                try:
                    self.current_territorio = self.territorios.get(pk=territorio_id)
                except Territorio.DoesNotExist:
                    self.current_territorio = None
                self.save()
            return self.current_territorio

        current_territorio = self.territorios.order_by("-ultima_alteracao").first()
        if not current_territorio:
            current_territorio = Territorio.objects.create()

        self.current_territorio = current_territorio
        self.save()
        return current_territorio

    def __str__(self):
        return self.get_full_name() if self.get_full_name() != "" else self.username

    def get_displayname(self):
        return self.first_name if self.first_name != "" else self.username

    def get_absolute_url(self):
        return reverse("usuaria_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("usuaria_update", args=(self.pk,))

    def set_full_name(self, full_name):
        full_name_splitted = re.sub(" +", " ", full_name).strip().split(" ", 1)
        if len(full_name_splitted) == 1:
            self.first_name = full_name_splitted[0]
            self.last_name = ""
        else:
            self.first_name, self.last_name = full_name_splitted

    @classmethod
    def gera_senha_aleatoria(cls):
        """
        :return: uma string com 7 dígitos
        """
        # pylint: disable=consider-using-f-string
        return "{input:07d}".format(input=randint(0, 9999999))

    # guarda um objeto Device associado a esta usuária com o token fornecido.
    def create_or_update_device(self, device_expo_id):
        if not self.id:
            raise IntegrityError("Usuaria is not saved yet")

        if device_expo_id is None:  # App não atualizado
            return

        changed = False
        try:
            device = Device.objects.get(push_token=device_expo_id)
        except Device.DoesNotExist:
            device = Device()
            device.push_token = device_expo_id
            changed = True

        if device.usuaria_id != self.id:
            device.usuaria_id = self.id
            changed = True

        if changed:
            device.active = True
            device.save()

    # def delete(self, *args, **kwargs):
    #     for territorio in self.territorios.all():
    #         # Remove all territorios that were created by this user and where this user
    #         # is the only creator
    #         if self.eh_cadastrante_do_territorio(territorio):
    #             if territorio.cadastrantes.count() == 1:
    #                 territorio.delete()

    #     super(Usuaria, self).delete(*args, **kwargs)

    def save(self, *args, force_insert=False, force_update=False, **kwargs):
        if self.codigo_uso_celular != self.__original_codigo_uso_celular:
            self.set_password(self.codigo_uso_celular)
        super(Usuaria, self).save(*args, force_insert, force_update, **kwargs)
        self.__original_codigo_uso_celular = self.codigo_uso_celular

    def ptt_validate(self):
        """Valida se há informações suficientes para o envio para o PTT."""
        missing_fields = []
        if not self.email:
            missing_fields.append("e-mail")
        if not self.cpf:
            missing_fields.append("cpf/cnpj")
        if not self.get_full_name():
            missing_fields.append("nome")

        if missing_fields:
            missing_fields_joined = ", ".join(missing_fields)
            return f"PTT: erro: cadastrante {self.username} não possui {missing_fields_joined}."

        return None

    class Meta:
        db_table = "usuaria"
        ordering = ("first_name", "last_name")


class Estado(models.Model):
    uf = models.CharField(max_length=2, blank=True, null=True)
    nome = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome


class Municipio(models.Model):
    estado = models.ForeignKey(Estado, models.CASCADE, related_name="municipios")
    nome = models.CharField(max_length=150, blank=True, null=True)
    latitude = models.DecimalField(
        blank=True, null=True, max_digits=7, decimal_places=5
    )
    longitude = models.DecimalField(
        blank=True, null=True, max_digits=8, decimal_places=5
    )
    capital = models.BooleanField(blank=True, null=True)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return f"{self.nome} / {self.estado.uf}"


class MunicipioUsuaria(models.Model):
    municipio = models.ForeignKey(
        Municipio, models.CASCADE, related_name="municipio_usuarias"
    )
    usuaria = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    def __str__(self):
        return str(self.municipio) + " <-> " + str(self.usuaria)


class TipoComunidade(BaseModel):
    nome = models.CharField(max_length=60, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome


class TerritorioStatus(BaseModel):
    # status exclusivo APP
    NADA_PREENCHIDO = 1
    EM_PREENCHIMENTO = 2
    PREENCHIDO = 6
    TELEFONE_NAO_REGISTRADO = 7

    ENVIADO_PENDENTE = 3  # Dashboard received tonomapa data and files from App
    OK_TONOMAPA = 4  # Tonomapa team approved the data sent from App

    REVISADO_COM_PENDENCIAS = 8  # Tonomapa team revised and need adjustments
    REJEITADO_TONOMAPA = 9  # Tonomapa team rejected the data sent from App

    # PTT
    EM_PREENCHIMENTO_PTT = 10  # App only: user is editing data in app
    ENVIO_PTT_NAFILA = (
        11  # Dashboard has recieved data from App, now must send data to PTT
    )
    ENVIO_ARQUIVOS_PTT_NAFILA = 12  # PTT data was sent, now must send files to PTT
    OK_PTT = 5  # PTT data and files were successfully sent to PTT. Finish.

    STATUS_APROVADOS = [
        OK_TONOMAPA,
        OK_PTT,
        ENVIO_ARQUIVOS_PTT_NAFILA,
        ENVIO_PTT_NAFILA,
        EM_PREENCHIMENTO_PTT,
    ]

    nome = models.CharField(max_length=255, blank=True, null=True)
    mensagem = models.TextField(blank=True, null=True)
    url_referencia = models.CharField(max_length=255, blank=True, null=True)
    mensagem_report = models.TextField(blank=True, null=True)
    has_fale_conosco_btn = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return str(self.id) + " - " + self.nome

    class Meta:
        ordering = ("id",)


class Territorio(DirtyFieldsMixin, BaseModel):
    DATATABLE_COLUMNS = [
        {
            "field": "id",
            "title": "id",
            "class": "colStartHidden not-colvis",
            "searchable": False,
            "orderable": False,
        },
        {
            "field": "relevancia",
            "title": "Relevância",
            "class": "",
            "searchable": False,
            "orderable": True,
        },
        {
            "field": "nome",
            "title": "Comunidade",
            "class": "cell_territorio",
            "searchable": True,
            "orderable": True,
        },
        {
            "field": "revisora",
            "title": "Revisor/a",
            "class": "",
            "searchable": [
                "revisora__first_name",
                "revisora__last_name",
            ],
            "search_pane": True,
            "orderable": True,
        },
        {
            "field": "cadastrante",
            "title": "Cadastrante",
            "class": "colHasSearchPane",
            "searchable": [
                "usuarias__first_name",
                "usuarias__last_name",
            ],
            "search_pane": True,
            "orderable": True,
        },
        {
            "field": "organizacao",
            "title": "Organização",
            "class": "colHasSearchPane organizacao",
            "searchable": [
                "organizacao__nome",
            ],
            "search_pane": True,
            "orderable": True,
        },
        {
            "field": "data_validacao",
            "title": "Validação",
            "class": "",
            "searchable": True,
            "orderable": True,
            "type": "date",
        },
        {
            "field": "ultima_alteracao",
            "title": "Alteração",
            "class": "colStartHidden",
            "searchable": True,
            "orderable": True,
            "type": "date",
        },
        {
            "field": "bioma",
            "title": "Bioma",
            "class": "",
            "searchable": [
                "bioma__bioma",
            ],
            "orderable": True,
        },
        {
            "field": "municipio_referencia__estado__nome",
            "title": "Estado",
            "class": "colHasSearchPane",
            "searchable": True,
            "search_pane": True,
            "orderable": True,
        },
        {
            "field": "municipio_referencia",
            "title": "Município",
            "class": "",
            "searchable": [
                "municipio_referencia__nome",
            ],
            "orderable": True,
        },
        {
            "field": "tipos_comunidade",
            "title": "Tipos",
            "class": "colHasSearchPaneTipos",
            "searchable_icontains": [
                "tipos_comunidade__nome",
            ],
            "searchable": False,
            "search_pane": True,
            "orderable": False,
            "many": "tipos_comunidade__nome",
        },
        {
            "field": "ano_fundacao",
            "title": "Fundação",
            "class": "colStartHidden",
            "searchable": True,
            "orderable": True,
        },
        {
            "field": "qtde_familias",
            "title": "Famílias",
            "class": "colStartHidden",
            "searchable": True,
            "orderable": True,
        },
        {
            "field": "areas_de_uso",
            "title": "Usos",
            "class": "colStartHidden",
            "orderable": False,
            # "searchable": [
            #     "areas_de_uso__nome",
            # ],
            "many": "areas_de_uso__nome",
        },
        {
            "field": "conflitos",
            "title": "Conflitos",
            "class": "colStartHidden",
            "orderable": False,
            # "searchable": [
            #     "conflitos__nome",
            # ],
            "many": "conflitos__nome",
        },
    ]
    TIPOS_ZONA_LOCALIZACAO = ["RURAL", "URBANA", "MISTA", "OUTRO"]
    PTT_FIELDS = [
        "cep",
        "descricao_acesso",
        "zona_localizacao",
        "outra_zona_localizacao",
        "auto_identificacao",
        "segmento",
        "historia_descricao",
        "ec_nome_contato",
        "ec_logradouro",
        "ec_numero",
        "ec_complemento",
        "ec_bairro",
        "ec_cep",
        "ec_caixa_postal",
        "ec_municipio",
        "ec_email",
        "ec_telefone",
    ]
    PTT_FIELDS_BOOLEAN = ["descricao_acesso_privacidade"]
    SAC_OPTIONS = {
        "ABERTO": 1,
        "ESPERANDO_RESPOSTA": 2,
        "FECHADO": 3,
        "RESOLVIDO": 4,
    }

    CHOICES_SAC = (
        (SAC_OPTIONS["ABERTO"], "Aberto"),
        (SAC_OPTIONS["ESPERANDO_RESPOSTA"], "Esperando resposta"),
        (SAC_OPTIONS["FECHADO"], "Fechado"),
        (SAC_OPTIONS["RESOLVIDO"], "Resolvido"),
    )

    nome = models.CharField(max_length=255, blank=True, null=True)
    poligono = models.MultiPolygonField(blank=True, null=True)
    bioma = models.ForeignKey(
        "Bioma", models.SET_NULL, related_name="territorios", blank=True, null=True
    )
    ano_fundacao = models.IntegerField(blank=True, null=True)
    qtde_familias = models.IntegerField(blank=True, null=True)
    municipio_referencia = models.ForeignKey(
        Municipio, models.PROTECT, related_name="territorios", blank=True, null=True
    )
    publico = models.BooleanField(default=False, blank=True, null=True)
    tipos_comunidade = models.ManyToManyField(
        TipoComunidade, related_name="territorios"
    )
    outro_tipo_comunidade = models.CharField(max_length=255, blank=True, null=True)

    # -----------------------------------------#
    # Campos de uso exclusivamente interno:
    # -----------------------------------------#
    status = models.ForeignKey(TerritorioStatus, models.SET_NULL, blank=True, null=True)
    eh_reenvio = models.BooleanField(default=False, blank=True, null=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    data_validacao = models.DateTimeField(blank=True, null=True)
    teste = models.BooleanField(default=False, blank=True, null=True)
    enviado_ptt = models.BooleanField(default=False, blank=True, null=True)
    contato_referencia_nome = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name="Contato de referência - Nome",
    )
    contato_referencia_telefone = models.CharField(
        max_length=15,
        blank=True,
        null=True,
        verbose_name="Contato de referência - Telefone",
    )
    observacoes = models.TextField(
        blank=True, null=True, verbose_name="Observações internas"
    )
    sac_status = models.IntegerField(choices=CHOICES_SAC, blank=True, null=True)
    sac_assigned_to = models.ForeignKey(
        Usuaria, models.SET_NULL, blank=True, null=True, related_name="territorios_sac"
    )

    # -----------------------------------------#
    # Campos específicos da PTT:
    # -----------------------------------------#
    cep = models.CharField(max_length=255, blank=True, null=True)
    descricao_acesso = models.TextField(blank=True, null=True)
    descricao_acesso_privacidade = models.BooleanField(default=False)
    zona_localizacao = models.CharField(max_length=60, blank=True, null=True)
    outra_zona_localizacao = models.TextField(blank=True, null=True)
    auto_identificacao = models.CharField(max_length=255, blank=True, null=True)
    segmento = models.ForeignKey(
        "TipoComunidadePTT",
        models.CASCADE,
        related_name="territorios",
        blank=True,
        null=True,
    )
    historia_descricao = models.TextField(blank=True, null=True)
    ptt_id_territorio = models.IntegerField(blank=True, null=True)
    ptt_status_territorio = models.CharField(max_length=60, blank=True, null=True)
    # Endereço de contato (também PTT)
    ec_nome_contato = models.CharField(max_length=255, blank=True, null=True)
    ec_logradouro = models.CharField(max_length=255, blank=True, null=True)
    ec_numero = models.CharField(max_length=60, blank=True, null=True)
    ec_complemento = models.CharField(max_length=255, blank=True, null=True)
    ec_bairro = models.CharField(max_length=255, blank=True, null=True)
    ec_cep = models.CharField(max_length=9, blank=True, null=True)
    ec_caixa_postal = models.CharField(max_length=255, blank=True, null=True)
    ec_municipio = models.ForeignKey(Municipio, models.SET_NULL, blank=True, null=True)
    ec_email = models.CharField(max_length=255, blank=True, null=True)
    ec_telefone = models.CharField(max_length=20, blank=True, null=True)

    # Específicos do Dashboard:
    organizacao = models.ForeignKey(
        "Organizacao",
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="territorios",
        verbose_name="Organização",
    )
    revisora = models.ForeignKey(
        Usuaria,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="territorios_revisora",
    )

    def __str__(self):
        return self.nome if self.nome else ""

    def get_messages_url(self):
        return reverse("territorio_mensagens", args=(self.pk,))

    def get_absolute_url(self):
        return reverse("territorio_detail", args=(self.pk,))

    def get_status_revisao(self):
        return self.status_revisao.first()

    def get_anexo_icone_ptt(self):
        icones = self.anexos.filter(tipo_anexo="ICONE")
        if len(icones) > 0:
            return icones[0]
        return None

    def get_anexos_informacoes_ptt(self):
        return self.anexos.filter(tipo_anexo="FONTE_INFORMACAO")

    def get_anexos_midias_ptt(self):
        return self.anexos.filter(tipo_anexo="ANEXO")

    def get_anexos_ptt(self):
        if self.get_anexo_icone_ptt() is not None:
            icone = [self.get_anexo_icone_ptt()]
        else:
            icone = []
        return (
            list(self.get_anexos_informacoes_ptt())
            + list(self.get_anexos_midias_ptt())
            + icone
        )

    # def map_to_PTT(self, field):
    #     if field in MAP_TO_PTT:
    #         return MAP_TO_PTT[field]
    #     return field

    def historia(self):
        return self

    def endereco_contato(self):
        return self

    @property
    def cadastrantes(self):
        cadastrantes = self.usuarias.filter(
            (Q(is_admin__isnull=True) | Q(is_admin=False)),
            (Q(is_reviewer__isnull=True) | Q(is_reviewer=False)),
            (Q(is_parceira__isnull=True) | Q(is_parceira=False)),
        ).distinct()
        if cadastrantes.count() == 0:
            cadastrantes = [self.revisora]
        return cadastrantes

    @property
    def main_cadastrante(self):
        cadastrantes = self.cadastrantes
        if len(cadastrantes) > 0:
            return self.cadastrantes[0]
        return None

    @property
    def geo_poligonos(self):
        try:
            poligonos = []
            # pylint: disable=not-an-iterable
            for poligono in self.poligono:
                coords = []
                for coord in poligono.coords[0]:
                    coords.append([coord[1], coord[0]])
                poligonos.append(coords)
            return poligonos
        except:
            return []

    @property
    def geo_posicao_inicial(self):
        if self.poligono:
            try:
                position_initial = [
                    self.poligono.point_on_surface.tuple[1],
                    self.poligono.point_on_surface.tuple[0],
                ]
            except:
                if self.areas_de_uso.count() > 0:
                    position_initial = [
                        self.areas_de_uso.first().posicao.tuple[1],
                        self.areas_de_uso.first().posicao.tuple[0],
                    ]
                elif self.conflitos.count() > 0:
                    position_initial = [
                        self.conflitos.first().posicao.tuple[1],
                        self.conflitos.first().posicao.tuple[0],
                    ]
                else:
                    position_initial = []
        elif self.areas_de_uso.count() > 0:
            position_initial = [
                self.areas_de_uso.first().posicao.tuple[1],
                self.areas_de_uso.first().posicao.tuple[0],
            ]
        elif self.conflitos.count() > 0:
            position_initial = [
                self.conflitos.first().posicao.tuple[1],
                self.conflitos.first().posicao.tuple[0],
            ]
        elif self.municipio_referencia and self.municipio_referencia.latitude:
            position_initial = [
                float(self.municipio_referencia.latitude),
                float(self.municipio_referencia.longitude),
            ]
        else:
            position_initial = []

        return position_initial

    @property
    def conflitos_unique(self):
        return list(set([conflito.nome for conflito in self.conflitos.all()]))

    @property
    def areas_de_uso_unique(self):
        return list(set([area.nome for area in self.areas_de_uso.all()]))

    @property
    def ultima_alteracao_timestamp(self):
        return self.ultima_alteracao.timestamp()

    @property
    def criacao_timestamp(self):
        return self.criacao.timestamp()

    def send_push_message(self, message, remetente=None, extra={}):
        """
        :type message: str
        :type remetente: Usuaria
        :type extra: dict, optional
        """
        devices = Device.objects.filter(
            usuaria__in=self.usuarias.all(), active=True
        ).all()

        if len(devices) > 0:
            data_envio = timezone.now()
            extra["texto"] = message
            extra["dataEnvio"] = f"{data_envio}"
            extra["originIsDevice"] = False
            extra["territorioId"] = self.id

            # cria objeto 'Mensagem'
            mensagem = Mensagem(
                remetente=remetente,
                territorio=self,
                texto=extra["texto"],
                extra=extra,
                data_envio=data_envio,
                origin_is_device=extra["originIsDevice"],
            )
            mensagem.save()
            extra["id"] = mensagem.id
            mensagem.save()

            mensagem_device_objects = mensagem.create_mensagem_device_objects()

            extra_json = json.dumps(extra)

            # body = message if message is not None and message != "" else "Novidade na comunidade " + self.nome + "!"
            responses = []
            for device in devices:
                push_message = PushMessage(
                    to=device.push_token, body=message, data=extra_json
                )
                try:
                    response = PushClient().publish(push_message)
                    responses.append(response)
                    if response.status == "ok":
                        print(f"Successfully sent to {device.push_token}")
                    else:
                        print(
                            f"Error for token {device.push_token}: {response.message}"
                        )
                        if response.status == "error":
                            # error_details = response.details
                            if response.message == "DeviceNotRegistered":
                                # Handle unregistered device (remove from database, etc.)
                                Device.objects.filter(
                                    push_token=device.push_token
                                ).update(active=False)
                except PushServerError as exc:
                    print(f"Server error for token {device.push_token}: {exc}")
                except DeviceNotRegisteredError:
                    # Remove invalid device tokens
                    Device.objects.filter(push_token=device.push_token).update(
                        active=False
                    )
                except Exception as e:
                    # Catch any other exceptions
                    print(f"Unexpected error for token {device.push_token}: {e}")

            for response in responses:
                current_md = None
                for mensagem_device_object in mensagem_device_objects:
                    if (
                        mensagem_device_object.device.push_token
                        == response.push_message.to
                    ):
                        current_md = mensagem_device_object
                try:
                    # We got a response back, but we don't know whether it's an error yet.
                    # This call raises errors so we can handle them with normal exception
                    # flows.
                    response.validate_response()
                    current_md.status = 1
                    current_md.save()
                    print("sent!")
                except DeviceNotRegisteredError as exc:
                    print(exc)
                    # Mark the device token as inactive
                    Device.objects.filter(push_token=response.push_message.to).update(
                        active=False
                    )
                    current_md.status = 5
                    current_md.save()
                except PushTicketError as exc:
                    print(exc)
                    # Encountered some other per-notification error.
                    current_md.status = 6
                    current_md.error_data = exc.push_response._asdict()
                    current_md.save()

                    # rollbar.report_exc_info(
                    #     extra_data={
                    #         'token': token,
                    #         'message': message,
                    #         'extra': extra,
                    #         'push_response': exc.push_response._asdict(),
                    #     })
                    # raise self.retry(exc=exc)
            return mensagem

    @property
    def mensagens(self):
        """
        lista de mensagens da comunidade
        :return:
        """
        return Mensagem.objects.filter(territorio_id=self.id)

    def save(self, *args, **kwargs) -> None:
        original_fields = self.get_dirty_fields(check_relationship=True)

        # Change status revisao depending on new territorio status
        try:
            status_revisao = self.status_revisao.get()
            mapa_status_rev = get_mapa_status_rev()
            rev_status_id = mapa_status_rev[status_revisao.status]
            if self.status.id != rev_status_id:
                mapa_status = get_mapa_status()
                status_revisao.status = mapa_status[self.status.id]
                status_revisao.save()
        except:
            pass

        can_save = True
        clean_ptt_fields = False
        status = original_fields.get("status", None)
        if status:  # houve mudança no status
            if self.status_id == TerritorioStatus.ENVIO_PTT_NAFILA:
                can_save = (self.enviado_ptt is False) and (
                    status == TerritorioStatus.EM_PREENCHIMENTO_PTT
                )
            if self.status_id == TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA:
                can_save = (self.enviado_ptt is False) and (
                    status == TerritorioStatus.ENVIO_PTT_NAFILA
                )
            if self.status_id == TerritorioStatus.OK_PTT:
                can_save = (self.enviado_ptt is False) and (
                    status == TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA
                )
                clean_ptt_fields = True
            if self.status_id == TerritorioStatus.REVISADO_COM_PENDENCIAS:
                self.eh_reenvio = True

        if clean_ptt_fields:
            for ptt_field in self.PTT_FIELDS:
                setattr(self, ptt_field, None)
            for ptt_field in self.PTT_FIELDS_BOOLEAN:
                setattr(self, ptt_field, False)

            # Permanent flag that it was already sent to PTT - this only happens once
            self.enviado_ptt = True

        if can_save:
            if (
                status
                and status != self.status_id
                and self.status_id
                not in [
                    TerritorioStatus.EM_PREENCHIMENTO_PTT,
                    TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA,
                    TerritorioStatus.ENVIO_PTT_NAFILA,
                    TerritorioStatus.OK_PTT,
                ]
            ):
                self.data_validacao = (
                    timezone.now()
                    if self.status_id == TerritorioStatus.OK_TONOMAPA
                    else None
                )

            with transaction.atomic():
                super().save(*args, **kwargs)
                if clean_ptt_fields:
                    anexos_ptt = self.get_anexos_ptt()
                    for anexo_ptt in anexos_ptt:
                        anexo_ptt.delete()
            if clean_ptt_fields:
                usuarias = self.usuarias.all()
                for usuaria in usuarias:
                    self.send_push_message("", usuaria, {"statusId": self.status_id})

        else:
            message = ""
            if self.enviado_ptt:
                message += "Esta comunidade já foi enviada à PTT. "
            else:
                message += "Mudança de status inválida."

            message += f"Antigo status: {original_fields['status']} | Novo status: {self.status_id} "

            raise ValidationError({"base": message})

    def validate_ptt(self):
        self.validate_ptt_fields()
        self.validate_ptt_anexos()

    def validate_ptt_anexos(self):
        # História
        anexos_informacoes = self.get_anexos_informacoes_ptt()
        if not anexos_informacoes:
            raise ValidationError(
                "PTT: erro: precisa ter pelo menos um arquivo definido como 'FONTE_INFORMACAO'"
            )

    def validate_ptt_fields(self):
        """
        Validates if all PTT information is present
        """

        ptt_validation_errors = {}
        if not self.nome:
            ptt_validation_errors["nome"] = (
                "PTT: nome da comunidade deve ser informado."
            )
        if not self.municipio_referencia:
            ptt_validation_errors["municipio_referencia"] = (
                "PTT: município da comunidade deve ser informado."
            )
        if not self.descricao_acesso:
            ptt_validation_errors["descricao_acesso"] = (
                "PTT: uma descrição do acesso deve ser informada."
            )
        if self.descricao_acesso_privacidade is None:
            ptt_validation_errors["descricao_acesso_privacidade"] = (
                "PTT: a descrição do acesso é pública ou privada?"
            )
        if self.zona_localizacao not in self.TIPOS_ZONA_LOCALIZACAO:
            ptt_validation_errors["zona_localizacao"] = (
                "PTT: zona da localização precisa ser informada corretamente."
            )
        if self.zona_localizacao == "OUTRO" and not self.outra_zona_localizacao:
            ptt_validation_errors["outra_zona_localizacao"] = (
                "PTT: zona da localização precisa ser detalhada."
            )
        if not self.segmento:
            ptt_validation_errors["segmento"] = "PTT: segmento precisa ser detalhado."
        if not self.poligono:
            ptt_validation_errors["poligono"] = (
                "PTT: polígono/geometria precisa ser fornecida."
            )

        # Endereço
        if not self.ec_municipio:
            ptt_validation_errors["ec_municipio"] = (
                "PTT: endereço de contato precisa ter um município especificado"
            )
        if not self.ec_email:
            ptt_validation_errors["ec_email"] = (
                "PTT: endereço de contato precisa ter um e-mail especificado"
            )
        if not self.ec_telefone:
            ptt_validation_errors["ec_telefone"] = (
                "PTT: endereço de contato precisa ter um telefone especificado"
            )

        if ptt_validation_errors:
            raise ValidationError(ptt_validation_errors)

        # Cadastrante
        cadastrante = self.main_cadastrante
        if cadastrante:
            cadastrante_error_message = cadastrante.ptt_validate()
            if cadastrante_error_message:
                raise ValidationError(cadastrante_error_message)
        else:
            raise ValidationError("PTT: erro: comunidade sem cadastrante")

    def clean(self) -> None:
        super().clean()

        if self.status_id == TerritorioStatus.ENVIO_PTT_NAFILA:
            self.validate_ptt_fields()

    class Meta:
        ordering = ("-ultima_alteracao",)


class ComunicacaoPttLog(BaseModel):
    url = models.CharField(null=True, blank=True, max_length=200)
    envio = models.TextField(null=True, blank=True)
    resposta = models.TextField(null=True, blank=True)
    mensagem_adicional = models.TextField(null=True, blank=True)
    http_method = models.CharField(null=True, blank=True, max_length=30)
    http_status = models.IntegerField(null=True, blank=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="ptt_logs")

    class Meta:
        pass


class TerritorioRevisaoStatus(BaseModel):
    # não dispara
    A_REVISAR = 1
    REVISANDO = 2
    COM_DUVIDAS = 3

    # Automatic statuses:
    ENVIADO_PTT = 9
    ENVIADO_PTT_NAFILA = 10
    ENVIADO_PTT_NAFILA_ARQUIVOS = 11
    EM_PREENCHIMENTO_TONOMAPA = 12

    # dispara mensagem com as pendencias
    APROVADO = 5  # TerritorioStatus.OK_TONOMAPA
    REVISADO_COM_PENDENCIAS = 6  # TerritorioStatus.REVISADO_COM_PENDENCIAS
    REJEITADO = 7  # TerritorioStatus.REJEITADO_TONOMAPA

    STATUS_RELEVANTES = [A_REVISAR, REVISANDO, COM_DUVIDAS]
    STATUS_VALIDADOS = [
        ENVIADO_PTT,
        ENVIADO_PTT_NAFILA,
        ENVIADO_PTT_NAFILA_ARQUIVOS,
        APROVADO,
    ]
    STATUS_EM_PREENCHIMENTO = [EM_PREENCHIMENTO_TONOMAPA]

    CHOICES = (
        (EM_PREENCHIMENTO_TONOMAPA, "Em preenchimento pela(o) cadastrante(o)"),
        (A_REVISAR, "A revisar"),
        (REVISANDO, "Em revisão"),
        (COM_DUVIDAS, "Em revisão - com dúvidas"),
        (APROVADO, "Validado"),
        (REVISADO_COM_PENDENCIAS, "Revisado com pendências - aguardando retorno"),
        (REJEITADO, "Rejeitado"),
        (ENVIADO_PTT, "Enviado ao PTT"),
        (ENVIADO_PTT_NAFILA, "Em processo de envio ao PTT"),
        (ENVIADO_PTT_NAFILA_ARQUIVOS, "Em processo de envio ao PTT (anexos)"),
    )

    status = models.IntegerField(
        choices=CHOICES, default=EM_PREENCHIMENTO_TONOMAPA, blank=True, null=True
    )
    alterado_por = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.SET_NULL, blank=True, null=True
    )
    territorio = models.ForeignKey(
        Territorio, models.CASCADE, related_name="status_revisao"
    )

    class Meta:
        ordering = ("status",)


class TerritorioUsuaria(models.Model):
    territorio = models.ForeignKey(
        Territorio, models.CASCADE, related_name="terr_usuarias"
    )
    usuaria = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    is_editor = models.BooleanField(blank=True, null=True)

    class Meta:
        ordering = (
            "usuaria",
            "territorio",
        )

    def __str__(self):
        return str(self.territorio) + " <-> " + str(self.usuaria)

    def save(self, *args, **kwargs):
        if not self.territorio.terr_usuarias.count():
            mapa_status = get_mapa_status()
            TerritorioRevisaoStatus.objects.create(
                status=mapa_status[self.territorio.status.id],
                alterado_por=self.usuaria,
                territorio=self.territorio,
            )

            super().save(*args, **kwargs)

            if self.territorio.revisora:
                target_url = reverse_lazy(
                    "territorio_detail", args=(self.territorio.id,)
                )
                notification_list = reverse_lazy("notifications:all")
                texto = f"Comunidade para revisar: {self.territorio} \n"
                notify.send(
                    self.usuaria,
                    recipient=self.territorio.revisora,
                    verb=texto,
                    action_object=self.territorio,
                    target=self.territorio,
                    description="",
                    target_url=target_url,
                    notification_list=notification_list,
                )
        else:
            super().save(*args, **kwargs)


class Anexo(BaseModel):
    nome = models.CharField(max_length=250)
    descricao = models.TextField(null=True, blank=True)
    mimetype = models.CharField(max_length=80, blank=True, null=True)
    file_size = models.IntegerField(blank=True, null=True)
    arquivo = models.FileField(
        upload_to="anexos/%Y/%m/%d/", max_length=400, null=True, blank=True
    )
    tipo_anexo = models.CharField(max_length=60, blank=True, null=True)
    territorio = models.ForeignKey(
        Territorio, models.CASCADE, related_name="anexos", blank=True, null=True
    )
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    publico = models.BooleanField(blank=True, null=True, default=False)

    thumb = models.ImageField(
        null=True, blank=True, upload_to="anexos/thumbs/%Y/%m/%d/"
    )

    def __str__(self):
        return self.nome

    class Meta:
        ordering = ("nome",)

    class Thumbnail(ImageSpec):
        processors = [Transpose(), ResizeToFill(1440, 720)]
        format = "JPEG"
        options = {"quality": 60}

    def save(self, *args, **kwargs):
        redo_thumb = kwargs.pop("redo_thumb", False)
        super(Anexo, self).save(*args, **kwargs)

        if self.arquivo is None:
            if self.mimetype is not None:
                self.file_size = None
                self.mimetype = None
                self.thumb = None
                super(Anexo, self).save()
        else:
            mime = magic.from_file(self.arquivo.path, mime=True)
            size = os.path.getsize(self.arquivo.path)
            do_save = False

            if self.mimetype != mime:
                self.mimetype = mime
                do_save = True

            if self.file_size != size:
                self.file_size = size
                do_save = True

            if do_save or redo_thumb:
                # generates thumbnail when mime is image
                if mime.startswith("image") and self.arquivo.name:
                    with open(self.arquivo.path, "rb") as source_file:
                        image_generator = self.Thumbnail(source=source_file)
                        thumb_file = image_generator.generate()
                        thumb_path = os.path.basename(self.arquivo.name)
                        self.thumb.save(thumb_path, thumb_file)
                else:
                    self.thumb = None

                super(Anexo, self).save()

    def is_image(self):
        return re.match(r"image/(gif|jpeg|png|webp)$", self.mimetype)

    def is_video(self):
        return re.match(r"video/(x-matroska|mp4|webm|ogg)$", self.mimetype)

    def thumbnail_tag(self):
        tag = None
        if self.is_image():
            tag = (
                '<img style="width: 100%; display: block;" src="'
                + self.arquivo.url
                + '" alt="image">'
            )
        else:
            results = re.match(r".*\.([^.]+)$", self.arquivo.url)
            if results is not None and results.lastindex > 0:
                extension = results[results.lastindex]
                filename = f"anexos/img/{extension}.png"
                if finders.find(filename) is None:
                    filename = "anexos/img/file.png"
                tag = (
                    '<img style="height: 100%; display: block;" src="'
                    + staticfiles_storage.url(filename)
                    + '" alt="">'
                )
        return tag

    def rename_arquivo_for_ptt(self):
        if self.arquivo is None:
            return None

        initial_path = self.arquivo.path
        file_extension = os.path.splitext(initial_path)[1]
        if file_extension == ".jpg":
            file_extension = ".jpeg"
        self.arquivo.name = f"anexos_ptt/{uuid.uuid4()}{file_extension}"
        new_path = f"{settings.MEDIA_ROOT}/{self.arquivo.name}"
        try:
            os.rename(initial_path, new_path)
            self.save()
            return self.arquivo
        except:
            return False


# ÁREA DE USO
class TipoAreaDeUso(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("nome",)
        verbose_name = "Área de Uso"

    def __str__(self):
        return self.nome


class AreaDeUso(BaseModel):
    nome = models.CharField(max_length=120, blank=True, null=True)
    posicao = models.PointField(blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)
    area = models.DecimalField(max_digits=20, decimal_places=4, blank=True, null=True)
    territorio = models.ForeignKey(
        Territorio,
        models.CASCADE,
        related_name="areas_de_uso",
        verbose_name="Áreas de Uso",
    )
    tipo_area_de_uso = models.ForeignKey(
        TipoAreaDeUso, models.CASCADE, related_name="areas_de_uso"
    )
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ("nome",)
        verbose_name = "Área de Uso"

    def __str__(self):
        return self.nome


# CONFLITO
class TipoConflito(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome


class Conflito(BaseModel):
    nome = models.CharField(max_length=120, blank=True, null=True)
    posicao = models.PointField(blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="conflitos")
    tipo_conflito = models.ForeignKey(
        TipoConflito, models.CASCADE, related_name="conflitos"
    )
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome


# ORGANIZAÇÃO
class Organizacao(BaseModel):
    nome = models.CharField(max_length=250)
    logo = models.ImageField(
        upload_to="main/img/logos/", verbose_name="Logomarca", blank=True, null=True
    )
    site = models.URLField(
        max_length=200, verbose_name="Site institucional", blank=True, null=True
    )
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    usuarias = models.ManyToManyField(
        Usuaria, through="OrganizacaoUsuaria", related_name="organizacoes"
    )
    municipios = models.ManyToManyField(Municipio, related_name="organizacoes")
    estados = models.ManyToManyField(Estado, related_name="organizacoes")

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse("usuaria_list", args=(self.pk,))

    def get_update_url(self):
        return reverse("organizacao_update", args=(self.pk,))

    def num_usuarias(self):
        return self.usuarias.all().count()

    def num_revisoras(self):
        return self.usuarias.filter(is_reviewer=True).count()

    def num_administradoras(self):
        return self.org_usuarias.filter(is_admin=True).count()

    def num_territorios(self):
        num_territorios = conta_territorios_organizacao(
            self, self.municipios, self.estados
        )
        return num_territorios

    def territorios_visiveis(self):
        territorio_list = get_territorios_visiveis_organizacao(
            self, self.municipios, self.estados
        )
        return territorio_list

    class Meta:
        ordering = ("nome",)
        verbose_name = "organização"
        verbose_name_plural = "organizações"


class OrganizacaoUsuaria(models.Model):
    is_admin = models.BooleanField(
        verbose_name="É admin da organização?", default=False
    )
    organizacao = models.ForeignKey(
        Organizacao,
        models.CASCADE,
        verbose_name="organização",
        related_name="org_usuarias",
    )
    usuaria = models.ForeignKey(
        Usuaria, models.CASCADE, verbose_name="usuária/o", related_name="org_usuarias"
    )

    class Meta:
        ordering = (
            "organizacao",
            "usuaria",
        )

    def __str__(self):
        return str(self.organizacao) + " <-> " + str(self.usuaria)


# CHAT / MENSAGERIA
class Mensagem(models.Model):
    remetente = models.ForeignKey(
        Usuaria, models.CASCADE, related_name="mensagens_enviadas", null=True
    )
    texto = models.TextField(blank=True, null=True)
    extra = JSONField(default=None, blank=True, null=True)
    data_envio = models.DateTimeField(blank=True, null=True)
    data_recebimento = models.DateTimeField(blank=True, null=True)
    origin_is_device = models.BooleanField(default=False)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="mensagens")

    class Meta:
        ordering = ("data_envio",)

    def create_mensagem_device_objects(self):
        if self.id and self.territorio:
            mds = []
            devices = Device.objects.filter(
                usuaria__in=self.territorio.usuarias.all(), active=True
            ).all()
            for device in devices:
                mds.append(
                    MensagemDevice.objects.create(
                        mensagem=self, device=device, status=0
                    )
                )
            return mds
        raise IntegrityError("Mensagem precisa estar gravada e ter comunidade")

    @cached_property
    def status(self):
        status = {"erro": False, "enviada": False, "recebida": False}
        for mensagem_device in self.mds.all():
            if mensagem_device.status == 1:
                status["enviada"] = True
            elif mensagem_device.status == 2:
                status["recebida"] = True
            elif mensagem_device.status > 2:
                status["erro"] = True
        return status


class MensagemDevice(models.Model):
    TIPOS_STATUS = (
        (0, "Não enviado"),
        (1, "Enviada para o dispositivo"),
        (2, "Recebida pelo dispositivo"),
        (3, "Não enviou - Erro no push server (provavelmente formatação/validação)"),
        (4, "Não enviou - Erro de conexão ou HTTP"),
        (5, "Erro - Dispositivo de destino não encontrado"),
        (6, "Erro - Outro tipo de erro de notificação"),
    )

    mensagem = models.ForeignKey(Mensagem, models.CASCADE, related_name="mds")
    device = models.ForeignKey(Device, models.CASCADE, related_name="mds")
    status = models.IntegerField(choices=TIPOS_STATUS, default=0)
    error_data = JSONField(default=None, null=True)


# MODELS DO PTT


class TipoComunidadePTT(BaseModel):
    nome = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        ordering = ("nome",)

    def __str__(self):
        return self.nome


# BIOMA
class Bioma(models.Model):
    bioma = models.CharField(max_length=254)
    cd_bioma = models.IntegerField()
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return self.bioma

    class Meta:
        ordering = ("bioma",)
