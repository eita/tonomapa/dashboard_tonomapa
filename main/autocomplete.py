from dal import autocomplete
from django.db.models import Q
from main.models import (
    Territorio,
    Usuaria,
    OrganizacaoUsuaria,
    Municipio,
    TipoComunidade,
    Estado,
    Bioma,
)


class TerritorioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Territorio.objects.none()

        queryset = Territorio.objects.all()

        estado = self.forwarded.get("estado", None)
        if estado:
            queryset = queryset.filter(municipio_referencia__estado__in=estado)

        comunidade = self.forwarded.get("comunidade", None)
        if comunidade:
            queryset = queryset.filter(tipos_comunidade__in=comunidade)

        if self.q:
            queryset = queryset.filter(nome__icontains=self.q)

        return queryset

    def get_selected_result_label(self, result):
        try:
            revisora = result.revisora
        except:
            return f"{result}"

        if revisora:
            return f"{result} (revisora: {revisora})"

        return f"{result}"

    # def get_result_label(self, item):
    #     return f"{item} ()"


class UserNotReviewerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Usuaria.objects.none()

        queryset = Usuaria.objects.filter(
            Q(is_reviewer=False) | Q(is_reviewer__isnull=True)
        )

        if self.q:
            queryset = queryset.filter(
                Q(first_name__icontains=self.q) | Q(username__icontains=self.q)
            )

        return queryset


class ReviewerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        user = self.request.user
        if not user.is_authenticated:
            return Usuaria.objects.none()

        queryset = Usuaria.objects.filter(is_reviewer=True)

        if not user.has_perm("territorio.ver_todos"):
            usuarias_ids = OrganizacaoUsuaria.objects.filter(
                organizacao_id__in=user.organizacoes_que_administra_ids
            ).values_list("usuaria_id")
            queryset = queryset.filter(id__in=usuarias_ids)

        if self.q:
            queryset = queryset.filter(
                Q(first_name__icontains=self.q) | Q(username__icontains=self.q)
            )

        return queryset


class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Municipio.objects.none()

        queryset = Municipio.objects.all()

        if self.q:
            queryset = queryset.filter(nome__icontains=self.q)

        return queryset

    def get_selected_result_label(self, result):
        try:
            revisora = result.municipio_usuarias.filter(usuaria__is_reviewer=True)[
                0
            ].usuaria
        except:
            return f"{result}"

        if revisora:
            return f"{result} (revisora: {revisora})"

        return f"{result}"


class TipoComunidadeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated:
        #     return TipoComunidade.objects.none()

        queryset = TipoComunidade.objects.all()

        estado = self.forwarded.get("estado", None)

        if estado:
            queryset = queryset.filter(
                territorios__municipio_referencia__estado__in=estado
            )

        if self.q:
            queryset = queryset.filter(nome__icontains=self.q)

        queryset = queryset.distinct()

        return queryset


class EstadoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated:
        #     return Estado.objects.none()

        queryset = Estado.objects.all()

        if self.q:
            queryset = queryset.filter(
                Q(nome__icontains=self.q) | Q(uf__icontains=self.q)
            )

        return queryset


class BiomaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        queryset = Bioma.objects.all()

        if self.q:
            queryset = queryset.filter(bioma__icontains=self.q)

        return queryset
