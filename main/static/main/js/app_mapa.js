let map;
const markersLayer = new L.LayerGroup();
// const markersLayer = L.markerClusterGroup();
// const territorioMarkersLayer = new L.LayerGroup();
let territorioMarkersLayer;
const territorioPoligonosLayer = new L.LayerGroup();
let bounds = new L.latLngBounds();

function zoomToTerritorio(territorioId) {
  for (const layer of territorioMarkersLayer.getLayers()) {
    if (layer.options.territorioId == territorioId) {
      layer.closePopup();
      break;
    }
  }
  map.removeLayer(territorioMarkersLayer);
  // map.addLayer(territorioPoligonosLayer);
  // map.addLayer(markersLayer);
  let b = new L.latLngBounds();
  let hasPoligonos = false;
  for (const layer of territorioPoligonosLayer.getLayers()) {
    if (layer.options.territorioId == territorioId) {
      hasPoligonos = true;
      b.extend(layer._latlngs[0]);
    }
  }
  if (hasPoligonos) {
    map.flyToBounds(b);
  } else {
    for (const layer of territorioMarkersLayer.getLayers()) {
      if (layer.options.territorioId == territorioId) {
        map.flyTo(layer._latlng, 11);
        break;
      }
    }
  }
}

$(document).ready(function () {
  territorioMarkersLayer = isSinglePage
    ? new L.LayerGroup()
    : L.markerClusterGroup();
  if (position_initial.length == 0 && territorios.length > 0) {
    $("#map").hide();
    $("#nomap").removeClass("d-none");
  } else {
    $("#form_map_filter_btn, #form_map_filter_close").on("click", function () {
      $("#form_map_filter_btn").toggle();
      $("#form_map_filter").toggle(250);
    });

    map = L.map("map", {
      center: position_initial,
      zoom: 11,
      scrollWheelZoom: false,
    });

    const biomas = L.geoJson(null, {
      style: function (feature) {
        return {
          color: stringToColour(feature.properties.nom_bioma),
          stroke: false,
          fillOpacity: 0.3,
        };
      },
      // onEachFeature: function (feature, layer) {
      //   if (feature.properties) {
      //     const content = `
      //       <table class='table table-striped table-bordered table-condensed'>
      //           <tr>
      //               <th>Bioma</th>
      //               <td>${feature.properties.nom_bioma}</td>
      //           </tr>
      //           <tr>
      //               <th>Área</th>
      //               <td>${feature.properties.val_area_k}</td>
      //           </tr>
      //           <tr>
      //               <th>Descrição</th>
      //               <td>
      //                   ${feature.properties.ds_sintese}<br /><br />
      //                   <i>
      //                       <string>Fonte:</strong> IBGE <a href='https://geoftp.ibge.gov.br/informacoes_ambientais/estudos_ambientais/biomas/documentos/Sintese_Descricao_Biomas.pdf'>https://geoftp.ibge.gov.br/informacoes_ambientais/estudos_ambientais/biomas/documentos/Sintese_Descricao_Biomas.pdf</a>
      //                   </i>
      //               </td>
      //           </tr>
      //       <table>`;
      //     layer.on({
      //       click: function (e) {
      //         $("#feature-title").html(feature.properties.nom_bioma);
      //         $("#feature-info").html(content);
      //         $("#featureModal").modal("show");
      //       },
      //     });
      //   }
      // },
    });
    $.getJSON(
      `${SITE_URL}/static/main/biomas/MMA_biomas.geojson`,
      function (data) {
        const returnData = { ...data };
        returnData.features = data.features.filter((feature) => {
          return (
            !filtros ||
            !filtros.Bioma ||
            filtros.Bioma.length === 0 ||
            filtros.Bioma.includes(feature.properties.nom_bioma)
          );
        });
        biomas.addData(returnData).addTo(map);
      }
    );

    openStreetMapTileLayer = L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 19,
        attribution:
          '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
        id: "openstreetmap",
      }
    );

    if (MAPBOX_ACCESS_TOKEN) {
      mapboxTileLayer = L.tileLayer(
        "https://api.mapbox.com/styles/v1/dtygel/ckmu0up7v17wt17panh3u8d5e/tiles/{z}/{x}/{y}?access_token=" +
          MAPBOX_ACCESS_TOKEN,
        {
          maxZoom: 19,
          attribution: '&copy; <a href="https://mapbox.com">MapBox</a>',
          id: "mapbox",
        }
      );

      const activeTileLayer = localStorage.getItem("activeTileLayer");

      if (!activeTileLayer || activeTileLayer === "mapbox") {
        mapboxTileLayer.addTo(map);
      } else {
        openStreetMapTileLayer.addTo(map);
      }

      L.control
        .layers(
          {
            Satélite: mapboxTileLayer,
            Mapa: openStreetMapTileLayer,
          },
          {
            Biomas: biomas,
          }
        )
        .addTo(map);
      map.on("baselayerchange", function (e) {
        localStorage.setItem("activeTileLayer", e.layer.options.id);
      });
    } else {
      openStreetMapTileLayer.addTo(map);
    }

    L.control.scale().addTo(map);

    L.easyButton({
      states: [
        {
          stateName: "show-all",
          icon: "fa-arrows-alt",
          title: "Mostrar todas as comunidades",
          onClick: function () {
            if (Object.keys(bounds).length > 0) {
              map.flyToBounds(bounds);
            } else {
              map.setView(position_initial, 4);
            }
          },
        },
      ],
    }).addTo(map);

    const showLayers = () => {
      const zoom = map.getZoom();
      if (zoom >= 10) {
        map.addLayer(markersLayer);
        map.removeLayer(territorioMarkersLayer);
        map.addLayer(territorioPoligonosLayer);
      } else if (zoom >= 8) {
        map.removeLayer(territorioMarkersLayer);
        map.addLayer(territorioPoligonosLayer);
        map.removeLayer(markersLayer);
      } else {
        map.addLayer(territorioMarkersLayer);
        map.removeLayer(territorioPoligonosLayer);
        map.removeLayer(markersLayer);
      }
    };

    const iconePoligono = L.icon({
      iconUrl: `${icone_url}/tonomapa_pin_publico_39x48.png`,
      shadowUrl: `${icone_url}/tonomapa_pin_shadow.png`,
      iconSize: [39, 48],
      shadowSize: [64, 48],
      shadowAnchor: [16, 26],
    });

    const adicionaMarker = (params) => {
      const descricao = params.descricao
        ? `<p class="balao-descricao">${params.descricao}</p>`
        : "";

      const botaoVisitar = isSinglePage
        ? ""
        : `<div class="balao-footer"><a class="btn btn-primary btn-sm" href="${params.territorioUrl}"><i class="fa fa-external-link"></i> Visitar "${params.territorioNome}"</a></div>`;

      let marker = L.marker(
        {
          lon: params.lon,
          lat: params.lat,
        },
        {
          icon: params.icon,
        }
      );

      marker.bindPopup(`
                <h4>${params.preTitulo} - ${params.nome}</h4>
                <p class="balao-tipo">Tipo: ${params.tipoNome}</p>
                ${descricao}
                ${botaoVisitar}
            `);

      marker.feature = {
        type: "Feature",
        properties: {
          name: `${params.preTitulo} - ${params.nome}`,
          description: params.descricao,
          id: params.territorioId,
          "marker-color": params.preTitulo == "Uso" ? "#0d0" : "#d00",
          Comunidade: params.territorioNome,
          Tipo: params.tipoNome,
        },
      };

      marker.addTo(markersLayer);
    };

    for (let territorio of territorios) {
      const bindPopupTerritorio = (addButtonToZoom) => {
        let button = "";
        if (addButtonToZoom) {
          button =
            territorio.areasDeUso.length > 0 ||
            territorio.conflitos.length > 0 ||
            territorio.poligonos.length > 0
              ? `<button class="btn btn-primary btn-sm" onclick="zoomToTerritorio(${territorio.id})">
                        <i class="fa fa-search-plus"></i> Fazer zoom
                        </button>`
              : `<button class="btn btn-primary btn-sm" onclick="alert('Não é possível fazer zoom, pois esta comunidade não tem polígonos, áreas de uso nem conflitos cadastrados')">
                        <i class="fa fa-search-plus"></i> Fazer zoom
                        </button>`;
        }

        return `
                <h4>Comunidade: ${territorio.nome}</h4>
                <p><b>Status da revisão:</b> ${territorio.statusRevisao}</p>
                <div class="balao-footer">
                <a class="btn btn-primary btn-sm mr-2" href="${territorio.territorio_url}">
                <i class="fa fa-external-link"></i> Visitar a página
                </a>
                ${button}
                </div>
                `;
      };

      if (
        territorio.poligonoMarker.length == 2 &&
        territorio.poligonoMarker[0] &&
        territorio.poligonoMarker[1]
      ) {
        const pMarker = L.marker(territorio.poligonoMarker, {
          territorioId: territorio.id,
          icon: iconePoligono,
        });
        // console.log('isSinglePage', isSinglePage);
        if (!isSinglePage) {
          pMarker.bindPopup(bindPopupTerritorio(true));
        }
        pMarker.addTo(territorioMarkersLayer);
        bounds.extend(territorio.poligonoMarker);
      }

      for (let poligono of territorio.poligonos) {
        const polygonLayer = L.polygon(poligono, {
          territorioId: territorio.id,
        });
        if (!isSinglePage) {
          polygonLayer.bindPopup(bindPopupTerritorio(false));
          polygonLayer.on("click", function (e) {
            map.flyToBounds(e.target.getBounds());
          });
        }
        polygonLayer.feature = {
          type: "Feature",
          properties: {
            name: territorio.nome,
            stroke: "#222222",
            "stroke-opacity": 1.0,
            "stroke-width": 1,
            fill: "#0000ff",
            "fill-opacity": 0.6,
            id: territorio.id,
            "Status da revisão": territorio.statusRevisao,
            "Link para a comunidade no Dashboard":
              SITE_URL + territorio.territorio_url,
          },
        };
        polygonLayer.addTo(territorioPoligonosLayer);
        bounds.extend(polygonLayer.getBounds());
      }

      for (let marker of territorio.conflitos) {
        const icon = L.icon({
          iconUrl: `${icone_url}/icone_conflito_${marker.tipo_conflito_id}_120x92.png`,
          iconSize: [32, 24],
        });
        adicionaMarker({
          descricao: marker.descricao,
          lon: marker.lng,
          lat: marker.lat,
          icon: icon,
          preTitulo: "Conflito",
          nome: marker.nome,
          tipoNome: marker.tipo_conflito_nome,
          territorioNome: territorio.nome,
          territorioId: territorio.id,
          territorioUrl: territorio.territorio_url,
        });
        bounds.extend({ lat: marker.lat, lon: marker.lng });
      }

      for (let marker of territorio.areasDeUso) {
        const icon = L.icon({
          iconUrl: `${icone_url}/icone_uso_${marker.tipo_area_de_uso_id}_100x120.png`,
          iconSize: [27, 32],
        });
        adicionaMarker({
          descricao: marker.descricao,
          lon: marker.lng,
          lat: marker.lat,
          icon: icon,
          preTitulo: "Uso",
          nome: marker.nome,
          tipoNome: marker.tipo_area_de_uso_nome,
          territorioNome: territorio.nome,
          territorioId: territorio.id,
          territorioUrl: territorio.territorio_url,
        });
        bounds.extend({ lat: marker.lat, lon: marker.lng });
      }
    }

    if (
      Object.keys(bounds).length > 0 &&
      bounds._southWest.lat != bounds._northEast.lat &&
      bounds._southWest.lng != bounds._northEast.lng
    ) {
      map.flyToBounds(bounds, { duration: 0.1 });
    } else {
      showLayers();
    }

    map.on("zoomend", function () {
      showLayers();
    });

    if ($("#downloadPoligonosDash").length) {
      const KMLPoligonos = tokml(territorioPoligonosLayer.toGeoJSON(), {
        name: "name",
        description: "description",
        simplestyle: true,
      });

      const KMLUsosEConflitos = tokml(markersLayer.toGeoJSON(), {
        name: "name",
        description: "description",
        simplestyle: true,
      });

      $("#downloadPoligonosDash").on("click", function (e) {
        e.preventDefault();
        let url = URL.createObjectURL(
          new Blob([KMLPoligonos], {
            type: "application/vnd.google-earth.kml+xml",
          })
        );
        var downloadLink = document.createElement("a");
        downloadLink.href = url;
        downloadLink.download = "poligonos.kml";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      });

      $("#downloadUsosEConflitos").on("click", function (e) {
        e.preventDefault();
        let url = URL.createObjectURL(
          new Blob([KMLUsosEConflitos], {
            type: "application/vnd.google-earth.kml+xml",
          })
        );
        var downloadLink = document.createElement("a");
        downloadLink.href = url;
        downloadLink.download = "usos_e_conflitos.kml";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      });
    }
  }
});
