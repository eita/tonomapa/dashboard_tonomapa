const UNIDADES_AREA = {
    METRO_QUADRADO: {
        sigla: "m²",
        siglaMilhar: "km²",
        nome: "Metro quadrado",
        razao: 1,
    },
    HECTARE: {
        sigla: "h",
        siglaMilhar: "",
        nome: "Hectare",
        razao: 1 / 10000,
    },
};

function formataNumeroBR(num, decimais = 2) {
    if (!num) {
        return "0";
    }
    return num
        .toFixed(decimais)
        .replace(".", ",")
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

function formataArea(areaCalc, unidadeArea = UNIDADES_AREA.HECTARE) {
    areaCalc = parseFloat(areaCalc);
    let unidade = unidadeArea.sigla;
    let decimals = 1;
    if (unidadeArea.sigla == UNIDADES_AREA.HECTARE.sigla) {
        decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
    } else {
        if (areaCalc > 1050000000) {
            areaCalc = Math.round(areaCalc / 100000000) / 10;
            unidade = "mil km²";
            decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950000000) {
            areaCalc = "";
            unidade = "1 mil km²";
        } else if (areaCalc > 1050000) {
            areaCalc = Math.round(areaCalc / 100000) / 10;
            unidade = "km²";
            decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950000) {
            areaCalc = "";
            unidade = "1 km²";
        } else if (areaCalc > 1050) {
            areaCalc = Math.round(areaCalc / 100) / 10;
            unidade = "mil m²";
            decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950) {
            areaCalc = "";
            unidade = "mil m²";
        } else {
            areaCalc = Math.round(areaCalc);
            unidade = "m²";
            decimals = 0;
        }
    }
    areaCalc =
        typeof areaCalc == "number"
            ? "~" + formataNumeroBR(areaCalc, decimals) + " " + unidade
            : "~" + unidade;

    return areaCalc;
}
