$(document).ready(function () {
    var options = {
        ...dataTableDefaultOptions,
        // fixedHeader: true,
        orderCellsTop: true,
        scrollY: "60vh",
        scrollCollapse: true,
        // order: [[ 1, "desc" ]],
        order: [],
        paging: true,
        dom: "frtip",
        lengthChange: false,
        pageLength: 100,
        // deferLoading: 57,
        processing: true,
        serverSide: true,
        ajax: DATATABLE_URL,
    };
    var objectListTable = $("#object_datatable");
    objectListTable.DataTable(options);
});
