var isSubmitting = {};
var onChangeHooks = [];
var calculosCallbacks = [];
const UNIDADES_AREA = {
  METRO_QUADRADO: {
    sigla: "m²",
    siglaMilhar: "km²",
    nome: "Metro quadrado",
    razao: 1,
  },
  HECTARE: {
    sigla: "h",
    siglaMilhar: "",
    nome: "Hectare",
    razao: 1 / 10000,
  },
};

window.onscroll = function () {
  scrollFunction();
};

// function toggleSideBar() {
//   var sideBarIsHidden = $("#sidebar-wrapper").css("display") == "none";
//   if (sideBarIsHidden) {
//     $("#sidebar-wrapper").fadeIn(200);
//   } else {
//     $("#sidebar-wrapper").fadeOut(200);
//   }
//   $("#page-content-wrapper").toggleClass(
//     "col-md-12 col-lg-12",
//     !sideBarIsHidden
//   );
//   $("#sidebar-button span.nav-link").toggleClass("closed", !sideBarIsHidden);
//   $("#page-content-wrapper").toggleClass("mt-4", sideBarIsHidden);

//   if ($(".linhadotempo_pane1").length) {
//     $("#page-content-wrapper").toggleClass("no-padding");
//     $(".linhadotempo_pane1, .linhadotempo_pane2").toggleClass("no-padding");
//     $("#breadcrumb-holder").toggleClass("d-none", sideBarIsHidden);
//   } else {
//     $("#breadcrumb-holder").toggleClass("d-none", sideBarIsHidden);
//   }
// }

function toggleSubmitForm(formEl, forceToggle = null) {
  var form = $(formEl);
  var f = FormChanges(formEl);
  var shouldDisable =
    f.length == 0 && (!forceToggle || forceToggle == "disable");
  form.toggleClass("form-disabled", shouldDisable);
  form.find(":submit").toggleClass("disabled", shouldDisable);
}

function cleanValues(tr) {
  tr.find(":input").each(function (i, inputEl) {
    var input = $(inputEl);
    if (input.parent("td").hasClass("decimalfield")) {
      //addMask(input);
    } else if (input.siblings("span").first().hasClass("select2")) {
      var select2Span = input
        .siblings("span")
        .first()
        .find(".select2-selection__rendered")
        .first();
      if (select2Span) {
        select2Span.html(
          '<span class="select2-selection__placeholder"></span>'
        );
      }
    }

    if (inputEl.className.match(/.*select2.*/)) {
      input.val("").change();
      input.prop("defaultValue", "");
    }
  });
}

function formatInputVal(inputEl, direction = "toText") {
  var input = $(inputEl);
  var val = input.val();
  if (direction == "toText") {
    val = formatNumber(val, "toFloat");
    if (isNaN(val)) {
      return;
    }
  }
  var valRet = formatNumber(val, direction);
  input.val(valRet);
}

function formatNumber(number, direction = "toText") {
  var numberRet = null;
  switch (direction) {
    case "toText":
      numberRet = number.toLocaleString("pt-BR", { minimumFractionDigits: 2 });
      break;
    case "toFloat":
      numberRet = parseFloat(number.replace(".", "").replace(",", "."));
      break;
  }
  return numberRet;
}

function scrollFunction() {
  $("#gotoTop").toggleClass(
    "d-none",
    document.body.scrollTop < 20 && document.documentElement.scrollTop < 20
  );
}

function gotoTop() {
  $("html, body").animate({ scrollTop: 0 }, 300);
}

function filtraTextoExportado(data, node) {
  if (node.nodeType == 1 && node.hasChildNodes()) {
    var ret = "";
    for (var i = 0; i < node.childNodes.length; i++) {
      var childNode = node.childNodes[i];
      if (
        childNode.nodeType != 1 ||
        !childNode.getAttribute("datatable-export-disabled")
      ) {
        ret += childNode.textContent.trim();
      }
    }
    return ret;
  } else {
    return node.nodeType != 1 || node.getAttribute("datatable-export-disabled")
      ? data
      : "";
  }
}

var dataTableDefaultOptions = {
  language: {
    sEmptyTable: "Nenhum registro encontrado",
    sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
    sInfoFiltered: "(Filtrados de _MAX_ registros)",
    sInfoPostFix: "",
    sInfoThousands: ".",
    sLengthMenu: "_MENU_ resultados por página",
    sLoadingRecords: "Carregando...",
    sProcessing: "Processando...",
    sZeroRecords: "Nenhum registro encontrado",
    sSearch: "Pesquisar",
    oPaginate: {
      sNext: "Próximo",
      sPrevious: "Anterior",
      sFirst: "Primeiro",
      sLast: "Último",
    },
    oAria: {
      sSortAscending: ": Ordenar colunas de forma ascendente",
      sSortDescending: ": Ordenar colunas de forma descendente",
    },
    buttons: {
      colvis: "Colunas visíveis",
    },
    searchPanes: {
      count: "{total}",
      countFiltered: "{shown} / {total}",
      clearMessage: "Limpar filtros",
      collapse: {
        0: "Filtros",
        1: "Filtros (1 aplicado)",
        _: "Filtros (%d aplicados)",
      },
      collapseMessage: "Fechar todas",
      emptyMessage: "<em>Sem dados</em>",
      loadMessage: "Carregando os filtros...",
      showMessage: "Mostrar todos",
      title: "Filtros ativos: %d",
    },
  },
  paging: true,
  searching: true,
  ordering: true,
  bInfo: false,
  responsive: false,
};
var dataTableExtraOptions = {
  "fit-to-page": {
    scrollY: "40vh",
    scrollCollapse: true,
    paging: false,
  },
  "add-export-buttons": {
    dom: "Bfrtip",
    buttons: ["excelHtml5", "csvHtml5", "pdfHtml5"],
  },
  "no-paging": {
    paging: false,
  },
  "datatable-indicators": {
    dom: "Bt",
    paging: false,
    searching: false,
    ordering: false,
    bInfo: false,
    responsive: false,
    buttons: [
      {
        extend: "excelHtml5",
        exportOptions: {
          columns: ":visible",
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
        },
      },
      {
        extend: "csvHtml5",
        exportOptions: {
          columns: ":visible",
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
        },
      },
      {
        extend: "pdfHtml5",
        exportOptions: {
          columns: ":visible",
          format: {
            body: function (data, row, column, node) {
              return filtraTextoExportado(data, node);
            },
          },
        },
      },
      {
        extend: "colvis",
        text: "Ocultar/mostrar colunas",
      },
    ],
  },
};

function addMask(input) {
  input.mask("#.##0,00", {
    reverse: true,
    onKeyPress: function (k, el) {
      var r = "";
      if (k.length == 1) {
        r = "0,0" + k.toString();
      } else if (k.length == 2) {
        r = "0," + k.toString();
      } else if (k[0] == 0) {
        r = k.substr(1);
      }
      if (r) {
        $(el.target).val(r);
      }
    },
  });
  input.prop("defaultValue", input.val());
}

function calcula_total(values) {
  let total = 0;
  for (value of values) {
    total += value;
  }
  return total;
}

$(document).ready(function () {
  // reset sidebar:
  $(".child_menu").hide();
  // Popula area mapeada:
  if (typeof areaMapeada != "undefined") {
    $("#areaMapeadaAprovada").text(
      formataArea(
        areaMapeadaAprovada.replace(",", "."),
        UNIDADES_AREA.METRO_QUADRADO
      )
    );
    $("#areaMapeada").text(
      "de " +
        formataArea(areaMapeada.replace(",", "."), UNIDADES_AREA.METRO_QUADRADO)
    );
  }

  // executes all calcs:
  setTimeout(function () {
    for (var i = 0; i < calculosCallbacks.length; i++) {
      eval(calculosCallbacks[i] + "()");
    }
  }, 1000);
  // monitor changes in form - and toggle submit button
  $(".monitor-formchanges").on("change", "input,select,textarea", function () {
    for (var i = 0; i < onChangeHooks.length; i++) {
      eval(onChangeHooks[i] + "(this)");
    }
    toggleSubmitForm(this.form);
  });
  $(".decimalfield input").each(function (i, inputEl) {
    //addMask($(inputEl));
  });
  $(".decimalfield_noniput").each(function (i, el) {
    var val = parseFloat($(el).text().replace(",", "."));
    if (!isNaN(val)) {
      minFields = val > 0 && val < 0.01 ? 4 : 2;
      $(el).text(
        val.toLocaleString("pt-BR", { minimumFractionDigits: minFields })
      );
    }
  });
  $(".decimalfield_noniput_currency").each(function (i, el) {
    var val = parseFloat($(el).text().replace(",", "."));
    if (!isNaN(val)) {
      $(el).text(
        val.toLocaleString("pt-BR", { currency: "BRL", style: "currency" })
      );
    }
  });
  $("form.convert-numbers-to-float").each(function (i, formEl) {
    var form = $(formEl);
    form.submit(function (e) {
      if (!form.hasClass("form-disabled")) {
        if (isSubmitting[formEl]) {
          e.preventDefault();
          return;
        }
        isSubmitting[formEl] = true;
        $(".decimalfield input, .decimalfield_direct").each(function (i, el) {
          var clean = $(el).val().replace(/\./g, "").replace(",", ".");
          $(el).val(clean);
        });
      }
    });
    form
      .find(".decimalfield input, .decimalfield_direct")
      .each(function (i, inputEl) {
        var valRaw = $(inputEl).val();
        if (!isNaN(parseFloat(valRaw))) {
          var valStr = formatNumber(parseFloat(valRaw), "toText");
          $(inputEl).val(valStr);
          $(inputEl).prop("defaultValue", valStr);
        }
        if (i == 0) {
          onChangeHooks.push("formatInputVal");
        }
      });
  });

  // $("#sidebar-button").click(function (e) {
  //   e.preventDefault();
  //   toggleSideBar();
  // });
  $(".list-group-item.disabled").click(function (e) {
    e.preventDefault();
    alert($(this).attr("data-alert-text"));
  });
  $(".href-button").click(function (e) {
    e.preventDefault();
    if ($(this).attr("data-target-url")) {
      window.location = $(this).attr("data-target-url");
    }
  });
  if ($(".datatable")) {
    $(".datatable").each(function (index, el) {
      var options = $.extend(true, {}, dataTableDefaultOptions);
      var extraOptions = $.extend(true, {}, dataTableExtraOptions);
      var table = $(el);
      for (var htmlClass in extraOptions) {
        if (table.hasClass(htmlClass)) {
          if (extraOptions.hasOwnProperty(htmlClass)) {
            var subOptions = $.extend({}, extraOptions[htmlClass]);
            for (var prop in extraOptions[htmlClass]) {
              if (subOptions.hasOwnProperty(prop)) {
                options[prop] = extraOptions[htmlClass][prop];
              }
            }
            if (options.buttons) {
              for (var i = 0; i < options.buttons.length; i++) {
                if (options.buttons[i].hasOwnProperty("exportOptions")) {
                  if (table.attr("datatable-title")) {
                    options.buttons[i].title = table.attr("datatable-title");
                  } else if (table.find("caption")) {
                    options.buttons[i].title = table
                      .find("caption")
                      .first()
                      .text();
                  }
                  if (table.attr("datatable-filename")) {
                    options.buttons[i].filename =
                      table.attr("datatable-filename");
                  }
                }
              }
            }
          }
        }
      }
      table.DataTable(options);
    });
  }

  $("[data-delete-url]").click(function (evt) {
    var elm = $(this);
    if (confirm(elm.data("delete-confirm-message"))) {
      $.ajax(elm.data("delete-url"), {
        method: "POST",
        success: function () {
          window.location = elm.data("success-url");
        },
        error: function () {
          alert("Não foi possível apagar.");
        },
      });
    }
    return false;
  });
  $(".button-delete").click(function (el) {
    toggleSubmitForm($(this).parents("form")[0], "enable");
    for (var i = 0; i < calculosCallbacks.length; i++) {
      eval(calculosCallbacks[i] + "()");
    }
  });

  var form_saving = [];
  if ($(".monitor-formchanges")) {
    $(".monitor-formchanges").each(function (i, formEl) {
      toggleSubmitForm(formEl);
      var form = $(formEl);
      if (form.hasClass("warn-changed-beforeunload")) {
        form_saving.push(false);
        form.submit(function (e) {
          if ($(this).hasClass("form-disabled")) {
            alert("não há alterações para salvar!");
            e.preventDefault();
            return;
          } else {
            form_saving[i] = true;
          }
        });
      }
    });
    $(window).on("beforeunload", function () {
      var hasUnsavedChanges = false;
      $(".monitor-formchanges").each(function (i, formEl) {
        var form = $(formEl);
        if (form.hasClass("warn-changed-beforeunload")) {
          if (!form_saving[i]) {
            var f = FormChanges(formEl);
            if (f.length > 0) {
              hasUnsavedChanges = true;
              return null;
            }
          }
        }
      });
      if (hasUnsavedChanges) {
        return "Você tem alterações não salvas! Tem certeza de que quer sair?";
      }
    });
  }

  $(".filtra_estado").on("click", function (e) {
    e.preventDefault();
    const target = $(e.currentTarget);
    const estado = target.text();
    const mostraTudo = estado == "Brasil";
    const canvasId = target.closest("ul").data("canvas");
    const canvas = $("#canvas-" + canvasId);
    let currentData = mostraTudo ? data[canvasId] : dataEstados[canvasId];
    if (currentData) {
      const label_field = canvas.data("label_field");
      const value_field = mostraTudo
        ? canvas.data("value_field")
        : canvas.data("value_estado_field");
      const estado_field = canvas.data("estado_field");
      if (!mostraTudo) {
        currentData = currentData.filter((elm) => {
          return elm[estado_field] == estado;
        });
      }
      if (currentData.length == 0) {
        alert("Não há dados para " + estado);
        return;
      }
      charts[canvasId].data.labels = currentData.map((elm) => elm[label_field]);
      charts[canvasId].data.datasets[0].data = currentData.map(
        (elm) => elm[value_field]
      );
      charts[canvasId].options.total = calcula_total(
        charts[canvasId].data.datasets[0].data
      );
      charts[canvasId].options.title.text = estado;
      charts[canvasId].options.title.display = !mostraTudo;
      charts[canvasId].update();
    } else {
      alert("Não há dados para " + estado);
    }
  });

  $(".filtra_estado_merged_plot").on("click", function (e) {
    e.preventDefault();
    const target = $(e.currentTarget);
    const estado = target.text();
    const mostraTudo = estado == "Brasil";
    const canvasId = target.closest("ul").data("canvas");
    const canvas = $("#canvas-" + canvasId);
    if (mostraTudo) {
      charts[canvasId].setData(data[canvasId]);
      charts[canvasId].setupGrid();
      charts[canvasId].draw();
      return;
    }

    let currentData = dataEstados[canvasId];
    if (currentData) {
      const label_field = canvas.data("label_field");
      const value_field = mostraTudo
        ? canvas.data("value_field")
        : canvas.data("value_estado_field");
      const estado_field = canvas.data("estado_field");
      let newDataSet = [];
      for (let i = 0; i < currentData.length; i++) {
        newDataSet.push({
          label: `${currentData[i].label} (${estado})`,
          data: currentData[i].data
            .filter((elm) => {
              return elm[estado_field] == estado;
            })
            .map((elm) => [parseInt(elm["dia"]) * 1000, elm["qtde"]]),
        });
      }
      charts[canvasId].setData(newDataSet);
      charts[canvasId].setupGrid();
      charts[canvasId].draw();

      // charts[canvasId].options.total = calcula_total(charts[canvasId].data.datasets[0].data);
      // charts[canvasId].options.title.text = estado;
      // charts[canvasId].options.title.display = (!mostraTudo);
      // charts[canvasId].update();
    } else {
      alert("Não há dados para " + estado);
    }
  });
});

var callbacks = {};

function calculate_table_cells(
  table_selector,
  origin1_selector,
  origin2_selector,
  destination_selector,
  grand_total_selector
) {
  var trs = $(table_selector + " tr");
  var grand_total = 0;
  var val1, val2, total, tr, text;
  for (var ix = 0; ix < trs.length; ix++) {
    tr = trs[ix];
    if ($(tr).is(":hidden")) {
      continue;
    }
    var origin1 = $(tr).find(origin1_selector);
    var origin2 = $(tr).find(origin2_selector);
    var destination = jQuery(tr).find(destination_selector);

    if (origin1.length && origin2.length && destination.length) {
      val1 = formatNumber(origin1.val(), "toFloat");
      val2 = formatNumber(origin2.val(), "toFloat");
      if (!isNaN(val1) && !isNaN(val2)) {
        total = val1 * val2;
        grand_total += total;
      } else {
        total = 0;
      }
      total_str = formatNumber(total, "toText");
      if (total == 0) {
        total_str = "-";
      }

      destination.text(total_str);
    }
  }

  //Grand total
  jQuery(table_selector + " " + grand_total_selector).text(
    formatNumber(grand_total, "toText")
  );
}

function bind_calc_to_inputs(
  table_selector,
  origin1_selector,
  origin2_selector,
  callback
) {
  jQuery(
    table_selector +
      " " +
      origin1_selector +
      "," +
      table_selector +
      " " +
      origin2_selector
  ).on("change", callback);
}

function init_calc(
  table_selector,
  origin1_selector,
  origin2_selector,
  destination_selector,
  grand_total_selector
) {
  if (callbacks[table_selector + destination_selector] == null) {
    callbacks[table_selector + destination_selector] = function () {
      calculate_table_cells(
        table_selector,
        origin1_selector,
        origin2_selector,
        destination_selector,
        grand_total_selector
      );
    };
  }
  callbacks[table_selector + destination_selector]();
  bind_calc_to_inputs(
    table_selector,
    origin1_selector,
    origin2_selector,
    callbacks[table_selector + destination_selector]
  );
}

// lista_colunas, valor_origem_name, out_quantidade_field, out_valor_field
function soma_colunas_bulk(table_selector, params) {
  var rows = [];
  var fn_calc_soma = function (elm) {
    var table;

    if (elm.tagName.toUpperCase() == "TABLE") {
      table = jQuery(elm);
    } else {
      table = jQuery(elm).closest("table");
    }

    var totais = {};

    if (rows.length == 0) {
      table.find("tr").each(function (ix, rowEl) {
        var inputsRaw = $(rowEl).find("input");
        if (inputsRaw.length) {
          var inputs = {};
          for (var i = 0; i < inputsRaw.length; i++) {
            if (!$(inputsRaw[i]).is(":hidden")) {
              inputs[inputsRaw[i].name.replace(/.*-/, "")] = $(inputsRaw[i]);
            }
          }
          if (Object.keys(inputs).length) {
            rows.push({ row: $(rowEl), inputs: inputs });
          }
        }
      });
    }

    for (r = 0; r < rows.length; r++) {
      var row = rows[r].row;
      var inputs = rows[r].inputs;
      for (n = 0; n < params.length; n++) {
        var item = params[n];
        var quantidade_linha = 0.0;
        for (i = 0; i < item.lista_colunas.length; i++) {
          var value_in_input = formatNumber(
            inputs[item.lista_colunas[i]].val(),
            "toFloat"
          );
          if (value_in_input && !isNaN(value_in_input)) {
            quantidade_linha += value_in_input;
          }
        }
        var valor_origem = formatNumber(
          inputs[item.valor_origem_name].val(),
          "toFloat"
        );
        var valor_total_linha = valor_origem * quantidade_linha;

        var quantidade_linha_str = formatNumber(quantidade_linha, "toText");
        var valor_total_linha_str = formatNumber(valor_total_linha, "toText");

        if (quantidade_linha_str == "NaN" || quantidade_linha_str == "0") {
          quantidade_linha_str = "";
        }

        if (valor_total_linha_str == "NaN" || valor_total_linha_str == "0,00") {
          valor_total_linha_str = "-";
        }

        if (item.out_quantidade_field) {
          row
            .find("[data-field-row=" + item.out_quantidade_field + "]")
            .text(quantidade_linha_str);
        }
        row
          .find("[data-field-row=" + item.out_valor_field + "]")
          .text(valor_total_linha_str);

        if (!(item.out_valor_field in totais)) {
          totais[item.out_valor_field] = 0.0;
        }
        if (!isNaN(valor_total_linha)) {
          totais[item.out_valor_field] += valor_total_linha;
        }
      }
    }

    for (n = 0; n < params.length; n++) {
      var item = params[n];
      var valor_total_tabela_str = formatNumber(
        totais[item.out_valor_field],
        "toText"
      );
      if (valor_total_tabela_str == "NaN" || valor_total_tabela_str == "0,00") {
        valor_total_tabela_str = "-";
      }
      table
        .find("[data-field-total=" + item.out_valor_field + "]")
        .text(valor_total_tabela_str);
    }
  };

  jQuery(table_selector).on("change", "input", function (evt) {
    fn_calc_soma(this);
  });
  fn_calc_soma(jQuery(table_selector)[0]);
}

//https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Math/round
// Closure
(function () {
  /**
   * Ajuste decimal de um número.
   *
   * @param    {String}    type    O tipo de arredondamento.
   * @param    {Number}    value    O número a arredondar.
   * @param    {Integer}    exp        O expoente (o logaritmo decimal da base pretendida).
   * @returns    {Number}            O valor depois de ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Se exp é indefinido ou zero...
    if (typeof exp === "undefined" || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Se o valor não é um número ou o exp não é inteiro...
    if (isNaN(value) || !(typeof exp === "number" && exp % 1 === 0)) {
      return NaN;
    }
    // Transformando para string
    value = value.toString().split("e");
    value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
    // Transformando de volta
    value = value.toString().split("e");
    return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
  }

  // Arredondamento decimal
  if (!Math.round10) {
    Math.round10 = function (value, exp) {
      return decimalAdjust("round", value, exp);
    };
  }
  // Decimal arredondado para baixo
  if (!Math.floor10) {
    Math.floor10 = function (value, exp) {
      return decimalAdjust("floor", value, exp);
    };
  }
  // Decimal arredondado para cima
  if (!Math.ceil10) {
    Math.ceil10 = function (value, exp) {
      return decimalAdjust("ceil", value, exp);
    };
  }
})();

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

jQuery.ajaxSetup({
  beforeSend: function (xhr, settings) {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != "") {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == name + "=") {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }

    if (settings.url.substring(window.location.host)) {
      // Only send the token to relative URLs i.e. locally.
      xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
    }
  },
});

function getSlug(str) {
  if (!str) {
    return "";
  }
  str = str.replace(/'/, "").replace(/ /g, "_").toLowerCase();
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

function baixaImagem(conteudo, nome) {
  var link = document.createElement("a");
  link.download = nome;
  link.href = conteudo;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}

function getRoundedMaxMinValues(values) {
  var maxTickValue = Math.ceil(Math.max.apply(Math, values) * 1.1);
  var minTickValue = Math.floor(Math.min.apply(Math, values) * 1.1);
  if (maxTickValue > 100000) {
    maxTickValue = Math.round(maxTickValue / 100000) * 100000;
  } else if (maxTickValue > 10000) {
    maxTickValue = Math.round(maxTickValue / 10000) * 10000;
  } else if (maxTickValue > 1000) {
    maxTickValue = Math.round(maxTickValue / 1000) * 1000;
  } else if (maxTickValue > 100) {
    maxTickValue = Math.round(maxTickValue / 100) * 100;
  } else if (maxTickValue > 10) {
    maxTickValue = Math.round(maxTickValue / 10) * 10;
  }
  if (minTickValue > 0) {
    minTickValue = 0;
  } else if (minTickValue < -100000) {
    minTickValue = Math.round(minTickValue / 100000) * 100000;
  } else if (minTickValue < -10000) {
    minTickValue = Math.round(minTickValue / 10000) * 10000;
  } else if (minTickValue < -1000) {
    minTickValue = Math.round(minTickValue / 1000) * 1000;
  } else if (minTickValue < -100) {
    minTickValue = Math.round(minTickValue / 100) * 100;
  } else if (minTickValue < -10) {
    minTickValue = Math.round(minTickValue / 10) * 10;
  }
  return { maxTickValue, minTickValue };
}

function valueToBRLCurrency(value, withCurrency = true, decimals = 2) {
  var prepend = withCurrency ? "R$ " : "";
  if (!value) {
    value = 0;
  }
  return (
    prepend +
    parseFloat(value).toLocaleString("pt-BR", {
      minimumFractionDigits: decimals,
      maximumFractionDigits: decimals,
    })
  );
}

function formataNumeroBR(num, decimais = 2) {
  if (!num) {
    return "0";
  }
  return num
    .toFixed(decimais)
    .replace(".", ",")
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

function formataArea(areaCalc, unidadeArea = UNIDADES_AREA.HECTARE) {
  areaCalc = parseFloat(areaCalc);
  let unidade = unidadeArea.sigla;
  let decimals = 1;
  if (unidadeArea.sigla == UNIDADES_AREA.HECTARE.sigla) {
    decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
  } else {
    if (areaCalc > 1050000000) {
      areaCalc = Math.round(areaCalc / 100000000) / 10;
      unidade = "mil km²";
      decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
    } else if (areaCalc > 950000000) {
      areaCalc = "";
      unidade = "1 mil km²";
    } else if (areaCalc > 1050000) {
      areaCalc = Math.round(areaCalc / 100000) / 10;
      unidade = "km²";
      decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
    } else if (areaCalc > 950000) {
      areaCalc = "";
      unidade = "1 km²";
    } else if (areaCalc > 1050) {
      areaCalc = Math.round(areaCalc / 100) / 10;
      unidade = "mil m²";
      decimals = areaCalc == Math.round(areaCalc) ? 0 : 1;
    } else if (areaCalc > 950) {
      areaCalc = "";
      unidade = "mil m²";
    } else {
      areaCalc = Math.round(areaCalc);
      unidade = "m²";
      decimals = 0;
    }
  }
  areaCalc =
    typeof areaCalc == "number"
      ? "~" + formataNumeroBR(areaCalc, decimals) + " " + unidade
      : "~" + unidade;

  return areaCalc;
}

function plotMergedGraph(elmId, dataSets) {
  const options = {
    xaxis: {
      mode: "time",
      timeformat: "%b/%Y",
      // min: (new Date("2020/05/01")).getTime(),
      monthNames: [
        "jan",
        "fev",
        "mar",
        "abr",
        "mai",
        "jun",
        "jul",
        "ago",
        "set",
        "out",
        "nov",
        "dez",
      ],
      timeBase: "milliseconds",
    },
    legend: {
      show: true,
      position: "nw",
    },
    series: {
      lines: {
        show: true,
        fill: true,
      },
    },
    colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 1)"],
  };
  return $.plot($(elmId), dataSets, options);
}

function stringToColour(str) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var colour = "#";
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xff;
    colour += ("00" + value.toString(16)).substr(-2);
  }
  return colour;
}
