$(document).ready(function () {

    $('.delete_organizacaousuaria').on('click', function() {
        var button = $(this);

        var organizacaousuaria_id = parseInt(button.data('organizacaousuaria-id'));
        var organizacao = button.data('organizacao');
        var usuaria = button.data('usuaria');

        if (confirm(`Excluir ${usuaria} da organização ${organizacao}?`)) {
            var dados = {
                csrfmiddlewaretoken: csrfmiddlewaretoken
            };
            jQuery.ajax(button.data('url'), {
                method: 'POST',
                data: dados,
                dataType: 'json',
                success: function (data) {
                    if (data.status == 'success') {
                        button.parents('tr').first().remove();
                    }
                },
                error: function (data) {
                    alert('Não foi possível excluir '+usuaria+' da organizacao '+organizacao+'.')
                }
            })
        }
    });

    $("#add_existing_member_user").on("change", function(e) {
        $("#add_existing_member_bt").attr("disabled", $(this).val()=="");
        $('#add_existing_member_is_reviewer').prop('checked', true);
    });


    $('#add_existing_member_user').select2();
});
