function kmlFormatter(params) {
  return `<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Document>
<name>Meu polígono.kml</name>
<Style id="s_ylw-pushpin">
    <IconStyle>
        <scale>1.1</scale>
        <Icon>
            <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
        </Icon>
        <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
    </IconStyle>
    <LineStyle>
        <color>ffff0000</color>
    </LineStyle>
    <PolyStyle>
        <color>b4ff5500</color>
    </PolyStyle>
</Style>
<StyleMap id="m_ylw-pushpin">
    <Pair>
        <key>normal</key>
        <styleUrl>#s_ylw-pushpin</styleUrl>
    </Pair>
    <Pair>
        <key>highlight</key>
        <styleUrl>#s_ylw-pushpin_hl</styleUrl>
    </Pair>
</StyleMap>
<Style id="s_ylw-pushpin_hl">
    <IconStyle>
        <scale>1.3</scale>
        <Icon>
            <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
        </Icon>
        <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
    </IconStyle>
    <LineStyle>
        <color>ffff0000</color>
    </LineStyle>
    <PolyStyle>
        <color>b4ff5500</color>
    </PolyStyle>
</Style>
<Placemark>
    <name>${params.nome}</name>
    <styleUrl>#m_ylw-pushpin</styleUrl>
    ${params.kml}
</Placemark>
</Document>
</kml>
    `;
}

function baixaKML(params) {
  var fileContents = kmlFormatter(params);
  var filename = `${params.nome}.kml`;
  var filetype = "application/vnd.google-earth.kml+xml";

  var a = document.createElement("a");
  dataURI = "data:" + filetype + ";base64," + btoa(fileContents);
  a.href = dataURI;
  a["download"] = filename;
  var e = document.createEvent("MouseEvents");
  // Use of deprecated function to satisfy TypeScript.
  e.initMouseEvent(
    "click",
    true,
    false,
    document.defaultView,
    0,
    0,
    0,
    0,
    0,
    false,
    false,
    false,
    false,
    0,
    null
  );
  a.dispatchEvent(e);
  a.remove();
}
