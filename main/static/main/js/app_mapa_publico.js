let map;
// const territorioMarkersLayer = new L.LayerGroup();
const territorioMarkersLayer = L.markerClusterGroup({
  maxClusterRadius: 40,
});
let bounds = new L.latLngBounds();

function zoomToTerritorio(territorioId) {
  for (const layer of territorioMarkersLayer.getLayers()) {
    if (layer.options.territorioId == territorioId) {
      layer.closePopup();
      break;
    }
  }
  let b = new L.latLngBounds();

  const activeTileLayer = localStorage.getItem("activeTileLayer");
  // console.log(activeTileLayer);

  for (const layer of territorioMarkersLayer.getLayers()) {
    if (layer.options.territorioId == territorioId) {
      map.flyTo(layer._latlng, activeTileLayer === "openTopoMap" ? 7 : 14);
      break;
    }
  }
}

$(document).ready(function () {
  if (position_initial.length == 0 && territorios.length > 0) {
    $("#map").hide();
    $("#nomap").removeClass("d-none");
    return;
  }

  $("#submit-id-submit")
    .parents(".form-group")
    .appendTo("#filterModal .modal-footer");

  $("#filterModal .modal-footer .btn").on("click", () => {
    $("#filterModal form").submit();
  });

  $("#form_map_filter_btn, #form_map_filter_close, #resultados").on(
    "click",
    function () {
      $("#filterModal").modal("show");
      // $("#form_map_filter_btn").toggle();
      // $("#form_map_filter").toggle(250);
    }
  );

  map = L.map("map", {
    center: position_initial,
    zoom: 10,
    scrollWheelZoom: false,
  });

  const biomas = L.geoJson(null, {
    style: function (feature) {
      return {
        color: stringToColour(feature.properties.nom_bioma),
        stroke: false,
        fillOpacity: 0.3,
      };
    },
    // onEachFeature: function (feature, layer) {
    //   if (feature.properties) {
    //     const content = `
    //       <table class='table table-striped table-bordered table-condensed'>
    //           <tr>
    //               <th>Bioma</th>
    //               <td>${feature.properties.nom_bioma}</td>
    //           </tr>
    //           <tr>
    //               <th>Área</th>
    //               <td>${feature.properties.val_area_k}</td>
    //           </tr>
    //           <tr>
    //               <th>Descrição</th>
    //               <td>
    //                   ${feature.properties.ds_sintese}<br /><br />
    //                   <i>
    //                       <string>Fonte:</strong> IBGE <a href='https://geoftp.ibge.gov.br/informacoes_ambientais/estudos_ambientais/biomas/documentos/Sintese_Descricao_Biomas.pdf'>https://geoftp.ibge.gov.br/informacoes_ambientais/estudos_ambientais/biomas/documentos/Sintese_Descricao_Biomas.pdf</a>
    //                   </i>
    //               </td>
    //           </tr>
    //       <table>`;
    //     layer.on({
    //       click: function (e) {
    //         $("#feature-title").html(feature.properties.nom_bioma);
    //         $("#feature-info").html(content);
    //         $("#featureModal").modal("show");
    //       },
    //     });
    //   }
    // },
  });
  $.getJSON(
    `${SITE_URL}/static/main/biomas/MMA_biomas.geojson`,
    function (data) {
      const returnData = { ...data };
      returnData.features = data.features.filter((feature) => {
        return (
          !filtros ||
          !filtros.Bioma ||
          filtros.Bioma.length === 0 ||
          filtros.Bioma.includes(feature.properties.nom_bioma)
        );
      });
      biomas.addData(returnData).addTo(map);
    }
  );

  L.Control.Legenda = L.Control.extend({
    options: {
      position: "bottomright",
    },
    onAdd: function (map) {
      const controlDiv = L.DomUtil.get("legenda");
      return controlDiv;
    },
  });
  map.addControl(new L.Control.Legenda());

  L.Control.Resultados = L.Control.extend({
    options: {
      position: "bottomright",
    },
    onAdd: function (map) {
      const controlDiv = L.DomUtil.get("resultados");
      return controlDiv;
    },
  });
  map.addControl(new L.Control.Resultados());

  if ($("#downloadPoligonos").length > 0) {
    L.Control.DownloadKML = L.Control.extend({
      options: {
        position: "bottomright",
      },
      onAdd: function (map) {
        const controlDiv = L.DomUtil.get("downloadPoligonos");
        return controlDiv;
      },
    });
    map.addControl(new L.Control.DownloadKML());
  }

  L.control.scale().addTo(map);

  let mapUrl;
  let attribution;

  const mapLayers = {
    openStreetMap: L.tileLayer(
      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 14,
        attribution:
          '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
        id: "openStreetMap",
      }
    ),
    openTopoMap: L.tileLayer(
      "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
      {
        minZoom: 4,
        maxZoom: 7,
        attribution:
          'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
        id: "openTopoMap",
      }
    ),
    esriWorldMap: L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
      {
        maxZoom: 14,
        attribution:
          "Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community",
        id: "esriWorldMap",
      }
    ),
  };
  // https://basemaps.arcgis.com/arcgis/rest/services/World_Basemap_v2/VectorTileServer/resources/fonts/Arial%20Unicode%20MS%20Bold/0-255.pbf
  // https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/3/3/4
  // const esriWorldMapLayer = L.tileLayer('https://basemaps.arcgis.com/arcgis/rest/services/World_Basemap_v2/VectorTileServer/tile/{z}/{y}/{x}', {
  //      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  // });

  L.control
    .layers(
      {
        Mapa: mapLayers.openStreetMap,
        "Mapa topológico": mapLayers.openTopoMap,
        Satélite: mapLayers.esriWorldMap,
      },
      {
        Biomas: biomas,
      }
    )
    .addTo(map);

  let activeTileLayer = localStorage.getItem("activeTileLayer");

  if (!mapLayers.hasOwnProperty(activeTileLayer)) {
    activeTileLayer = "openStreetMap";
    localStorage.setItem("activeTileLayer", "openStreetMap");
  }

  mapLayers[activeTileLayer].addTo(map);

  map.on("baselayerchange", function (e) {
    localStorage.setItem("activeTileLayer", e.layer.options.id);
  });

  L.easyButton({
    states: [
      {
        stateName: "show-all",
        icon: "fa-arrows-alt",
        title: "Mostrar todas as comunidades",
        onClick: function () {
          if (Object.keys(bounds).length > 0) {
            map.flyToBounds(bounds);
          } else {
            map.setView(position_initial, 4);
          }
        },
      },
    ],
  }).addTo(map);

  map.addControl(
    new L.Control.Fullscreen({
      title: {
        false: "Ver em tela cheia",
        true: "Sair da tela cheia",
      },
    })
  );

  L.Control.Filtros = L.Control.extend({
    options: {
      position: "topright",
    },
    onAdd: function (map) {
      const controlDiv = L.DomUtil.get("form_map_filter_btn_wrapper");

      return controlDiv;
    },
  });
  map.addControl(new L.Control.Filtros());

  const iconeTerritorioPublico = L.icon({
    iconUrl: `${icone_url}/tonomapa_pin_privado_39x48.png`,
    shadowUrl: `${icone_url}/tonomapa_pin_shadow.png`,
    iconSize: [39, 48],
    shadowSize: [64, 48],
    shadowAnchor: [16, 26],
  });
  const iconeTerritorios = L.icon({
    iconUrl: `${icone_url}/tonomapa_pin_publico_39x48.png`,
    shadowUrl: `${icone_url}/tonomapa_pin_shadow.png`,
    iconSize: [39, 48],
    shadowSize: [64, 48],
    shadowAnchor: [16, 26],
  });

  const tooltipOptions = { offset: [0, 20], direction: "bottom" };

  for (let territorio of territorios) {
    if (
      !territorio.publico ||
      (territorio.publico &&
        territorio.poligonoMarker.length == 2 &&
        territorio.poligonoMarker[0] &&
        territorio.poligonoMarker[1])
    ) {
      const segmentosLabel =
        territorio.segmentos.length == 1 ? "Segmento" : "Segmentos";
      const segmentos = territorio.segmentos.join(", ");

      const bindPopupTerritorio = territorio.publico
        ? `
                            <h4>Comunidade: ${territorio.nome}</h4>
                            <p>
                                <b>Município:</b> ${territorio.municipio}<br />
                                <b>Estado:</b> ${territorio.estado}<br />
                                <b>Quantidade de famílias:</b> ${territorio.qtde_familias}<br />
                                <b>${segmentosLabel}:</b> ${segmentos}
                            </p>
                            <div class="balao-footer">
                                <button class="btn btn-primary btn-sm" onclick="zoomToTerritorio(${territorio.id})">
                                    <i class="fa fa-search-plus"></i> Fazer zoom
                                </button>
                            </div>`
        : `
                            <h4>${territorio.nome}</h4>
                            <p>
                                <b>${segmentosLabel}:</b> ${segmentos}
                            </p>
                            <div class="balao-footer">
                                <button class="btn btn-primary btn-sm" onclick="zoomToTerritorio(${territorio.id})">
                                    <i class="fa fa-search-plus"></i> Fazer zoom
                                </button>
                            </div>`;

      const tooltip = territorio.publico
        ? `Comunidade: ${territorio.nome}`
        : territorio.nome;

      const icone = territorio.publico
        ? iconeTerritorioPublico
        : iconeTerritorios;

      const coords = territorio.publico
        ? territorio.poligonoMarker
        : [territorio.lat, territorio.lng];

      const pMarker = L.marker(coords, {
        territorioId: territorio.id,
        icon: icone,
      });

      const featureNome = territorio.publico
        ? `Comunidade: ${territorio.nome}`
        : `${territorio.nome}`;
      const featureDescricao = territorio.publico
        ? `
          Município: ${territorio.municipio}
          Estado: ${territorio.estado}
          Quantidade de famílias: ${territorio.qtde_familias}
          ${segmentosLabel}: ${segmentos}
        `
        : `
          ${segmentosLabel}: ${segmentos}
        `;
      pMarker.feature = {
        type: "Feature",
        properties: {
          name: featureNome,
          description: featureDescricao,
        },
      };

      pMarker
        .bindPopup(bindPopupTerritorio)
        .bindTooltip(tooltip, tooltipOptions)
        .openTooltip()
        .on("mouseover", () => {
          pMarker.getTooltip().setOpacity(pMarker.isPopupOpen() ? 0.0 : 1.0);
        })
        .on("click", () => {
          pMarker.getTooltip().setOpacity(0.0);
        })
        .addTo(territorioMarkersLayer);

      bounds.extend(coords);
    }
  }

  map.addLayer(territorioMarkersLayer);

  if ($("#downloadPoligonos").length > 0) {
    const KMLExport = tokml(territorioMarkersLayer.toGeoJSON(), {
      name: "name",
      description: "description",
      simplestyle: true,
    });

    $("#downloadPoligonos").on("click", function (e) {
      e.preventDefault();
      let url = URL.createObjectURL(
        new Blob([KMLExport], {
          type: "application/vnd.google-earth.kml+xml",
        })
      );
      var downloadLink = document.createElement("a");
      downloadLink.href = url;
      downloadLink.download = "mapa_publico_tonomapa.kml";
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    });
  }

  if (
    Object.keys(bounds).length > 0 &&
    bounds._southWest.lat != bounds._northEast.lat &&
    bounds._southWest.lng != bounds._northEast.lng
  ) {
    map.flyToBounds(bounds, { duration: 0.1 });
  }
});
