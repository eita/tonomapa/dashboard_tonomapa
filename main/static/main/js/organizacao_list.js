function initListenerBadges() {
    $('.organizacao_remover_municipio, .organizacao_remover_estado')
        .off('click')
        .on('click', function(e) {
            e.preventDefault();
            if (!confirm('Deseja realmente tornar este município ou estado inacessível a esta organização?')) {
                return false;
            }
            var elem = $(this);
            // console.log(elem);
            var url = elem.attr('href');
            $.post(url, function(data){
                if (data.status === 200) {
                    const tr = elem.parents('tr');
                    elem.parents('.badge').remove();
                    tr.find('.cell_num_territorios').text(data.num_territorios);
                }
            });
        });
}

$(document).ready(function () {
    $('#id_estado').on('change', function(e) {
        $('#id_municipio').val(null).trigger('change');
        ajax_estado_id = ($('#id_estado').val())
            ? $('#id_estado').val()
            : '';
    });

    $('.adicionar_municipio_ou_estado').on('click', function(e) {
        e.preventDefault();
        const elem_modal = $('#municipio-visibilizar-modal');
        const tr = $(this).parents('tr');
        ajax_organizacao_id = $(tr).data('organizacao_id')
        $('#id_estado').val(null).trigger('change');
        $('#id_organizacao').val(ajax_organizacao_id);
        elem_modal.find('.modal-title span').text($(tr).data('organizacao'))
        elem_modal.modal('show');
    });

    $('#municipio-associar-submit').on('click', function(e) {
        e.preventDefault();
        const elem_form = $('#municipio-visibilizar-modal').find('form');
        const form_data = elem_form.serialize();
        $.post('', form_data, function( response ) {
            // console.log(response);
            if (response.status !== 200) {
                alert('Não foi possível associar a organização!\n\n' + response.content);
                return;
            }

            $('#municipio-visibilizar-modal').modal('hide');

            html = `<span class="badge badge-${response.tipo}">${response.nome}`;
            html += `<a href="${response.desassociar_url}" data-${response.tipo}_id="${response.id}" data-${response.tipo}="${response.nome}" class="organizacao_remover_${response.tipo}">`;
            html += `<i class="fa fa-times"></i>`;
            html += `</a></span>`
            $(html).appendTo(`tr[data-organizacao_id='${response.organizacao_id}'] .inner_cell_content`);
            $(`tr[data-organizacao_id='${response.organizacao_id}'] .cell_num_territorios`).text(response.num_territorios);
            initListenerBadges()
        });
    });

    $('#id_estado').select2({
        placeholder: "Escolha o estado...",
        ajax: {
            type: 'GET',
            url: function() {
                return `${ajax_estados_url}${ajax_organizacao_id}`;
            }
        },
        width: '100%',
        dropdownParent: $('#municipio-visibilizar-modal')
    });
    $('#id_municipio').select2({
        placeholder: "Todo o estado",
        ajax: {
            type: 'GET',
            url: function() {
                return `${ajax_municipios_url}${ajax_organizacao_id}/${ajax_estado_id}`;
            }
        },
        width: '100%',
        dropdownParent: $('#municipio-visibilizar-modal')
    });
    setTimeout(function() {
        initListenerBadges();
    }, 500)

    const spLayout = 'columns-3';
    const spColumns = [1]
    const options = {
        ...dataTableDefaultOptions,
        orderCellsTop: true,
        scrollY: '68vh',
        scrollCollapse: true,
        paging: false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ''
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ''
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'colvis',
                columns: ':not(.not-colvis)',
            }
        ],
        order: []
    };
    var organizacaoListTable = $('#organizacoes_list_table');
    setTimeout(function(){
        organizacaoListTable.DataTable(options);
    }, 5);
});
