const createMapImage = async () => {
    const width = 800;
    const height = 800;
    const mapElement = document.getElementById("map");
    const mapWrapperElement = document.getElementById("map_wrapper");
    domtoimage
        .toJpeg(mapElement, { width, height, quality: 0.8 })
        .then(function (dataURL) {
            mapWrapperElement.removeChild(mapElement);
            const imgElement = document.createElement("img");
            imgElement.src = dataURL;
            mapWrapperElement.appendChild(imgElement);
        });
};

$(document).ready(function () {
    const area = parseFloat(territorios[0].area.replace(",", "."));
    if (area) {
        $("#spanTerritorioArea").text(
            formataArea(area, UNIDADES_AREA.METRO_QUADRADO)
        );
    } else {
        $("#spanTerritorioArea").html(
            "<i class='small'>(Não há polígonos.)</i>"
        );
    }
    // const map = L.map('map').setView([51.505, -0.09], 13);
    const markersLayer = new L.LayerGroup();
    const territorioPoligonosLayer = new L.LayerGroup();
    const bounds = new L.latLngBounds();
    const map = L.map("map", {
        center: position_initial,
        zoom: 11,
        zoomControl: false,
        boxZoom: false,
        doubleClickZoom: false,
        dragging: false,
        trackResize: false,
        keyboard: false,
        scrollWheelZoom: false,
        touchZoom: false,
        zoomAnimation: false,
        fadeAnimation: false,
        markerZoomAnimation: false,
        animate: false,
    });
    if (MAPBOX_ACCESS_TOKEN) {
        L.tileLayer(
            `https://api.mapbox.com/styles/v1/dtygel/ckmu0up7v17wt17panh3u8d5e/tiles/{z}/{x}/{y}?access_token=${MAPBOX_ACCESS_TOKEN}`,
            {
                maxZoom: 19,
                attribution: '&copy; <a href="https://mapbox.com">MapBox</a>',
            }
        )
            .addTo(map)
            .on("load", function () {
                // createMapImage();
            });
    } else {
        L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
            maxZoom: 19,
            attribution:
                '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        }).addTo(map);
    }

    const adicionaMarker = (params) => {
        let marker = L.marker(
            {
                lon: params.lon,
                lat: params.lat,
            },
            {
                icon: params.icon,
            }
        );

        marker.feature = {
            type: "Feature",
            properties: {
                name: `${params.preTitulo} - ${params.nome}`,
                description: params.descricao,
                id: params.territorioId,
                "marker-color": params.preTitulo == "Uso" ? "#0d0" : "#d00",
                Comunidade: params.territorioNome,
                Tipo: params.tipoNome,
            },
        };

        marker.addTo(markersLayer);
    };

    for (let territorio of territorios) {
        for (let poligono of territorio.poligonos) {
            const polygonLayer = L.polygon(poligono, {
                territorioId: territorio.id,
            });
            polygonLayer.feature = {
                type: "Feature",
                properties: {
                    name: territorio.nome,
                    stroke: "#222222",
                    "stroke-opacity": 1.0,
                    "stroke-width": 1,
                    fill: "#0000ff",
                    "fill-opacity": 0.6,
                    id: territorio.id,
                    "Status da revisão": territorio.statusRevisao,
                    "Link para a comunidade no Dashboard":
                        SITE_URL + territorio.territorio_url,
                },
            };
            polygonLayer.addTo(territorioPoligonosLayer);
            bounds.extend(polygonLayer.getBounds());
        }

        for (let marker of territorio.conflitos) {
            const icon = L.icon({
                iconUrl: `${icone_url}/icone_conflito_${marker.tipo_conflito_id}_120x92.png`,
                iconSize: [32, 24],
            });
            adicionaMarker({
                descricao: marker.descricao,
                lon: marker.lng,
                lat: marker.lat,
                icon: icon,
                preTitulo: "Conflito",
                nome: marker.nome,
                tipoNome: marker.tipo_conflito_nome,
                territorioNome: territorio.nome,
                territorioId: territorio.id,
                territorioUrl: territorio.territorio_url,
            });
            bounds.extend({ lat: marker.lat, lon: marker.lng });
        }

        for (let marker of territorio.areasDeUso) {
            const icon = L.icon({
                iconUrl: `${icone_url}/icone_uso_${marker.tipo_area_de_uso_id}_100x120.png`,
                iconSize: [27, 32],
            });
            adicionaMarker({
                descricao: marker.descricao,
                lon: marker.lng,
                lat: marker.lat,
                icon: icon,
                preTitulo: "Uso",
                nome: marker.nome,
                tipoNome: marker.tipo_area_de_uso_nome,
                territorioNome: territorio.nome,
                territorioId: territorio.id,
                territorioUrl: territorio.territorio_url,
            });
            bounds.extend({ lat: marker.lat, lon: marker.lng });
        }
    }

    map.addLayer(markersLayer);
    map.addLayer(territorioPoligonosLayer);

    if (
        Object.keys(bounds).length > 0 &&
        bounds._southWest.lat != bounds._northEast.lat &&
        bounds._southWest.lng != bounds._northEast.lng
    ) {
        map.fitBounds(bounds);
    }
});
