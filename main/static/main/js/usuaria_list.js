function initListenerBadges() {
    $('.organizacao_remover_usuaria')
        .off('click')
        .on('click', function(e) {
            e.preventDefault();
            if (! confirm('Deseja realmente retirar esta/e usuária/o desta organização?')) {
                return false;
            }
            var elem = $(this);
            // console.log(elem);
            var url = elem.attr('href');
            $.post(url, function(data){
                if (data.status === 200) {
                    const trElem = elem.parents('tr');
                    elem.parents('.badge').remove();
                    numOrgs = trElem.find('td .badge').length;
                    if (numOrgs === 0 && trElem.find('.cell_perfil').text() !== 'Revisor/a') {
                        trElem.find('.cell_perfil').text('Cadastrante');
                    }
                }
            });
        });
}

function submit_form_associar(params) {
    $.post('', params, function( response ) {
        // console.log(response);
        if (response.status !== 200) {
            alert('Não foi possível associar a organização!\n\n' + response.content);
            return;
        }
        $('#organizacao-associar-modal').modal('hide');
        $('#loading-modal').modal('show');
        setTimeout(function() {
            location.reload();
        }, 600)

        // let organizacaoText = response.organizacao;
        // if (response.is_admin) {
        //     organizacaoText += ' <span class="small">(admin)</span>';
        // }
        //
        // const html = `<span class="badge badge-default organizacao-${response.organizacao_id}"><a href="${response.organizacao_update_url}">${organizacaoText}</a> <a href="${response.organizacao_remover_usuaria_url}" class="organizacao_remover_usuaria"><i class="fa fa-times-circle"></i></a></span>`;
        // $(html).appendTo(`tr[data-usuaria_id='${response.usuaria_id}'] .inner_cell_content`);
        // const perfil = (response.is_admin)
        //     ? 'Administrador/a de Organização'
        //     : 'Revisor/a';
        // $(`tr[data-usuaria_id='${response.usuaria_id}'] .cell_perfil`).text(perfil);
        //
        // initListenerBadges()
    });
}

let usuariaListTable;
$(document).ready(function () {
    $('.organizacao_adicionar_usuaria').on('click', function(e) {
        e.preventDefault();

        $('#organizacao-associar-modal form').each (function(){
            this.reset();
        });

        const elem_modal = $('#organizacao-associar-modal');
        const tr = $(this).parents('tr');
        $('#id_usuaria').val($(tr).data('usuaria_id'));
        elem_modal.find('.modal-title').text(`Usuária/o "${$(tr).data('usuaria')}"`)
        elem_modal.modal('show');
    });

    $('#organizacao-associar-submit').on('click', function(e) {
        e.preventDefault();

        const elem_form = $('#organizacao-associar-modal').find('form');
        const params = elem_form.serialize();

        const values_list = elem_form.serializeArray();
        const values_obj = {};
        for (let value of values_list) {
            values_obj[value.name] = value.value
        }
        values_obj['is_admin'] = (values_obj['perfil'] === 'is_admin');

        let organizacoes = [];
        $(`tr[data-usuaria_id='${values_obj.usuaria}'] .badge`).each(function(key, org) {
            organizacoes.push({
                id: $(org).data('organizacao_id'),
                nome: $(org).data('organizacao'),
                is_admin: ($(org).data('is_admin') == '1')
            });
        });
        const organizacoes_as_admin = organizacoes.filter(org => {
            return org.is_admin && org.id !== parseInt(values_obj.organizacao)
        });
        const organizacoes_as_reviewer = organizacoes.filter(org => {
            return !org.is_admin && org.id !== parseInt(values_obj.organizacao)
        });

        // If:
        // 1. It's her first organization association, .
        // 2. She will be reviewer, and isn't admin of any other organization,
        // 3. She will be admin, and isn't reviewer of any other organization,
        // Then no extended impact will be felt: go submit form!
        if (
            (organizacoes.length === 0) ||
            (!values_obj['is_admin'] && organizacoes_as_admin.length === 0) ||
            (values_obj['is_admin'] && organizacoes_as_reviewer.length === 0)
        ) {
            submit_form_associar(params);
            return;
        }

        const usuaria = $(`tr[data-usuaria_id='${values_obj.usuaria}']`).data('usuaria');
        const organizacao = $('#id_organizacao :selected').text();
        const verbo = (values_obj['is_admin'])
            ? "administrar"
            : "revisor/a de"
        $('#modal-to-reviewer').toggle(!values_obj['is_admin']);
        $('#modal-to-admin').toggle(values_obj['is_admin']);
        $('.modal_nome_usuaria').text(usuaria);
        $('.modal_nome_organizacao').text(organizacao);
        $('.modal_verbo').text(verbo);
        const list = (values_obj['is_admin'])
            ? organizacoes_as_reviewer
            : organizacoes_as_admin;
        const list_id = (values_obj['is_admin'])
            ? '#modal-to-admin ul'
            : '#modal-to-reviewer ul';
        for (let organizacao of list) {
            const li = `<li>${organizacao.nome}</li>`;
            $(list_id).append(li)
        };
        $('#organizacao-associar-confirma-submit').data('params', params);
        $('#organizacao-associar-modal').modal("hide");
        $('#alerta-modal').modal('show');
    });

    $('#organizacao-associar-confirma-submit').on('click', function(e) {
        $('#alerta-modal').modal('hide');
        submit_form_associar($(this).data('params'));
    });

    setTimeout(function() {
        initListenerBadges();
    }, 500);

    usuariaListTable = $('#usuarias_list_table');
    let spColumnsActive = [];
    let spColumns = [];
    let spColumnsOrg = [];
    let spColumnsComun = [];
    usuariaListTable.find('th').each(function(key, el) {
        if ($(el).hasClass('colHasSearchPane')) {
            spColumnsActive.push(key);
            spColumns.push(key);
        }
        if ($(el).hasClass('colHasSearchPaneOrg')) {
            spColumnsActive.push(key);
            spColumnsOrg.push(key);
        }
        if ($(el).hasClass('colHasSearchPaneComun')) {
            spColumnsActive.push(key);
            spColumnsComun.push(key);
        }
    });
    spLayout = `columns-${spColumnsActive.length}`;


    const hasOrgPane = ($('.colHasSearchPaneOrg').length > 0);
    let orgPaneOptions = () => {return []};
    if (hasOrgPane) {
        let organizacoes = [];
        $('#id_organizacao option').each(function(key, option) {
            const id = $(option).attr('value');
            organizacoes.push({
                id: id,
                nome: (id != "") ? $(option).text() : "<i>(nenhuma)</i>"
            });
        });
        // console.log(organizacoes);

        orgPaneOptions = () => {
            let options = [];
            for (let organizacao of organizacoes) {
                const option = {
                    label: organizacao.nome,
                    value: function(rowData, rowIdx) {
                        const idx = spColumnsOrg[0];
                        if (organizacao.id === "") {
                            return ($(rowData[idx]).text().trim() === "");
                        }
                        console.log($(rowData[idx]));
                        return false;
                        let hasOrg = false;
                        $(rowData[idx]).find('.badge').each(function(key, org) {
                            const organizacao_nome = $(org).text().replace('(admin)', '').trim();
                            if (organizacao_nome === organizacao.nome) {
                                hasOrg = true;
                                return;
                            }
                        })
                        return hasOrg;
                    }
                }
                options.push(option)
            }
            return options;
        };
    }

    const hasComunPane = ($('.colHasSearchPaneComun').length > 0);
    let comunPaneOptions = () => {return []};
    if (hasComunPane) {
        let territorios = [{
            id: "",
            nome: "<i>(nenhuma)</i>"
        }];
        $('.badge-comunidade, .badge-comunidade_em_revisao').each(function(key, territorio) {
            territorio_nome = $(territorio).text().trim();
            if (territorios.filter(terr => {return terr.nome === territorio_nome}).length === 0) {
                territorios.push({
                    id: key,
                    nome: territorio_nome
                });
            }
        });
        // console.log(territorios);

        comunPaneOptions = () => {
            let options = [];
            for (let territorio of territorios) {
                const option = {
                    label: territorio.nome,
                    value: function(rowData, rowIdx) {
                        const idx = spColumnsComun[0];
                        if (territorio.id === "") {
                            return ($(rowData[idx]).text().trim() === "");
                        }
                        let hasComun = false;
                        $(rowData[idx]).find('.badge-comunidade, .badge-comunidade_em_revisao').each(function(key, terr) {
                            const territorio_nome = $(terr).text().trim();
                            if (territorio_nome === territorio.nome) {
                                hasComun = true;
                                return;
                            }
                        })
                        return hasComun;
                    }
                }
                options.push(option)
            }
            return options;
        };
    }

    const options = {
        ...dataTableDefaultOptions,
        orderCellsTop: true,
        scrollY: '68vh',
        scrollCollapse: true,
        paging: false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'searchPanes',
                config: {
                    initCollapsed: false,
                    cascadePanes: false,
                    viewTotal: false,
                    layout: spLayout,
                    columns: spColumnsActive,
                    orderable: false,
                    collapse: true,
                    controls: true
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ''
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ''
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'colvis',
                columns: ':not(.not-colvis)',
            }
        ],
        columnDefs: [
            {
                targets: spColumns,
                searchPanes: {
                    show:true,
                    clearable: false,
                    orthogonal:'sp'
                },
                render: function (data, type, row, meta) {
                    if (['sp', 'filter'].includes(type)) {
                        data = $(`<i>${data}</i>`)
                            .text()
                            .trim();
                        if (data === '') {
                            data = '<i>(em branco)</i>';
                        }
                    }
                    return data;
                }
            },
            {
                targets: spColumnsOrg,
                searchPanes: {
                    show: true,
                    options: orgPaneOptions()
                }
            },
            {
                targets: spColumnsComun,
                searchPanes: {
                    show: true,
                    options: comunPaneOptions()
                }
            },
            {
                targets: '_all',
                visible: true,
                searchPanes: {
                    show: false
                }
            }
        ],
        order: []
    };
    setTimeout(function(){
        usuariaListTable.DataTable(options);
    }, 5);
});
