function init_desassociar_revisora_listeners() {
  $(document)
    .off("click", ".revisora_desassociar")
    .on("click", ".revisora_desassociar", function (evt) {
      evt.preventDefault();

      const tr = $(this).parents("tr");
      const a_link = tr.find(".cell_comunidade");
      const territorio = a_link.data("territorio");
      const revisora = a_link.data("revisora");
      const revisora_id = a_link.data("revisora_id");
      const textoConfirmacao =
        parseInt(revisora_id) === request_user_id
          ? `Deseja realmente deixar de revisar da comunidade "${territorio}"?`
          : `Deseja realmente desassociar o/a revisor/a ${revisora} da comunidade "${territorio}"?`;
      if (!confirm(textoConfirmacao)) {
        return false;
      }

      var elem = $(this);
      var url = elem.attr("href");
      $.post(url, function (data) {
        // console.log(data);
        if (data.status === 200) {
          territorioListDataTable.draw();
        }
      });
    });
}

function init_desassociar_organizacao_listeners() {
  $(document)
    .off("click", ".organizacao_desassociar")
    .on("click", ".organizacao_desassociar", function (evt) {
      evt.preventDefault();

      const tr = $(this).parents("tr");
      const a_link = tr.find(".cell_comunidade");
      const territorio = a_link.data("territorio");
      const organizacao = a_link.data("organizacao");
      if (
        !confirm(
          `Deseja realmente desassociar a comunidade "${territorio}" da organização ${organizacao}?`
        )
      ) {
        return false;
      }

      var elem = $(this);
      var url = elem.attr("href");
      $.post(url, function (data) {
        // console.log(data);
        if (data.status === 200) {
          territorioListDataTable.draw();
        }
      });
    });
}

$(document).ready(function () {
  // associar
  $(document).on("click", "td .revisora_associar", function (evt) {
    evt.preventDefault();
    const elem_modal = $("#revisora-associar-modal");
    const tr = $(this).parents("tr");
    const a_link = tr.find(".cell_comunidade");
    const territorio = a_link.data("territorio");
    const title = `Comunidade ${territorio}`;
    const territorio_id = a_link.data("territorio_id");
    elem_modal.find(".modal-title").text(title);
    elem_modal.find("#id_territorio").val(territorio_id);
    elem_modal.modal("show");
  });

  $(document).on("click", "td .organizacao_associar", function (evt) {
    evt.preventDefault();
    const elem_modal = $("#organizacao-associar-modal");
    const tr = $(this).parents("tr");
    const a_link = tr.find(".cell_comunidade");
    const territorio = a_link.data("territorio");
    const title = `Comunidade ${territorio}`;
    const territorio_id = a_link.data("territorio_id");
    elem_modal.find(".modal-title").text(title);
    elem_modal.find("#id_territorio").val(territorio_id);
    elem_modal.modal("show");
  });

  $("#revisora-associar-submit").click(function (evt) {
    evt.preventDefault();

    var elem_form = $("#revisora-associar-modal").find("form");
    var form_data = elem_form.serialize();

    $.post("", form_data, function (response) {
      if (response.status !== 200) {
        alert("Não foi possível associar a revisora!\n\n" + response.content);
        return;
      }

      elem_modal = $("#revisora-associar-modal");
      elem_modal.modal("hide");
      territorioListDataTable.draw();
    });
  });

  $("#organizacao-associar-submit").click(function (evt) {
    evt.preventDefault();

    var elem_form = $("#organizacao-associar-modal").find("form");
    var form_data = elem_form.serialize();

    $.post("", form_data, function (response) {
      if (response.status !== 200) {
        alert(
          "Não foi possível associar a organização!\n\n" + response.content
        );
        return;
      }

      elem_modal = $("#organizacao-associar-modal");
      elem_modal.modal("hide");
      territorioListDataTable.draw();
    });
  });

  const territorioListTable = $("#territorios_list_table");
  let spColumnsActive = [];
  let spColumns = [];
  let spColumnsTipos = [];
  territorioListTable.find("th").each(function (key, el) {
    if ($(el).hasClass("colHasSearchPane")) {
      spColumnsActive.push(key);
      spColumns.push(key);
    }
    if ($(el).hasClass("colHasSearchPaneTipos")) {
      spColumnsActive.push(key);
      spColumnsTipos.push(key);
    }
  });

  hasTiposPane = spColumnsTipos.length > 0;
  let tiposPaneOptions = () => {
    return [];
  };
  if (hasTiposPane) {
    let tipos = [
      {
        id: "",
        nome: "<i>(nenhuma)</i>",
      },
    ];
    $(".badge-tipo").each(function (key, tipo) {
      tipo_nome = $(tipo).text().trim();
      if (
        tipos.filter((tipo) => {
          return tipo.nome === tipo_nome;
        }).length === 0
      ) {
        tipos.push({
          id: key,
          nome: tipo_nome,
        });
      }
    });

    tiposPaneOptions = () => {
      let options = [];
      for (let tipo of tipos) {
        const option = {
          label: tipo.nome,
          value: function (rowData, rowIdx) {
            const idx = spColumnsTipos[0];
            if (tipo.id === "") {
              return $(rowData[idx]).text().trim() === "";
            }
            let hasTipo = false;
            $(rowData[idx]).each(function (key, el) {
              const tipo_nome = $(el).text().trim();
              if (tipo_nome === tipo.nome) {
                hasTipo = true;
                return;
              }
            });
            return hasTipo;
          },
        };
        options.push(option);
      }
      return options;
    };
  }

  const kwargs = [];
  if (viewFacet) {
    kwargs.push(`view_facet=${viewFacet}`);
  }
  if (organizacaoId) {
    kwargs.push(`organizacao_id=${organizacaoId}`);
  }
  const kwargsTxt = kwargs.length === 0 ? "" : `?${kwargs.join("&")}`;
  spLayout = `columns-${spColumnsActive.length}`;
  const options = {
    ...dataTableDefaultOptions,
    ajax: {
      url: DATATABLE_URL,
      type: "POST",
    },
    processing: true,
    serverSide: true,
    info: true,
    orderCellsTop: true,
    scrollY: false,
    scrollX: true,
    scrollCollapse: true,
    paging: true,
    dom: "Bfrtip",
    drawCallback: function () {
      init_desassociar_organizacao_listeners();
      init_desassociar_revisora_listeners();
    },
    buttons: [
      // {
      //   extend: "searchPanes",
      //   config: {
      //     initCollapsed: true,
      //     initCollapsed: true,
      //     cascadePanes: false,
      //     viewTotal: false,
      //     layout: spLayout,
      //     columns: spColumnsActive,
      //     orderable: false,
      //     collapse: false,
      //     clear: true,
      //   },
      // },
      {
        extend: "download",
        url: `/relatorios/datatable/territorios/export-all/${kwargsTxt}`,
        onDownload: (dt) => {
          dt.processing(false);
        },
      },
      // {
      //   extend: "excelHtml5",
      //   exportOptions: {
      //     columns: "",
      //   },
      // },
      // {
      //   extend: "csvHtml5",
      //   exportOptions: {
      //     columns: "",
      //   },
      // },
      // {
      //   extend: "pdfHtml5",
      //   title: "Lista de comunidades Tô no Mapa",
      //   messageTop:
      //     "Relatório gerado pelo Dashboard Tô no Mapa em " +
      //     new Date().toLocaleDateString(),
      //   orientation: "landscape",
      //   pageSize: "A4",
      //   customize: function (doc) {
      //     doc.styles.tableHeader.fontSize = 9;
      //     doc.defaultStyle.fontSize = 9;
      //   },
      //   exportOptions: {
      //     columns: ":visible",
      //   },
      // },
      {
        extend: "colvis",
        columns: ":not(.not-colvis)",
      },
    ],
    columnDefs: [
      {
        targets: "colStartHidden",
        visible: false,
      },
      // {
      //   targets: [...spColumns, ...spColumnsTipos],
      //   searchPanes: {
      //     show: true,
      //     clearable: false,
      //     orthogonal: "sp",
      //   },
      //   render: function (data, type, row, meta) {
      //     if (["sp", "filter"].includes(type)) {
      //       data = $(`<i>${data}</i>`).text().trim();
      //       if (data === "") {
      //         switch (meta.col) {
      //           case 1:
      //             data = "<i>(sem revisor/a)</i>";
      //             break;
      //           case 3:
      //             data = "<i>(nenhuma)</i>";
      //             break;
      //           case 5:
      //           case 6:
      //             data = "<i>(sem status)</i>";
      //             break;
      //         }
      //       }
      //     }
      //     return data;
      //   },
      // },
      // {
      //     targets: spColumnsTipos,
      //     searchPanes: {
      //         show: true,
      //         options: tiposPaneOptions(),
      //     },
      // },
    ],
  };

  // const options = {
  //   ajax: {
  //     url: DATATABLE_URL,
  //     type: "GET",
  //     // dataSrc: function (json) {
  //     //   console.log(json);
  //     //   const data = json.results.map((item) => {
  //     //     return Object.values(item);
  //     //   });
  //     //   console.log(data);
  //     //   return json.results;
  //     // },
  //     dataSrc: {
  //       data: "results",
  //       recordsTotal: "count",
  //       recordsFiltered: "count",
  //     },
  //   },
  //   columns: [
  //     { data: "id" },
  //     { data: "nome" },
  //     { data: "label" },
  //     { data: "criacao" },
  //   ],
  //   processing: true,
  //   serverSide: true,
  // };

  var datatable_form_data;

  $.fn.DataTable.Api.register("buttons.exportData()", function (options) {
    if (this.context.length) {
      var jsonResult = $.ajax({
        url: DATATABLE_URL + "export-all",
        type: "POST",
        async: false,
        data: datatable_form_data,
      });

      let header = [];
      let body = [];

      if ($(".dataTable:first th:visible").length > 0) {
        let activeColumns = [];
        $(".dataTable:first th:visible").each((i, el) => {
          activeColumns.push($(el).attr("data-idx"));
          header.push(el.innerHTML);
        });
        for (let rowAll of jsonResult.responseJSON.data) {
          let row = [];
          for (column_id of activeColumns) {
            row.push($.trim(rowAll[column_id]));
          }
          body.push(row);
        }
      } else {
        header = $("#territorios_list_table thead tr:first th")
          .map(function () {
            return this.innerHTML;
          })
          .get();
        body = jsonResult.responseJSON.data;
      }

      $(".dt-button-background, .dt-button-collection").hide();
      $(".dt-button").removeClass("processing");

      return {
        body: body,
        header: header,
      };
    }
  });

  territorioListDataTable = territorioListTable.DataTable(options);

  // Prepare search panes
  // territorioListDataTable.on(
  //   "buttons-action",
  //   function (e, buttonApi, dataTable, node, config) {
  //     button_name = buttonApi.text();
  //     if (button_name != "Filtros") return;

  //     if ($(".dtsp-emptyMessage:visible").length) {
  //       // datatables_ajax_url = dataTable.ajax
  //       //     .url()
  //       //     .replace('searchpanes_visibled=0', 'searchpanes_visibled=1')
  //       // dataTable.ajax.url(datatables_ajax_url).load()
  //       $(".dtsp-panesContainer").html(
  //         "Carregando filtros, por favor aguarde ..."
  //       );
  //     }

  //     return;
  //   }
  // );
});
