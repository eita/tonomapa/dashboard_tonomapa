function init_desassociar_revisora_listeners() {
  $(".revisora_desassociar")
    .off("click")
    .on("click", function (evt) {
      evt.preventDefault();

      const tr = $(this).parents("tr");
      const territorio = tr.data("territorio");
      const revisora = tr.data("revisora");
      const revisora_id = tr.data("revisora_id");
      const textoConfirmacao =
        parseInt(revisora_id) === request_user_id
          ? `Deseja realmente deixar de revisar da comunidade "${territorio}"?`
          : `Deseja realmente desassociar o/a revisor/a ${revisora} da comunidade "${territorio}"?`;
      if (!confirm(textoConfirmacao)) {
        return false;
      }

      var elem = $(this);
      var url = elem.attr("href");
      $.post(url, function (data) {
        // console.log(data);
        if (data.status === 200) {
          if (data.territorio_inacessivel) {
            elem.parents("tr").fadeOut(500, function () {
              territorioListDataTable.row(elem.parents("tr")).remove().draw();
            });
            return;
          }

          cell = elem.parents("td");
          link_associar = `<button class='revisora_associar btn btn-sm btn-default'><i class="fa fa-plus"></i></a>`;
          cell.find(".inner_cell_content").html(null);
          cell.find(".inner_cell_button").html(link_associar);
        }
      });
    });
}

function init_desassociar_organizacao_listeners() {
  $(".organizacao_desassociar")
    .off("click")
    .on("click", function (evt) {
      evt.preventDefault();

      const tr = $(this).parents("tr");
      const territorio = tr.data("territorio");
      const organizacao = tr.data("organizacao");
      if (
        !confirm(
          `Deseja realmente desassociar a comunidade "${territorio}" da organização ${organizacao}?`
        )
      )
        return false;

      var elem = $(this);
      var url = elem.attr("href");
      $.post(url, function (data) {
        // console.log(data);
        if (data.status === 200) {
          cell = elem.parents("td");
          link_associar = `<button class='organizacao_associar btn btn-sm btn-default'><i class="fa fa-plus"></i></a>`;
          cell.find(".inner_cell_content").html(null);
          cell.find(".inner_cell_button").html(link_associar);
        }
      });
    });
}

let territorioListDataTable;

$(document).ready(function () {
  // associar
  $("td").on("click", ".revisora_associar", function (evt) {
    evt.preventDefault();
    const elem_modal = $("#revisora-associar-modal");
    const tr = $(this).parents("tr");
    const territorio = tr.data("territorio");
    const title = `Comunidade ${territorio}`;
    const territorio_id = tr.data("territorio_id");
    elem_modal.find(".modal-title").text(title);
    elem_modal.find("#id_territorio").val(territorio_id);
    elem_modal.modal("show");
  });

  $("td").on("click", ".organizacao_associar", function (evt) {
    evt.preventDefault();
    const elem_modal = $("#organizacao-associar-modal");
    const tr = $(this).parents("tr");
    const territorio = tr.data("territorio");
    const title = `Comunidade ${territorio}`;
    const territorio_id = tr.data("territorio_id");
    elem_modal.find(".modal-title").text(title);
    elem_modal.find("#id_territorio").val(territorio_id);
    elem_modal.modal("show");
  });

  $("#revisora-associar-submit").click(function (evt) {
    evt.preventDefault();

    var elem_form = $("#revisora-associar-modal").find("form");
    var form_data = elem_form.serialize();

    $.post("", form_data, function (response) {
      if (response.status !== 200) {
        alert("Não foi possível associar a revisora!\n\n" + response.content);
      } else {
        elem_modal = $("#revisora-associar-modal");
        elem_modal.modal("hide");
        td = $(
          `tr[data-territorio_id='${response.territorio_id}'] td.cell_revisora`
        );
        const html_link_revisora =
          parseInt(response.revisora_id) === request_user_id
            ? `${response.revisora}`
            : `<a href='${response.revisora_update_url}'>${response.revisora}</a>`;
        td.find(".inner_cell_content").html(html_link_revisora);
        const html_link_desassociar = `<a href='${response.revisora_desassociar_url}' class='revisora_desassociar btn btn-sm btn-remove'><i class="fa fa-minus"></i></a>`;
        td.find(".inner_cell_button").html(html_link_desassociar);
        init_desassociar_revisora_listeners();
      }
    });
  });

  $("#organizacao-associar-submit").click(function (evt) {
    evt.preventDefault();

    var elem_form = $("#organizacao-associar-modal").find("form");
    var form_data = elem_form.serialize();

    $.post("", form_data, function (response) {
      if (response.status !== 200) {
        alert(
          "Não foi possível associar a organização!\n\n" + response.content
        );
      } else {
        elem_modal = $("#organizacao-associar-modal");
        elem_modal.modal("hide");
        td = $(
          `tr[data-territorio_id='${response.territorio_id}'] td.cell_organizacao`
        );
        const html_link_organizacao = `<a href='${response.organizacao_update_url}'>${response.organizacao}</a>`;
        td.find(".inner_cell_content").html(html_link_organizacao);
        const html_link_desassociar = `<a href='${response.organizacao_desassociar_url}' class='organizacao_desassociar btn btn-sm btn-remove'><i class="fa fa-minus"></i></a>`;
        td.find(".inner_cell_button").html(html_link_desassociar);
        init_desassociar_organizacao_listeners();
      }
    });
  });

  setTimeout(function () {
    init_desassociar_revisora_listeners();
    init_desassociar_organizacao_listeners();
  }, 500);

  const territorioListTable = $("#territorios_list_table");
  let spColumnsActive = [];
  let spColumns = [];
  let spColumnsTipos = [];
  territorioListTable.find("th").each(function (key, el) {
    if ($(el).hasClass("colHasSearchPane")) {
      spColumnsActive.push(key);
      spColumns.push(key);
    }
    if ($(el).hasClass("colHasSearchPaneTipos")) {
      spColumnsActive.push(key);
      spColumnsTipos.push(key);
    }
  });
  spLayout = `columns-${spColumnsActive.length}`;

  hasTiposPane = spColumnsTipos.length > 0;
  let tiposPaneOptions = () => {
    return [];
  };
  if (hasTiposPane) {
    let tipos = [
      {
        id: "",
        nome: "<i>(nenhuma)</i>",
      },
    ];
    $(".badge-tipo").each(function (key, tipo) {
      tipo_nome = $(tipo).text().trim();
      if (
        tipos.filter((tipo) => {
          return tipo.nome === tipo_nome;
        }).length === 0
      ) {
        tipos.push({
          id: key,
          nome: tipo_nome,
        });
      }
    });

    tiposPaneOptions = () => {
      let options = [];
      for (let tipo of tipos) {
        const option = {
          label: tipo.nome,
          value: function (rowData, rowIdx) {
            const idx = spColumnsTipos[0];
            if (tipo.id === "") {
              return $(rowData[idx]).text().trim() === "";
            }
            let hasTipo = false;
            $(rowData[idx]).each(function (key, el) {
              const tipo_nome = $(el).text().trim();
              if (tipo_nome === tipo.nome) {
                hasTipo = true;
                return;
              }
            });
            return hasTipo;
          },
        };
        options.push(option);
      }
      return options;
    };
  }

  const options = {
    ...dataTableDefaultOptions,
    orderCellsTop: true,
    scrollY: "62vh",
    scrollX: true,
    scrollCollapse: true,
    paging: true,
    dom: "Bfrtip",
    buttons: [
      {
        extend: "searchPanes",
        config: {
          initCollapsed: false,
          cascadePanes: false,
          viewTotal: false,
          layout: spLayout,
          columns: spColumnsActive,
          orderable: false,
          collapse: false,
          clear: true,
        },
      },
      {
        extend: "excelHtml5",
        exportOptions: {
          columns: "",
        },
      },
      {
        extend: "csvHtml5",
        exportOptions: {
          columns: "",
        },
      },
      {
        extend: "pdfHtml5",
        title: "Lista de comunidades Tô no Mapa",
        messageTop:
          "Relatório gerado pelo Dashboard Tô no Mapa em " +
          new Date().toLocaleDateString(),
        orientation: "landscape",
        pageSize: "A4",
        customize: function (doc) {
          doc.styles.tableHeader.fontSize = 9;
          doc.defaultStyle.fontSize = 9;
        },
        exportOptions: {
          columns: ":visible",
        },
      },
      {
        extend: "colvis",
        columns: ":not(.not-colvis)",
      },
    ],
    columnDefs: [
      {
        targets: "colStartHidden",
        visible: false,
      },
      {
        targets: spColumns,
        searchPanes: {
          show: true,
          clearable: false,
          orthogonal: "sp",
        },
        render: function (data, type, row, meta) {
          if (["sp", "filter"].includes(type)) {
            data = $(`<i>${data}</i>`).text().trim();
            if (data === "") {
              switch (meta.col) {
                case 1:
                  data = "<i>(sem revisor/a)</i>";
                  break;
                case 3:
                  data = "<i>(nenhuma)</i>";
                  break;
                case 5:
                case 6:
                  data = "<i>(sem status)</i>";
                  break;
              }
            }
          }
          return data;
        },
      },
      {
        targets: spColumnsTipos,
        searchPanes: {
          show: true,
          options: tiposPaneOptions(),
        },
      },
    ],
    order: [],
  };
  setTimeout(function () {
    territorioListDataTable = territorioListTable.DataTable(options);
  }, 5);
});
