function fill_notification_list_customized(data) {
  var menus = document.getElementsByClassName(notify_menu_class);
  if (menus) {
    var messages = get_messages(data);
    if (!messages) {
      messages += get_empty_message(data);
    }
    messages += get_footer(data)

    for (var i = 0; i < menus.length; i++){
      menus[i].innerHTML = messages;
    }
  }
}

function get_messages(data) {
    var messages = data.unread_list.map(function (item) {
      var message = "";

      message += get_header(item);
      message += get_message(item);

      return get_tag(item, message);
    }).join('')
	return messages;
}

function get_header(item) {
  message = get_actor(item);
  message += get_timestamp(item);
  return '<span>' + message + '</span>';
}

function get_actor(item) {
  if(typeof item.actor !== 'undefined'){
    return '<span>' + item.actor + '</span>';
  }
  return '';
}

function get_timestamp(item) {
  if(typeof item.timestamp !== 'undefined'){
    return '<span class="time">' + $.timeago(item.timestamp) + '</span>';
  }
  return '';
}

function get_message(item) {
  return get_verb(item) + get_description(item);
}

function get_verb(item) {
  if(typeof item.verb !== 'undefined'){
    return '<span class="message">' + item.verb + '</span>';
  }
  return '';
}

function get_description(item) {
  if(typeof item.description !== 'undefined' && item.description){
    return '<i class="message">' + item.description + '</i>';
  }
  return '';
}

function get_tag(item, message) {
  var target = get_target(item);
  if(! target){
    return '<li><a>' + message + '</a></li>';
  }
  return '<li><a href="'+target+'?notify=' + item.id + '">' + message + '</a></li>';
}

function get_target(item) {
  if(typeof item.data == 'undefined' || item.data == null){
    return '';
  }
  if(typeof item.data.target_url == 'undefined'){
    return '';
  }

  return item.data.target_url;
}

function get_footer(data) {
  return '<li>\
			<div class="text-center">\
              <a href="/inbox/notifications/">\
				<strong>Ver todas notificações</strong>\
				<i class="fa fa-angle-right"></i>\
			  </a>\
			</div>\
		  </li>';
}

function get_empty_message(data){
  return '<li>\
			<div class="text-center">\
              <i>Nenhuma novidade!</i>\
			</div>\
		  </li>';
}
