$(document).ready(function () {
    // associar
    $("table").on("click", ".object_associar", function (evt) {
        evt.preventDefault();

        var elem_modal = $("#revisora-associar-modal");
        var title = $(this).data("title");
        var object_id = $(this).data("object_id");

        elem_modal.find(".modal-title").text(title);
        elem_modal.find("#id_object_id").val(object_id);
        // TODO reset select2 on open modal
        elem_modal.modal("show");
    });

    $("#revisora-associar-submit").click(function (evt) {
        evt.preventDefault();

        var elem_form = $("#revisora-associar-modal").find("form");
        var form_data = elem_form.serialize();

        $.post("", form_data, function (response) {
            console.log(response);
            if (response.status !== 200) {
                alert(
                    "Não foi possível associar a revisora!\n\n" +
                        response.content
                );
            } else {
                elem_modal = $("#revisora-associar-modal");
                elem_modal.modal("hide");

                elem = $("a[data-object_id='" + response.object_id + "']");
                cell = elem.parents("td");
                link_revisora =
                    '<a href="' +
                    response.revisora_update_url +
                    '">' +
                    response.revisora +
                    "</a> ";
                link_desassociar =
                    '(<a href="' +
                    response.object_desassociar_url +
                    '" class="object_desassociar">desassociar</a>)';
                cell.html(link_revisora + link_desassociar);
            }
        });
    });

    // desassociar
    $("table").on("click", ".object_desassociar", function (evt) {
        evt.preventDefault();

        if (!confirm("Deseja realmente desassociar a revisora?")) return false;

        var elem = $(this);
        var url = elem.attr("href");
        $.post(url, function (data) {
            console.log(data);
            if (data.status == 200) {
                cell = elem.parents("td");
                link_associar =
                    '<a href="#" data-title="' +
                    data.title +
                    '" \
        data-object_id="' +
                    data.object_id +
                    '" class="object_associar">associar revisor(a)</a></a>';
                cell.html(link_associar);
            }
        });
    });
});
