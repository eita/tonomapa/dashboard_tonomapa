import rules
from main.models import OrganizacaoUsuaria
from main.utils import get_territorios_visiveis


@rules.predicate
def eh_superusuario(usuaria):
    return usuaria.is_superuser


@rules.predicate
def eh_admin(usuaria):
    if not usuaria.is_authenticated:
        return False
    return usuaria.is_admin


eh_admin_geral = rules.is_group_member("admin")


@rules.predicate
def eh_anonimo(usuaria):
    return usuaria.is_anonymous


@rules.predicate
def eh_admin_da_organizacao(usuaria, organizacao):
    if not organizacao or usuaria.is_anonymous:
        return False
    try:
        # pylint: disable=unused-variable
        organizacao_usuaria = OrganizacaoUsuaria.objects.get(
            usuaria_id=usuaria.id, organizacao_id=organizacao.id, is_admin=True
        )
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


@rules.predicate
def eh_admin_de_alguma_organizacao(usuaria):
    if usuaria.is_anonymous:
        return False
    return not usuaria.is_reviewer and len(usuaria.organizacoes_que_administra_ids) > 0


@rules.predicate
def eh_integrante_de_alguma_organizacao(usuaria):
    if usuaria.is_anonymous:
        return False
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id)
    return ous.count() > 0


@rules.predicate
def eh_integrante_da_organizacao(usuaria, organizacao):
    if not organizacao or usuaria.is_anonymous:
        return False
    try:
        organizacoes = usuaria.organizacoes.filter(organizacao_id=organizacao.id)
        return organizacoes.count() > 0
    except:
        return False


@rules.predicate
def pode_visualizar_territorio(usuaria, territorio):
    if not territorio or usuaria.is_anonymous:
        return False
    try:
        territorio = get_territorios_visiveis(usuaria).get(pk=territorio.id)
        return True
    except:
        return False


@rules.predicate
def eh_revisora_do_territorio(usuaria, territorio):
    if not territorio or usuaria.is_anonymous:
        return False
    return usuaria == territorio.revisora


@rules.predicate
def eh_cadastrante_do_territorio(usuaria, territorio):
    if not territorio or usuaria.is_anonymous:
        return False
    return usuaria.eh_cadastrante_do_territorio(territorio)


@rules.predicate
def eh_parceira(usuaria):
    return (
        False
        if usuaria.is_anonymous or usuaria.is_parceira is None
        else usuaria.is_parceira
    )


@rules.predicate
def eh_revisora(usuaria):
    return (
        False
        if usuaria.is_anonymous or usuaria.is_reviewer is None
        else usuaria.is_reviewer
    )


rules.add_perm("admin_geral", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm("organizacao.ver_todos", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm("organizacao.cadastrar", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm(
    "organizacao.alterar",
    eh_superusuario | eh_admin | eh_admin_geral | eh_admin_da_organizacao,
)
rules.add_perm(
    "organizacao.visualizar",
    eh_superusuario | eh_admin | eh_admin_geral | eh_integrante_da_organizacao,
)
rules.add_perm("organizacao.apagar", eh_superusuario | eh_admin | eh_admin_geral)

rules.add_perm(
    "territorio.ver_todos", eh_superusuario | eh_admin | eh_admin_geral | eh_parceira
)
rules.add_perm("territorio.cadastrar", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm("territorio.alterar", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm(
    "territorio.visualizar",
    eh_superusuario
    | eh_admin
    | eh_admin_geral
    | eh_parceira
    | pode_visualizar_territorio,
)
rules.add_perm("territorio.revisora", eh_revisora_do_territorio)
rules.add_perm("territorio.apagar", eh_superusuario | eh_admin | eh_admin_geral)
rules.add_perm(
    "territorio.status_revisao.alterar",
    eh_superusuario | eh_admin | eh_admin_geral | eh_revisora_do_territorio,
)

rules.add_perm(
    "permissao_padrao",
    eh_superusuario
    | eh_admin
    | eh_admin_geral
    | eh_parceira
    | eh_revisora
    | eh_admin_de_alguma_organizacao,
)

rules.add_perm(
    "usuaria.listar",
    eh_superusuario | eh_admin | eh_admin_geral | eh_admin_de_alguma_organizacao,
)
rules.add_perm(
    "usuaria.administrar",
    eh_superusuario | eh_admin | eh_admin_geral | eh_admin_de_alguma_organizacao,
)
rules.add_perm(
    "usuaria.administrar_organizacao",
    eh_superusuario | eh_admin | eh_admin_geral | eh_admin_de_alguma_organizacao,
)
