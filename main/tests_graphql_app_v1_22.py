import json

from graphene_django.utils.testing import GraphQLTestCase

from dashboard_tonomapa.schema import schema
from main.models import Usuaria, Estado, Municipio, Territorio, TerritorioUsuaria, TerritorioStatus


class ToNoMapaGraphQLTestCase(GraphQLTestCase):
    fixtures = ['territorio_status.json', ]

    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = '/graphql'

    # setUp
    def create_dummy_estado(self):
        e = Estado()
        e.id = 31
        e.nome = "Minas Gerais"
        e.uf = "MG"
        e.save()
        return e

    def create_dummy_municipio(self, estado):
        m = Municipio()
        m.id = 3107208
        m.nome = "Bocaina de Minas"
        m.capital = False
        m.latitude = "-22.16970"
        m.longitude = "-44.39720"
        m.estado = estado
        m.save()
        return m

    def create_dummy_usuaria(self):
        user = Usuaria.objects.create_user('vinicius', 'vinicius@eita.org.br', '***1 Password***')
        user.first_name = 'Vinicius'
        user.last_name = 'Brand'
        user.save()
        return user

    def create_empty_territorio(self):
        t = Territorio()
        t.municipio_referencia = self.municipio
        t.nome = "Territorio de Teste"
        t.status = TerritorioStatus.objects.get(pk=TerritorioStatus.EM_PREENCHIMENTO)
        t.save()
        return t

    def create_territorio_usuaria(self, territorio, usuaria):
        tu = TerritorioUsuaria()
        tu.territorio = territorio
        tu.usuaria = usuaria
        tu.save()
        return tu

    def setUp(self):
        self.estado = self.create_dummy_estado()
        self.municipio = self.create_dummy_municipio(self.estado)
        self.dummy_usuaria = self.create_dummy_usuaria()
        self.dummy_territorio = self.create_empty_territorio()
        self.create_territorio_usuaria(self.dummy_territorio, self.dummy_usuaria)

    # GraphQL queries
    LISTA_TERRITORIOS_FIELDS = '''
        id,
        nome,
        municipioReferenciaId,
    '''

    def get_lista_territorios_data(self, id_usuaria):
        l = Usuaria.objects.all()
        i = 1

        return f"""
        {{
            territorios(idUsuaria:{id_usuaria}) {{
                {self.LISTA_TERRITORIOS_FIELDS}
            }}
        }}
        """

    USUARIA_FIELDS = '''
        __typename,
        id,
        username,
        fullName,
        codigoUsoCelular,
        cpf,
        organizacoes {
            __typename,
            nome
        },
        '''

    TERRITORIO_FIELDS = '''
        __typename,
        id,
        nome,
        anoFundacao,
        qtdeFamilias,
        statusId,
        tiposComunidade {
            __typename,
            id
        },
        municipioReferenciaId,
        poligono,
        conflitos {
            __typename,
            id,
            nome,
            posicao,
            descricao,
            tipoConflitoId
        },
        areasDeUso {
            __typename,
            id,
            nome,
            posicao,
            descricao,
            tipoAreaDeUsoId
        },
        anexos {
            __typename,
            id,
            nome,
            descricao,
            tipoAnexo,
            mimetype,
            fileSize,
            arquivo,
            thumb,
            criacao
        },
        mensagens {
            __typename,
            id,
            texto,
            extra,
            dataEnvio,
            dataRecebimento,
            originIsDevice
        },
        publico
        '''

    def get_territorio(self, id_territorio):
        return f"""
            {{
                currentUser {{
                    {self.USUARIA_FIELDS},
                    currentTerritorio(id:{id_territorio}) {{
                        {self.TERRITORIO_FIELDS}
                    }}
                }}
            }}
        """

    @property
    def GET_USER_DATA_QUERY(self):
        return f"""
            currentUser {{
                {self.USUARIA_FIELDS},
                currentTerritorio {{
                    {self.TERRITORIO_FIELDS}
                }}
            }}
        """

    @property
    def GET_USER_DATA(self):
        return "{" + self.GET_USER_DATA_QUERY + "}"

    @property
    def SEND_USER_DATA(self):
        return f"""
            mutation updateAppData($currentUser: UsuariaInput){{
                updateAppData (input: {{currentUser: $currentUser}}) {{
                    token,
                    errors,
                    success,
                    {self.GET_USER_DATA_QUERY}
                }}
            }}
            """

    # SETTINGS graphql query is local to the app

    def test_get_lista_territorios_data(self):
        response = self.query(
            self.get_lista_territorios_data(self.dummy_usuaria.id),
            op_name="territorios"
        )

        content = json.loads(response.content)

        # This validates the status code and if you get errors
        self.assertResponseNoErrors(response)

    def test_get_territorio(self):
        response = self.query(
            self.get_territorio(1),
            op_name="currentUser"
        )

        content = json.loads(response.content)

        # This validates the status code and if you get errors
        self.assertResponseNoErrors(response)

    def test_GET_USER_DATA(self):
        response = self.query(
            self.GET_USER_DATA,
            op_name="currentUser"
        )

        content = json.loads(response.content)

        # This validates the status code and if you get errors
        self.assertResponseNoErrors(response)

    def test_SEND_USER_DATA(self):
        response = self.query(
            self.SEND_USER_DATA,
            op_name="updateAppData"
        )

        content = json.loads(response.content)

        # This validates the status code and if you get errors
        self.assertResponseNoErrors(response)
