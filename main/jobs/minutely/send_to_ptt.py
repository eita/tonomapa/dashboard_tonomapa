import shutil
import uuid
import datetime
import logging
from pathlib import Path

from django.core.files import File
from django_extensions.management.jobs import MinutelyJob
from django.conf import settings

from main.models import Anexo, Territorio, TerritorioStatus
from main.utils import PTTUtils, is_valid_uuid

logger = logging.getLogger("EnvioPTT")


class Job(MinutelyJob):

    help = "Send territorios to ptt"

    def execute(self):
        territorios = Territorio.objects.filter(
            status_id=TerritorioStatus.ENVIO_PTT_NAFILA
        )
        t_files_tosend = Territorio.objects.filter(
            status_id=TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA
        )

        if territorios.count() > 0 or t_files_tosend.count() > 0:
            print(datetime.datetime.now())

        if territorios.count() > 0:
            logger.info(
                f" {territorios.count()} comunidade(s) a enviar",
            )
        # First, convert filenames to hash if necessary, and add empty pdf file to FONTE_INFORMACAO if there is none:
        for territorio in territorios:
            qtde_fontes_informacao = territorio.get_anexos_informacoes_ptt().count()
            if qtde_fontes_informacao == 0:
                try:
                    shutil.copyfile(
                        f"{settings.MEDIA_ROOT}/_empty_FONTE_INFORMACAO.pdf",
                        f"{settings.MEDIA_ROOT}/empty_FONTE_INFORMACAO.pdf",
                    )
                    path = Path(f"{settings.MEDIA_ROOT}/empty_FONTE_INFORMACAO.pdf")
                    with path.open(mode="rb") as f:
                        arquivo = File(f, name=path.name)
                        anexo = Anexo.objects.create(
                            nome="Arquivo vazio do tipo FONTE_INFORMACAO",
                            descricao="Gerado automaticamente pelo app Tô no Mapa",
                            tipo_anexo="FONTE_INFORMACAO",
                            arquivo=arquivo,
                        )
                        territorio.anexos.add(anexo)
                except Exception as error:
                    print(
                        f"Erro no territorio '{territorio.nome} ({territorio.id})': Não encontrei o arquivo vazio em pdf 'empty_FONTE_INFORMACAO' para conseguir enviar os dados à PTT!"
                    )
                    print(error)

            for anexo in territorio.get_anexos_ptt():
                new_anexo = anexo.rename_arquivo_for_ptt()
                if new_anexo == False:
                    print(
                        f"Erro no anexo '{anexo.nome}' ({anexo.arquivo.path}) do territorio '{territorio.nome} ({territorio.id})': Não encontrei o anexo!"
                    )

        # Then, send:
        for territorio in territorios:
            try:
                print(f"   * {territorio.nome}... ", end=" ")
                result_code = PTTUtils.send_territorio_to_ptt(territorio)
                if result_code == 200:
                    logger.info(
                        f"dados da comunidade {territorio.nome} ({territorio.id}) enviados com sucesso"
                    )
                else:
                    print(
                        f"erro ao enviar dados da comunidade {territorio.nome} ({territorio.id}): servidor retornou status {result_code}"
                    )
                if territorio.status_id == TerritorioStatus.ENVIO_ARQUIVOS_PTT_NAFILA:
                    print("     - arquivos... ", end=" ")
                    result_code = PTTUtils.send_territorio_files_to_ptt(territorio)
                    if result_code == 200:
                        print("arquivos enviados")
                    else:
                        logger.error(
                            f"erro ao enviar arquivos da comunidade {territorio.nome} ({territorio.id}): servidor retornou status {result_code}"
                        )
            except Exception as error:
                logger.error("EXCEPTION: " + str(error))

        if t_files_tosend.count() > 0:
            logger.info(
                f" {t_files_tosend.count()} comunidade(s) a enviar (apenas arquivos)"
            )
        for territorio in t_files_tosend:
            try:
                print(f"   * {territorio.nome}... ", end=" ")
                result_code = PTTUtils.send_territorio_files_to_ptt(territorio)
                if result_code == 200:
                    logger.info(
                        f"arquivos da comunidade {territorio.nome} ({territorio.id}) enviados com sucesso"
                    )
                else:
                    logger.error(
                        f"erro ao enviar arquivos da comunidade {territorio.nome} ({territorio.id}): servidor retornou status {result_code}"
                    )
            except Exception as error:
                print("EXCEPTION: " + str(error))
