import json

from deepdiff import DeepDiff
from django.test import TestCase

from main.models import Territorio, Estado, Municipio, Usuaria
from main.utils import PTTUtils


class TonomapaTestCase(TestCase):
    def setUp(self):
        pass

    def test_testing_is_working(self):
        self.assertEqual(1, 1)


class TerritorioTestCase(TestCase):

    def create_dummy_estado(self):
        e = Estado()
        e.id = 31
        e.nome = "Minas Gerais"
        e.uf = "MG"
        e.save()
        return e

    def create_dummy_municipio(self, estado):
        m = Municipio()
        m.id = 3107208
        m.nome = "Bocaina de Minas"
        m.capital = False
        m.latitude = "-22.16970"
        m.longitude = "-44.39720"
        m.estado = estado
        m.save()
        return m

    def create_dummy_usuaria(self):
        user = Usuaria.objects.create_user('vinicius', 'vinicius@eita.org.br', '***1 Password***')
        user.first_name = 'Vinicius'
        user.last_name = 'Brand'
        user.save()
        return user

    def new_empty_territorio(self):
        t = Territorio()
        t.municipio_referencia = self.municipio
        t.nome = "Territorio de Teste"
        t.save()
        return t

    def setUp(self):
        self.estado = self.create_dummy_estado()
        self.municipio = self.create_dummy_municipio(self.estado)
        self.dummy_usuaria = self.create_dummy_usuaria()

    def test_serialize_empty_territorio_to_ptt(self):
        t = self.new_empty_territorio()
        serialized = PTTUtils.serialize_territorio(t)
        i = 1
        expected = '{"nome":"Territorio de Teste","cadastrante":null,"idMunicipio":3107208,"privacidade":"PRIVADO","uf":"MG","areaEstimada":null,"cep":null,"descricaoAcesso":null,"descricaoAcessoPrivacidade":false,"zonaLocalizacao":null,"outraZonaLocalizacao":null,"autoIdentificacao":null,"geometrias":null,"segmento":"","historia":{"descricao":null,"iconeSegmento":null,"arquivosInformacoes":[],"arquivosMidias":[]},"enderecoContato":{"nomeContato":null,"logradouro":null,"numero":null,"complemento":null,"bairro":null,"cep":null,"caixaPostal":null,"email":null,"telefone":null,"idMunicipio":null,"uf":null}}'

        diff = DeepDiff(json.loads(serialized), json.loads(expected))
        self.assertEqual(diff, {})

    # def test_serialize_filled_territorio_to_ptt(self):
    #     t = self.new_empty_territorio()
    #     t.usuarias.add(self.dummy_usuaria)
    #     serialized = PTTUtils.serialize_territorio(t)
    #     print(serialized)
