import json
import os.path

# from django.core import serializers
from rest_framework import serializers as rest_serializers

from relatorios.models import TerritorioAgregado
from .utils import get_model


class AnexoSerializer(rest_serializers.ModelSerializer):
    thumb = rest_serializers.ImageField(read_only=True)

    class Meta:
        model = get_model("Anexo")
        fields = (
            "id",
            "nome",
            "descricao",
            "mimetype",
            "file_size",
            "arquivo",
            "tipo_anexo",
            "territorio",
            "criacao",
            "ultima_alteracao",
            "thumb",
            "publico",
        )


class PTTAnexoSerializer(rest_serializers.ModelSerializer):
    privacidade = rest_serializers.SerializerMethodField()
    hashArquivo = rest_serializers.SerializerMethodField("get_arquivo_name")
    tipoAnexo = rest_serializers.CharField(source="tipo_anexo")

    class Meta:
        model = get_model("Anexo")
        fields = ("nome", "descricao", "privacidade", "tipoAnexo", "hashArquivo")

    def get_privacidade(self, obj):
        return "PUBLICO" if obj.publico else "PRIVADO"

    def get_arquivo_name(self, obj):
        if obj.arquivo is not None:
            return os.path.basename(obj.arquivo.name)
        else:
            return None


# {
#         "nome": "video_Festival.mp4",
#         "privacidade": "PUBLICO",
#         "descricao": "Video do dia do festival em comemoração aos 250 anos do povoado",
#         "hashArquivo": "hash2314whbt23sd123gdfsd4s5dfg445wed.mp4",
#         "tipoAnexo": "ANEXO"
#       }


class PTTEnderecoContatoSerializer(rest_serializers.ModelSerializer):
    nomeContato = rest_serializers.CharField(source="ec_nome_contato")
    logradouro = rest_serializers.CharField(source="ec_logradouro")
    numero = rest_serializers.CharField(source="ec_numero")
    complemento = rest_serializers.CharField(source="ec_complemento")
    bairro = rest_serializers.CharField(source="ec_bairro")
    cep = rest_serializers.CharField(source="ec_cep")
    caixaPostal = rest_serializers.CharField(source="ec_caixa_postal")
    email = rest_serializers.CharField(source="ec_email")
    telefone = rest_serializers.CharField(source="ec_telefone")
    idMunicipio = rest_serializers.SerializerMethodField("get_id_municipio")
    uf = rest_serializers.SerializerMethodField("get_uf")

    class Meta:
        model = get_model("Territorio")
        fields = [
            "nomeContato",
            "logradouro",
            "numero",
            "complemento",
            "bairro",
            "cep",
            "caixaPostal",
            "email",
            "telefone",
            "idMunicipio",
            "uf",
        ]

    def get_id_municipio(self, obj):
        return obj.ec_municipio.id if obj.ec_municipio is not None else None

    def get_uf(self, obj):
        return obj.ec_municipio.estado.uf if obj.ec_municipio is not None else None


#     "enderecoContato": {
#       "nomeContato": "Maria das Dores",
#       "logradouro": "Rua Dr. Pedro Álvares",
#       "numero": "125A",
#       "complemento": "Próximo ao ponto da Petrobrás",
#       "bairro": "Centro",
#       "cep": "37200-000",
#       "caixaPostal": "11111",
#       "idMunicipio": 3138203,
#       "uf": "MG",
#       "email": "maria@exemplo.com",
#       "telefone": "(35) 99911-9911"
#     }
# }


class PTTCadastranteSerializer(rest_serializers.ModelSerializer):
    cpfCnpj = rest_serializers.CharField(source="cpf")
    nome = rest_serializers.SerializerMethodField("get_nome")

    class Meta:
        model = get_model("Usuaria")
        fields = ["email", "nome", "cpfCnpj"]

    def get_nome(self, obj):
        return obj.get_full_name()


class PTTHistoriaSerializer(rest_serializers.ModelSerializer):
    descricao = rest_serializers.CharField(source="historia_descricao")
    iconeSegmento = PTTAnexoSerializer(read_only=True, source="get_anexo_icone_ptt")
    arquivosInformacoes = PTTAnexoSerializer(many=True, read_only=True, source="get_anexos_informacoes_ptt")
    arquivosMidias = PTTAnexoSerializer(many=True, read_only=True, source="get_anexos_midias_ptt")

    class Meta:
        model = get_model("Territorio")
        fields = ["descricao", "iconeSegmento", "arquivosInformacoes", "arquivosMidias"]


class PTTSerializer(rest_serializers.ModelSerializer):
    nome = rest_serializers.CharField()
    cadastrante = PTTCadastranteSerializer(read_only=True, source="main_cadastrante")
    idMunicipio = rest_serializers.SerializerMethodField("get_id_municipio")
    privacidade = rest_serializers.SerializerMethodField()
    uf = rest_serializers.SerializerMethodField()
    areaEstimada = rest_serializers.SerializerMethodField("get_area")
    cep = rest_serializers.CharField()  # criado
    descricaoAcesso = rest_serializers.CharField(source="descricao_acesso")  # criado
    descricaoAcessoPrivacidade = rest_serializers.BooleanField(source="descricao_acesso_privacidade")  # criado
    zonaLocalizacao = rest_serializers.CharField(source="zona_localizacao")  # criado
    outraZonaLocalizacao = rest_serializers.CharField(source="outra_zona_localizacao")  # criado
    autoIdentificacao = rest_serializers.CharField(source="auto_identificacao")  # criado
    geometrias = rest_serializers.SerializerMethodField("get_poligono")
    segmento = rest_serializers.SerializerMethodField()  # criado
    historia = PTTHistoriaSerializer(read_only=True)
    enderecoContato = PTTEnderecoContatoSerializer(read_only=True, source="endereco_contato")  # criado

    class Meta:
        model = get_model("Territorio")
        fields = [
            "nome",
            "cadastrante",
            "idMunicipio",
            "privacidade",
            "uf",
            "areaEstimada",
            "cep",
            "descricaoAcesso",
            "descricaoAcessoPrivacidade",
            "zonaLocalizacao",
            "outraZonaLocalizacao",
            "autoIdentificacao",
            "geometrias",
            "segmento",
            "historia",
            "enderecoContato",
        ]

    def get_poligono(self, obj):
        return [json.loads(obj.poligono.geojson)] if obj.poligono is not None else None

    def get_id_municipio(self, obj):
        return obj.municipio_referencia.id if obj.municipio_referencia is not None else None

    def get_uf(self, obj):
        return obj.municipio_referencia.estado.uf if obj.municipio_referencia is not None else None

    def get_privacidade(self, obj):
        return "PUBLICO" if obj.publico else "PRIVADO"

    def get_area(self, obj):
        return TerritorioAgregado.objects.get(id=obj.id).area

    def get_segmento(self, obj):
        return "" if not obj.segmento else obj.segmento.nome


#     "historia":{
#       "descricao": "A nossa comunidade foi fundada em 1771, por um imigrante...",
#       "iconeSegmento": [
#         {
#         "nome": "icone.png",
#         "privacidade": "PUBLICO",
#         "descricao": "Foto da entrada do povoado",
#         "hashArquivo": "hash2314whbt23sd12345wed.png",
#         "tipoAnexo": "ICONE"
#         },
#
#         {
#         "nome": "icone2.png",
#         "privacidade": "PRIVACIDADE",
#         "descricao": "Foto da saída do povoado",
#         "hashArquivo": "hash2314whss23sd12345wed.png",
#         "tipoAnexo": "ICONE"
#         },
#
#         ],
#
#       "arquivosInformacoes": [{
#         "nome": "historia_quilombo.pdf",
#         "privacidade": "PUBLICO",
#         "descricao": "Tese de doutorado de Aluno da faculdade XXX detalhando história do nosso povo",
#         "hashArquivo": "hash2314whbt23syrtyugh45gui4d12345wed.pdf",
#         "tipoAnexo": "FONTE_INFORMACAO"
#       }],
#       "arquivosMidias": [{
#         "nome": "video_Festival.mp4",
#         "privacidade": "PUBLICO",
#         "descricao": "Video do dia do festival em comemoração aos 250 anos do povoado",
#         "hashArquivo": "hash2314whbt23sd123gdfsd4s5dfg445wed.mp4",
#         "tipoAnexo": "ANEXO"
#       }]
#     },
#     "enderecoContato": {
#       "nomeContato": "Maria das Dores",
#       "logradouro": "Rua Dr. Pedro Álvares",
#       "numero": "125A",
#       "complemento": "Próximo ao ponto da Petrobrás",
#       "bairro": "Centro",
#       "cep": "37200-000",
#       "caixaPostal": "11111",
#       "idMunicipio": 3138203,
#       "uf": "MG",
#       "email": "maria@exemplo.com",
#       "telefone": "(35) 99911-9911"
#     }
# }
