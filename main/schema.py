import re
import json
from django.db.utils import IntegrityError
from django.db.models import JSONField
from django.db import transaction
import graphene
from graphene import Field, String, Argument
from graphene.types.scalars import Int
from rest_framework_jwt.settings import api_settings
from graphene_django.types import DjangoObjectType
from graphene_django.debug import DjangoDebug
from graphene_django.converter import convert_django_field
from graphene_file_upload.scalars import Upload

# from graphene_django.forms.mutation import DjangoModelFormMutation
# from rest_framework_jwt.serializers import JSONWebTokenSerializer
# from graphene_file_upload.scalars import Upload

# from main.forms import TerritorioForm
from main.utils import inserir_territorio
from main import models


@convert_django_field.register(JSONField)
def convert_json_field_to_string(field, registry=None):
    return graphene.String()


class TipoComunidadeType(DjangoObjectType):
    class Meta:
        model = models.TipoComunidade
        fields = ("id", "nome", "descricao")


class SegmentoType(DjangoObjectType):
    class Meta:
        model = models.TipoComunidadePTT
        fields = ("id", "nome")


class MensagemType(DjangoObjectType):
    class Meta:
        model = models.Mensagem


class SimpleTerritorioType(DjangoObjectType):
    municipio_referencia_id = Field(String)

    class Meta:
        model = models.Territorio
        fields = ("id", "municipio_referencia_id", "nome")


class TerritorioStatusType(DjangoObjectType):
    class Meta:
        model = models.TerritorioStatus
        fields = ("id", "nome", "mensagem", "url_referencia", "has_fale_conosco_btn")


class EstadoType(DjangoObjectType):
    class Meta:
        model = models.Estado


class MunicipioType(DjangoObjectType):
    class Meta:
        model = models.Municipio


class AnexoType(DjangoObjectType):
    thumb = Field(String)

    # pylint: disable=unused-argument
    def resolve_thumb(self, info):
        if self.thumb.name:
            return self.thumb.url
        return None

    class Meta:
        model = models.Anexo


class TipoAreaDeUsoType(DjangoObjectType):
    class Meta:
        model = models.TipoAreaDeUso
        fields = ("id", "nome", "descricao")


class AreaDeUsoType(DjangoObjectType):
    tipo_area_de_uso_id = Field(String)

    class Meta:
        model = models.AreaDeUso


class TipoConflitoType(DjangoObjectType):
    class Meta:
        model = models.TipoConflito
        fields = ("id", "nome", "descricao")


class ConflitoType(DjangoObjectType):
    tipo_conflito_id = Field(String)

    class Meta:
        model = models.Conflito


class TerritorioUsuariaType(DjangoObjectType):
    class Meta:
        model = models.TerritorioUsuaria


class TerritorioType(DjangoObjectType):
    tipo_comunidade_id = Field(String)
    municipio_referencia_id = Field(String)
    segmento_id = Field(String)
    ec_municipio_id = Field(String)
    status_id = Field(Int)
    tipos_comunidade = graphene.List(TipoComunidadeType)

    class Meta:
        model = models.Territorio
        geojson_field = "poligono"

    # pylint: disable=unused-argument
    def resolve_tipo_comunidade_id(self, info):
        tipo_comunidade = self.tipos_comunidade.first()
        if tipo_comunidade:
            return tipo_comunidade.id
        return None

    # pylint: disable=unused-argument
    def resolve_mensagens(self, info):
        mensagens_out = []
        for mensagem in self.mensagens.all():
            mensagem_out = mensagem
            mensagem_out.extra = json.dumps(mensagem.extra)
            mensagens_out.append(mensagem_out)
        return mensagens_out

    # pylint: disable=unused-argument
    def resolve_tipos_comunidade(self, info):
        return self.tipos_comunidade.all()


class UsuariaType(DjangoObjectType):
    class Meta:
        model = models.Usuaria

    current_territorio = Field(TerritorioType, id=graphene.ID())
    full_name = Field(String)

    # pylint: disable=unused-argument
    def resolve_current_territorio(self, info, **kwargs):
        territorio_id = kwargs.get("id", None)
        # pylint: disable=not-callable
        return self.get_current_territorio(territorio_id)

    # pylint: disable=unused-argument
    def resolve_full_name(self, info):
        return f"{self.first_name} {self.last_name}"


class Query(graphene.AbstractType):
    debug = graphene.Field(DjangoDebug, name="_debug")
    current_user = graphene.Field(UsuariaType)

    territorio = graphene.Field(TerritorioType, id=graphene.ID())
    territorios = graphene.List(
        SimpleTerritorioType, id_usuaria=graphene.ID(), usuaria_id=graphene.ID()
    )

    # mensagens = graphene.List(MensagemType, since=graphene.DateTime(), remetente_id=graphene.ID())
    mensagens = graphene.List(
        MensagemType,
        territorio_id=graphene.ID(),
        min_id=graphene.ID(),
        sender_is_dashboard=graphene.Boolean(),
        json_dump_extra=graphene.Boolean(),
    )

    anexo = graphene.Field(AnexoType, id=graphene.ID())
    anexos = graphene.List(AnexoType, territorio_id=graphene.ID())

    tipos_comunidade = graphene.List(TipoComunidadeType)
    conflitos = graphene.List(ConflitoType, territorio_id=graphene.ID())
    tipos_conflito = graphene.List(TipoConflitoType)
    areas_de_uso = graphene.List(AreaDeUsoType, territorio_id=graphene.ID())
    tipos_area_de_uso = graphene.List(TipoAreaDeUsoType)
    segmentos = graphene.List(SegmentoType)
    territorio_status = graphene.List(TerritorioStatusType)
    territorio_status_id = graphene.ID(territorio_id=graphene.ID())

    def resolve_current_user(self, info):
        context = info.context
        if not context.user.is_authenticated:
            return None
        return context.user

    # pylint: disable=unused-argument
    def resolve_tipos_comunidade(self, info):
        return models.TipoComunidade.objects.all()

    # pylint: disable=unused-argument
    def resolve_conflitos(self, info, **kwargs):
        territorio_id = kwargs.get("territorio_id", None)
        conflitos = models.Conflito.objects

        if territorio_id:
            conflitos = conflitos.filter(territorio_id=territorio_id)

        return conflitos

    # pylint: disable=unused-argument
    def resolve_tipos_conflito(self, info):
        return models.TipoConflito.objects.all()

    # pylint: disable=unused-argument
    def resolve_areas_de_uso(self, info, **kwargs):
        territorio_id = kwargs.get("territorio_id", None)
        areas_de_uso = models.AreaDeUso.objects

        if territorio_id:
            areas_de_uso = areas_de_uso.filter(territorio_id=territorio_id)

        return areas_de_uso

    # pylint: disable=unused-argument
    def resolve_tipos_area_de_uso(self, info):
        return models.TipoAreaDeUso.objects.all()

    # pylint: disable=unused-argument
    def resolve_segmentos(self, info):
        return models.TipoComunidadePTT.objects.all()

    # pylint: disable=unused-argument
    def resolve_territorio_status(self, info):
        return models.TerritorioStatus.objects.all()

    # pylint: disable=unused-argument
    def resolve_territorios(self, info, **kwargs):
        usuaria_id = kwargs.get("usuaria_id", None)
        if not usuaria_id:
            usuaria_id = kwargs.get("id_usuaria", None)
        territorios = models.Territorio.objects

        if usuaria_id:
            territorios = territorios.filter(usuarias__in=[usuaria_id])

        return territorios.order_by("nome").all()

    # pylint: disable=unused-argument
    def resolve_territorio(self, info, **kwargs):
        territorio_id = kwargs.get("id", None)
        if territorio_id:
            return models.Territorio.objects.get(pk=territorio_id)
        return None

    # pylint: disable=unused-argument
    def resolve_anexos(self, info, **kwargs):
        territorio_id = kwargs.get("territorio_id", None)
        anexos = models.Anexo.objects

        if territorio_id:
            anexos = anexos.filter(territorio_id=territorio_id)

        return anexos

    # pylint: disable=unused-argument
    def resolve_territorio_status_id(self, info, **kwargs):
        territorio_id = kwargs.get("territorio_id", None)
        if territorio_id:
            territorio = models.Territorio.objects.get(pk=territorio_id)
            return territorio.status.id
        return None

    # pylint: disable=unused-argument
    # def resolve_mensagens(self, info, **kwargs):
    #     since = kwargs.get("since", None)
    #     remetente_id = kwargs.get("remetente_id", None)
    #     mensagens = models.Mensagem.objects
    #     if since:
    #         mensagens = mensagens.filter(data_envio__gte=since)
    #     if remetente_id:
    #         mensagens = mensagens.filter(remetente_id=remetente_id)
    #     mensagens_out = []
    #     for mensagem in mensagens.all():
    #         mensagem_out = mensagem
    #         mensagem_out.extra = json.dumps(mensagem.extra)
    #         mensagens_out.append(mensagem_out)
    #     return mensagens_out

    # pylint: disable=unused-argument
    def resolve_mensagens(self, info, **kwargs):
        territorio_id = kwargs.get("territorio_id", None)
        min_id = kwargs.get("min_id", None)
        sender_is_dashboard = kwargs.get("sender_is_dashboard", False)
        json_dump_extra = kwargs.get("json_dump_extra", False)
        if territorio_id:
            territorio = models.Territorio.objects.get(pk=territorio_id)
            mensagens = territorio.mensagens.all()

            if min_id:
                mensagens = mensagens.filter(id__gt=min_id)

            if sender_is_dashboard:
                mensagens = mensagens.filter(origin_is_device=False)

            if not json_dump_extra:
                return mensagens

            mensagens_out = []
            for mensagem in mensagens:
                mensagem_out = mensagem
                mensagem_out.extra = json.dumps(mensagem.extra)
                mensagens_out.append(mensagem_out)
            return mensagens_out

        return models.Mensagens.objects.none()


# ------------------------------------------------------------ #
# Mutações de usuária


class AnexoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    descricao = graphene.String()
    mimetype = graphene.String()
    file_size = graphene.Int()
    arquivo = graphene.String(required=False)
    tipo_anexo = graphene.String(required=False)
    territorio_id = graphene.ID()
    criacao = graphene.DateTime()
    ultima_alteracao = graphene.DateTime()
    thumb = graphene.String(required=False)
    publico = graphene.Boolean(required=False)


class AnexoWithUploadInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    territorio_id = graphene.ID()
    nome = graphene.String(required=True)
    descricao = graphene.String()
    arquivo = Upload(required=True)
    tipo_anexo = graphene.String(required=False)
    publico = graphene.Boolean(required=False)


class MensagemInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    territorio_id = graphene.ID(required=False)
    remetente_id = graphene.String(required=False)
    texto = graphene.String()
    extra = graphene.JSONString(required=False)
    data_envio = graphene.DateTime(required=False)
    data_recebimento = graphene.DateTime(required=False)
    origin_is_device = graphene.Boolean(required=False)


class AreaDeUsoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    territorio_id = graphene.ID()
    nome = graphene.String()
    posicao = graphene.String()
    descricao = graphene.String()
    area = graphene.Decimal(required=False)
    tipo_area_de_uso_id = graphene.String()


class ConflitoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    territorio_id = graphene.ID()
    nome = graphene.String()
    posicao = graphene.String()
    descricao = graphene.String()
    tipo_conflito_id = graphene.String()


class TipoComunidadeInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    descricao = graphene.String()


class TipoComunidadePTTInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()


class TerritorioInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String(required=True)
    poligono = graphene.String()
    ano_fundacao = graphene.Int()
    qtde_familias = graphene.Int()
    status_id = graphene.Int()
    tipo_comunidade_id = graphene.String()
    municipio_referencia_id = graphene.Int(required=True)
    conflitos = graphene.List(ConflitoInput)
    areas_de_uso = graphene.List(AreaDeUsoInput)
    anexos = graphene.List(AnexoInput)
    mensagens = graphene.List(MensagemInput)
    publico = graphene.Boolean(required=False, default_value=False)
    enviado_ptt = graphene.Boolean(required=False, default_value=False)
    tipos_comunidade = graphene.List(TipoComunidadeInput)
    outro_tipo_comunidade = graphene.String(required=False)

    # PTT
    cep = graphene.String(required=False)
    descricao_acesso = graphene.String(required=False)
    descricao_acesso_privacidade = graphene.Boolean(required=False, default_value=False)
    zona_localizacao = graphene.String(required=False)
    outra_zona_localizacao = graphene.String(required=False)
    auto_identificacao = graphene.String(required=False)
    segmento_id = graphene.String(required=False)
    historia_descricao = graphene.String(required=False)
    ptt_id_territorio = graphene.Int(required=False)
    ptt_status_territorio = graphene.String(required=False)

    # Endereço de contato (também PTT)
    ec_nome_contato = graphene.String(required=False)
    ec_logradouro = graphene.String(required=False)
    ec_numero = graphene.String(required=False)
    ec_complemento = graphene.String(required=False)
    ec_bairro = graphene.String(required=False)
    ec_cep = graphene.String(required=False)
    ec_caixa_postal = graphene.String(required=False)
    ec_municipio_id = graphene.String(required=False)
    ec_email = graphene.String(required=False)
    ec_telefone = graphene.String(required=False)

    def get_local_data(self):
        # tirar os campos foreign_key para inserir/atualizar territorio
        local_data = dict(self.__dict__)
        local_data.pop("conflitos", None)
        local_data.pop("areas_de_uso", None)
        local_data.pop("anexos", None)
        local_data.pop("mensagens", None)
        local_data.pop("tipos_comunidade", None)
        local_data.pop("tipo_comunidade_id", None)
        # tirar o campo status para averiguar se o mutate é anterior a eventual mudança feita no dashboard (offline first)
        local_data.pop("status", None)
        return local_data


class UsuariaInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    username = graphene.String(required=True)
    full_name = graphene.String()
    email = graphene.String(required=False)
    current_territorio = graphene.InputField(TerritorioInput)
    push_token = graphene.String()
    cpf = graphene.String()
    territorios = graphene.List(TerritorioInput)


class DataSourceInput(graphene.InputObjectType):
    current_user = graphene.InputField(UsuariaInput)


# MUTATIONS


class DeleteTerritorioMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=unused-argument
    def mutate(self, info, **kwargs):
        territorio_id = kwargs.get("id", None)
        success = True
        errors = []
        usuaria = info.context.user

        if not territorio_id:
            errors.append("Só é possível apagar uma comunidade enviando o seu id!")
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                territorio = models.Territorio.objects.get(pk=territorio_id)
            except models.Territorio.DoesNotExist:
                errors.append(f"A comunidade com id {territorio_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            territorio.delete()

        return DeleteTerritorioMutation(success=success, errors=errors)


class CreateOrUpdateCurrentUser(graphene.Mutation):
    class Arguments:
        input = Argument(DataSourceInput)

    token = graphene.String()
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    current_user = graphene.Field(lambda: UsuariaType)

    def mutate(self, info, input=None):
        usuaria = None
        token = None
        errors = []
        success = False
        context = info.context

        if input.current_user is not None:
            if input.current_user.id is not None:
                # verificar se pessoa tentando inserir é a mesma do current_user
                if context.user.is_authenticated and int(context.user.id) == int(
                    input.current_user.id
                ):
                    # atualiza o nome (e futuramente outras propriedades) de usuária, se mudou
                    user_change = False
                    if (
                        input.current_user.full_name
                        != context.user.first_name + " " + context.user.last_name
                    ):
                        context.user.set_full_name(input.current_user.full_name)
                        user_change = True
                    if (
                        input.current_user.cpf is not None
                        and input.current_user.cpf != context.user.cpf
                    ):
                        context.user.cpf = input.current_user.cpf
                        user_change = True
                    if (
                        input.current_user.email is not None
                        and input.current_user.email != context.user.email
                    ):
                        context.user.email = input.current_user.email
                        user_change = True
                    if context.user.solicitou_codigo:
                        context.user.solicitou_codigo = False
                        user_change = True

                    if user_change:
                        context.user.save()

                    usuaria = context.user
                    usuaria.create_or_update_device(input.current_user.push_token)

            else:
                # insere nova usuária
                try:
                    # gera senha temporária
                    senha_aleatoria = models.Usuaria.gera_senha_aleatoria()

                    full_name_splitted = (
                        re.sub(" +", " ", input.current_user.full_name)
                        .strip()
                        .split(" ", 1)
                    )
                    if len(full_name_splitted) == 1:
                        first_name = full_name_splitted[0]
                        last_name = ""
                    else:
                        first_name, last_name = full_name_splitted

                    usuaria = models.Usuaria.objects.create_user(
                        username=input.current_user.username,
                        password=senha_aleatoria,
                        first_name=first_name,
                        last_name=last_name,
                        codigo_uso_celular=senha_aleatoria,
                        is_active=True,
                        cpf=input.current_user.cpf,
                    )
                    # generate token for user
                    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                    payload = jwt_payload_handler(usuaria)
                    token = jwt_encode_handler(payload)

                    usuaria.create_or_update_device(input.current_user.push_token)

                    # serializer = JSONWebTokenSerializer(
                    #     data={'username': input.current_user.username, 'password': senha_aleatoria})
                    # if serializer.is_valid():
                    #     token = serializer.object["token"]
                    # else:
                    #     i = 1
                except IntegrityError:
                    errors = ["username", "Telefone já cadastrado"]

        if usuaria is not None and input.current_user.current_territorio is not None:
            territorio, created = inserir_territorio(
                input.current_user.current_territorio, context
            )
            if created:
                models.TerritorioUsuaria.objects.create(
                    territorio_id=territorio.id, usuaria_id=usuaria.id
                )
            usuaria.current_territorio = territorio
            usuaria.save()

        if len(errors) == 0 and usuaria:
            success = True

        return CreateOrUpdateCurrentUser(
            token=token, errors=errors, success=success, current_user=usuaria
        )


class EnviaTerritorioParaAnaliseMutation(graphene.Mutation):
    class Arguments:
        territorio_id = graphene.ID()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(TerritorioType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None

        usuaria = info.context.user

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id da comunidade é obrigatório!"
            )
            errors.append(msg)
            success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            territorio.status_id = models.TerritorioStatus.ENVIADO_PENDENTE
            territorio.save()

        return EnviaTerritorioParaAnaliseMutation(
            data=territorio, success=success, errors=errors
        )


class EnviaTerritorioParaPttMutation(graphene.Mutation):
    class Arguments:
        territorio_id = graphene.ID()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(TerritorioType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None

        usuaria = info.context.user

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id da comunidade é obrigatório!"
            )
            errors.append(msg)
            success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            territorio.status_id = models.TerritorioStatus.ENVIO_PTT_NAFILA
            territorio.save()

        return EnviaTerritorioParaPttMutation(
            data=territorio, success=success, errors=errors
        )


class UploadAnexoMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=False)
        territorio_id = graphene.ID()
        nome = graphene.String(required=True)
        descricao = graphene.String()
        arquivo = Upload(required=True)
        tipo_anexo = graphene.String(required=False)
        publico = graphene.Boolean(required=False)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(AnexoType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        model = models.Anexo
        object_id = kwargs.get("id", None)
        object = None
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None
        usuaria = info.context.user

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        nome = kwargs.get("nome", None)
        if not nome:
            errors.append("O nome do anexo é obrigatório")
            success = False

        arquivo = kwargs.get("arquivo", None)
        if not arquivo:
            errors.append("O arquivo tem que ser enviado. Não recebi arquivo algum")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id do comunidade do anexo é obrigatório!"
            )
            errors.append(msg)
            success = False

        if object_id:
            try:
                model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"Não há anexo com o id {object_id}")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            defaults = kwargs
            defaults.pop("id", None)
            object, _ = model.objects.update_or_create(pk=object_id, defaults=defaults)

        return UploadAnexoMutation(data=object, success=success, errors=errors)


class DeleteAnexoMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=invalid-name
    def mutate(self, info, id=None):
        object_id = id
        success = True
        errors = []
        model = models.Anexo
        usuaria = info.context.user

        if not object_id:
            errors.append("Só é possível apagar um anexo enviando o seu id!")
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                object = model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"O anexo com id {object_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(object.territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{object.territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            object.delete()

        return DeleteAnexoMutation(success=success, errors=errors)


class CreateOrUpdateAreaDeUsoMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=False)
        territorio_id = graphene.ID()
        nome = graphene.String()
        posicao = graphene.String()
        descricao = graphene.String()
        area = graphene.Decimal(required=False)
        tipo_area_de_uso_id = graphene.String()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(AreaDeUsoType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        object_id = kwargs.get("id", None)
        object = None
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None

        usuaria = info.context.user
        model = models.AreaDeUso

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id da comunidade é obrigatório!"
            )
            errors.append(msg)
            success = False

        if object_id:
            try:
                model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"Não há objeto com o id {object_id}")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            defaults = kwargs
            defaults.pop("id", None)
            object, _ = model.objects.update_or_create(pk=object_id, defaults=defaults)

        return CreateOrUpdateAreaDeUsoMutation(
            data=object, success=success, errors=errors
        )


class DeleteAreaDeUsoMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=invalid-name
    def mutate(self, info, id=None):
        object_id = id
        success = True
        errors = []

        model = models.AreaDeUso
        usuaria = info.context.user

        if not object_id:
            errors.append(
                "Id obrigatório: Só é possível apagar o objeto enviando o seu id!"
            )
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                object = model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"O objeto com id {object_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(object.territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{object.territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            object.delete()

        return DeleteAreaDeUsoMutation(success=success, errors=errors)


class CreateOrUpdateConflitoMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=False)
        territorio_id = graphene.ID()
        nome = graphene.String()
        posicao = graphene.String()
        descricao = graphene.String()
        tipo_conflito_id = graphene.String()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(ConflitoType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        object_id = kwargs.get("id", None)
        object = None
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None

        usuaria = info.context.user
        model = models.Conflito

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id da comunidade é obrigatório!"
            )
            errors.append(msg)
            success = False

        if object_id:
            try:
                model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"Não há objeto com o id {object_id}")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            defaults = kwargs
            defaults.pop("id", None)
            object, _ = model.objects.update_or_create(pk=object_id, defaults=defaults)

        return CreateOrUpdateConflitoMutation(
            data=object, success=success, errors=errors
        )


class DeleteConflitoMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=invalid-name
    def mutate(self, info, id=None):
        object_id = id
        success = True
        errors = []

        model = models.Conflito
        usuaria = info.context.user

        if not object_id:
            errors.append(
                "Id obrigatório: Só é possível apagar o objeto enviando o seu id!"
            )
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                object = model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"O objeto com id {object_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(object.territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{object.territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            object.delete()

        return DeleteConflitoMutation(success=success, errors=errors)


class DeleteAllUsosEConflitosMutation(graphene.Mutation):
    class Arguments:
        territorio_id = graphene.ID()

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, territorio_id=None):
        success = True
        errors = []
        usuaria = info.context.user

        if not territorio_id:
            errors.append(
                "Id obrigatório: Só é possível apagar áreas e conflitos enviando o id da comunidade!"
            )
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                territorio = models.Territorio.objects.get(pk=territorio_id)
            except models.Territorio.DoesNotExist:
                errors.append(f"A comunidade com id {territorio_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            if usuaria.solicitou_codigo:
                usuaria.solicitou_codigo = False
                usuaria.save()
            territorio.areas_de_uso.all().delete()
            territorio.conflitos.all().delete()

        return DeleteAllUsosEConflitosMutation(success=success, errors=errors)


class CreateOrUpdateMensagemMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=False)
        territorio_id = graphene.ID(required=False)
        remetente_id = graphene.String(required=False)
        texto = graphene.String()
        extra = graphene.JSONString(required=False)
        data_envio = graphene.DateTime(required=False)
        data_recebimento = graphene.DateTime(required=False)
        origin_is_device = graphene.Boolean(required=False)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    data = graphene.Field(MensagemType)

    def mutate(self, info, **kwargs):
        success = True
        errors = []
        object_id = kwargs.get("id", None)
        object = None
        territorio_id = kwargs.get("territorio_id", None)
        territorio = None

        usuaria = info.context.user
        model = models.Mensagem

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        try:
            territorio = models.Territorio.objects.get(pk=territorio_id)
        except models.Territorio.DoesNotExist:
            msg = (
                f"Não há comunidade com o id {territorio_id}"
                if territorio_id is not None
                else "O id da comunidade é obrigatório!"
            )
            errors.append(msg)
            success = False

        if object_id:
            try:
                model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"Não há objeto com o id {object_id}")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{territorio.nome}"
                )
                success = False

        if success:
            defaults = kwargs
            defaults.pop("id", None)
            origin_is_device = defaults.get("origin_is_device", None)
            if origin_is_device:
                defaults["remetente"] = usuaria
                territorio.sac_status = models.Territorio.SAC_OPTIONS["ABERTO"]
                territorio.save()
            object, _ = model.objects.update_or_create(pk=object_id, defaults=defaults)
            new_extra = json.dumps(object.extra)
            object.extra = new_extra

        return CreateOrUpdateMensagemMutation(
            data=object, success=success, errors=errors
        )


class DeleteMensagemMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=invalid-name
    def mutate(self, info, id=None):
        object_id = id
        success = True
        errors = []

        model = models.Mensagem
        usuaria = info.context.user

        if not object_id:
            errors.append(
                "Id obrigatório: Só é possível apagar o objeto enviando o seu id!"
            )
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            try:
                object = model.objects.get(pk=object_id)
            except model.DoesNotExist:
                errors.append(f"O objeto com id {object_id} não existe!")
                success = False

        if success:
            if not usuaria.eh_cadastrante_do_territorio(object.territorio):
                errors.append(
                    f"Este/a usuário/a não é cadastrante da comunidade '{object.territorio.nome}"
                )
                success = False

        if success:
            object.delete()

        return DeleteMensagemMutation(success=success, errors=errors)


class DeleteUsuariaMutation(graphene.Mutation):
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    class Arguments:
        id = graphene.ID()

    # pylint: disable=unused-argument
    def mutate(self, info, **kwargs):
        usuaria_id = kwargs.get("id", None)
        success = True
        errors = []
        usuaria = info.context.user

        if not usuaria_id:
            errors.append("Só é possível apagar uma usuária enviando o seu id!")
            success = False

        if not usuaria.is_authenticated:
            errors.append("A usuária não está conectada!")
            success = False

        if success:
            if int(usuaria_id) != usuaria.id:
                errors.append("Só a mesma usuária pode se apagar!")
                success = False

        if success:
            usuaria.delete()

        return DeleteUsuariaMutation(success=success, errors=errors)


# Legacy for devices still on version 2.x!
class UpdateAppData(graphene.Mutation):
    class Arguments:
        input = Argument(DataSourceInput)

    token = graphene.String()
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    current_user = graphene.Field(lambda: UsuariaType)

    def mutate(self, info, input=None):
        with transaction.atomic():
            usuaria = None
            token = None
            errors = []
            success = False
            context = info.context

            if input.current_user is not None:
                if input.current_user.id is not None:
                    # verificar se pessoa tentando inserir é a mesma do current_user
                    if context.user.is_authenticated and int(context.user.id) == int(
                        input.current_user.id
                    ):
                        # atualiza o nome (e futuramente outras propriedades) de usuária, se mudou
                        user_change = False
                        if (
                            input.current_user.full_name
                            != context.user.first_name + " " + context.user.last_name
                        ):
                            context.user.set_full_name(input.current_user.full_name)
                            user_change = True
                        if (
                            input.current_user.cpf is not None
                            and input.current_user.cpf != context.user.cpf
                        ):
                            context.user.cpf = input.current_user.cpf
                            user_change = True
                        if (
                            input.current_user.email is not None
                            and input.current_user.email != context.user.email
                        ):
                            context.user.email = input.current_user.email
                            user_change = True
                        if context.user.solicitou_codigo:
                            context.user.solicitou_codigo = False
                            user_change = True
                        if user_change:
                            context.user.save()
                        usuaria = context.user
                        usuaria.create_or_update_device(input.current_user.push_token)

                else:
                    # insere nova usuária
                    try:
                        # gera senha temporária
                        senha_aleatoria = models.Usuaria.gera_senha_aleatoria()

                        full_name_splitted = (
                            re.sub(" +", " ", input.current_user.full_name)
                            .strip()
                            .split(" ", 1)
                        )
                        if len(full_name_splitted) == 1:
                            first_name = full_name_splitted[0]
                            last_name = ""
                        else:
                            first_name, last_name = full_name_splitted

                        usuaria = models.Usuaria.objects.create_user(
                            username=input.current_user.username,
                            password=senha_aleatoria,
                            first_name=first_name,
                            last_name=last_name,
                            codigo_uso_celular=senha_aleatoria,
                            is_active=True,
                            cpf=input.current_user.cpf,
                        )
                        # generate token for user
                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                        payload = jwt_payload_handler(usuaria)
                        token = jwt_encode_handler(payload)

                        usuaria.create_or_update_device(input.current_user.push_token)

                        # serializer = JSONWebTokenSerializer(
                        #     data={'username': input.current_user.username, 'password': senha_aleatoria})
                        # if serializer.is_valid():
                        #     token = serializer.object["token"]
                        # else:
                        #     i = 1
                    except IntegrityError:
                        errors = ["username", "Telefone já cadastrado"]

            if (
                usuaria is not None
                and input.current_user.current_territorio is not None
            ):
                territorio, created = inserir_territorio(
                    input.current_user.current_territorio, context
                )
                if created:
                    models.TerritorioUsuaria.objects.create(
                        territorio_id=territorio.id, usuaria_id=usuaria.id
                    )
                usuaria.current_territorio = territorio
                usuaria.save()

            if len(errors) == 0 and usuaria:
                success = True

            return UpdateAppData(
                token=token, errors=errors, success=success, current_user=usuaria
            )


# ------------------------------------------------------------ #


class Mutation(graphene.ObjectType):
    update_current_user = CreateOrUpdateCurrentUser.Field()
    delete_territorio = DeleteTerritorioMutation.Field()
    envia_territorio_para_analise = EnviaTerritorioParaAnaliseMutation.Field()
    envia_territorio_para_ptt = EnviaTerritorioParaPttMutation.Field()

    update_anexo = UploadAnexoMutation.Field()
    delete_anexo = DeleteAnexoMutation.Field()

    delete_usuaria = DeleteUsuariaMutation.Field()

    update_area_de_uso = CreateOrUpdateAreaDeUsoMutation.Field()
    delete_area_de_uso = DeleteAreaDeUsoMutation.Field()

    update_conflito = CreateOrUpdateConflitoMutation.Field()
    delete_conflito = DeleteConflitoMutation.Field()

    delete_all_usos_e_conflitos = DeleteAllUsosEConflitosMutation.Field()

    update_mensagem = CreateOrUpdateMensagemMutation.Field()
    delete_mensagem = DeleteMensagemMutation.Field()

    # Legacy for devices still on version 2.x!
    update_app_data = UpdateAppData.Field()
