import logging
import requests
from datetime import date
from django.conf import settings
from django.contrib.auth.models import Group
from django.db import IntegrityError, transaction
from django.db.models import ProtectedError, Q, F, Exists, OuterRef
from django.http import JsonResponse, HttpResponseRedirect, FileResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (
    TemplateView,
    DeleteView,
    FormView,
    ListView,
    CreateView,
    DetailView,
    UpdateView,
)
from django.views.generic.edit import FormMixin
from hardcopy.views import PDFViewMixin
from hardcopy import bytestring_to_pdf
from notifications.models import Notification  # type: ignore
from notifications.signals import notify  # type: ignore
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rules.contrib.views import PermissionRequiredMixin  # type: ignore
from PIL import Image
from pikepdf import Pdf, PdfImage, Name
import zlib
import io

from main.utils import (
    get_areas_de_uso_list,
    get_conflitos_list,
    conta_territorios_organizacao,
    get_territorios_visiveis_revisora,
    get_mapa_status_rev,
)
from main.forms import (
    OrganizacaoForm,
    OrganizacaoUsuariaForm,
    UsuariaCreationForm,
    UsuariaUpdateForm,
    MensagemCreationForm,
    SacForm,
    TerritorioForm,
    TerritorioDetailForm,
    UsuariaTerritorioForm,
    UsuariaRevisoraForm,
    UsuariaMunicipioForm,
    OrganizacaoUsuariaAssociarForm,
    OrganizacaoMunicipioOuEstadoAssociarForm,
)
from main.mixins import AjaxableResponseMixin
from main.models import (
    Organizacao,
    Usuaria,
    OrganizacaoUsuaria,
    Anexo,
    Municipio,
    Estado,
    Mensagem,
    Territorio,
    TerritorioRevisaoStatus,
)
from relatorios.models import TerritorioAgregado
from .serializers import AnexoSerializer, PTTSerializer


logging.basicConfig(
    filename="debug_local.log", filemode="w", format="CooperativaEITA - %(message)s"
)


class IndexView(TemplateView):
    template_name = "main/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_anonymous:
            return redirect("RelatoriosIndex")

        return super(IndexView, self).dispatch(request, *args, **kwargs)


class BaseDeleteView(DeleteView):
    def delete(self, request, *args, **kwargs):
        response_status = 200

        try:
            self.get_object().delete()
            payload = {"delete": "ok"}
        except (ProtectedError, IntegrityError) as error:
            # pylint: disable=consider-using-f-string
            payload = {"error": "Erro ao apagar", "message": "{0}".format(error)}
            response_status = 405  # Method not allowed

        return JsonResponse(payload, status=response_status)


class OrganizacaoListView(PermissionRequiredMixin, ListView, FormMixin):
    model = Organizacao
    permission_required = "admin_geral"
    form_class = OrganizacaoMunicipioOuEstadoAssociarForm

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return Organizacao.objects.none()

        queryset = Organizacao.objects.all()

        if "organizacao_id" in self.kwargs:
            queryset = queryset.filter(id=self.kwargs["organizacao_id"])

        if not self.request.user.has_perm("organizacao.ver_todos"):
            queryset = queryset.filter(usuarias=self.request.user)

        return queryset.prefetch_related(
            "territorios",
            "org_usuarias",
            "org_usuarias__usuaria",
            "municipios",
            "estados",
            "municipios__estado",
        )

    def get_organizacoes_list(self):
        organizacoes_list = []
        for organizacao in self.get_queryset():
            num_revisoras = 0
            num_administradoras = 0
            for org_usuaria in organizacao.org_usuarias.all():
                if org_usuaria.is_admin:
                    num_administradoras += 1
                if org_usuaria.usuaria.is_reviewer:
                    num_revisoras += 1
            num_territorios = conta_territorios_organizacao(
                organizacao, organizacao.municipios, organizacao.estados
            )
            organizacoes_list.append(
                {
                    "id": organizacao.id,
                    "nome": organizacao.nome,
                    "municipios": organizacao.municipios.all(),
                    "estados": organizacao.estados.all(),
                    "num_revisoras": num_revisoras,
                    "num_administradoras": num_administradoras,
                    "num_territorios": num_territorios,
                    "url": reverse(
                        "usuaria_list", kwargs={"organizacao_id": organizacao.id}
                    ),
                }
            )
        return organizacoes_list

    def get_context_data(self, **kwargs):
        organizacoes_list = self.get_organizacoes_list()
        context = super().get_context_data(**kwargs)
        context["modulo"] = "organizacao"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["organizacoes_list"] = organizacoes_list
        context["navHideOrg"] = True
        context["temFooter"] = False
        return context

    def post(self, request, *args, **kwargs):
        """metodo POST utilizado para inserir e remover usuárias em organização"""

        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            organizacao = Organizacao.objects.get(
                pk=request.POST.get("organizacao", None)
            )
        except:
            status_error["content"] = "Organização não encontrada"
            return JsonResponse(status_error)

        try:
            estado = Estado.objects.get(pk=request.POST.get("estado", None))
        except:
            status_error["content"] = "Estado inexistente"
            return JsonResponse(status_error)

        municipio_id = request.POST.get("municipio", None)

        if municipio_id:
            try:
                municipio = Municipio.objects.get(pk=municipio_id)
            except:
                status_error["content"] = "Município inexistente"
                return JsonResponse(status_error)
            organizacao.municipios.add(municipio)
            desassociar_url = reverse(
                "organizacao_remover_municipio",
                kwargs={
                    "organizacao_id": organizacao.id,
                    "municipio_id": municipio.id,
                },
            )
        else:
            organizacao.estados.add(estado)
            desassociar_url = reverse(
                "organizacao_remover_estado",
                kwargs={
                    "organizacao_id": organizacao.id,
                    "estado_id": estado.id,
                },
            )

        status_success = {
            "status": 200,
            "statusText": "OK",
            "organizacao": f"{organizacao}",
            "organizacao_id": f"{organizacao.id}",
            "tipo": "estado" if not municipio_id else "municipio",
            "nome": f"{estado.uf}" if not municipio_id else f"{municipio}",
            "id": f"{estado.id}" if not municipio_id else f"{municipio.id}",
            "num_territorios": conta_territorios_organizacao(organizacao),
            "desassociar_url": desassociar_url,
        }
        return JsonResponse(status_success)


class OrganizacaoCreateView(PermissionRequiredMixin, CreateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = "organizacao.cadastrar"
    titulo = "Nova Organização"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "organizacao"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["temFooter"] = True
        return context

    def get_success_url(self):
        return reverse("organizacao_list")


class OrganizacaoDetailView(PermissionRequiredMixin, DetailView):
    model = Organizacao
    permission_required = "organizacao.visualizar"
    organizacao = None

    def dispatch(self, *args, **kwargs):
        self.organizacao = Organizacao.objects.get(
            pk=self.kwargs.get("organizacao_id", None)
        )
        return super(OrganizacaoDetailView, self).dispatch(*args, **kwargs)

    def get_object(self):
        return self.organizacao

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuarias_do_sistema = (
            Usuaria.objects.filter(is_superuser=False)
            .difference(self.object.usuarias.all())
            .order_by("first_name", "username")
        )

        context["usuarias_do_sistema"] = usuarias_do_sistema
        context["modulo"] = "organizacao"
        context["titulo"] = "Integrantes"
        context["title"] = f"Integrantes da organização {self.organizacao}"
        context["organizacao_id"] = self.organizacao.id
        context["temFooter"] = True
        context["org_usuarias"] = self.organizacao.org_usuarias.order_by(
            "usuaria__username"
        )
        context["navGeralUrl"] = reverse("organizacao_list")
        return context


class OrganizacaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = "organizacao.alterar"
    titulo = "Editar Organização"

    def get_object(self):
        return Organizacao.objects.get(pk=self.kwargs["organizacao_id"])

    def get_form_kwargs(self):
        kwargs = super(OrganizacaoUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "organizacao.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["title"] = self.titulo
        context["modulo"] = "organizacao"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["temFooter"] = True
        context["org_usuarias"] = self.get_object().org_usuarias.order_by(
            "usuaria__username"
        )
        return context


class OrganizacaoDeleteView(PermissionRequiredMixin, BaseDeleteView):
    model = Organizacao
    permission_required = "organizacao.apagar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrganizacaoDeleteView, self).dispatch(*args, **kwargs)


class OrganizacaoUsuariaListView(ListView):
    model = OrganizacaoUsuaria


class OrganizacaoUsuariaUpdateView(PermissionRequiredMixin, UpdateView):
    model = OrganizacaoUsuaria
    form_class = OrganizacaoUsuariaForm
    permission_required = "organizacao.alterar"

    def get_permission_object(self):
        return self.get_object().organizacao

    def get_object(self, queryset=None):
        data = self.request.POST
        usuaria_id = data.get("usuaria", None)
        organizacao_id = self.kwargs.get("organizacao_id", None)
        # TODO: treat error when no usuaria_id or no organizacao_id is passed

        with transaction.atomic():
            obj, _ = OrganizacaoUsuaria.objects.get_or_create(
                usuaria_id=usuaria_id, organizacao_id=organizacao_id
            )

            perfil = data.get("perfil", None)
            obj.is_admin = perfil == "is_admin"
            obj.save()

            # If usuaria is admin of organization, she is not reviewer anymore. Both are excludent roles:
            usuaria = Usuaria.objects.get(pk=usuaria_id)
            usuaria.is_reviewer = perfil == "is_reviewer"
            usuaria.save()

            return obj

    def get_success_url(self):
        return reverse(
            "organizacao_detail",
            kwargs={"organizacao_id": self.kwargs.get("organizacao_id", None)},
        )


class OrganizacaoUsuariaDeleteView(
    PermissionRequiredMixin, AjaxableResponseMixin, DeleteView
):
    model = OrganizacaoUsuaria
    organizacao = None
    permission_required = "organizacao.alterar"

    def get_permission_object(self):
        return self.get_object().organizacao

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.organizacao = self.get_object().organizacao
        self.object.delete()

        data = {
            "status": "success",
            "message": "Apagado com sucesso!",
            "redirect": self.get_success_url(),
        }
        return JsonResponse(data)

    def get_success_url(self):
        return reverse(
            "organizacao_detail", kwargs={"organizacao_id": self.organizacao.pk}
        )


class UsuariaCreateView(PermissionRequiredMixin, CreateView):
    model = Usuaria
    form_class = UsuariaCreationForm
    permission_required = "usuaria.administrar"
    titulo = "Cadastro de nova/o usuária/o"
    titulo_revisora = "Cadastro de novo/a revisor/a"
    organizacao_id = None
    organizacao = None

    def dispatch(self, request, *args, **kwargs):
        self.organizacao_id = kwargs.get("organizacao_id", None)
        if not self.request.user.has_perm("admin_geral"):
            self.titulo = self.titulo_revisora

        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)
            self.titulo = f"{self.titulo} - {self.organizacao}"
        return super(UsuariaCreateView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.organizacao_id:
            return reverse(
                "usuaria_list", kwargs={"organizacao_id": self.organizacao_id}
            )
        return reverse("usuaria_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "usuaria"
        context["organizacao_id"] = self.organizacao_id
        context["organizacao"] = self.organizacao
        context["temFooter"] = True
        context["titulo"] = self.titulo
        context["title"] = self.titulo
        return context

    def get_form_kwargs(self):
        kwargs = super(UsuariaCreateView, self).get_form_kwargs()
        kwargs.update(
            {"request_user": self.request.user, "organizacao_id": self.organizacao_id}
        )
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)

        # request_user = self.request.user

        if not self.organizacao_id:
            obj.save()
            is_admin_organizacao = False
            is_reviewer = obj.is_reviewer
        else:
            perfil = form.cleaned_data.get("perfil", None)
            is_admin_organizacao = perfil == "is_admin"
            is_reviewer = perfil == "is_reviewer" or not perfil
            obj.is_reviewer = is_reviewer
            obj.save()

            organizacao_usuaria, _ = OrganizacaoUsuaria.objects.update_or_create(
                organizacao_id=self.organizacao_id, usuaria_id=obj.id
            )
            if organizacao_usuaria.is_admin != is_admin_organizacao:
                organizacao_usuaria.is_admin = is_admin_organizacao
                organizacao_usuaria.save()

            gr_usuarios = Group.objects.get(name="usuarios")
            gr_usuarios.user_set.add(obj)

        return HttpResponseRedirect(self.get_success_url())


class UsuariaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Usuaria
    form_class = UsuariaUpdateForm
    permission_required = "usuaria.administrar"
    titulo = "Editar dados de usuária/o"
    titulo_revisora = "Editar dados de revisor/a"
    organizacao_id = None
    organizacao = None

    def dispatch(self, request, *args, **kwargs):
        self.organizacao_id = kwargs.get("organizacao_id", None)
        if not self.request.user.has_perm("admin_geral"):
            self.titulo = self.titulo_revisora

        if self.organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=self.organizacao_id)
            self.titulo = f"{self.titulo} - {self.organizacao}"
        return super(UsuariaUpdateView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.organizacao_id:
            return reverse(
                "usuaria_list", kwargs={"organizacao_id": self.organizacao_id}
            )
        return reverse("usuaria_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "usuaria"
        context["organizacao_id"] = self.organizacao_id
        context["organizacao"] = self.organizacao
        context["temFooter"] = True
        context["titulo"] = self.titulo
        context["title"] = self.titulo
        return context

    def get_form_kwargs(self):
        kwargs = super(UsuariaUpdateView, self).get_form_kwargs()
        kwargs.update(
            {"request_user": self.request.user, "organizacao_id": self.organizacao_id}
        )
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)

        request_user = self.request.user

        if not self.organizacao_id or (
            obj.id == request_user.id and request_user.has_perm("admin_geral")
        ):
            obj.save()
            is_admin_organizacao = False
            is_reviewer = obj.is_reviewer
        else:
            if obj.id == request_user.id:
                organizacao_usuaria = OrganizacaoUsuaria.objects.get(
                    organizacao_id=self.organizacao_id, usuaria_id=request_user.id
                )
                is_admin_organizacao = organizacao_usuaria.is_admin
                is_reviewer = request_user.is_reviewer
            else:
                perfil = form.cleaned_data.get("perfil", None)
                is_admin_organizacao = perfil == "is_admin"
                is_reviewer = perfil == "is_reviewer" or not perfil

            obj.is_reviewer = is_reviewer
            obj.save()

            organizacao_usuaria, created = OrganizacaoUsuaria.objects.update_or_create(
                organizacao_id=self.organizacao_id, usuaria_id=obj.id
            )
            if organizacao_usuaria.is_admin != is_admin_organizacao:
                organizacao_usuaria.is_admin = is_admin_organizacao
                organizacao_usuaria.save()

            if created:
                gr_usuarios = Group.objects.get(name="usuarios")
                gr_usuarios.user_set.add(obj)

        # Post-processing impacts of changing user's nature from reviewer to non-reviewer and vice-versa
        # TODO: Alert (modal) to admin geral of the consequences of these changes in usuaria_form.html
        if not is_reviewer:
            # If an user is defined as admin in an organization, admin_geral, cadastrante or parceira, she won't be reviewer anymore:
            obj.territorios_revisora.update(revisora=None)
            # Besides that, she will be REMOVED from all organizations where she was reviewer:
            OrganizacaoUsuaria.objects.filter(usuaria=obj, is_admin=False).delete()
        else:
            # If an user is defined as reviewer, she can't be admin of any organization:
            obj.org_usuarias.update(is_admin=False)

        return HttpResponseRedirect(self.get_success_url())


class RevisoraListView(PermissionRequiredMixin, FormView):
    form_class = UsuariaRevisoraForm
    permission_required = "usuaria.administrar"
    template_name = "main/revisora_list.html"
    success_url = reverse_lazy("revisora_list")

    def get_queryset(self):
        return Usuaria.objects.filter(is_reviewer=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        revisoras = form.data.getlist("nova_usuaria_revisora")
        Usuaria.objects.filter(id__in=revisoras).update(is_reviewer=True)

        return super().form_valid(form)


class UsuariaListView(PermissionRequiredMixin, ListView, FormMixin):
    model = Usuaria
    permission_required = "usuaria.listar"
    form_class = OrganizacaoUsuariaAssociarForm
    titulo = "Usuárias/os do sistema"
    titulo_em_organizacao = "Integrantes da organização"
    organizacao = None

    def dispatch(self, request, *args, **kwargs):
        organizacao_id = kwargs.get("organizacao_id", None)
        if organizacao_id:
            self.organizacao = Organizacao.objects.get(pk=organizacao_id)
            self.titulo = self.titulo_em_organizacao

        if not self.request.user.has_perm("admin_geral") and not self.organizacao:
            organizacoes_que_administra_ids = (
                self.request.user.organizacoes_que_administra_ids
            )
            organizacao_que_administra_id = organizacoes_que_administra_ids.first()[0]
            return redirect(
                "usuaria_list", organizacao_id=organizacao_que_administra_id
            )

        return super(UsuariaListView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(UsuariaListView, self).get_form_kwargs()
        kwargs.update({"request_user": self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = self.titulo
        context["titulo"] = self.titulo
        if self.organizacao:
            context["organizacao_id"] = self.organizacao.id
            context["organizacao"] = self.organizacao
        return context

    def get_queryset(self):
        queryset = Usuaria.objects.prefetch_related(
            "municipios", "territorios", "territorios_revisora", "org_usuarias"
        )

        if self.organizacao:
            queryset = queryset.filter(organizacoes__id=self.organizacao.id)

        if self.request.user.has_perm("admin_geral"):
            return queryset.filter(is_superuser=False).order_by("-ultima_alteracao")

        organizacoes_ids = OrganizacaoUsuaria.objects.filter(
            usuaria_id=self.request.user.id
        ).values_list("organizacao_id")

        usuarias = OrganizacaoUsuaria.objects.filter(
            organizacao_id__in=organizacoes_ids
        ).values_list("usuaria_id")

        return queryset.filter(id__in=usuarias).order_by("-ultima_alteracao")

    def post(self, request, *args, **kwargs):
        """metodo POST utilizado para inserir e remover usuárias em organização"""

        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            organizacao = Organizacao.objects.get(
                pk=request.POST.get("organizacao", None)
            )
        except:
            status_error["content"] = "Organização não encontrada"
            return JsonResponse(status_error)

        if not request.user.has_perm("organizacao.alterar", organizacao):
            return {
                "status": 403,
                "statusText": "PERMISSION DENIED",
                "content": "Permissão negada para esta ação!",
            }

        try:
            usuaria = Usuaria.objects.get(pk=request.POST.get("usuaria", None))
        except:
            status_error["content"] = "Usuária/o inexistente"
            return JsonResponse(status_error)

        organizacao_usuaria, created = OrganizacaoUsuaria.objects.update_or_create(
            usuaria=usuaria, organizacao=organizacao
        )
        is_admin = request.POST.get("perfil", None) == "is_admin"
        organizacao_usuaria.is_admin = is_admin
        organizacao_usuaria.save()

        usuaria.is_reviewer = not is_admin
        usuaria.save()

        if is_admin:
            # If an user is defined as admin in an organization, she won't be reviewer anymore:
            usuaria.territorios_revisora.update(revisora=None)
            # Besides that, she will be REMOVED from all organizations where she was reviewer:
            OrganizacaoUsuaria.objects.filter(usuaria=usuaria, is_admin=False).delete()

        else:
            # If an user is defined as reviewer in an organization, she won't be admin of any other organizations:
            usuaria.org_usuarias.update(is_admin=False)

        status_success = {
            "status": 200,
            "created": created,
            "statusText": "OK",
            "organizacao": f"{organizacao}",
            "organizacao_id": f"{organizacao.id}",
            "usuaria": f"{usuaria}",
            "usuaria_id": f"{usuaria.id}",
            "is_admin": is_admin,
            "organizacao_update_url": reverse(
                "organizacao_update", args=(organizacao.id,)
            ),
            "organizacao_remover_usuaria_url": reverse(
                "organizacao_remover_usuaria",
                kwargs={"organizacao_id": organizacao.id, "usuaria_id": usuaria.id},
            ),
        }
        return JsonResponse(status_success)


class UsuariaDetailView(PermissionRequiredMixin, DetailView):
    model = Usuaria
    permission_required = "usuaria.administrar"


class MensagemListView(PermissionRequiredMixin, ListView):
    model = Mensagem
    permission_required = "usuaria.administrar"
    template_name = "main/territorio_mensagens_list.html"

    def get_queryset(self):
        territorio = get_object_or_404(Territorio, id=self.kwargs["territorio_id"])
        return Mensagem.objects.filter(territorio=territorio).order_by("data_envio")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["usuaria"] = get_object_or_404(Usuaria, id=self.kwargs["usuaria_id"])
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context


class MensagemCreateView(PermissionRequiredMixin, CreateView):
    model = Mensagem
    form_class = MensagemCreationForm
    permission_required = "permissao_padrao"
    titulo = "Enviar Mensagem"
    territorio = None

    def dispatch(self, request, *args, **kwargs):
        self.territorio = get_object_or_404(Territorio, id=kwargs["territorio_id"])
        return super(MensagemCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["territorio"] = self.territorio
        context["sac_form"] = SacForm(territorio=self.territorio)
        context["titulo"] = f"{self.territorio}: Mensagens e SAC"
        context["title"] = f"Mensagens e SAC - {self.territorio}"
        context["mensagens_antigas"] = Mensagem.objects.filter(
            territorio=self.territorio
        )
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context

    def post(self, request, *args, **kwargs):
        if "sac_status" in request.POST:
            sac_status = self.request.POST.get("sac_status", None)
            self.territorio.sac_status = sac_status
            sac_assigned_to_id = self.request.POST.get("sac_assigned_to", None)
            sac_assigned_to = (
                None
                if not sac_assigned_to_id
                else Usuaria.objects.get(pk=sac_assigned_to_id)
            )
            self.territorio.sac_assigned_to = sac_assigned_to
            self.territorio.save()
            return HttpResponseRedirect(self.territorio.get_messages_url())
        else:
            return super(MensagemCreateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        self.territorio.send_push_message(form.cleaned_data["texto"], self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "territorio_mensagens",
            kwargs={"territorio_id": self.kwargs["territorio_id"]},
        )


class AnexoView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request, *args, **kwargs):
        anexos = Anexo.objects.all()
        serializer = AnexoSerializer(anexos, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        anexo = None
        try:
            anexo = Anexo.objects.get(id=request.data["id"])
        except:
            pass

        anexos_serializer = AnexoSerializer(data=request.data, instance=anexo)

        arquivo = request.data.get("arquivo", None)
        has_arquivo = anexo is not None or arquivo is not None

        if anexos_serializer.is_valid() and has_arquivo:
            anexo = anexos_serializer.save()
            # se o tipo de anexo for video, cria o thumbnail na mão, pois dá um erro no automático
            return Response(anexos_serializer.data, status=status.HTTP_201_CREATED)

        print("error", anexos_serializer.errors)
        # logging.warning("Erro enviando arquivo! " + anexos_serializer.errors)
        return Response(anexos_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PedidoCodigoSegurancaView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        usuaria = None
        try:
            username = request.data.get("username", None)
            cpf = request.data.get("cpf", None)
            usuaria = Usuaria.objects.get(username=username, cpf=cpf)
        except:
            error = {
                "error": "Não foi possível encontrar um(a) usuário(a) com este telefone e CPF."
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if usuaria.solicitou_codigo:
            response = {
                "title": "Pedido já foi realizado antes",
                "body": f"Olá {usuaria.first_name}!\n\nVocê já solicitou o código antes.\n\nPedimos por favor que aguarde o contato da equipe Tô no Mapa, que vai ser por telefone ou WhatsApp.\n\nOu então envie um e-mail diretamente para contato@tonomapa.org.br .",
            }
            return Response(response, status=status.HTTP_208_ALREADY_REPORTED)

        try:
            usuaria.solicitou_codigo = True
            usuaria.foto_documento = request.data.get("foto_documento", None)
            usuaria.save()

            response = {
                "nome": usuaria.first_name,
                "codigo_uso_celular": usuaria.codigo_uso_celular,
                "title": "Pedido realizado com sucesso",
                "body": f"Olá {usuaria.first_name}!\n\nEntraremos em breve em contato com você pelo seu telefone ou WhatsApp para solucionarmos essa questão.",
            }
            return Response(response, status=status.HTTP_200_OK)
        except:
            response = {
                "title": "Erro no envio dos dados!",
                "body": f"Olá {usuaria.first_name}!\n\nHouve um erro no envio dos dados, provavelmente por causa da foto do documento.\n\nFavor tentar com outra foto ou entrar em contato com a equipe do Tô no Mapa pelo site tonomapa.org.br",
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class TerritorioCreateView(PermissionRequiredMixin, CreateView):
    model = Territorio
    form_class = TerritorioForm
    permission_required = "territorio.cadastrar"
    titulo = "Nova Comunidade"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "territorio"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["temFooter"] = True
        return context


class TerritorioDetailReportView(PDFViewMixin, TemplateView):
    permission_required = "cadastrante"
    object = None
    filename = "relatorio.pdf"
    template_name = "main/territorio_detail_report.html"

    def dispatch(self, request, *args, **kwargs):
        access_allowed = True
        if not request.user.has_perm("permissao_padrao"):
            territorio_id = self.kwargs.get("pk", None)
            territorio = Territorio.objects.get(pk=territorio_id)
            access_allowed = request.user.eh_cadastrante_do_territorio(territorio)

        if access_allowed:
            return super().dispatch(request, *args, **kwargs)

        return JsonResponse("Acesso não permitido", status=500, safe=False)

    def get_file_response(self, content, output_file, extra_args):
        extra_args["print-to-pdf-no-header"] = None
        bytestring_to_pdf(content, output_file, **extra_args)

        pdf = Pdf.open(output_file.name, allow_overwriting_input=True)
        for page in pdf.pages:
            for key in page.images:
                image = page.images[key]
                pdfimage = PdfImage(image)
                rawimage = pdfimage.obj
                pillowimage = pdfimage.as_pil_image()
                if pillowimage.format == "JPEG":
                    buffer = io.BytesIO()
                    pillowimage.save(buffer, "JPEG", optimize=True, quality=15)
                    buffer.seek(0)
                    read = buffer.read()
                    rawimage.write(read, filter=Name("/DCTDecode"))
        pdf.save()
        output_file.seek(0)

        return FileResponse(output_file, content_type="application/pdf")

    def get_filename(self):
        date_txt = date.today().strftime("%d-%m-%Y")
        comunidade = slugify(self.object.__str__())
        return f"ToNoMapa_{comunidade}_{date_txt}.pdf"

    def get_context_data(self, **kwargs):
        territorio_id = kwargs.get("pk", None)
        self.object = Territorio.objects.get(pk=territorio_id)
        context = super().get_context_data(**kwargs)
        context["today"] = date.today()
        context["is_report"] = True
        context["object"] = self.object
        context["request"] = self.request
        context["modulo"] = "territorio"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["title"] = self.object.nome
        context["temFooter"] = True
        context["territorio"] = self.get_territorio()
        context["MAPBOX_ACCESS_TOKEN"] = settings.MAPBOX_ACCESS_TOKEN
        context["SITE_URL"] = settings.SITE_URL
        context["MAPA_STATUS_REV"] = get_mapa_status_rev()
        context["territorio_revisao_status"] = TerritorioRevisaoStatus
        context["ptt_testes"] = (
            JSONRenderer().render(PTTSerializer(self.object).data).decode("utf-8")
        )

        data = self.request.GET
        notify_id = data.get("notify", 0)
        if notify_id:
            try:
                notify = Notification.objects.get(
                    id=notify_id, recipient=self.request.user
                )
                notify.mark_as_read()
            except:
                pass

        return context

    def get_territorio(self):
        territorio_agregado = TerritorioAgregado.objects.get(id=self.object.id)
        return {
            "id": self.object.id,
            "nome": self.object.nome,
            "conflitos": get_conflitos_list(self.object.conflitos),
            "areas_de_uso": get_areas_de_uso_list(self.object.areas_de_uso),
            "poligonos": self.object.geo_poligonos,
            "area": territorio_agregado.area,
            "kml": territorio_agregado.poligono_kml,
        }


class TerritorioDetailView(PermissionRequiredMixin, UpdateView):
    model = Territorio
    permission_required = "permissao_padrao"
    form_class = TerritorioDetailForm
    template_name = "main/territorio_detail.html"

    def get_object(self):
        territorio = (
            Territorio.objects.select_related("revisora")
            .annotate(
                tem_ata=Exists(
                    Anexo.objects.filter(territorio_id=OuterRef("pk"), tipo_anexo="ata")
                )
            )
            .get(pk=self.kwargs["pk"])
        )
        return territorio

    def get_form_kwargs(self):
        kwargs = super(TerritorioDetailView, self).get_form_kwargs()
        kwargs.update({"request_user": self.request.user})
        return kwargs

    def territorio_json(self):
        r = JSONRenderer().render(PTTSerializer(self.object).data).decode("utf-8")
        return r

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modulo"] = "territorio"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["title"] = self.object.nome
        context["temFooter"] = True
        context["territorio"] = self.get_territorio()
        context["MAPBOX_ACCESS_TOKEN"] = settings.MAPBOX_ACCESS_TOKEN
        context["SITE_URL"] = settings.SITE_URL
        context["MAPA_STATUS_REV"] = get_mapa_status_rev()
        context["territorio_revisao_status"] = TerritorioRevisaoStatus
        context["ptt_testes"] = (
            JSONRenderer().render(PTTSerializer(self.object).data).decode("utf-8")
        )

        data = self.request.GET
        notify_id = data.get("notify", 0)
        if notify_id:
            try:
                notify = Notification.objects.get(
                    id=notify_id, recipient=self.request.user
                )
                notify.mark_as_read()
            except:
                pass

        return context

    def get_territorio(self):
        territorio_agregado = TerritorioAgregado.objects.get(id=self.object.id)
        return {
            "id": self.object.id,
            "nome": self.object.nome,
            "conflitos": get_conflitos_list(self.object.conflitos),
            "areas_de_uso": get_areas_de_uso_list(self.object.areas_de_uso),
            "poligonos": self.object.geo_poligonos,
            "area": territorio_agregado.area,
            "kml": territorio_agregado.poligono_kml,
        }

    def send_push_message(self, form):
        territorio = self.object
        texto = ""
        mensagem = form.cleaned_data["mensagem"]
        if mensagem:
            texto += mensagem

        status_revisao = territorio.status_revisao.get()

        if "status" in form.changed_data:
            send_mensagem = territorio.send_push_message(
                texto, self.request.user, {"statusId": territorio.status.id}
            )

    def send_message_to_reviewer(self, form):
        territorio = self.object

        texto = ""
        if "status_revisao" in form.changed_data:
            if (
                int(form.cleaned_data["status_revisao"])
                == TerritorioRevisaoStatus.A_REVISAR
            ):
                texto += f"Territorio para revisar: {territorio} \n"
                mensagem = form.cleaned_data.get("mensagem", "")

        if texto:
            target_url = reverse("territorio_detail", args=(self.object.id,))
            notification_list = reverse_lazy("notifications:all")
            notify.send(
                self.request.user,
                recipient=self.object.revisora,
                verb=texto,
                action_object=territorio,
                target=territorio,
                description=mensagem,
                target_url=target_url,
                notification_list=notification_list,
            )

    def save_status_revisao(self, form):
        territorio = self.object
        if "status_revisao" in form.changed_data:
            status_revisao, created = TerritorioRevisaoStatus.objects.get_or_create(
                territorio=territorio,
            )
            status_revisao.alterado_por = self.request.user
            status_revisao.status = form.cleaned_data["status_revisao"]
            status_revisao.save()

    def form_valid(self, form):
        super().form_valid(form)

        self.save_status_revisao(form)
        self.send_push_message(form)
        self.send_message_to_reviewer(form)

        return super().form_valid(form)


class TerritorioUpdateView(PermissionRequiredMixin, UpdateView):
    model = Territorio
    form_class = TerritorioForm
    permission_required = "territorio.alterar"
    titulo = "Editar Comunidade"

    def get_form_kwargs(self):
        kwargs = super(TerritorioUpdateView, self).get_form_kwargs()
        kwargs.update(
            {
                "can_delete": self.request.user.has_perm(
                    "territorio.apagar", self.get_object()
                )
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["titulo"] = self.titulo
        context["modulo"] = "territorio"
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        context["temFooter"] = True
        context["org_usuarias"] = self.get_object().org_usuarias.order_by(
            "usuaria__username"
        )
        return context


class TerritorioDeleteView(PermissionRequiredMixin, DeleteView):
    model = Territorio
    permission_required = "territorio.apagar"
    success_url = reverse_lazy("TerritorioAgregadoIndex", current_app="relatorios")

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TerritorioDeleteView, self).dispatch(*args, **kwargs)


class TermosDeUsoView(TemplateView):
    template_name = "main/termosdeuso.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url = (
            "https://tonomapa.org.br/wp-json/wp/v2/pages/?slug=politica-de-privacidade"
        )
        termosdeuso = requests.get(url).json()
        date = termosdeuso[0]["modified"]
        context["date"] = date[8:10] + "/" + date[5:7] + "/" + date[0:4]
        context["title"] = termosdeuso[0]["title"]["rendered"]
        context["content"] = termosdeuso[0]["content"]["rendered"]
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context


class TICCAsView(TemplateView):
    template_name = "main/ticcas.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url = "https://tonomapa.org.br/wp-json/wp/v2/pages/?slug=ticcas"
        ticcas = requests.get(url).json()
        date = ticcas[0]["modified"]
        context["date"] = date[8:10] + "/" + date[5:7] + "/" + date[0:4]
        context["title"] = ticcas[0]["title"]["rendered"]
        context["content"] = ticcas[0]["content"]["rendered"]
        context["organizacao_id"] = self.kwargs.get("organizacao_id", None)
        return context


class UsuariaMunicipioUpdateView(PermissionRequiredMixin, UpdateView):
    model = Usuaria
    form_class = UsuariaMunicipioForm
    permission_required = "usuaria.administrar"
    template_name = "main/usuaria_municipio_form.html"
    success_url = reverse_lazy("usuaria_list")


class UsuariaTerritorioUpdateView(PermissionRequiredMixin, UpdateView):
    model = Usuaria
    form_class = UsuariaTerritorioForm
    permission_required = "usuaria.administrar"
    template_name = "main/usuaria_territorio_form.html"
    success_url = reverse_lazy("usuaria_list")


class RevisoraTerritorioDesassociarView(
    PermissionRequiredMixin, AjaxableResponseMixin, FormView
):
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            territorio = Territorio.objects.get(pk=kwargs.get("pk", None))
        except:
            return JsonResponse(status_error)

        territorio.revisora = None
        territorio.save()

        territorio_inacessivel = False
        if request.user.is_reviewer:
            territorios_visiveis = get_territorios_visiveis_revisora(
                request.user
            ).filter(id=territorio.id)
            territorio_inacessivel = territorios_visiveis.count() == 0

        status_success = {
            "status": 200,
            "statusText": "OK",
            "territorio": f"{territorio}",
            "territorio_id": territorio.id,
        }
        if territorio_inacessivel:
            status_success["territorio_inacessivel"] = "t"

        return JsonResponse(status_success)


class OrganizacaoTerritorioDesassociarView(
    PermissionRequiredMixin, AjaxableResponseMixin, FormView
):
    permission_required = "permissao_padrao"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            territorio = Territorio.objects.get(pk=kwargs.get("pk", None))
        except:
            return JsonResponse(status_error)

        territorio.organizacao = None
        territorio.save()

        status_success = {
            "status": 200,
            "statusText": "OK",
            "territorio": f"{territorio}",
            "territorio_id": territorio.id,
        }

        return JsonResponse(status_success)


class UsuariaMunicipioDesassociarView(
    PermissionRequiredMixin, AjaxableResponseMixin, FormView
):
    permission_required = "usuaria.administrar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            municipio = Municipio.objects.get(pk=kwargs.get("pk", None))
        except:
            return JsonResponse(status_error)

        try:
            usuaria = Usuaria.objects.get(pk=kwargs.get("usuaria_id", None))
        except:
            return JsonResponse(status_error)

        usuaria.municipios.remove(municipio)

        status_success = {
            "status": 200,
            "statusText": "OK",
            "title": f"Município '{municipio}'",
            "object_id": municipio.id,
        }

        return JsonResponse(status_success)


class OrganizacaoUsuariaRemoverView(
    PermissionRequiredMixin, AjaxableResponseMixin, DeleteView
):
    model = OrganizacaoUsuaria
    permission_required = "organizacao.alterar"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_permission_object(self):
        return self.get_object().organizacao

    def get_object(self):
        return OrganizacaoUsuaria.objects.get(
            usuaria_id=self.kwargs.get("usuaria_id", None),
            organizacao_id=self.kwargs.get("organizacao_id", None),
        )

    def post(self, request, *args, **kwargs):
        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            obj = self.get_object()
            obj.delete()
            status_success = {
                "status": 200,
                "statusText": "OK",
                "usuaria": f"{obj.usuaria}",
                "usuaria_id": obj.usuaria.id,
            }
            return JsonResponse(status_success)
        except:
            return JsonResponse(status_error)


class OrganizacaoMunicipioOuEstadoRemoverView(
    PermissionRequiredMixin, AjaxableResponseMixin, DeleteView
):
    model = Organizacao
    permission_required = "admin_geral"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return Organizacao.objects.get(id=self.kwargs.get("organizacao_id", None))

    def post(self, request, *args, **kwargs):
        status_error = {"status": 500, "statusText": "FAIL"}

        try:
            organizacao = self.get_object()
            estado_id = kwargs.get("estado_id", None)
            municipio_id = kwargs.get("municipio_id", None)

            if estado_id:
                estado = Estado.objects.get(pk=estado_id)
                organizacao.estados.remove(estado)

            if municipio_id:
                municipio = Municipio.objects.get(pk=municipio_id)
                organizacao.municipios.remove(municipio)

            status_success = {
                "status": 200,
                "statusText": "OK",
                "num_territorios": conta_territorios_organizacao(organizacao),
            }
            return JsonResponse(status_success)
        except:
            return JsonResponse(status_error)


class AjaxMunicipiosOuEstadosList(ListView):
    organizacao_id = None
    estado_id = None
    is_empty = None
    q = None
    """
    Intended to be called via ajax. Returns json list of Cities from State, or list of States.
    """

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            self.organizacao_id = self.kwargs.get("organizacao_id", None)
            self.estado_id = self.kwargs.get("estado_id", None)
            self.is_empty = self.kwargs.get("empty", False)
            self.q = self.request.GET.get("q", None)
            return super(AjaxMunicipiosOuEstadosList, self).dispatch(
                request, *args, **kwargs
            )
        return JsonResponse("Acesso não permitido", status=500, safe=False)

    def get_queryset(self):
        if self.is_empty:
            return Municipio.objects.none()

        organizacao = Organizacao.objects.get(pk=self.organizacao_id)

        if self.estado_id:
            qs = Municipio.objects.filter(estado=self.estado_id).exclude(
                id__in=organizacao.municipios.all()
            )
            if self.q:
                qs = qs.filter(nome__icontains=self.q)
            return qs

        qs = Estado.objects.exclude(id__in=organizacao.estados.all())
        if self.q:
            qs = qs.filter(Q(nome__icontains=self.q) | Q(uf__icontains=self.q))
        return qs

    def get(self, request, *args, **kwargs):
        values = ("id", "text")
        annotate = {"text": F("nome") if (self.estado_id) else F("nome")}
        queryset = self.get_queryset().annotate(**annotate).values(*values)
        data = list(queryset)
        return JsonResponse({"results": data}, status=200, safe=False)
