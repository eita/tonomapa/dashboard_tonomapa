from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from main.utils import get_choice

# from django.db.models import Q

from main import models


class UsuariaUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = models.Usuaria


class UsuariaAdmin(UserAdmin):
    form = UsuariaUpdateForm
    ordering = ("-ultima_alteracao",)
    list_display = (
        "id",
        "username",
        "first_name",
        "last_name",
        "ultima_alteracao",
        "solicitou_codigo",
        "codigo_uso_celular",
        "devices",
        "is_admin",
        "is_reviewer",
        "is_parceira",
    )
    fieldsets = UserAdmin.fieldsets + (
        (
            None,
            {
                "fields": (
                    "is_admin",
                    "is_reviewer",
                    "is_parceira",
                    "telefone",
                    "codigo_uso_celular",
                    "solicitou_codigo",
                    "foto_documento",
                    "cpf",
                )
            },
        ),
    )

    def get_queryset(self, request):
        queryset = super(UsuariaAdmin, self).get_queryset(request)
        return queryset.prefetch_related("devices")

    def devices(self, obj):
        devices = obj.devices.all()
        return ", ".join([device.push_token for device in devices])


class TerritorioAdmin(admin.ModelAdmin):
    list_display = (
        "nome",
        "status",
        "teste",
        "revisora",
        "cadastrante_display",
        "enviado_ptt",
        "status_revisao_choice",
        "municipio_referencia",
        "criacao",
        "ultima_alteracao",
        "publico",
    )
    readonly_fields = (
        "municipio_referencia",
        "ec_municipio",
    )
    search_fields = (
        "nome",
        "revisora__first_name",
        "revisora__last_name",
        "municipio_referencia__nome",
        "terr_usuarias__usuaria__first_name",
        "terr_usuarias__usuaria__last_name",
    )
    autocomplete_fields = ["revisora", "organizacao", "tipos_comunidade"]

    def get_queryset(self, request):
        queryset = super(TerritorioAdmin, self).get_queryset(request)
        return queryset.prefetch_related(
            "terr_usuarias", "terr_usuarias__usuaria", "status_revisao"
        ).select_related(
            "status",
            "municipio_referencia",
            "municipio_referencia__estado",
            "revisora",
        )

    def status_revisao_choice(self, obj):
        status_revisao = obj.get_status_revisao()
        return get_choice(status_revisao.status)

    status_revisao_choice.short_description = "Status revisão"

    def cadastrante_display(self, obj):
        return ", ".join(
            [terr_usuaria.usuaria.__str__() for terr_usuaria in obj.terr_usuarias.all()]
        )

    cadastrante_display.short_description = "Cadastrante"


class TerritorioUsuariaAdmin(admin.ModelAdmin):
    list_display = (
        "usuaria",
        "territorio",
    )
    search_fields = ("usuaria__first_name", "usuaria__last_name", "territorio__nome")
    autocomplete_fields = ("usuaria", "territorio")


class OrganizacaoAdmin(admin.ModelAdmin):
    list_display = ("nome",)
    search_fields = ("nome",)


class TipoComunidadeAdmin(admin.ModelAdmin):
    list_display = ("nome",)
    search_fields = ("nome",)


class TerritorioRevisaoStatusAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "status",
    )


class AreaDeUsoAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "nome",
        "tipo_area_de_uso",
        "area",
    )


class ConflitoAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "nome",
        "tipo_conflito",
    )


class AnexoAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "nome",
        "tipo_anexo",
        "mimetype",
        "arquivo",
        "file_size",
    )
    search_fields = ("territorio__nome", "nome", "tipo_anexo")
    autocomplete_fields = ["territorio"]


class DeviceAdmin(admin.ModelAdmin):
    list_display = (
        "usuaria",
        "active",
        "push_token",
    )


class MensagemAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "remetente",
        "data_envio",
        "origin_is_device",
    )

    def get_queryset(self, request):
        queryset = super(MensagemAdmin, self).get_queryset(request)
        return queryset.select_related(
            "territorio",
            "remetente",
        )


class MensagemDeviceAdmin(admin.ModelAdmin):
    list_display = (
        "territorio",
        "remetente",
        "data_envio",
        "status",
        "error_data",
        "push_token",
    )

    def remetente(self, obj):
        remetente = obj.mensagem.remetente
        return remetente.__str__()

    def territorio(self, obj):
        territorio = obj.mensagem.territorio
        return territorio.nome

    def data_envio(self, obj):
        data_envio = obj.mensagem.data_envio
        return data_envio

    def push_token(self, obj):
        push_token = obj.device.push_token
        return push_token

    def get_queryset(self, request):
        queryset = super(MensagemDeviceAdmin, self).get_queryset(request)
        return queryset.select_related(
            "device",
            "mensagem",
            "mensagem__territorio",
            "mensagem__remetente",
        )


class BiomaAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Usuaria, UsuariaAdmin)
admin.site.register(models.Organizacao, OrganizacaoAdmin)
admin.site.register(models.Estado)
admin.site.register(models.Municipio)
admin.site.register(models.Territorio, TerritorioAdmin)
admin.site.register(models.TerritorioStatus)
admin.site.register(models.TerritorioRevisaoStatus, TerritorioRevisaoStatusAdmin)
admin.site.register(models.TipoAreaDeUso)
admin.site.register(models.TipoComunidade, TipoComunidadeAdmin)
admin.site.register(models.TipoConflito)
admin.site.register(models.TerritorioUsuaria, TerritorioUsuariaAdmin)
admin.site.register(models.MunicipioUsuaria)
admin.site.register(models.OrganizacaoUsuaria)
admin.site.register(models.Conflito, ConflitoAdmin)
admin.site.register(models.AreaDeUso, AreaDeUsoAdmin)
admin.site.register(models.Mensagem, MensagemAdmin)
admin.site.register(models.Device, DeviceAdmin)
admin.site.register(models.MensagemDevice, MensagemDeviceAdmin)
admin.site.register(models.Anexo, AnexoAdmin)
admin.site.register(models.Bioma, BiomaAdmin)

# PTT
admin.site.register(models.ComunicacaoPttLog)
admin.site.register(models.TipoComunidadePTT)
