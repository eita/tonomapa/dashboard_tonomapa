from django.urls import path, include
from . import views

urlpatterns = [
    path("organizacoes", views.OrganizacaoListView.as_view(), name="organizacao_list"),
    path("organizacoes/create/", views.OrganizacaoCreateView.as_view(), name="organizacao_create"),
    path("organizacao/<int:organizacao_id>/detail/", views.OrganizacaoDetailView.as_view(), name="organizacao_detail"),
    path("organizacao/<int:organizacao_id>/update/", views.OrganizacaoUpdateView.as_view(), name="organizacao_update"),
    path("organizacao/<int:organizacao_id>/delete/", views.OrganizacaoDeleteView.as_view(), name="organizacao_delete"),
    path(
        "organizacao/<int:organizacao_id>/remover_usuaria/<int:usuaria_id>",
        views.OrganizacaoUsuariaRemoverView.as_view(),
        name="organizacao_remover_usuaria",
    ),
    path(
        "organizacao/<int:organizacao_id>/organizacaousuaria/update/",
        views.OrganizacaoUsuariaUpdateView.as_view(),
        name="organizacaousuaria_update",
    ),
    path(
        "organizacaousuaria/delete/<int:pk>",
        views.OrganizacaoUsuariaDeleteView.as_view(),
        name="organizacaousuaria_delete",
    ),
    path(
        "organizacao/<int:organizacao_id>/remover_municipio/<int:municipio_id>",
        views.OrganizacaoMunicipioOuEstadoRemoverView.as_view(),
        name="organizacao_remover_municipio",
    ),
    path(
        "organizacao/<int:organizacao_id>/remover_estado/<int:estado_id>",
        views.OrganizacaoMunicipioOuEstadoRemoverView.as_view(),
        name="organizacao_remover_estado",
    ),
]
