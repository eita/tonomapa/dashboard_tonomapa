# Generated by Django 3.0.3 on 2020-04-02 23:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20200402_2315'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizacaousuaria',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
