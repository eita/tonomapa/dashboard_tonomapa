# Generated by Django 3.2.14 on 2023-01-20 02:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0115_auto_20230112_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anexo',
            name='arquivo',
            field=models.FileField(blank=True, max_length=400, null=True, upload_to='anexos/%Y/%m/%d/'),
        ),
    ]
