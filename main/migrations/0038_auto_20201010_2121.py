# Generated by Django 3.0.3 on 2020-10-10 21:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_auto_20201009_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anexo',
            name='territorio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='anexos', to='main.Territorio'),
        ),
    ]
