# Generated by Django 3.0.3 on 2021-03-29 00:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0050_usuaria_municipios'),
    ]

    operations = [
        migrations.AlterField(
            model_name='municipiousuaria',
            name='municipio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviewers', to='main.Municipio'),
        ),
        migrations.AlterField(
            model_name='territoriousuaria',
            name='territorio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviewers', to='main.Territorio'),
        ),
    ]
