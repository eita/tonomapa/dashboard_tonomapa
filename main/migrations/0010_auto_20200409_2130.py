# Generated by Django 3.0.3 on 2020-04-09 21:30

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20200409_2051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='territorio',
            name='poligono',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=4326),
        ),
    ]
