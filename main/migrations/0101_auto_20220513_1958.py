# Generated by Django 3.2 on 2022-05-13 22:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0100_auto_20220507_1006'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ModoDeVida',
        ),
        migrations.DeleteModel(
            name='TipoProducao',
        ),
    ]
