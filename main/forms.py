# from typing import Any
from dal import autocomplete  # type: ignore
from crispy_forms.helper import FormHelper  # type: ignore
from crispy_forms.layout import Button, Submit  # type: ignore
from django import forms
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.gis.forms import GeometryField, BaseGeometryWidget  # type: ignore
from django.urls import reverse
from main.models import (
    TipoComunidade,
    Usuaria,
    Estado,
    Municipio,
    Anexo,
    TipoAreaDeUso,
    AreaDeUso,
    TipoConflito,
    Conflito,
    OrganizacaoUsuaria,
    Organizacao,
    Territorio,
    TerritorioRevisaoStatus,
    TerritorioStatus,
    Mensagem,
    Bioma,
)


class BaseModelForm(forms.ModelForm):
    can_delete = False
    delete_confirm_message = ""
    delete_success_url = ""
    delete_url = ""

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        if self.can_delete:
            self.helper.add_input(
                Button(
                    "delete-button",
                    "Apagar",
                    css_class="btn btn-danger",
                    data_delete_confirm_message=self.delete_confirm_message,
                    data_success_url=self.delete_success_url,
                    data_delete_url=self.delete_url,
                )
            )
        self.helper.add_input(Submit("submit", "Gravar"))


class BaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        self.helper.add_input(Submit("submit", "Gravar"))


class TipoComunidadeForm(BaseModelForm):
    class Meta:
        model = TipoComunidade
        fields = ["nome", "descricao"]


class UsuariaForm(BaseModelForm):
    class Meta:
        model = Usuaria
        fields = ["username", "first_name", "last_name", "email"]


class EstadoForm(BaseModelForm):
    class Meta:
        model = Estado
        fields = ["uf", "nome"]


class MunicipioForm(BaseModelForm):
    class Meta:
        model = Municipio
        fields = ["nome", "estado"]


class AnexoForm(BaseModelForm):
    class Meta:
        model = Anexo
        fields = [
            "nome",
            "descricao",
            "mimetype",
            "file_size",
            "territorio",
            "tipo_anexo",
            "publico",
        ]


class TipoAreaDeUsoForm(BaseModelForm):
    class Meta:
        model = TipoAreaDeUso
        fields = ["nome", "descricao"]


class AreaDeUsoForm(BaseModelForm):
    class Meta:
        model = AreaDeUso
        fields = [
            "nome",
            "posicao",
            "descricao",
            "area",
            "territorio",
            "tipo_area_de_uso",
        ]


class TipoConflitoForm(BaseModelForm):
    class Meta:
        model = TipoConflito
        fields = ["nome", "descricao"]


class ConflitoForm(BaseModelForm):
    class Meta:
        model = Conflito
        fields = ["nome", "posicao", "descricao", "territorio", "tipo_conflito"]


class OrganizacaoUsuariaForm(forms.ModelForm):
    class Meta:
        model = OrganizacaoUsuaria
        fields = ["usuaria"]


class OrganizacaoForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop("can_delete", False)
        if self.can_delete:
            self.delete_confirm_message = "Deseja realmente apagar esta organização e todos os elementos que fazem parte dela (agroecossistemas, etc)?"
            self.delete_url = reverse(
                "organizacao_delete",
                kwargs={"organizacao_id": kwargs.get("instance").id},
            )
            self.delete_success_url = reverse("organizacao_list")

        super(OrganizacaoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Organizacao
        fields = ["nome", "site", "logo"]


class TerritorioForm(BaseModelForm):
    poligono = GeometryField(srid=4326, required=False, widget=BaseGeometryWidget())

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop("can_delete", False)
        if self.can_delete:
            self.delete_confirm_message = "Deseja realmente apagar esta comunidade e todos os elementos que fazem parte dele (mensagens, etc)?"
            self.delete_url = reverse(
                "territorio_delete", kwargs={"pk": kwargs.get("instance").id}
            )
            # self.delete_success_url = reverse('territorio_list')

        super(TerritorioForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Territorio
        fields = [
            "nome",
            "poligono",
            "ano_fundacao",
            "qtde_familias",
            "status",
            "municipio_referencia",
        ]


class TerritorioDetailForm(BaseModelForm):
    teste = forms.BooleanField(label="É comunidade de teste?", required=False)
    status_revisao = forms.ChoiceField(
        label="Status da revisão",
        required=False,
        choices=(("", "Selecione"),) + TerritorioRevisaoStatus.CHOICES,
    )
    status = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    mensagem = forms.CharField(widget=forms.Textarea(attrs={"rows": 8}), required=False)

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request_user")
        super().__init__(*args, **kwargs)
        if kwargs["instance"]:
            try:
                status_revisao = TerritorioRevisaoStatus.objects.get(
                    territorio=kwargs["instance"]
                ).status
            except:
                status_revisao = ""
            self.fields["status_revisao"].initial = status_revisao
        if not request_user.has_perm("admin_geral"):
            self.fields.pop("organizacao")

    def clean(self):
        cleaned_data = self.cleaned_data
        status = cleaned_data.get("status")
        cleaned_data["status"] = TerritorioStatus.objects.get(id=status)
        return cleaned_data

    class Meta:
        model = Territorio
        fields = [
            "teste",
            "organizacao",
            "contato_referencia_nome",
            "contato_referencia_telefone",
            "observacoes",
            "status_revisao",
            "status",
            "mensagem",
        ]
        widgets = {
            "contato_referencia_telefone": forms.TextInput(
                attrs={"data-mask": "(00)00000-0000"}
            )
        }


class MensagemCreationForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        super(MensagemCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_class = "form-horizontal"
        # self.helper.label_class = "d-none"
        # self.helper.field_class = "col-lg-12"
        self.helper.add_input(Submit("submit", "Enviar"))

    class Meta:
        model = Mensagem
        fields = ["texto"]
        labels = {"texto": "Nova mensagem", "extra": "Extra (JSON)"}
        required = {"texto": True, "extra": False}


class SacForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        territorio = kwargs.pop("territorio", None)
        super(SacForm, self).__init__(*args, **kwargs)
        self.fields["sac_assigned_to"].queryset = Usuaria.objects.filter(
            Q(is_superuser=True) | Q(is_admin=True)
        )
        if territorio:
            self.fields["sac_status"].initial = territorio.sac_status
            self.fields["sac_assigned_to"].initial = territorio.sac_assigned_to
        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-3"
        self.helper.field_class = "col-lg-9"
        self.helper.add_input(Submit("submit", "Atualizar acompanhamento SAC"))

    class Meta:
        model = Territorio
        fields = ["sac_status", "sac_assigned_to"]
        labels = {"sac_status": "Status", "sac_assigned_to": "Pessoa responsável"}


class UsuariaCreationForm(UserCreationForm, BaseModelForm):
    perfil = forms.ChoiceField(
        choices=[()], widget=forms.RadioSelect, initial="", required=True
    )

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request_user", None)
        organizacao_id = kwargs.pop("organizacao_id", None)
        super(UsuariaCreationForm, self).__init__(*args, **kwargs)
        if organizacao_id:
            self.fields.pop("is_reviewer")
            if request_user.has_perm("admin_geral"):
                self.fields["perfil"].choices = [
                    ("is_reviewer", "Revisor/a"),
                    ("is_admin", "Administrador/a da organização"),
                ]
                self.fields["perfil"].initial = (
                    "is_reviewer" if request_user.is_reviewer else "is_admin"
                )
            else:
                self.fields.pop("perfil")
        else:
            self.fields["perfil"].choices = [
                ("", "Cadastrante"),
                ("is_reviewer", "Revisor/a"),
                ("is_parceira", "Parceiro/a"),
                ("is_admin", "Administrador/a Geral do Sistema"),
            ]

    class Meta:
        model = Usuaria
        fields = [
            "username",
            "first_name",
            "last_name",
            "cpf",
            "email",
            "password1",
            "password2",
            "telefone",
            "codigo_uso_celular",
            "is_admin",
            "is_reviewer",
            "is_parceira",
        ]
        labels = {
            "username": "Nome de usuária/o",
            "first_name": "Nome",
            "last_name": "Sobrenome",
            "password1": "Senha",
            "password2": "Confirmar Senha",
            "email": "Endereço de e-mail",
            "telefone": "Número do celular",
            "codigo_uso_celular": "Código de segurança",
        }
        help_texts = {
            "telefone": "Com DDD e somente números",
            "codigo_uso_celular": "Para entrar pelo App no celular",
        }
        required = {
            "email": True,
            "first_name": True,
            "telefone": True,
            "codigo_uso_celular": True,
        }
        widgets = {
            "is_admin": forms.HiddenInput(),
            "is_reviewer": forms.HiddenInput(),
            "is_parceira": forms.HiddenInput(),
        }


class UsuariaUpdateForm(UserChangeForm, BaseModelForm):
    perfil = forms.ChoiceField(
        choices=[()], widget=forms.RadioSelect, initial="", required=False
    )

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request_user", None)
        organizacao_id = kwargs.pop("organizacao_id", None)
        super(UsuariaUpdateForm, self).__init__(*args, **kwargs)
        if organizacao_id:
            self.fields.pop("is_reviewer")
            if request_user.has_perm("admin_geral"):
                self.fields["perfil"].choices = [
                    ("is_reviewer", "Revisor/a"),
                    ("is_admin", "Administrador/a da organização"),
                ]
                self.fields["perfil"].initial = (
                    "is_reviewer" if request_user.is_reviewer else "is_admin"
                )
            else:
                self.fields.pop("perfil")
        else:
            self.fields["perfil"].choices = [
                ("", "Cadastrante"),
                ("is_reviewer", "Revisor/a"),
                ("is_parceira", "Parceiro/a"),
                ("is_admin", "Administrador/a Geral do Sistema"),
            ]

    class Meta:
        model = Usuaria
        fields = [
            "username",
            "first_name",
            "last_name",
            "cpf",
            "email",
            "telefone",
            "codigo_uso_celular",
            "solicitou_codigo",
            "is_admin",
            "is_reviewer",
            "is_parceira",
        ]
        labels = {
            "username": "Nome de usuária/o",
            "first_name": "Nome",
            "last_name": "Sobrenome",
            "email": "Endereço de e-mail",
            "telefone": "Número do celular",
            "codigo_uso_celular": "Código de segurança",
            "solicitou_codigo": "É preciso enviar o código de segurança?",
        }
        help_texts = {
            "telefone": "Com DDD e somente números",
            "codigo_uso_celular": "Para entrar pelo App no celular",
        }
        required = {
            "email": True,
            "first_name": True,
            "telefone": True,
            "codigo_uso_celular": True,
        }
        widgets = {
            "is_admin": forms.HiddenInput(),
            "is_reviewer": forms.HiddenInput(),
            "is_parceira": forms.HiddenInput(),
        }


class UsuariaTerritorioForm(BaseModelForm):
    class Meta:
        model = Usuaria
        fields = [
            "territorios",
        ]
        widgets = {
            "territorios": autocomplete.ModelSelect2Multiple(
                url="territorio-autocomplete"
            )
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        territorios = cleaned_data.get("territorios")
        form_usuaria = self.instance

        if form_usuaria.is_reviewer:
            territorios_revisoras = [
                territorio.nome
                for territorio in territorios
                if territorio.terr_usuarias.filter(usuaria__is_reviewer=True)
                .exclude(usuaria=form_usuaria)
                .count()
                > 0
            ]
            if territorios_revisoras:
                raise forms.ValidationError(
                    f"As seguintes comunidades já tem uma revisora relacionada: {', '.join(territorios_revisoras)}"
                )

        return cleaned_data


class UsuariaMunicipioForm(BaseModelForm):
    class Meta:
        model = Usuaria
        fields = [
            "municipios",
        ]
        widgets = {
            "municipios": autocomplete.ModelSelect2Multiple(
                url="municipio-autocomplete"
            )
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        municipios = cleaned_data.get("municipios")
        form_usuaria = self.instance

        if form_usuaria.is_reviewer:
            municipios_revisoras = [
                municipio.nome
                for municipio in municipios
                if municipio.municipio_usuarias.filter(usuaria__is_reviewer=True)
                .exclude(usuaria=form_usuaria)
                .count()
                > 0
            ]
            if municipios_revisoras:
                raise forms.ValidationError(
                    f"Os seguintes municípios já tem uma revisora relacionada: {', '.join(municipios_revisoras)}"
                )

        return cleaned_data


class UsuariaRevisoraForm(BaseForm):
    nova_usuaria_revisora = forms.ModelChoiceField(
        queryset=Usuaria.objects.filter(is_reviewer=False),
        widget=autocomplete.ModelSelect2Multiple(url="user-not-reviewer-autocomplete"),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        self.helper.add_input(Submit("submit", "Associar"))


class MapaFiltroForm(forms.Form):
    estado = forms.ModelMultipleChoiceField(
        queryset=Estado.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url="estado-autocomplete"),
        required=False,
        label="Estado",
    )

    bioma = forms.ModelMultipleChoiceField(
        queryset=Bioma.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url="bioma-autocomplete"),
        required=False,
        label="Bioma",
    )

    comunidade = forms.ModelMultipleChoiceField(
        queryset=TipoComunidade.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            url="comunidade-autocomplete", forward=["estado"]
        ),
        required=False,
        label="Tipo de comunidade",
    )

    status_revisao = forms.MultipleChoiceField(
        choices=(TerritorioRevisaoStatus.CHOICES),
        widget=autocomplete.ModelSelect2Multiple(),
        required=False,
        label="Status da revisão",
    )

    publico = forms.ChoiceField(
        choices=(("", "Todos"), ("0", "Não"), ("1", "Sim")),
        required=False,
        label="Dados públicos",
    )

    revisora = forms.ModelMultipleChoiceField(
        queryset=Usuaria.objects.filter(is_reviewer=True),
        widget=autocomplete.ModelSelect2Multiple(url="reviewer-autocomplete"),
        required=False,
        label="Revisor/a",
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if user.is_parceira:
            self.fields.pop("status_revisao")
            self.fields.pop("publico")
            self.fields.pop("revisora")
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", "Filtrar"))


class MapaPublicoFiltroForm(forms.Form):
    estado = forms.ModelMultipleChoiceField(
        queryset=Estado.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url="estado-autocomplete"),
        required=False,
        label="Estado",
    )

    bioma = forms.ModelMultipleChoiceField(
        queryset=Bioma.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url="bioma-autocomplete"),
        required=False,
        label="Bioma",
    )

    comunidade = forms.ModelMultipleChoiceField(
        queryset=TipoComunidade.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            url="comunidade-autocomplete", forward=["estado"]
        ),
        required=False,
        label="Tipo de comunidade",
    )

    publico = forms.ChoiceField(
        choices=(("", "Todos"), ("0", "Não"), ("1", "Sim")),
        required=False,
        label="Dados públicos",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", "Filtrar"))


class UsuariaTerritorioAssociarForm(forms.Form):
    territorio = forms.CharField(widget=forms.HiddenInput(), required=False)
    revisora = forms.ModelChoiceField(queryset=None, required=False, label="Revisor(a)")

    def __init__(self, *args, **kwargs):
        usuaria = kwargs.pop("usuaria", None)
        organizacao_id = kwargs.pop("organizacao_id", None)
        super(UsuariaTerritorioAssociarForm, self).__init__(*args, **kwargs)
        revisoras_qs = usuaria.revisoras_de_organizacoes_que_administra
        if organizacao_id:
            revisoras_qs = revisoras_qs.filter(
                org_usuarias__organizacao_id=organizacao_id
            )
        self.fields["revisora"].queryset = revisoras_qs


class TerritorioAutoAssociarRevisoraForm(forms.Form):
    territorio = forms.CharField(widget=forms.HiddenInput(), required=False)
    check = forms.BooleanField(required=True, label="Deseja assumir a revisão?")


class OrganizacaoTerritorioAssociarForm(forms.Form):
    territorio = forms.CharField(widget=forms.HiddenInput(), required=False)
    organizacao = forms.ModelChoiceField(
        queryset=None, required=False, label="Organização"
    )

    def __init__(self, *args, **kwargs):
        usuaria = kwargs.pop("usuaria", None)
        super(OrganizacaoTerritorioAssociarForm, self).__init__(*args, **kwargs)
        self.fields["organizacao"].queryset = usuaria.organizacoes_que_administra


class UsuariaAssociarForm(forms.Form):
    object_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    revisora = forms.ModelChoiceField(
        queryset=Usuaria.objects.filter(is_reviewer=True),
        widget=autocomplete.ModelSelect2(url="reviewer-autocomplete"),
        required=False,
        label="Revisor(a)",
    )


class OrganizacaoUsuariaAssociarForm(forms.Form):
    usuaria = forms.CharField(widget=forms.HiddenInput(), required=False)
    organizacao = forms.ModelChoiceField(
        queryset=None, required=False, label="Organização"
    )
    perfil = forms.ChoiceField(
        choices=[("is_reviewer", "Revisor/a"), ("is_admin", "Administrador/a")],
        required=True,
        label="Perfil na organização",
    )

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request_user", None)
        super(OrganizacaoUsuariaAssociarForm, self).__init__(*args, **kwargs)
        self.fields["organizacao"].queryset = request_user.organizacoes_que_administra


class OrganizacaoMunicipioOuEstadoAssociarForm(forms.Form):
    organizacao = forms.CharField(widget=forms.HiddenInput(), required=False)
    estado = forms.ChoiceField(choices=[], required=True, label="Estado")
    municipio = forms.ChoiceField(choices=[], required=False, label="Município")
