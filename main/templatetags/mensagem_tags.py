from django import template

register = template.Library()


@register.inclusion_tag("main/inclusiontags/mensagem_status_tag.html")
def mensagem_status(mensagem):
    if mensagem.origin_is_device:
        # icone = "fa-exclamation-circle" if mensagem.is_new_message else "fa-check-circle"
        # css_class = "text-danger" if mensagem.is_new_message else "text-success"
        icone = "fa-check-circle"
        css_class = "text-success"
        return {"mensagem": mensagem, "icone": icone, "css_class": css_class}

    icone = "fa-circle"
    css_class = ""
    status = mensagem.status
    if status["erro"]:
        icone = "fa-exclamation-circle"
    elif status["enviada"]:
        icone = "fa-check-circle"

    if status["enviada"]:
        css_class = "text-success"
    elif status["erro"]:
        css_class = "text-danger"

    return {"mensagem": mensagem, "icone": icone, "css_class": css_class}


@register.inclusion_tag("main/inclusiontags/mensagem_status_report_tag.html")
def mensagem_status_report(mensagem):
    return {
        "titulo": mensagem.nome if mensagem.nome else "",
        "texto": mensagem.mensagem_report if mensagem.mensagem_report else mensagem.mensagem,
    }
