from django import template
from main.models import OrganizacaoUsuaria

register = template.Library()


@register.inclusion_tag("main/inclusiontags/organizacao_associavel_territorio.html")
# pylint: disable=unused-argument
def organizacao_associavel_territorio(*args, **kwargs):
    if not kwargs["territorio"].organizacao:
        return {"territorio": kwargs["territorio"], "admin_geral": kwargs["usuaria"].has_perm("admin_geral")}
    administra_organizacao = (
        True
        if kwargs["usuaria"].has_perm("admin_geral")
        else OrganizacaoUsuaria.objects.filter(
            usuaria=kwargs["usuaria"], organizacao=kwargs["territorio"].organizacao, is_admin=True
        ).exists()
    )
    return {"territorio": kwargs["territorio"], "administra_organizacao": administra_organizacao}


@register.inclusion_tag("main/inclusiontags/organizacoes_associaveis_usuaria.html")
def organizacoes_associaveis_usuaria(*args, **kwargs):  # pylint: disable=unused-argument
    organizacoes_que_administra_ids = kwargs["request_user"].organizacoes_que_administra_ids
    organizacoes_usuarias = kwargs["usuaria"].org_usuarias.all()
    object_list = []
    for org_usuaria in organizacoes_usuarias:
        object_list.append(
            {
                "organizacao": org_usuaria.organizacao,
                "is_admin": org_usuaria.is_admin,
                "request_user_is_admin": True
                if kwargs["request_user"].has_perm("admin_geral")
                else (org_usuaria.organizacao.id in organizacoes_que_administra_ids),
            }
        )
    return {
        "administra_alguma_organizacao": kwargs["request_user"].has_perm("usuaria.administrar_organizacao"),
        "usuaria": kwargs["usuaria"],
        "object_list": object_list,
    }
