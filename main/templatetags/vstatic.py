from django import template
from django.templatetags.static import static
from django.conf import settings

register = template.Library()


@register.simple_tag
def vstatic(path):
    url = static(path)
    if settings.STATIC_VERSION:
        url += "?v=" + settings.STATIC_VERSION
    return url

@register.simple_tag
def static_report_url(path):
    url = settings.STATIC_REPORT_URL + static(path)
    if settings.STATIC_VERSION:
        url += "?v=" + settings.STATIC_VERSION
    return url
