import simplejson
from django.template import Library
from django.templatetags.static import static
from django.utils.safestring import mark_safe

from main.utils import get_perfil

register = Library()


# Usage:
# {{ book.objects.all | jsonify_queryset:"id,title" }}
@register.filter
def jsonify_queryset(object, fields):
    return simplejson.dumps(list(object.values(*fields.split(","))))


@register.filter
def jsonify_list(list):
    return simplejson.dumps(list)


@register.filter
def percentage(object, total):
    return round(object / total * 100, 1)


@register.filter
def get_items(value, arg):
    return value[arg].items()


@register.filter
def get_dict_attr(dict, attr):
    return dict.get(attr)


@register.filter
def perfil(usuaria, org_usuarias):
    return get_perfil(usuaria, org_usuarias)


@register.filter
def is_telefone(username):
    return username.isdigit() and len(username) == 11


@register.filter(is_safe=True)
def format_telefone(username):
    if not is_telefone(username):
        return username
    # pylint: disable=consider-using-f-string
    text = "(%s%s)%s%s%s%s%s-%s%s%s%s" % tuple(username)
    href = f"https://web.whatsapp.com/send?phone=55{username}&text&app_absent=0"
    icon_url = static("main/img/logo_zap.png")
    img = f"<img src='{icon_url}' class='mr-1'>"
    html = f"<a class='telefone' target='_blank' href='{href}' alt='Conversar pelo WhatsApp' title='Conversar pelo WhatsApp'>{img}{text}</a>"
    return mark_safe(html)


@register.filter(is_safe=True)
def n_to_br(text):
    if not text:
        return ""
    # pylint: disable=consider-using-f-string
    html = text.replace("\\n", "<br />")
    return mark_safe(html)
