import re
from django import template
from main.models import Organizacao
from main.utils import get_prefix_url_organizacao

register = template.Library()


@register.simple_tag(takes_context=True)
def prefix_url_organizacao(context):
    request = context["request"]
    return get_prefix_url_organizacao(context.get("organizacao_id", None), request.path)


@register.inclusion_tag("main/organizacao_nav_options.html", takes_context=True)
def organizacao_nav_options(context):
    request = context["request"]
    organizacao_ativa = None
    organizacao_id = context.get("organizacao_id", None)
    if organizacao_id:
        organizacao_ativa = Organizacao.objects.get(pk=organizacao_id)

    suffix_url_organizacao = re.sub(r".*(\/organizacao\/[0-9]+)(.*)?", r"\2", request.path)

    navGeralUrl = suffix_url_organizacao if not "navGeralUrl" in context else context["navGeralUrl"]

    return {
        "admin_geral": request.user.has_perm("admin_geral"),
        "organizacoes": request.user.organizacoes_que_administra,
        "organizacao_ativa": organizacao_ativa,
        "suffix_url_organizacao": suffix_url_organizacao,
        "navHideOrg": context.get("navHideOrg", False),
        "navGeralUrl": navGeralUrl,
    }
