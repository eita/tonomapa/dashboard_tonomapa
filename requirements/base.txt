# Django LTS
# https://www.djangoproject.com/download/
Django<4


# checar necessidade
django-extensions



# Models
django-model-utils

# DB
postgres
psycopg2

#Forms
# django-braces
# django-crispy-forms
django-crispy-forms<2

djangorestframework
djangorestframework-gis
djangorestframework-jwt
djangorestframework-stubs

django-autocomplete-light
django-bootstrap4
django-cleanup
django-crispy-forms
django-datatables-view
django-filter

beautifulsoup4
Unidecode

graphene
django-graphql-geojson
graphene
graphene-django
graphene-file-upload
graphene-stubs
graphql-core
graphql-relay

exponent-server-sdk

appdirs
CacheControl
chardet
colorama
contextlib2
curlify
deepdiff
defusedxml
distlib
distro
django-allauth
django-appconf

git+https://github.com/coletivoEITA/django-archive-mixin.git@0bd9d5e6f378995e12183c232e70c1f22781c7d9#egg=django_archive_mixin

django-cors-headers
django-currentuser
django-dirtyfields
django-imagekit
django-notifications-hq
django-stubs

html5lib
Impostor
ipaddr
jsonfield
jsonpickle
lockfile
Markdown
matplotlib-inline
msgpack
oauthlib
ordered-set
parso
pep517
pilkit
Pillow
progress
PyJWT
python-magic
python-resize-image
python3-openid
toml
pytoml
PyYAML
requests-oauthlib
requests-toolbelt
retrying
rules
Rx
simplejson
singledispatch
swapper
webencodings
pikepdf
django-log-viewer
django-hardcopy
